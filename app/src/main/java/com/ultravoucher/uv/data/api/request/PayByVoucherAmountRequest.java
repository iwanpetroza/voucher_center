package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.PayByVoucherAmountReq;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 16/08/18.
 */

public class PayByVoucherAmountRequest {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("voucherClass")
    @Expose
    private int voucherClass;

    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;

    @SerializedName("vouchers")
    @Expose
    private List<PayByVoucherAmountReq> vouchers = new ArrayList<PayByVoucherAmountReq>();

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getVoucherClass() {
        return voucherClass;
    }

    public void setVoucherClass(int voucherClass) {
        this.voucherClass = voucherClass;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<PayByVoucherAmountReq> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<PayByVoucherAmountReq> vouchers) {
        this.vouchers = vouchers;
    }
}
