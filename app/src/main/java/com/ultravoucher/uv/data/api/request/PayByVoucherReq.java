package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class PayByVoucherReq{

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("merchantId")
	private String merchantId;

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	@Override
 	public String toString(){
		return 
			"PayByVoucherReq{" + 
			"orderNumber = '" + orderNumber + '\'' + 
			",voucherClass = '" + voucherClass + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}