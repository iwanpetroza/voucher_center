package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.PayByVoucherMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/9/2018.
 */

public class PayByVoucherResponse extends GenericResponse {

    @SerializedName("totalVoucherPayment")
    @Expose
    private int totalVoucherPayment;

    @SerializedName("totalVoucherUsed")
    @Expose
    private int totalVoucherUsed;

    @SerializedName("orders")
    @Expose
    private List<PayByVoucherMsgResp> orders = new ArrayList<PayByVoucherMsgResp>();


    public int getTotalVoucherPayment() {
        return totalVoucherPayment;
    }

    public void setTotalVoucherPayment(int totalVoucherPayment) {
        this.totalVoucherPayment = totalVoucherPayment;
    }

    public int getTotalVoucherUsed() {
        return totalVoucherUsed;
    }

    public void setTotalVoucherUsed(int totalVoucherUsed) {
        this.totalVoucherUsed = totalVoucherUsed;
    }

    public List<PayByVoucherMsgResp> getOrders() {
        return orders;
    }

    public void setOrders(List<PayByVoucherMsgResp> orders) {
        this.orders = orders;
    }
}
