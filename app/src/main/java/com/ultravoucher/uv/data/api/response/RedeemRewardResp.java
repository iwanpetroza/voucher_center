package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.RewardRedemptionResponse;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class RedeemRewardResp {

    @SerializedName("rewardRedemptionResponse")
    @Expose
    private RewardRedemptionResponse rewardRedemptionResponse;

    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    /**
     *
     * @return
     * The rewardRedemptionResponse
     */
    public RewardRedemptionResponse getRewardRedemptionResponse() {
        return rewardRedemptionResponse;
    }

    /**
     *
     * @param rewardRedemptionResponse
     * The rewardRedemptionResponse
     */
    public void setRewardRedemptionResponse(RewardRedemptionResponse rewardRedemptionResponse) {
        this.rewardRedemptionResponse = rewardRedemptionResponse;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}

