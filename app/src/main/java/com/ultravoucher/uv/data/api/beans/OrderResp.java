package com.ultravoucher.uv.data.api.beans;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderResp {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("orderDate")
    @Expose
    private String orderDate;
    @SerializedName("paymentChannel")
    @Expose
    private Object paymentChannel;
    @SerializedName("vaNumber")
    @Expose
    private Object vaNumber;
    @SerializedName("totalPayment")
    @Expose
    private Object totalPayment;
    @SerializedName("paymentStatus")
    @Expose
    private Object paymentStatus;
    @SerializedName("paymentDate")
    @Expose
    private Object paymentDate;
    @SerializedName("paymentReferenceCode")
    @Expose
    private Object paymentReferenceCode;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("variantId")
    @Expose
    private String variantId;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("totalAmount")
    @Expose
    private double totalAmount;
    @SerializedName("taxAmount")
    @Expose
    private double taxAmount;
    @SerializedName("discountName")
    @Expose
    private String discountName;
    @SerializedName("discountType")
    @Expose
    private String discountType;
    @SerializedName("discountValue")
    @Expose
    private double discountValue;
    @SerializedName("transactionRefNumber")
    @Expose
    private Object transactionRefNumber;
    @SerializedName("shippingBy")
    @Expose
    private Object shippingBy;
    @SerializedName("shippingAmount")
    @Expose
    private Object shippingAmount;
    @SerializedName("pickupDate")
    @Expose
    private Object pickupDate;
    @SerializedName("pickupNote")
    @Expose
    private Object pickupNote;
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("deliveryAddress")
    @Expose
    private Object deliveryAddress;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("postalCode")
    @Expose
    private Object postalCode;
    @SerializedName("phoneNumber")
    @Expose
    private Object phoneNumber;
    @SerializedName("email")
    @Expose
    private Object email;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("variants")
    @Expose
    private Object variants;
    @SerializedName("productImage")
    @Expose
    private Object productImage;
    @SerializedName("productName")
    @Expose
    private Object productName;
    @SerializedName("productType")
    @Expose
    private Object productType;
    @SerializedName("variantName")
    @Expose
    private Object variantName;
    @SerializedName("basePrice")
    @Expose
    private Object basePrice;
    @SerializedName("sellingPrice")
    @Expose
    private Object sellingPrice;

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     *
     * @param orderNumber
     * The orderNumber
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     *
     * @return
     * The orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     *
     * @param orderDate
     * The orderDate
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     *
     * @return
     * The paymentChannel
     */
    public Object getPaymentChannel() {
        return paymentChannel;
    }

    /**
     *
     * @param paymentChannel
     * The paymentChannel
     */
    public void setPaymentChannel(Object paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    /**
     *
     * @return
     * The vaNumber
     */
    public Object getVaNumber() {
        return vaNumber;
    }

    /**
     *
     * @param vaNumber
     * The vaNumber
     */
    public void setVaNumber(Object vaNumber) {
        this.vaNumber = vaNumber;
    }

    /**
     *
     * @return
     * The totalPayment
     */
    public Object getTotalPayment() {
        return totalPayment;
    }

    /**
     *
     * @param totalPayment
     * The totalPayment
     */
    public void setTotalPayment(Object totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     *
     * @return
     * The paymentStatus
     */
    public Object getPaymentStatus() {
        return paymentStatus;
    }

    /**
     *
     * @param paymentStatus
     * The paymentStatus
     */
    public void setPaymentStatus(Object paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     *
     * @return
     * The paymentDate
     */
    public Object getPaymentDate() {
        return paymentDate;
    }

    /**
     *
     * @param paymentDate
     * The paymentDate
     */
    public void setPaymentDate(Object paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     *
     * @return
     * The paymentReferenceCode
     */
    public Object getPaymentReferenceCode() {
        return paymentReferenceCode;
    }

    /**
     *
     * @param paymentReferenceCode
     * The paymentReferenceCode
     */
    public void setPaymentReferenceCode(Object paymentReferenceCode) {
        this.paymentReferenceCode = paymentReferenceCode;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The variantId
     */
    public String getVariantId() {
        return variantId;
    }

    /**
     *
     * @param variantId
     * The variantId
     */
    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    /**
     *
     * @return
     * The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     * The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     * The totalAmount
     */
    public double getTotalAmount() {
        return totalAmount;
    }

    /**
     *
     * @param totalAmount
     * The totalAmount
     */
    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     *
     * @return
     * The taxAmount
     */
    public double getTaxAmount() {
        return taxAmount;
    }

    /**
     *
     * @param taxAmount
     * The taxAmount
     */
    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     *
     * @return
     * The discountName
     */
    public String getDiscountName() {
        return discountName;
    }

    /**
     *
     * @param discountName
     * The discountName
     */
    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    /**
     *
     * @return
     * The discountType
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     *
     * @param discountType
     * The discountType
     */
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    /**
     *
     * @return
     * The discountValue
     */
    public double getDiscountValue() {
        return discountValue;
    }

    /**
     *
     * @param discountValue
     * The discountValue
     */
    public void setDiscountValue(double discountValue) {
        this.discountValue = discountValue;
    }

    /**
     *
     * @return
     * The transactionRefNumber
     */
    public Object getTransactionRefNumber() {
        return transactionRefNumber;
    }

    /**
     *
     * @param transactionRefNumber
     * The transactionRefNumber
     */
    public void setTransactionRefNumber(Object transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    /**
     *
     * @return
     * The shippingBy
     */
    public Object getShippingBy() {
        return shippingBy;
    }

    /**
     *
     * @param shippingBy
     * The shippingBy
     */
    public void setShippingBy(Object shippingBy) {
        this.shippingBy = shippingBy;
    }

    /**
     *
     * @return
     * The shippingAmount
     */
    public Object getShippingAmount() {
        return shippingAmount;
    }

    /**
     *
     * @param shippingAmount
     * The shippingAmount
     */
    public void setShippingAmount(Object shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     *
     * @return
     * The pickupDate
     */
    public Object getPickupDate() {
        return pickupDate;
    }

    /**
     *
     * @param pickupDate
     * The pickupDate
     */
    public void setPickupDate(Object pickupDate) {
        this.pickupDate = pickupDate;
    }

    /**
     *
     * @return
     * The pickupNote
     */
    public Object getPickupNote() {
        return pickupNote;
    }

    /**
     *
     * @param pickupNote
     * The pickupNote
     */
    public void setPickupNote(Object pickupNote) {
        this.pickupNote = pickupNote;
    }

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The deliveryAddress
     */
    public Object getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     *
     * @param deliveryAddress
     * The deliveryAddress
     */
    public void setDeliveryAddress(Object deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     *
     * @return
     * The city
     */
    public Object getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(Object city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public Object getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postalCode
     */
    public void setPostalCode(Object postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public Object getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phoneNumber
     */
    public void setPhoneNumber(Object phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The email
     */
    public Object getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(Object email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The status
     */
    public Object getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Object status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The variants
     */
    public Object getVariants() {
        return variants;
    }

    /**
     *
     * @param variants
     * The variants
     */
    public void setVariants(Object variants) {
        this.variants = variants;
    }

    /**
     *
     * @return
     * The productImage
     */
    public Object getProductImage() {
        return productImage;
    }

    /**
     *
     * @param productImage
     * The productImage
     */
    public void setProductImage(Object productImage) {
        this.productImage = productImage;
    }

    /**
     *
     * @return
     * The productName
     */
    public Object getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The productName
     */
    public void setProductName(Object productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productType
     */
    public Object getProductType() {
        return productType;
    }

    /**
     *
     * @param productType
     * The productType
     */
    public void setProductType(Object productType) {
        this.productType = productType;
    }

    /**
     *
     * @return
     * The variantName
     */
    public Object getVariantName() {
        return variantName;
    }

    /**
     *
     * @param variantName
     * The variantName
     */
    public void setVariantName(Object variantName) {
        this.variantName = variantName;
    }

    /**
     *
     * @return
     * The basePrice
     */
    public Object getBasePrice() {
        return basePrice;
    }

    /**
     *
     * @param basePrice
     * The basePrice
     */
    public void setBasePrice(Object basePrice) {
        this.basePrice = basePrice;
    }

    /**
     *
     * @return
     * The sellingPrice
     */
    public Object getSellingPrice() {
        return sellingPrice;
    }

    /**
     *
     * @param sellingPrice
     * The sellingPrice
     */
    public void setSellingPrice(Object sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}