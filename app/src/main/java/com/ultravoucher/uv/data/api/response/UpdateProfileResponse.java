package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.UpdateProfileMsgResp;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class UpdateProfileResponse extends GenericResponse{

    @SerializedName("memberResponse")
    @Expose
    private UpdateProfileMsgResp memberResponse;

    public UpdateProfileMsgResp getMemberResponse() {
        return memberResponse;
    }

    public void setMemberResponse(UpdateProfileMsgResp memberResponse) {
        this.memberResponse = memberResponse;
    }
}
