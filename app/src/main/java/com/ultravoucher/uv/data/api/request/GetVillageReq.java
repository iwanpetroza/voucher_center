package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class GetVillageReq {

    @SerializedName("districtId")
    @Expose
    private String districtId;

    /**
     *
     * @return
     * The districtId
     */
    public String getDistrictId() {
        return districtId;
    }

    /**
     *
     * @param districtId
     * The districtId
     */
    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }
}
