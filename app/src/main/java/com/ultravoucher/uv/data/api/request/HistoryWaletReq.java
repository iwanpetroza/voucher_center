package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class HistoryWaletReq{

	@SerializedName("dateTo")
	private String dateTo;

	@SerializedName("dateFrom")
	private String dateFrom;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("pin")
	private String pin;

	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}

	public String getDateTo(){
		return dateTo;
	}

	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}

	public String getDateFrom(){
		return dateFrom;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	@Override
 	public String toString(){
		return 
			"HistoryWaletReq{" + 
			"dateTo = '" + dateTo + '\'' + 
			",dateFrom = '" + dateFrom + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}