package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class OrderPay {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("totalAmount")
    @Expose
    private int totalAmount;
    @SerializedName("taxAmount")
    @Expose
    private int taxAmount;
    @SerializedName("shippingAmount")
    @Expose
    private int shippingAmount;
    @SerializedName("discountValue")
    @Expose
    private int discountValue;

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The totalAmount
     */
    public int getTotalAmount() {
        return totalAmount;
    }

    /**
     *
     * @param totalAmount
     * The totalAmount
     */
    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     *
     * @return
     * The taxAmount
     */
    public int getTaxAmount() {
        return taxAmount;
    }

    /**
     *
     * @param taxAmount
     * The taxAmount
     */
    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     *
     * @return
     * The shippingAmount
     */
    public int getShippingAmount() {
        return shippingAmount;
    }

    /**
     *
     * @param shippingAmount
     * The shippingAmount
     */
    public void setShippingAmount(int shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     *
     * @return
     * The discountValue
     */
    public int getDiscountValue() {
        return discountValue;
    }

    /**
     *
     * @param discountValue
     * The discountValue
     */
    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

}

