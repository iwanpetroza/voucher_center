package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.BillerCategoryMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 26/09/18.
 */

public class BillerCategoryResponse extends GenericResponse{

    @SerializedName("billerCategoryMsgResponseList")
    @Expose
    private List<BillerCategoryMsgResp> billerCategoryMsgResponseList = new ArrayList<BillerCategoryMsgResp>();

    public List<BillerCategoryMsgResp> getBillerCategoryMsgResponseList() {
        return billerCategoryMsgResponseList;
    }

    public void setBillerCategoryMsgResponseList(List<BillerCategoryMsgResp> billerCategoryMsgResponseList) {
        this.billerCategoryMsgResponseList = billerCategoryMsgResponseList;
    }
}
