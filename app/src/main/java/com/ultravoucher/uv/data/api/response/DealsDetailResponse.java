package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.DealsDetailResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/9/2017.
 */

public class DealsDetailResponse extends GenericResponse {

    @SerializedName("deals")
    @Expose
    private List<DealsDetailResp> deals = new ArrayList<DealsDetailResp>();

    public List<DealsDetailResp> getDeals() {
        return deals;
    }

    public void setDeals(List<DealsDetailResp> deals) {
        this.deals = deals;
    }
}
