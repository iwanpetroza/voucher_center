package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.BillerHistory;
import com.ultravoucher.uv.data.api.beans.BillingResponse;

import java.util.List;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerHistoryResponse{
    @SerializedName("billerHistory")
    @Expose
    public List<BillerHistory> billerHistory = null;
    @SerializedName("abstractResponse")
    @Expose
    public AbstractResponse abstractResponse;

    public List<BillerHistory> getBillerHistory() {
        return billerHistory;
    }

    public void setBillerHistory(List<BillerHistory> billerHistory) {
        this.billerHistory = billerHistory;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
