package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class VoucherRedemption {

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("transactionIdReference")
	private String transactionIdReference;

	@SerializedName("voucherCode")
	private String voucherCode;

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setTransactionIdReference(String transactionIdReference){
		this.transactionIdReference = transactionIdReference;
	}

	public String getTransactionIdReference(){
		return transactionIdReference;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	@Override
 	public String toString(){
		return 
			"VoucherRedemption{" +
			"merchantId = '" + merchantId + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",transactionIdReference = '" + transactionIdReference + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			"}";
		}
}