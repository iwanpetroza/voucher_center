package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.DealsDetailFisikMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/10/2017.
 */

public class DealsDetailFisikResponse extends GenericResponse {

    @SerializedName("deals")
    @Expose
    private List<DealsDetailFisikMsgResp> deals = new ArrayList<DealsDetailFisikMsgResp>();

    public List<DealsDetailFisikMsgResp> getDeals() {
        return deals;
    }

    public void setDeals(List<DealsDetailFisikMsgResp> deals) {
        this.deals = deals;
    }
}