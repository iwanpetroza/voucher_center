package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by firwandi.ramli on 11/8/2017.
 */

public class AdvertisingMsgResp implements Serializable {

    @SerializedName("advertisingId")
    @Expose
    private String advertisingId;
    @SerializedName("advertisingName")
    @Expose
    private String advertisingName;
    @SerializedName("advertisingImage")
    @Expose
    private String advertisingImage;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("rewardId")
    @Expose
    private Object rewardId;
    @SerializedName("postId")
    @Expose
    private Object postId;

    @SerializedName("productId")
    @Expose
    private String productId;

    @SerializedName("url")
    @Expose
    private String url;
    /**
     *
     * @return
     * The advertisingId
     */
    public String getAdvertisingId() {
        return advertisingId;
    }

    /**
     *
     * @param advertisingId
     * The advertisingId
     */
    public void setAdvertisingId(String advertisingId) {
        this.advertisingId = advertisingId;
    }

    /**
     *
     * @return
     * The advertisingName
     */
    public String getAdvertisingName() {
        return advertisingName;
    }

    /**
     *
     * @param advertisingName
     * The advertisingName
     */
    public void setAdvertisingName(String advertisingName) {
        this.advertisingName = advertisingName;
    }

    /**
     *
     * @return
     * The advertisingImage
     */
    public String getAdvertisingImage() {
        return advertisingImage;
    }

    /**
     *
     * @param advertisingImage
     * The advertisingImage
     */
    public void setAdvertisingImage(String advertisingImage) {
        this.advertisingImage = advertisingImage;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    public Object getRewardId() {
        return rewardId;
    }

    /**
     *
     * @param rewardId
     * The rewardId
     */
    public void setRewardId(Object rewardId) {
        this.rewardId = rewardId;
    }

    /**
     *
     * @return
     * The postId
     */
    public Object getPostId() {
        return postId;
    }

    /**
     *
     * @param postId
     * The postId
     */
    public void setPostId(Object postId) {
        this.postId = postId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
