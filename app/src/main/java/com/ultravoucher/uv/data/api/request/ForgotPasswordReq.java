package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/30/2017.
 */

public class ForgotPasswordReq {

    @SerializedName("email")
    private String email;

    public void setEmail(String email){
        this.email = email;
    }

    public String getEmail(){
        return email;
    }

    @Override
    public String toString(){
        return
                "ForgotPassReq{" +
                        "email = '" + email + '\'' +
                        "}";
    }

}
