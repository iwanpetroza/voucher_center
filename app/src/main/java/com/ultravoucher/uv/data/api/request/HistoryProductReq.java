package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class HistoryProductReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("orderStatus")
	private int orderStatus;

	@SerializedName("page")
	private int page;

	@SerializedName("memberId")
	private String memberId;

	public int getnRecords() {
		return nRecords;
	}

	public void setnRecords(int nRecords) {
		this.nRecords = nRecords;
	}

	public void setOrderStatus(int orderStatus){
		this.orderStatus = orderStatus;
	}

	public int getOrderStatus(){
		return orderStatus;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	@Override
 	public String toString(){
		return 
			"HistoryProductReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",orderStatus = '" + orderStatus + '\'' + 
			",page = '" + page + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}