package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class RegistrationReq{

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("password")
	private String password;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("deviceUniqueId")
	private String deviceUniqueId;

	@SerializedName("isCorporateMember")
	private int isCorporateMember;

	@SerializedName("username")
	private String username;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setDeviceUniqueId(String deviceUniqueId){
		this.deviceUniqueId = deviceUniqueId;
	}

	public String getDeviceUniqueId(){
		return deviceUniqueId;
	}

	public int getIsCorporateMember() {
		return isCorporateMember;
	}

	public void setIsCorporateMember(int isCorporateMember) {
		this.isCorporateMember = isCorporateMember;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"RegistrationReq{" + 
			"firstName = '" + firstName + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",password = '" + password + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",deviceUniqueId = '" + deviceUniqueId + '\'' +
			",username = '" + username + '\'' + 
			"}";
		}
}