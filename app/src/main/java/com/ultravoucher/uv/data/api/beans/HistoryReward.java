package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HistoryReward implements Serializable{

	@SerializedName("merchantCode")
	private String merchantCode;

	@SerializedName("voucherExpiredDate")
	private String voucherExpiredDate;

	@SerializedName("transactionDate")
	private String transactionDate;

	@SerializedName("merchantName")
	private String merchantName;

	@SerializedName("voucherTransactionId")
	private String voucherTransactionId;

	@SerializedName("lotteryId")
	private String lotteryId;

	@SerializedName("rewardName")
	private String rewardName;

	@SerializedName("rewardId")
	private String rewardId;

	@SerializedName("termAndCondition")
	private String termAndCondition;

	@SerializedName("productImage")
	private String productImage;

	@SerializedName("lotteryCode")
	private String lotteryCode;

	@SerializedName("requiredPts")
	private int requiredPts;

	@SerializedName("pin")
	private String pin;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("redeemedDate")
	private String redeemedDate;

	@SerializedName("rewardDescription")
	private String rewardDescription;

	@SerializedName("rewardCode")
	private String rewardCode;

	@SerializedName("voucherCode")
	private String voucherCode;

	@SerializedName("productType")
	private int productType;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("pickupDate")
	private String pickupDate;

	public void setMerchantCode(String merchantCode){
		this.merchantCode = merchantCode;
	}

	public String getMerchantCode(){
		return merchantCode;
	}

	public void setVoucherExpiredDate(String voucherExpiredDate){
		this.voucherExpiredDate = voucherExpiredDate;
	}

	public String getVoucherExpiredDate(){
		return voucherExpiredDate;
	}

	public void setTransactionDate(String transactionDate){
		this.transactionDate = transactionDate;
	}

	public String getTransactionDate(){
		return transactionDate;
	}

	public void setMerchantName(String merchantName){
		this.merchantName = merchantName;
	}

	public String getMerchantName(){
		return merchantName;
	}

	public void setVoucherTransactionId(String voucherTransactionId){
		this.voucherTransactionId = voucherTransactionId;
	}

	public String getVoucherTransactionId(){
		return voucherTransactionId;
	}

	public void setLotteryId(String lotteryId){
		this.lotteryId = lotteryId;
	}

	public String getLotteryId(){
		return lotteryId;
	}

	public void setRewardName(String rewardName){
		this.rewardName = rewardName;
	}

	public String getRewardName(){
		return rewardName;
	}

	public void setRewardId(String rewardId){
		this.rewardId = rewardId;
	}

	public String getRewardId(){
		return rewardId;
	}

	public void setTermAndCondition(String termAndCondition){
		this.termAndCondition = termAndCondition;
	}

	public String getTermAndCondition(){
		return termAndCondition;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setLotteryCode(String lotteryCode){
		this.lotteryCode = lotteryCode;
	}

	public String getLotteryCode(){
		return lotteryCode;
	}

	public void setRequiredPts(int requiredPts){
		this.requiredPts = requiredPts;
	}

	public int getRequiredPts(){
		return requiredPts;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setRedeemedDate(String redeemedDate){
		this.redeemedDate = redeemedDate;
	}

	public String getRedeemedDate(){
		return redeemedDate;
	}

	public void setRewardDescription(String rewardDescription){
		this.rewardDescription = rewardDescription;
	}

	public String getRewardDescription(){
		return rewardDescription;
	}

	public void setRewardCode(String rewardCode){
		this.rewardCode = rewardCode;
	}

	public String getRewardCode(){
		return rewardCode;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	public void setProductType(int productType){
		this.productType = productType;
	}

	public int getProductType(){
		return productType;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	@Override
 	public String toString(){
		return 
			"HistoryReward{" + 
			"merchantCode = '" + merchantCode + '\'' + 
			",voucherExpiredDate = '" + voucherExpiredDate + '\'' + 
			",transactionDate = '" + transactionDate + '\'' + 
			",merchantName = '" + merchantName + '\'' + 
			",voucherTransactionId = '" + voucherTransactionId + '\'' + 
			",lotteryId = '" + lotteryId + '\'' + 
			",rewardName = '" + rewardName + '\'' + 
			",rewardId = '" + rewardId + '\'' + 
			",termAndCondition = '" + termAndCondition + '\'' + 
			",productImage = '" + productImage + '\'' + 
			",lotteryCode = '" + lotteryCode + '\'' + 
			",requiredPts = '" + requiredPts + '\'' + 
			",pin = '" + pin + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",redeemedDate = '" + redeemedDate + '\'' + 
			",rewardDescription = '" + rewardDescription + '\'' + 
			",rewardCode = '" + rewardCode + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			",productType = '" + productType + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}