package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderPay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class PayOrderReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;

    @SerializedName("orders")
    @Expose
    private List<OrderPay> orders = new ArrayList<OrderPay>();

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<OrderPay> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderPay> orders) {
        this.orders = orders;
    }
}

