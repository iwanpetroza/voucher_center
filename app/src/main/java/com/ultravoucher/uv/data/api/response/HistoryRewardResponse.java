package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.HistoryReward;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 2/12/2018.
 */

public class HistoryRewardResponse extends GenericResponse {

    @SerializedName("detailResponses")
    @Expose
    private List<HistoryReward> detailResponses = new ArrayList<HistoryReward>();


    public List<HistoryReward> getDetailResponses() {
        return detailResponses;
    }

    public void setDetailResponses(List<HistoryReward> detailResponses) {
        this.detailResponses = detailResponses;
    }
}
