package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class GetDistrictReq {

    @SerializedName("cityId")
    @Expose
    private String cityId;

    /**
     *
     * @return
     * The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The cityId
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

}
