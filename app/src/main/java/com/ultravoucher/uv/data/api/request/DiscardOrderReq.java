package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.DisOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/29/2017.
 */

public class DiscardOrderReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("orders")
    @Expose
    private List<DisOrder> orders = new ArrayList<DisOrder>();

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<DisOrder> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<DisOrder> orders) {
        this.orders = orders;
    }


}
