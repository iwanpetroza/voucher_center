package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tunggul.jati on 6/20/2018.
 */

public class InisiateBillerAmount {

    @SerializedName("idPulsaPre_Telkomsel_50")
    @Expose
    public Integer idPulsaPreTelkomsel50;
    @SerializedName("idPlnPre_100")
    @Expose
    public Integer idPlnPre100;
    @SerializedName("idPulsaPre_Telkomsel_5")
    @Expose
    public Integer idPulsaPreTelkomsel5;
    @SerializedName("idPlnPre_500")
    @Expose
    public Integer idPlnPre500;
    @SerializedName("idPulsaPasca_Indosat")
    @Expose
    public Integer idPulsaPascaIndosat;
    @SerializedName("idPulsaPre_Telkomsel_100")
    @Expose
    public Integer idPulsaPreTelkomsel100;
    @SerializedName("idPlnPre_200")
    @Expose
    public Integer idPlnPre200;
    @SerializedName("idPulsaPre_Indosat_50")
    @Expose
    public Integer idPulsaPreIndosat50;
    @SerializedName("idPulsaPre_Indosat_25")
    @Expose
    public Integer idPulsaPreIndosat25;
    @SerializedName("idPulsaPre_Indosat_100")
    @Expose
    public Integer idPulsaPreIndosat100;
    @SerializedName("idPulsaPre_Axis_15")
    @Expose
    public Integer idPulsaPreAxis15;
    @SerializedName("idPlnPost")
    @Expose
    public Integer idPlnPost;
    @SerializedName("idPulsaPre_Telkomsel_10")
    @Expose
    public Integer idPulsaPreTelkomsel10;
    @SerializedName("idPulsaPre_XL_25")
    @Expose
    public Integer idPulsaPreXL25;
    @SerializedName("idPulsaPre_Indosat_10")
    @Expose
    public Integer idPulsaPreIndosat10;
    @SerializedName("idPulsaPre_Axis_50")
    @Expose
    public Integer idPulsaPreAxis50;
    @SerializedName("idPulsaPre_Smartfren_10")
    @Expose
    public Integer idPulsaPreSmartfren10;
    @SerializedName("idPulsaPre_Axis_100")
    @Expose
    public Integer idPulsaPreAxis100;
    @SerializedName("idPlnPre_20")
    @Expose
    public Integer idPlnPre20;
    @SerializedName("idPulsaPre_Axis_10")
    @Expose
    public Integer idPulsaPreAxis10;
    @SerializedName("idPulsaPre_XL_100")
    @Expose
    public Integer idPulsaPreXL100;
    @SerializedName("idPulsaPre_Indosat_5")
    @Expose
    public Integer idPulsaPreIndosat5;
    @SerializedName("idPulsaPre_Smartfren_100")
    @Expose
    public Integer idPulsaPreSmartfren100;
    @SerializedName("idPulsaPasca_Axis")
    @Expose
    public Integer idPulsaPascaAxis;
    @SerializedName("idPlnPre_50")
    @Expose
    public Integer idPlnPre50;
    @SerializedName("idPulsaPre_XL_50")
    @Expose
    public Integer idPulsaPreXL50;
    @SerializedName("idPlnPre_1000")
    @Expose
    public Integer idPlnPre1000;
    @SerializedName("idPulsaPre_Smartfren_50")
    @Expose
    public Integer idPulsaPreSmartfren50;
    @SerializedName("idPulsaPre_Axis_5")
    @Expose
    public Integer idPulsaPreAxis5;
    @SerializedName("idPulsaPre_Axis_25")
    @Expose
    public Integer idPulsaPreAxis25;
    @SerializedName("idPulsaPre_Telkomsel_25")
    @Expose
    public Integer idPulsaPreTelkomsel25;
    @SerializedName("idPulsaPre_Smartfren_20")
    @Expose
    public Integer idPulsaPreSmartfren20;
    @SerializedName("idPulsaPasca_XL")
    @Expose
    public Integer idPulsaPascaXL;
    @SerializedName("idPulsaPre_Smartfren_25")
    @Expose
    public Integer idPulsaPreSmartfren25;
    @SerializedName("idPulsaPre_Smartfren_5")
    @Expose
    public Integer idPulsaPreSmartfren5;
    @SerializedName("idPulsaPasca_Telkomsel")
    @Expose
    public Integer idPulsaPascaTelkomsel;
    @SerializedName("idPulsaPre_XL_10")
    @Expose
    public Integer idPulsaPreXL10;
    @SerializedName("idPulsaPre_XL_5")
    @Expose
    public Integer idPulsaPreXL5;

    //THREE

    @SerializedName("idPulsaPasca_Three")
    @Expose
    public Integer idPulsaPasca_Three;

    @SerializedName("idPulsaPre_Three_5")
    @Expose
    public Integer idPulsaPre_Three_5;

    @SerializedName("idPulsaPre_Three_10")
    @Expose
    public Integer idPulsaPre_Three_10;

    @SerializedName("idPulsaPre_Three_20")
    @Expose
    public Integer idPulsaPre_Three_20;

    @SerializedName("idPulsaPre_Three_50")
    @Expose
    public Integer idPulsaPre_Three_50;

    @SerializedName("idPulsaPre_Three_100")
    @Expose
    public Integer idPulsaPre_Three_100;


    public Integer getIdPulsaPreTelkomsel50() {
        return idPulsaPreTelkomsel50;
    }

    public void setIdPulsaPreTelkomsel50(Integer idPulsaPreTelkomsel50) {
        this.idPulsaPreTelkomsel50 = idPulsaPreTelkomsel50;
    }

    public Integer getIdPlnPre100() {
        return idPlnPre100;
    }

    public void setIdPlnPre100(Integer idPlnPre100) {
        this.idPlnPre100 = idPlnPre100;
    }

    public Integer getIdPulsaPreTelkomsel5() {
        return idPulsaPreTelkomsel5;
    }

    public void setIdPulsaPreTelkomsel5(Integer idPulsaPreTelkomsel5) {
        this.idPulsaPreTelkomsel5 = idPulsaPreTelkomsel5;
    }

    public Integer getIdPlnPre500() {
        return idPlnPre500;
    }

    public void setIdPlnPre500(Integer idPlnPre500) {
        this.idPlnPre500 = idPlnPre500;
    }

    public Integer getIdPulsaPascaIndosat() {
        return idPulsaPascaIndosat;
    }

    public void setIdPulsaPascaIndosat(Integer idPulsaPascaIndosat) {
        this.idPulsaPascaIndosat = idPulsaPascaIndosat;
    }

    public Integer getIdPulsaPreTelkomsel100() {
        return idPulsaPreTelkomsel100;
    }

    public void setIdPulsaPreTelkomsel100(Integer idPulsaPreTelkomsel100) {
        this.idPulsaPreTelkomsel100 = idPulsaPreTelkomsel100;
    }

    public Integer getIdPlnPre200() {
        return idPlnPre200;
    }

    public void setIdPlnPre200(Integer idPlnPre200) {
        this.idPlnPre200 = idPlnPre200;
    }

    public Integer getIdPulsaPreIndosat50() {
        return idPulsaPreIndosat50;
    }

    public void setIdPulsaPreIndosat50(Integer idPulsaPreIndosat50) {
        this.idPulsaPreIndosat50 = idPulsaPreIndosat50;
    }

    public Integer getIdPulsaPreIndosat25() {
        return idPulsaPreIndosat25;
    }

    public void setIdPulsaPreIndosat25(Integer idPulsaPreIndosat25) {
        this.idPulsaPreIndosat25 = idPulsaPreIndosat25;
    }

    public Integer getIdPulsaPreIndosat100() {
        return idPulsaPreIndosat100;
    }

    public void setIdPulsaPreIndosat100(Integer idPulsaPreIndosat100) {
        this.idPulsaPreIndosat100 = idPulsaPreIndosat100;
    }

    public Integer getIdPulsaPreAxis15() {
        return idPulsaPreAxis15;
    }

    public void setIdPulsaPreAxis15(Integer idPulsaPreAxis15) {
        this.idPulsaPreAxis15 = idPulsaPreAxis15;
    }

    public Integer getIdPlnPost() {
        return idPlnPost;
    }

    public void setIdPlnPost(Integer idPlnPost) {
        this.idPlnPost = idPlnPost;
    }

    public Integer getIdPulsaPreTelkomsel10() {
        return idPulsaPreTelkomsel10;
    }

    public void setIdPulsaPreTelkomsel10(Integer idPulsaPreTelkomsel10) {
        this.idPulsaPreTelkomsel10 = idPulsaPreTelkomsel10;
    }

    public Integer getIdPulsaPreXL25() {
        return idPulsaPreXL25;
    }

    public void setIdPulsaPreXL25(Integer idPulsaPreXL25) {
        this.idPulsaPreXL25 = idPulsaPreXL25;
    }

    public Integer getIdPulsaPreIndosat10() {
        return idPulsaPreIndosat10;
    }

    public void setIdPulsaPreIndosat10(Integer idPulsaPreIndosat10) {
        this.idPulsaPreIndosat10 = idPulsaPreIndosat10;
    }

    public Integer getIdPulsaPreAxis50() {
        return idPulsaPreAxis50;
    }

    public void setIdPulsaPreAxis50(Integer idPulsaPreAxis50) {
        this.idPulsaPreAxis50 = idPulsaPreAxis50;
    }

    public Integer getIdPulsaPreSmartfren10() {
        return idPulsaPreSmartfren10;
    }

    public void setIdPulsaPreSmartfren10(Integer idPulsaPreSmartfren10) {
        this.idPulsaPreSmartfren10 = idPulsaPreSmartfren10;
    }

    public Integer getIdPulsaPreAxis100() {
        return idPulsaPreAxis100;
    }

    public void setIdPulsaPreAxis100(Integer idPulsaPreAxis100) {
        this.idPulsaPreAxis100 = idPulsaPreAxis100;
    }

    public Integer getIdPlnPre20() {
        return idPlnPre20;
    }

    public void setIdPlnPre20(Integer idPlnPre20) {
        this.idPlnPre20 = idPlnPre20;
    }

    public Integer getIdPulsaPreAxis10() {
        return idPulsaPreAxis10;
    }

    public void setIdPulsaPreAxis10(Integer idPulsaPreAxis10) {
        this.idPulsaPreAxis10 = idPulsaPreAxis10;
    }

    public Integer getIdPulsaPreXL100() {
        return idPulsaPreXL100;
    }

    public void setIdPulsaPreXL100(Integer idPulsaPreXL100) {
        this.idPulsaPreXL100 = idPulsaPreXL100;
    }

    public Integer getIdPulsaPreIndosat5() {
        return idPulsaPreIndosat5;
    }

    public void setIdPulsaPreIndosat5(Integer idPulsaPreIndosat5) {
        this.idPulsaPreIndosat5 = idPulsaPreIndosat5;
    }

    public Integer getIdPulsaPreSmartfren100() {
        return idPulsaPreSmartfren100;
    }

    public void setIdPulsaPreSmartfren100(Integer idPulsaPreSmartfren100) {
        this.idPulsaPreSmartfren100 = idPulsaPreSmartfren100;
    }

    public Integer getIdPulsaPascaAxis() {
        return idPulsaPascaAxis;
    }

    public void setIdPulsaPascaAxis(Integer idPulsaPascaAxis) {
        this.idPulsaPascaAxis = idPulsaPascaAxis;
    }

    public Integer getIdPlnPre50() {
        return idPlnPre50;
    }

    public void setIdPlnPre50(Integer idPlnPre50) {
        this.idPlnPre50 = idPlnPre50;
    }

    public Integer getIdPulsaPreXL50() {
        return idPulsaPreXL50;
    }

    public void setIdPulsaPreXL50(Integer idPulsaPreXL50) {
        this.idPulsaPreXL50 = idPulsaPreXL50;
    }

    public Integer getIdPlnPre1000() {
        return idPlnPre1000;
    }

    public void setIdPlnPre1000(Integer idPlnPre1000) {
        this.idPlnPre1000 = idPlnPre1000;
    }

    public Integer getIdPulsaPreSmartfren50() {
        return idPulsaPreSmartfren50;
    }

    public void setIdPulsaPreSmartfren50(Integer idPulsaPreSmartfren50) {
        this.idPulsaPreSmartfren50 = idPulsaPreSmartfren50;
    }

    public Integer getIdPulsaPreAxis5() {
        return idPulsaPreAxis5;
    }

    public void setIdPulsaPreAxis5(Integer idPulsaPreAxis5) {
        this.idPulsaPreAxis5 = idPulsaPreAxis5;
    }

    public Integer getIdPulsaPreAxis25() {
        return idPulsaPreAxis25;
    }

    public void setIdPulsaPreAxis25(Integer idPulsaPreAxis25) {
        this.idPulsaPreAxis25 = idPulsaPreAxis25;
    }

    public Integer getIdPulsaPreTelkomsel25() {
        return idPulsaPreTelkomsel25;
    }

    public void setIdPulsaPreTelkomsel25(Integer idPulsaPreTelkomsel25) {
        this.idPulsaPreTelkomsel25 = idPulsaPreTelkomsel25;
    }

    public Integer getIdPulsaPreSmartfren20() {
        return idPulsaPreSmartfren20;
    }

    public void setIdPulsaPreSmartfren20(Integer idPulsaPreSmartfren20) {
        this.idPulsaPreSmartfren20 = idPulsaPreSmartfren20;
    }

    public Integer getIdPulsaPascaXL() {
        return idPulsaPascaXL;
    }

    public void setIdPulsaPascaXL(Integer idPulsaPascaXL) {
        this.idPulsaPascaXL = idPulsaPascaXL;
    }

    public Integer getIdPulsaPreSmartfren25() {
        return idPulsaPreSmartfren25;
    }

    public void setIdPulsaPreSmartfren25(Integer idPulsaPreSmartfren25) {
        this.idPulsaPreSmartfren25 = idPulsaPreSmartfren25;
    }

    public Integer getIdPulsaPreSmartfren5() {
        return idPulsaPreSmartfren5;
    }

    public void setIdPulsaPreSmartfren5(Integer idPulsaPreSmartfren5) {
        this.idPulsaPreSmartfren5 = idPulsaPreSmartfren5;
    }

    public Integer getIdPulsaPascaTelkomsel() {
        return idPulsaPascaTelkomsel;
    }

    public void setIdPulsaPascaTelkomsel(Integer idPulsaPascaTelkomsel) {
        this.idPulsaPascaTelkomsel = idPulsaPascaTelkomsel;
    }

    public Integer getIdPulsaPreXL10() {
        return idPulsaPreXL10;
    }

    public void setIdPulsaPreXL10(Integer idPulsaPreXL10) {
        this.idPulsaPreXL10 = idPulsaPreXL10;
    }

    public Integer getIdPulsaPreXL5() {
        return idPulsaPreXL5;
    }

    public void setIdPulsaPreXL5(Integer idPulsaPreXL5) {
        this.idPulsaPreXL5 = idPulsaPreXL5;
    }

    //THREE


    public Integer getIdPulsaPasca_Three() {
        return idPulsaPasca_Three;
    }

    public void setIdPulsaPasca_Three(Integer idPulsaPasca_Three) {
        this.idPulsaPasca_Three = idPulsaPasca_Three;
    }

    public Integer getIdPulsaPre_Three_5() {
        return idPulsaPre_Three_5;
    }

    public void setIdPulsaPre_Three_5(Integer idPulsaPre_Three_5) {
        this.idPulsaPre_Three_5 = idPulsaPre_Three_5;
    }

    public Integer getIdPulsaPre_Three_10() {
        return idPulsaPre_Three_10;
    }

    public void setIdPulsaPre_Three_10(Integer idPulsaPre_Three_10) {
        this.idPulsaPre_Three_10 = idPulsaPre_Three_10;
    }

    public Integer getIdPulsaPre_Three_20() {
        return idPulsaPre_Three_20;
    }

    public void setIdPulsaPre_Three_20(Integer idPulsaPre_Three_20) {
        this.idPulsaPre_Three_20 = idPulsaPre_Three_20;
    }

    public Integer getIdPulsaPre_Three_50() {
        return idPulsaPre_Three_50;
    }

    public void setIdPulsaPre_Three_50(Integer idPulsaPre_Three_50) {
        this.idPulsaPre_Three_50 = idPulsaPre_Three_50;
    }

    public Integer getIdPulsaPre_Three_100() {
        return idPulsaPre_Three_100;
    }

    public void setIdPulsaPre_Three_100(Integer idPulsaPre_Three_100) {
        this.idPulsaPre_Three_100 = idPulsaPre_Three_100;
    }
}
