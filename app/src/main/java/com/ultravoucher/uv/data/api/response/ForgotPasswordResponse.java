package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;

/**
 * Created by firwandi.ramli on 10/30/2017.
 */

public class ForgotPasswordResponse {

    @SerializedName("abstractResponse")
    private AbstractResponse abstractResponse;

    @SerializedName("code")
    private String code;

    public void setAbstractResponse(AbstractResponse abstractResponse){
        this.abstractResponse = abstractResponse;
    }

    public AbstractResponse getAbstractResponse(){
        return abstractResponse;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getCode(){
        return code;
    }

    @Override
    public String toString(){
        return
                "ForgotPassResp{" +
                        "abstractResponse = '" + abstractResponse + '\'' +
                        ",code = '" + code + '\'' +
                        "}";
    }
}
