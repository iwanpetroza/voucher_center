package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class JNECheckRatesReq {

	@SerializedName("destinationCode")
	private String destinationCode;

	public void setDestinationCode(String destinationCode){
		this.destinationCode = destinationCode;
	}

	public String getDestinationCode(){
		return destinationCode;
	}

	@Override
 	public String toString(){
		return 
			"JNECheckRatesReq{" +
			"destinationCode = '" + destinationCode + '\'' + 
			"}";
		}
}