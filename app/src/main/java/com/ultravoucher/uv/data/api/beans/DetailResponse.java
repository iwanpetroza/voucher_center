package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class DetailResponse {

    @SerializedName("detailResponseId")
    @Expose
    private String detailResponseId;
    @SerializedName("detailResponseMessage")
    @Expose
    private String detailResponseMessage;
    @SerializedName("rewardId")
    @Expose
    private String rewardId;
    @SerializedName("rewardCode")
    @Expose
    private String rewardCode;
    @SerializedName("rewardName")
    @Expose
    private String rewardName;
    @SerializedName("pointUsed")
    @Expose
    private int pointUsed;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("productTypeDescription")
    @Expose
    private String productTypeDescription;
    @SerializedName("voucher")
    @Expose
    private List<Voucher> voucher = new ArrayList<>();

    /**
     *
     * @return
     * The detailResponseId
     */
    public String getDetailResponseId() {
        return detailResponseId;
    }

    /**
     *
     * @param detailResponseId
     * The detailResponseId
     */
    public void setDetailResponseId(String detailResponseId) {
        this.detailResponseId = detailResponseId;
    }

    /**
     *
     * @return
     * The detailResponseMessage
     */
    public String getDetailResponseMessage() {
        return detailResponseMessage;
    }

    /**
     *
     * @param detailResponseMessage
     * The detailResponseMessage
     */
    public void setDetailResponseMessage(String detailResponseMessage) {
        this.detailResponseMessage = detailResponseMessage;
    }

    /**
     *
     * @return
     * The rewardId
     */
    public String getRewardId() {
        return rewardId;
    }

    /**
     *
     * @param rewardId
     * The rewardId
     */
    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    /**
     *
     * @return
     * The rewardCode
     */
    public String getRewardCode() {
        return rewardCode;
    }

    /**
     *
     * @param rewardCode
     * The rewardCode
     */
    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    /**
     *
     * @return
     * The rewardName
     */
    public String getRewardName() {
        return rewardName;
    }

    /**
     *
     * @param rewardName
     * The rewardName
     */
    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    /**
     *
     * @return
     * The pointUsed
     */
    public int getPointUsed() {
        return pointUsed;
    }

    /**
     *
     * @param pointUsed
     * The pointUsed
     */
    public void setPointUsed(int pointUsed) {
        this.pointUsed = pointUsed;
    }

    /**
     *
     * @return
     * The productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     *
     * @param productType
     * The productType
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     *
     * @return
     * The productTypeDescription
     */
    public String getProductTypeDescription() {
        return productTypeDescription;
    }

    /**
     *
     * @param productTypeDescription
     * The productTypeDescription
     */
    public void setProductTypeDescription(String productTypeDescription) {
        this.productTypeDescription = productTypeDescription;
    }

    /**
     *
     * @return
     * The voucher
     */
    public List<Voucher> getVoucher() {
        return voucher;
    }

    /**
     *
     * @param voucher
     * The voucher
     */
    public void setVoucher(List<Voucher> voucher) {
        this.voucher = voucher;
    }
}