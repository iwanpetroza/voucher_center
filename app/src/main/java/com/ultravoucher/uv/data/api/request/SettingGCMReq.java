package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 03/09/18.
 */

public class SettingGCMReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("deviceUniqueId")
    @Expose
    private String deviceUniqueId;
    @SerializedName("regKey")
    @Expose
    private String regKey;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The deviceUniqueId
     */
    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    /**
     *
     * @param deviceUniqueId
     * The deviceUniqueId
     */
    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    /**
     *
     * @return
     * The regKey
     */
    public String getRegKey() {
        return regKey;
    }

    /**
     *
     * @param regKey
     * The regKey
     */
    public void setRegKey(String regKey) {
        this.regKey = regKey;
    }
}
