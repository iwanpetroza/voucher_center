package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by firwandi.ramli on 1/3/2018.
 */

public class Product implements Serializable {

    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("productName")
    @Expose
    private String productName;
    @SerializedName("productType")
    @Expose
    private int productType;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("merchantName")
    @Expose
    private String merchantName;
    @SerializedName("basePrice")
    @Expose
    private Object basePrice;
    @SerializedName("currencyId")
    @Expose
    private Object currencyId;
    @SerializedName("stock")
    @Expose
    private int stock;
    @SerializedName("productImage")
    @Expose
    private String productImage;

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productType
     */
    public int getProductType() {
        return productType;
    }

    /**
     *
     * @param productType
     * The productType
     */
    public void setProductType(int productType) {
        this.productType = productType;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     *
     * @param merchantName
     * The merchantName
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     *
     * @return
     * The basePrice
     */
    public Object getBasePrice() {
        return basePrice;
    }

    /**
     *
     * @param basePrice
     * The basePrice
     */
    public void setBasePrice(Object basePrice) {
        this.basePrice = basePrice;
    }

    /**
     *
     * @return
     * The currencyId
     */
    public Object getCurrencyId() {
        return currencyId;
    }

    /**
     *
     * @param currencyId
     * The currencyId
     */
    public void setCurrencyId(Object currencyId) {
        this.currencyId = currencyId;
    }

    /**
     *
     * @return
     * The stock
     */
    public int getStock() {
        return stock;
    }

    /**
     *
     * @param stock
     * The stock
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     *
     * @return
     * The productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     *
     * @param productImage
     * The productImage
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

}
