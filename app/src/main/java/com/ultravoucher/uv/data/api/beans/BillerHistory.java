package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerHistory {

    @SerializedName("transactionId")
    @Expose
    public String transactionId;
    @SerializedName("productName")
    @Expose
    public String productName;
    @SerializedName("productType")
    @Expose
    public Integer productType;
    @SerializedName("transactionDate")
    @Expose
    public String transactionDate;
    @SerializedName("contractNo")
    @Expose
    public String contractNo;
    @SerializedName("paymentMethod")
    @Expose
    public String paymentMethod;
    @SerializedName("paymentExpiredDate")
    @Expose
    public String paymentExpiredDate;
    @SerializedName("vaNumber")
    @Expose
    public String vaNumber;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("totalAmount")
    @Expose
    public Integer totalAmount;
    @SerializedName("token")
    @Expose
    public String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentExpiredDate() {
        return paymentExpiredDate;
    }

    public void setPaymentExpiredDate(String paymentExpiredDate) {
        this.paymentExpiredDate = paymentExpiredDate;
    }

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }
}
