package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderReq;

public class StoreByIdReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("isDeals")
	private int isDeals;

	@SerializedName("id")
	private String id;

	@SerializedName("page")
	private int page;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("order")
	private OrderReq order;

	@SerializedName("sVoucherClass")
	private String voucherClass;

	@SerializedName("productShownType")
	private String productShownType;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setIsDeals(int isDeals){
		this.isDeals = isDeals;
	}

	public int getIsDeals(){
		return isDeals;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setKeyword(String keyword){
		this.keyword = keyword;
	}

	public String getKeyword(){
		return keyword;
	}

	public String getVoucherClass() {
		return voucherClass;
	}

	public void setVoucherClass(String voucherClass) {
		this.voucherClass = voucherClass;
	}

	public OrderReq getOrder() {
		return order;
	}

	public void setOrder(OrderReq order) {
		this.order = order;
	}

	public String getProductShownType() {
		return productShownType;
	}

	public void setProductShownType(String productShownType) {
		this.productShownType = productShownType;
	}

	@Override
 	public String toString(){
		return 
			"StoreByIdReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",isDeals = '" + isDeals + '\'' + 
			",id = '" + id + '\'' + 
			",page = '" + page + '\'' + 
			",keyword = '" + keyword + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}