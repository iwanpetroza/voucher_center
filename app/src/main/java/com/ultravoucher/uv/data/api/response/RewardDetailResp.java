package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.RecordInfo;
import com.ultravoucher.uv.data.api.beans.RewardResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class RewardDetailResp {

    @SerializedName("rewardResponse")
    @Expose
    private List<RewardResp> rewardResponse = new ArrayList<>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;
    @SerializedName("recordInfo")
    @Expose
    private RecordInfo recordInfo;

    /**
     *
     * @return
     * The rewardResponse
     */
    public List<RewardResp> getRewardResponse() {
        return rewardResponse;
    }

    /**
     *
     * @param rewardResponse
     * The rewardResponse
     */
    public void setRewardResponse(List<RewardResp> rewardResponse) {
        this.rewardResponse = rewardResponse;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

    /**
     *
     * @return
     * The recordInfo
     */
    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    /**
     *
     * @param recordInfo
     * The recordInfo
     */
    public void setRecordInfo(RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }


}
