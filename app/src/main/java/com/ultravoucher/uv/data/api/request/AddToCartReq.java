package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class AddToCartReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("origin")
    @Expose
    private String origin;

    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("orders")
    @Expose
    private List<OrderRequest> orders = new ArrayList<OrderRequest>();

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     *
     * @return
     * The orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     *
     * @param orderNumber
     * The orderNumber
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<OrderRequest> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<OrderRequest> orders) {
        this.orders = orders;
    }
}