package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class CreateAddressReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("addressAlias")
    @Expose
    private String addressAlias;
    @SerializedName("recipientName")
    @Expose
    private String recipientName;
    @SerializedName("line")
    @Expose
    private String line;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("village")
    @Expose
    private String village;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("stateProvId")
    @Expose
    private String stateProvId;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The addressAlias
     */
    public String getAddressAlias() {
        return addressAlias;
    }

    /**
     *
     * @param addressAlias
     * The addressAlias
     */
    public void setAddressAlias(String addressAlias) {
        this.addressAlias = addressAlias;
    }

    /**
     *
     * @return
     * The recipientName
     */
    public String getRecipientName() {
        return recipientName;
    }

    /**
     *
     * @param recipientName
     * The recipientName
     */
    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    /**
     *
     * @return
     * The line
     */
    public String getLine() {
        return line;
    }

    /**
     *
     * @param line
     * The line
     */
    public void setLine(String line) {
        this.line = line;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The district
     */
    public String getDistrict() {
        return district;
    }

    /**
     *
     * @param district
     * The district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     *
     * @return
     * The village
     */
    public String getVillage() {
        return village;
    }

    /**
     *
     * @param village
     * The village
     */
    public void setVillage(String village) {
        this.village = village;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     *
     * @param mobileNumber
     * The mobileNumber
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public String getStateProvId() {
        return stateProvId;
    }

    public void setStateProvId(String stateProvId) {
        this.stateProvId = stateProvId;
    }
}
