package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.MemberWaletDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/14/2017.
 */

public class MemberWaletResponse extends GenericResponse {

    @SerializedName("totalBalance")
    @Expose
    private int totalBalance;

    @SerializedName("dealsDetailBalance")
    @Expose
    private List<MemberWaletDetail> dealsDetailBalance = new ArrayList<MemberWaletDetail>();

    public int getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(int totalBalance) {
        this.totalBalance = totalBalance;
    }

    public List<MemberWaletDetail> getDealsDetailBalance() {
        return dealsDetailBalance;
    }

    public void setDealsDetailBalance(List<MemberWaletDetail> dealsDetailBalance) {
        this.dealsDetailBalance = dealsDetailBalance;
    }
}
