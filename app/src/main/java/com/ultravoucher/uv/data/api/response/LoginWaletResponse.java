package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.request.LoginWalet;

/**
 * Created by firwandi.ramli on 1/24/2018.
 */

public class LoginWaletResponse extends GenericResponse {

    @SerializedName("logPayResponse")
    @Expose
    private LoginWalet logPayResponse;

    public LoginWalet getLogPayResponse() {
        return logPayResponse;
    }

    public void setLogPayResponse(LoginWalet logPayResponse) {
        this.logPayResponse = logPayResponse;
    }
}
