package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.api.beans.RecordInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class MyCartResponse {
    @SerializedName("totalTrxAmount")
    @Expose
    private int totalTrxAmount;

    @SerializedName("highestVoucherClass")
    @Expose
    private int highestVoucherClass;

    @SerializedName("orders")
    @Expose
    private List<Order> orders = new ArrayList<Order>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;
    @SerializedName("recordInfo")
    @Expose
    private RecordInfo recordInfo;

    /**
     *
     * @return
     * The totalTrxAmount
     */
    public int getTotalTrxAmount() {
        return totalTrxAmount;
    }

    /**
     *
     * @param totalTrxAmount
     * The totalTrxAmount
     */
    public void setTotalTrxAmount(int totalTrxAmount) {
        this.totalTrxAmount = totalTrxAmount;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

    /**
     *
     * @return
     * The recordInfo
     */
    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    /**
     *
     * @param recordInfo
     * The recordInfo
     */
    public void setRecordInfo(RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    public int getHighestVoucherClass() {
        return highestVoucherClass;
    }

    public void setHighestVoucherClass(int highestVoucherClass) {
        this.highestVoucherClass = highestVoucherClass;
    }
}