package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class AddToCartResponse extends GenericResponse{

    @SerializedName("totalTrxAmount")
    @Expose
    private double totalTrxAmount;
    @SerializedName("orders")
    @Expose
    private List<OrderResp> orders = new ArrayList<OrderResp>();

    /**
     *
     * @return
     * The totalTrxAmount
     */
    public double getTotalTrxAmount() {
        return totalTrxAmount;
    }

    /**
     *
     * @param totalTrxAmount
     * The totalTrxAmount
     */
    public void setTotalTrxAmount(double totalTrxAmount) {
        this.totalTrxAmount = totalTrxAmount;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<OrderResp> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<OrderResp> orders) {
        this.orders = orders;
    }
}