package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class AutoRegistrationReq{

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("creator")
	private String creator;

	@SerializedName("mobileNumber")
	private String mobileNumber;

	@SerializedName("username")
	private String username;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setCreator(String creator){
		this.creator = creator;
	}

	public String getCreator(){
		return creator;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"AutoRegistrationReq{" + 
			"firstName = '" + firstName + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",creator = '" + creator + '\'' + 
			",mobileNumber = '" + mobileNumber + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}