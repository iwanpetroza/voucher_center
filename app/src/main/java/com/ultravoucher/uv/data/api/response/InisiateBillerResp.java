package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.InisiateBillerActive;
import com.ultravoucher.uv.data.api.beans.InisiateBillerAmount;
import com.ultravoucher.uv.data.api.beans.InisiateBillerFee;
import com.ultravoucher.uv.data.api.beans.InisiateBillerProductValue;

/**
 * Created by tunggul.jati on 6/20/2018.
 */

public class InisiateBillerResp {

    @SerializedName("amount")
    @Expose
    public InisiateBillerAmount amount;
    @SerializedName("fee")
    @Expose
    public InisiateBillerFee fee;
    @SerializedName("active")
    @Expose
    public InisiateBillerActive active;
    @SerializedName("productValue")
    @Expose
    public InisiateBillerProductValue productValue;
    @SerializedName("abstractResponse")
    @Expose
    public AbstractResponse abstractResponse;

    public InisiateBillerProductValue getProductValue() {
        return productValue;
    }

    public void setProductValue(InisiateBillerProductValue productValue) {
        this.productValue = productValue;
    }

    public InisiateBillerAmount getAmount() {
        return amount;
    }

    public void setAmount(InisiateBillerAmount amount) {
        this.amount = amount;
    }

    public InisiateBillerFee getFee() {
        return fee;
    }

    public void setFee(InisiateBillerFee fee) {
        this.fee = fee;
    }

    public InisiateBillerActive getActive() {
        return active;
    }

    public void setActive(InisiateBillerActive active) {
        this.active = active;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
