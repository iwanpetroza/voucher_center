package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class InquiryShareReq{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("voucherValue")
	private int voucherValue;

	@SerializedName("sVoucherClass")
	private String sVoucherClass;

	@SerializedName("page")
	private int page;

	@SerializedName("memberId")
	private String memberId;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setVoucherValue(int voucherValue){
		this.voucherValue = voucherValue;
	}

	public int getVoucherValue(){
		return voucherValue;
	}

	public void setSVoucherClass(String sVoucherClass){
		this.sVoucherClass = sVoucherClass;
	}

	public String getSVoucherClass(){
		return sVoucherClass;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	@Override
 	public String toString(){
		return 
			"InquiryShareReq{" + 
			"quantity = '" + quantity + '\'' + 
			",nRecords = '" + nRecords + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",voucherValue = '" + voucherValue + '\'' + 
			",sVoucherClass = '" + sVoucherClass + '\'' + 
			",page = '" + page + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}