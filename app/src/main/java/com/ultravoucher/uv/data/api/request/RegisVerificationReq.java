package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.response.GenericResponse;

public class RegisVerificationReq extends GenericResponse{

	@SerializedName("code")
	private String code;

	@SerializedName("username")
	private String username;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"RegisVerificationReq{" + 
			"�code� = '" + code + '\'' + 
			",�username� = '" + username + '\'' + 
			"}";
		}
}