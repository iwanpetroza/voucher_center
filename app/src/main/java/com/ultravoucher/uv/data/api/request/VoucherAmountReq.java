package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class VoucherAmountReq{

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("status")
	private int status;

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"VoucherAmountReq{" + 
			"voucherClass = '" + voucherClass + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}