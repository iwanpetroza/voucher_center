package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class DigitalPocketReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("sVoucherClass")
	private String sVoucherClass;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("page")
	private int page;

	@SerializedName("memberId")
	private String memberId;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setSVoucherClass(String sVoucherClass){
		this.sVoucherClass = sVoucherClass;
	}

	public String getSVoucherClass(){
		return sVoucherClass;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public int getVoucherClass() {
		return voucherClass;
	}

	public void setVoucherClass(int voucherClass) {
		this.voucherClass = voucherClass;
	}

	@Override
 	public String toString(){
		return 
			"DigitalPocketReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",sVoucherClass = '" + sVoucherClass + '\'' + 
			",page = '" + page + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}