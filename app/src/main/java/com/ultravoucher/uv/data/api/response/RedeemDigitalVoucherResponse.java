package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.RedeemDigitalVoucherMsgResp;

/**
 * Created by Firwandi S Ramli on 23/04/18.
 */

public class RedeemDigitalVoucherResponse extends GenericResponse {

    @SerializedName("voucherResponse")
    @Expose
    private RedeemDigitalVoucherMsgResp voucherResponse;

    public RedeemDigitalVoucherMsgResp getVoucherResponse() {
        return voucherResponse;
    }

    public void setVoucherResponse(RedeemDigitalVoucherMsgResp voucherResponse) {
        this.voucherResponse = voucherResponse;
    }
}
