package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class DetailProductByIdReq{

	@SerializedName("id")
	private String id;

	@SerializedName("keyword")
	private String keyword;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setKeyword(String keyword){
		this.keyword = keyword;
	}

	public String getKeyword(){
		return keyword;
	}

	@Override
 	public String toString(){
		return 
			"DetailProductByIdReq{" + 
			"id = '" + id + '\'' + 
			",keyword = '" + keyword + '\'' + 
			"}";
		}
}