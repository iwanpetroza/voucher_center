package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 3/13/2018.
 */

public class HistoryProductResponse extends GenericResponse {

    @SerializedName("orders")
    @Expose
    private List<Order> orders = new ArrayList<Order>();

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
