package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/23/2018.
 */

public class InqPayOrderReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;

    @SerializedName("pin")
    @Expose
    private String pin;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }


    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     *
     * @return
     * The pin
     */
    public String getPin() {
        return pin;
    }

    /**
     *
     * @param pin
     * The pin
     */
    public void setPin(String pin) {
        this.pin = pin;
    }
}

