package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.HistoryBillerDetailMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerDetailResponse extends GenericResponse {

    @SerializedName("billerHistory")
    @Expose
    private List<HistoryBillerDetailMsgResp> billerHistory = new ArrayList<HistoryBillerDetailMsgResp>();

    public List<HistoryBillerDetailMsgResp> getBillerHistory() {
        return billerHistory;
    }

    public void setBillerHistory(List<HistoryBillerDetailMsgResp> billerHistory) {
        this.billerHistory = billerHistory;
    }
}
