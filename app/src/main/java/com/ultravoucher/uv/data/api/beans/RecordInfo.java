package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class RecordInfo {

    @SerializedName("totalRecords")
    @Expose
    private int totalRecords;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("nrecords")
    @Expose
    private int nrecords;

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getNrecords() {
        return nrecords;
    }

    public void setNrecords(int nrecords) {
        this.nrecords = nrecords;
    }

}