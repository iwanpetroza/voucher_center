package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.DeliveryAddressResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class AddAddressResponse {

    @SerializedName("deliveryAddress")
    @Expose
    private List<DeliveryAddressResp> deliveryAddress = new ArrayList<>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    /**
     *
     * @return
     * The deliveryAddress
     */
    public List<DeliveryAddressResp> getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     *
     * @param deliveryAddress
     * The deliveryAddress
     */
    public void setDeliveryAddress(List<DeliveryAddressResp> deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

}
