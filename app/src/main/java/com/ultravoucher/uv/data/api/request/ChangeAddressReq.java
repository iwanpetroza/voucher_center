package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderChangeAddress;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/15/2018.
 */

public class ChangeAddressReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("orders")
    @Expose
    private List<OrderChangeAddress> orders = new ArrayList<OrderChangeAddress>();

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<OrderChangeAddress> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<OrderChangeAddress> orders) {
        this.orders = orders;

    }
}
