package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.RegisteredMember;
import com.ultravoucher.uv.data.api.beans.UnRegisteredMember;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/8/2017.
 */

public class MemberExistResponse extends GenericResponse {

    @SerializedName("registeredMember")
    @Expose
    private List<RegisteredMember> registeredMember = new ArrayList<RegisteredMember>();

    @SerializedName("unregisteredMember")
    @Expose
    private List<UnRegisteredMember> unregisteredMember = new ArrayList<UnRegisteredMember>();

    public List<RegisteredMember> getRegisteredMember() {
        return registeredMember;
    }

    public void setRegisteredMember(List<RegisteredMember> registeredMember) {
        this.registeredMember = registeredMember;
    }

    public List<UnRegisteredMember> getUnregisteredMember() {
        return unregisteredMember;
    }

    public void setUnregisteredMember(List<UnRegisteredMember> unregisteredMember) {
        this.unregisteredMember = unregisteredMember;
    }
}
