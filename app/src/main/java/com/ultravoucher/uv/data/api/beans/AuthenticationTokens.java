package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.response.GenericResponse;

/**
 * Created by firwandi.ramli on 10/4/2017.
 */

public class AuthenticationTokens extends GenericResponse {
    @SerializedName("authtokenResponse")
    @Expose
    private AuthTokenResp authtokenResponse;

    /**
     *
     * @return
     * The authtokenResponse
     */
    public AuthTokenResp getAuthtokenResponse() {
        return authtokenResponse;
    }

    /**
     *
     * @param authtokenResponse
     * The authtokenResponse
     */
    public void setAuthtokenResponse(AuthTokenResp authtokenResponse) {
        this.authtokenResponse = authtokenResponse;
    }
}