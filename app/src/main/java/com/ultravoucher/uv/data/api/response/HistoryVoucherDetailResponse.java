package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.OrderVouchersHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 31/07/18.
 */

public class HistoryVoucherDetailResponse extends GenericResponse {

    @SerializedName("orderVouchers")
    private List<OrderVouchersHistory> orderVouchers = new ArrayList<OrderVouchersHistory>();

    public List<OrderVouchersHistory> getOrderVouchers() {
        return orderVouchers;
    }

    public void setOrderVouchers(List<OrderVouchersHistory> orderVouchers) {
        this.orderVouchers = orderVouchers;
    }
}
