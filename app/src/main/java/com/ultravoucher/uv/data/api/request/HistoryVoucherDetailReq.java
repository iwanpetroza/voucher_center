package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 31/07/18.
 */

public class HistoryVoucherDetailReq {

    @SerializedName("page")
    @Expose
    private int page;

    @SerializedName("nRecords")
    @Expose
    private int nRecords;

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("sVoucherClass")
    @Expose
    private String sVoucherClass;

    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getnRecords() {
        return nRecords;
    }

    public void setnRecords(int nRecords) {
        this.nRecords = nRecords;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getsVoucherClass() {
        return sVoucherClass;
    }

    public void setsVoucherClass(String sVoucherClass) {
        this.sVoucherClass = sVoucherClass;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
