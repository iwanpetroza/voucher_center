package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.VoucherRedemption;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 17/05/18.
 */

public class VoucherRedemptionResponse extends GenericResponse {

    @SerializedName("voucherRedemtionResponse")
    @Expose
    private List<VoucherRedemption> voucherRedemptionResponse = new ArrayList<VoucherRedemption>();

    public List<VoucherRedemption> getVoucherRedemptionResponse() {
        return voucherRedemptionResponse;
    }

    public void setVoucherRedemptionResponse(List<VoucherRedemption> voucherRedemptionResponse) {
        this.voucherRedemptionResponse = voucherRedemptionResponse;
    }
}
