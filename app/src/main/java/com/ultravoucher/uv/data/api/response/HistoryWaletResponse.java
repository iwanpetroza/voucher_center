package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.HistoryWalet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 2/9/2018.
 */

public class HistoryWaletResponse extends GenericResponse{

    @SerializedName("history")
    @Expose
    private List<HistoryWalet> history = new ArrayList<HistoryWalet>();

    public List<HistoryWalet> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryWalet> history) {
        this.history = history;
    }
}
