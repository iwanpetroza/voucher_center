package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 31/07/18.
 */

public class OrderVouchersHistory {

    @SerializedName("isRedeemed")
    private int isRedeemed;

    @SerializedName("voucherId")
    private String voucherId;

    @SerializedName("expiredDate")
    private String expiredDate;

    @SerializedName("redeemedDate")
    private String redeemedDate;

    @SerializedName("voucherCode")
    private String voucherCode;

    @SerializedName("voucherClass")
    private int voucherClass;

    @SerializedName("voucherUsage")
    private int voucherUsage;

    @SerializedName("voucherUsageDetails")
    private String voucherUsageDetails;

    public int getIsRedeemed() {
        return isRedeemed;
    }

    public void setIsRedeemed(int isRedeemed) {
        this.isRedeemed = isRedeemed;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getRedeemedDate() {
        return redeemedDate;
    }

    public void setRedeemedDate(String redeemedDate) {
        this.redeemedDate = redeemedDate;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public int getVoucherClass() {
        return voucherClass;
    }

    public void setVoucherClass(int voucherClass) {
        this.voucherClass = voucherClass;
    }

    public int getVoucherUsage() {
        return voucherUsage;
    }

    public void setVoucherUsage(int voucherUsage) {
        this.voucherUsage = voucherUsage;
    }

    public String getVoucherUsageDetails() {
        return voucherUsageDetails;
    }

    public void setVoucherUsageDetails(String voucherUsageDetails) {
        this.voucherUsageDetails = voucherUsageDetails;
    }
}
