package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.JNECheckRatesMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/9/2018.
 */

public class JNECheckRatesResponse extends GenericResponse {

    @SerializedName("listPackage")
    @Expose
    public List<JNECheckRatesMsgResp> listPackage = new ArrayList<JNECheckRatesMsgResp>();

    public List<JNECheckRatesMsgResp> getListPackage() {
        return listPackage;
    }

    public void setListPackage(List<JNECheckRatesMsgResp> listPackage) {
        this.listPackage = listPackage;
    }
}
