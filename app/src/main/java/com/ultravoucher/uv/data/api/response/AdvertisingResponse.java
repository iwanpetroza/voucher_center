package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/8/2017.
 */

public class AdvertisingResponse {

    @SerializedName("advertisingResponse")
    @Expose
    private List<AdvertisingMsgResp> advertisingResponse = new ArrayList<AdvertisingMsgResp>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    /**
     *
     * @return
     * The advertisingResponse
     */
    public List<AdvertisingMsgResp> getAdvertisingResponse() {
        return advertisingResponse;
    }

    /**
     *
     * @param advertisingResponse
     * The advertisingResponse
     */
    public void setAdvertisingResponse(List<AdvertisingMsgResp> advertisingResponse) {
        this.advertisingResponse = advertisingResponse;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

}
