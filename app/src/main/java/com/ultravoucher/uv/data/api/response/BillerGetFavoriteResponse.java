package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.BillerHistory;
import com.ultravoucher.uv.data.api.beans.ContractFavorite;

import java.util.List;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerGetFavoriteResponse{

    @SerializedName("memberId")
    @Expose
    public String memberId;
    @SerializedName("category")
    @Expose
    public Integer category;
    @SerializedName("contractFavorite")
    @Expose
    public List<ContractFavorite> contractFavorite = null;
    @SerializedName("abstractResponse")
    @Expose
    public AbstractResponse abstractResponse;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public List<ContractFavorite> getContractFavorite() {
        return contractFavorite;
    }

    public void setContractFavorite(List<ContractFavorite> contractFavorite) {
        this.contractFavorite = contractFavorite;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
