package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class JNECheckRatesMsgResp{

	@SerializedName("serviceCode")
	private String serviceCode;

	@SerializedName("price")
	private int price;

	@SerializedName("destinationName")
	private String destinationName;

	@SerializedName("serviceDisplay")
	private String serviceDisplay;

	@SerializedName("originName")
	private String originName;

	public void setServiceCode(String serviceCode){
		this.serviceCode = serviceCode;
	}

	public String getServiceCode(){
		return serviceCode;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setDestinationName(String destinationName){
		this.destinationName = destinationName;
	}

	public String getDestinationName(){
		return destinationName;
	}

	public void setServiceDisplay(String serviceDisplay){
		this.serviceDisplay = serviceDisplay;
	}

	public String getServiceDisplay(){
		return serviceDisplay;
	}

	public void setOriginName(String originName){
		this.originName = originName;
	}

	public String getOriginName(){
		return originName;
	}

	@Override
 	public String toString(){
		return 
			"JNECheckRatesMsgResp{" + 
			"serviceCode = '" + serviceCode + '\'' + 
			",price = '" + price + '\'' + 
			",destinationName = '" + destinationName + '\'' + 
			",serviceDisplay = '" + serviceDisplay + '\'' + 
			",originName = '" + originName + '\'' + 
			"}";
		}
}