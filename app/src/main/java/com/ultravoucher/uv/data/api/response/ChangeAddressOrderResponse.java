package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/15/2018.
 */

public class ChangeAddressOrderResponse {

    @SerializedName("totalTrxAmount")
    @Expose
    private String totalTrxAmount;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = new ArrayList<>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    /**
     *
     * @return
     * The totalTrxAmount
     */
    public String getTotalTrxAmount() {
        return totalTrxAmount;
    }

    /**
     *
     * @param totalTrxAmount
     * The totalTrxAmount
     */
    public void setTotalTrxAmount(String totalTrxAmount) {
        this.totalTrxAmount = totalTrxAmount;
    }

    /**
     *
     * @return
     * The orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     *
     * @param orders
     * The orders
     */
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

}


