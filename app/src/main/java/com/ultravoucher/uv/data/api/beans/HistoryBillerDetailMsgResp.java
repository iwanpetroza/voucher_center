package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerDetailMsgResp {

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("productName")
    private String productName;

    @SerializedName("productType")
    private int productType;

    @SerializedName("transactionDate")
    private String transactionDate;

    @SerializedName("contractName")
    private String contractName;

    @SerializedName("contractNo")
    private String contractNo;

    @SerializedName("paymentMethod")
    private String paymentMethod;

    @SerializedName("paymentExpiredDate")
    private String paymentExpiredDate;

    @SerializedName("vaNumber")
    private String vaNumber;

    @SerializedName("status")
    private String status;

    @SerializedName("totalAmount")
    private int totalAmount;

    @SerializedName("token")
    private String token;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentExpiredDate() {
        return paymentExpiredDate;
    }

    public void setPaymentExpiredDate(String paymentExpiredDate) {
        this.paymentExpiredDate = paymentExpiredDate;
    }

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
