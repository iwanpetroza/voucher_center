package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class Order {
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("orderDate")
    @Expose
    private String orderDate;
    @SerializedName("paymentChannel")
    @Expose
    private String paymentChannel;
    @SerializedName("vaNumber")
    @Expose
    private String vaNumber;
    @SerializedName("totalPayment")
    @Expose
    private int totalPayment;
    @SerializedName("paymentStatus")
    @Expose
    private int paymentStatus;
    @SerializedName("paymentDate")
    @Expose
    private String paymentDate;
    @SerializedName("paymentReferenceCode")
    @Expose
    private String paymentReferenceCode;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("variantId")
    @Expose
    private String variantId;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("totalAmount")
    @Expose
    private int totalAmount;
    @SerializedName("taxAmount")
    @Expose
    private int taxAmount;
    @SerializedName("discountName")
    @Expose
    private String discountName;
    @SerializedName("discountType")
    @Expose
    private String discountType;
    @SerializedName("discountValue")
    @Expose
    private int discountValue;
    @SerializedName("transactionRefNumber")
    @Expose
    private String transactionRefNumber;
    @SerializedName("shippingBy")
    @Expose
    private String shippingBy;
    @SerializedName("shippingAmount")
    @Expose
    private int shippingAmount;
    @SerializedName("pickupDate")
    @Expose
    private String pickupDate;
    @SerializedName("pickupNote")
    @Expose
    private String pickupNote;
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("deliveryAddress")
    @Expose
    private String deliveryAddress;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("variants")
    @Expose
    private Object variants;
    @SerializedName("productImage")
    @Expose
    private String productImage;

    @SerializedName("productName")
    @Expose
    private String productName;

    @SerializedName("productType")
    @Expose
    private int productType;

    @SerializedName("variantName")
    @Expose
    private String variantName;

    @SerializedName("variantStock")
    @Expose
    private int variantStock;

    @SerializedName("basePrice")
    @Expose
    private int basePrice;
    @SerializedName("sellingPrice")
    @Expose
    private int sellingPrice;

    @SerializedName("voucherClass")
    @Expose
    private int voucherClass;

    @SerializedName("deliveryMethod")
    @Expose
    private int deliveryMethod;


    @SerializedName("deliveryAddressId")
    @Expose
    private String deliveryAddressId;

    @SerializedName("province")
    @Expose
    private String province;

    @SerializedName("district")
    @Expose
    private String district;

    @SerializedName("village")
    @Expose
    private String village;

    @SerializedName("checkoutDate")
    @Expose
    private String checkoutDate;

    @SerializedName("paymentExpiredDate")
    @Expose
    private String paymentExpiredDate;

    @SerializedName("insuranceFee")
    @Expose
    private int insuranceFee;

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     *
     * @param orderNumber
     * The orderNumber
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     *
     * @return
     * The orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     *
     * @param orderDate
     * The orderDate
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     *
     * @return
     * The paymentChannel
     */
    public String getPaymentChannel() {
        return paymentChannel;
    }

    /**
     *
     * @param paymentChannel
     * The paymentChannel
     */
    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    /**
     *
     * @return
     * The vaNumber
     */
    public String getVaNumber() {
        return vaNumber;
    }

    /**
     *
     * @param vaNumber
     * The vaNumber
     */
    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    /**
     *
     * @return
     * The totalPayment
     */
    public int getTotalPayment() {
        return totalPayment;
    }

    /**
     *
     * @param totalPayment
     * The totalPayment
     */
    public void setTotalPayment(int totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     *
     * @return
     * The paymentStatus
     */
    public int getPaymentStatus() {
        return paymentStatus;
    }

    /**
     *
     * @param paymentStatus
     * The paymentStatus
     */
    public void setPaymentStatus(int paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     *
     * @return
     * The paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     *
     * @param paymentDate
     * The paymentDate
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     *
     * @return
     * The paymentReferenceCode
     */
    public String getPaymentReferenceCode() {
        return paymentReferenceCode;
    }

    /**
     *
     * @param paymentReferenceCode
     * The paymentReferenceCode
     */
    public void setPaymentReferenceCode(String paymentReferenceCode) {
        this.paymentReferenceCode = paymentReferenceCode;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The variantId
     */
    public String getVariantId() {
        return variantId;
    }

    /**
     *
     * @param variantId
     * The variantId
     */
    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    /**
     *
     * @return
     * The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     * The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     * The totalAmount
     */
    public int getTotalAmount() {
        return totalAmount;
    }

    /**
     *
     * @param totalAmount
     * The totalAmount
     */
    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     *
     * @return
     * The taxAmount
     */
    public int getTaxAmount() {
        return taxAmount;
    }

    /**
     *
     * @param taxAmount
     * The taxAmount
     */
    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     *
     * @return
     * The discountName
     */
    public String getDiscountName() {
        return discountName;
    }

    /**
     *
     * @param discountName
     * The discountName
     */
    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    /**
     *
     * @return
     * The discountType
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     *
     * @param discountType
     * The discountType
     */
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    /**
     *
     * @return
     * The discountValue
     */
    public int getDiscountValue() {
        return discountValue;
    }

    /**
     *
     * @param discountValue
     * The discountValue
     */
    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    /**
     *
     * @return
     * The transactionRefNumber
     */
    public String getTransactionRefNumber() {
        return transactionRefNumber;
    }

    /**
     *
     * @param transactionRefNumber
     * The transactionRefNumber
     */
    public void setTransactionRefNumber(String transactionRefNumber) {
        this.transactionRefNumber = transactionRefNumber;
    }

    /**
     *
     * @return
     * The shippingBy
     */
    public String getShippingBy() {
        return shippingBy;
    }

    /**
     *
     * @param shippingBy
     * The shippingBy
     */
    public void setShippingBy(String shippingBy) {
        this.shippingBy = shippingBy;
    }

    /**
     *
     * @return
     * The shippingAmount
     */
    public int getShippingAmount() {
        return shippingAmount;
    }

    /**
     *
     * @param shippingAmount
     * The shippingAmount
     */
    public void setShippingAmount(int shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     *
     * @return
     * The pickupDate
     */
    public String getPickupDate() {
        return pickupDate;
    }

    /**
     *
     * @param pickupDate
     * The pickupDate
     */
    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    /**
     *
     * @return
     * The pickupNote
     */
    public String getPickupNote() {
        return pickupNote;
    }

    /**
     *
     * @param pickupNote
     * The pickupNote
     */
    public void setPickupNote(String pickupNote) {
        this.pickupNote = pickupNote;
    }

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     *
     * @param deliveryAddress
     * The deliveryAddress
     */
    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * The postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     *
     * @return
     * The phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * The phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The status
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The variants
     */
    public Object getVariants() {
        return variants;
    }

    /**
     *
     * @param variants
     * The variants
     */
    public void setVariants(Object variants) {
        this.variants = variants;
    }

    /**
     *
     * @return
     * The productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     *
     * @param productImage
     * The productImage
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The productName
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The productType
     */
    public int getProductType() {
        return productType;
    }

    /**
     *
     * @param productType
     * The productType
     */
    public void setProductType(int productType) {
        this.productType = productType;
    }

    /**
     *
     * @return
     * The variantName
     */
    public String getVariantName() {
        return variantName;
    }

    /**
     *
     * @param variantName
     * The variantName
     */
    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }


    // ================== Tambahan Baru VariantStock =============================
    /**
     *
     * @return
     * The variantStock
     */
    public int getVariantStock() {
        return variantStock;
    }

    /**
     *
     * @param variantStock
     * The variantStock
     */
    public void setVariantStock(int variantStock) {
        this.variantStock = variantStock;
    }

    public String getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(String checkoutDate) {
        this.checkoutDate = checkoutDate;
    }


    // ================== End Of Tambahan Baru VariantStock =============================




    /**
     *
     * @return
     * The basePrice
     */
    public int getBasePrice() {
        return basePrice;
    }

    /**
     *
     * @param basePrice
     * The basePrice
     */
    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    /**
     *
     * @return
     * The sellingPrice
     */
    public int getSellingPrice() {
        return sellingPrice;
    }

    /**
     *
     * @param sellingPrice
     * The sellingPrice
     */
    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getVoucherClass() {
        return voucherClass;
    }

    public void setVoucherClass(int voucherClass) {
        this.voucherClass = voucherClass;
    }

    public int getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(int deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public String getDeliveryAddressId() {
        return deliveryAddressId;
    }

    public void setDeliveryAddressId(String deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPaymentExpiredDate() {
        return paymentExpiredDate;
    }

    public void setPaymentExpiredDate(String paymentExpiredDate) {
        this.paymentExpiredDate = paymentExpiredDate;
    }

    public int getInsuranceFee() {
        return insuranceFee;
    }

    public void setInsuranceFee(int insuranceFee) {
        this.insuranceFee = insuranceFee;
    }
}