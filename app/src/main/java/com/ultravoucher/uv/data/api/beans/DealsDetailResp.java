package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DealsDetailResp implements Serializable{

	@SerializedName("totalVoucherValue")
	private int totalVoucherValue;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("merchantLogo")
	private String merchantLogo;

	@SerializedName("merchantName")
	private String merchantName;

	@SerializedName("dealsDetail")
	@Expose
	private List<DealsDetailMsgResp> dealsDetail = new ArrayList<DealsDetailMsgResp>();

	public void setTotalVoucherValue(int totalVoucherValue){
		this.totalVoucherValue = totalVoucherValue;
	}

	public int getTotalVoucherValue(){
		return totalVoucherValue;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setMerchantLogo(String merchantLogo){
		this.merchantLogo = merchantLogo;
	}

	public String getMerchantLogo(){
		return merchantLogo;
	}

	public void setMerchantName(String merchantName){
		this.merchantName = merchantName;
	}

	public String getMerchantName(){
		return merchantName;
	}

	public List<DealsDetailMsgResp> getDealsDetail() {
		return dealsDetail;
	}

	public void setDealsDetail(List<DealsDetailMsgResp> dealsDetail) {
		this.dealsDetail = dealsDetail;
	}

	@Override
 	public String toString(){
		return 
			"DealsDetailResp{" +
			"totalVoucherValue = '" + totalVoucherValue + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",merchantLogo = '" + merchantLogo + '\'' + 
			",merchantName = '" + merchantName + '\'' + 
			"}";
		}
}