package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 12/29/2017.
 */

public class DisOrder {
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("variantId")
    @Expose
    private String variantId;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("productType")
    @Expose
    private int productType;

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The variantId
     */
    public String getVariantId() {
        return variantId;
    }

    /**
     *
     * @param variantId
     * The variantId
     */
    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    /**
     *
     * @return
     * The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     * The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }


}


