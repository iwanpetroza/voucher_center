package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class Reward {

    @SerializedName("rewardId")
    @Expose
    private String rewardId;
    @SerializedName("quantity")
    @Expose
    private int quantity;

    /**
     *
     * @return
     * The rewardId
     */
    public String getRewardId() {
        return rewardId;
    }

    /**
     *
     * @param rewardId
     * The rewardId
     */
    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    /**
     *
     * @return
     * The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     * The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
