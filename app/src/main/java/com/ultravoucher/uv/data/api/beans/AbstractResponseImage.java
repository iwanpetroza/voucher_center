package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 12/27/2017.
 */

public class AbstractResponseImage {

    @SerializedName("responseStatus")
    @Expose
    private String responseStatus;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("urlImage")
    @Expose
    private String urlImage;

    /**
     * @return The responseStatus
     */
    public String getResponseStatus() {
        return responseStatus;
    }

    /**
     * @param responseStatus The responseStatus
     */
    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    /**
     * @return The responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage The responseMessage
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return The urlImage
     */
    public String getUrlImage() {
        return urlImage;
    }

    /**
     * @param urlImage The urlImage
     */
    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

}