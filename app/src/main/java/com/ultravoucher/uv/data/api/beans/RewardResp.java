package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/3/2018.
 */

public class RewardResp implements Serializable {

    @SerializedName("rewardId")
    @Expose
    private String rewardId;
    @SerializedName("rewardCode")
    @Expose
    private String rewardCode;
    @SerializedName("rewardName")
    @Expose
    private String rewardName;
    @SerializedName("rewardDescription")
    @Expose
    private String rewardDescription;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("tierIds")
    @Expose
    private List<String> tierIds = new ArrayList<String>();
    @SerializedName("requiredPts")
    @Expose
    private int requiredPts;
    @SerializedName("pointCost")
    @Expose
    private Object pointCost;
    @SerializedName("expiredDate")
    @Expose
    private Object expiredDate;
    @SerializedName("rewardTermAndCondition")
    @Expose
    private String rewardTermAndCondition;

    /**
     *
     * @return
     * The rewardId
     */
    public String getRewardId() {
        return rewardId;
    }

    /**
     *
     * @param rewardId
     * The rewardId
     */
    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    /**
     *
     * @return
     * The rewardCode
     */
    public String getRewardCode() {
        return rewardCode;
    }

    /**
     *
     * @param rewardCode
     * The rewardCode
     */
    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    /**
     *
     * @return
     * The rewardName
     */
    public String getRewardName() {
        return rewardName;
    }

    /**
     *
     * @param rewardName
     * The rewardName
     */
    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    /**
     *
     * @return
     * The rewardDescription
     */
    public String getRewardDescription() {
        return rewardDescription;
    }

    /**
     *
     * @param rewardDescription
     * The rewardDescription
     */
    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The product
     */
    public Product getProduct() {
        return product;
    }

    /**
     *
     * @param product
     * The product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     *
     * @return
     * The tierIds
     */
    public List<String> getTierIds() {
        return tierIds;
    }

    /**
     *
     * @param tierIds
     * The tierIds
     */
    public void setTierIds(List<String> tierIds) {
        this.tierIds = tierIds;
    }

    /**
     *
     * @return
     * The requiredPts
     */
    public int getRequiredPts() {
        return requiredPts;
    }

    /**
     *
     * @param requiredPts
     * The requiredPts
     */
    public void setRequiredPts(int requiredPts) {
        this.requiredPts = requiredPts;
    }

    /**
     *
     * @return
     * The pointCost
     */
    public Object getPointCost() {
        return pointCost;
    }

    /**
     *
     * @param pointCost
     * The pointCost
     */
    public void setPointCost(Object pointCost) {
        this.pointCost = pointCost;
    }

    /**
     *
     * @return
     * The expiredDate
     */
    public Object getExpiredDate() {
        return expiredDate;
    }

    /**
     *
     * @param expiredDate
     * The expiredDate
     */
    public void setExpiredDate(Object expiredDate) {
        this.expiredDate = expiredDate;
    }

    /**
     *
     * @return
     * The rewardTermAndCondition
     */
    public String getRewardTermAndCondition() {
        return rewardTermAndCondition;
    }

    /**
     *
     * @param rewardTermAndCondition
     * The rewardTermAndCondition
     */
    public void setRewardTermAndCondition(String rewardTermAndCondition) {
        this.rewardTermAndCondition = rewardTermAndCondition;
    }

}
