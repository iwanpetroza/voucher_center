package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class SharingBalanceDetail{

	@SerializedName("isRedeemed")
	private int isRedeemed;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("redeemedDate")
	private String redeemedDate;

	@SerializedName("voucherCode")
	private String voucherCode;

	public void setIsRedeemed(int isRedeemed){
		this.isRedeemed = isRedeemed;
	}

	public int getIsRedeemed(){
		return isRedeemed;
	}

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setRedeemedDate(String redeemedDate){
		this.redeemedDate = redeemedDate;
	}

	public String getRedeemedDate(){
		return redeemedDate;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	@Override
 	public String toString(){
		return 
			"SharingBalanceDetail{" + 
			"isRedeemed = '" + isRedeemed + '\'' + 
			",voucherClass = '" + voucherClass + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",redeemedDate = '" + redeemedDate + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			"}";
		}
}