package com.ultravoucher.uv.data.api;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by firwandi.ramli on 9/25/2017.
 */

public class VoucherService {
//    private static final String BASE_URL = "http://52.76.117.11:8081/lifestyle-ws-vc/api-rest/"; //URL DEV
        private static final String BASE_URL = "https://ws.ultravoucher.co.id/lifestyle-ws-vc/api-rest/"; // URL PROD
    private VoucherApi api;

    public VoucherService() {
        Retrofit retrofitSvc = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getCustomClient())
                .build();
        this.api = retrofitSvc.create(VoucherApi.class);
    }


    private OkHttpClient getCustomClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(
                new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder requestBuilder = chain.request().newBuilder();
                        requestBuilder.header("Content-Type", "application/json");
                        return chain.proceed(requestBuilder.build());
                    }
                }
        )/*.addInterceptor(logging)*/
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        return httpClient;
    }

    public VoucherApi getApi(){
        return this.api;
    }
}
