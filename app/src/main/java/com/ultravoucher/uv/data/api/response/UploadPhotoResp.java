package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponseImage;

/**
 * Created by firwandi.ramli on 12/27/2017.
 */

public class UploadPhotoResp {

    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponseImage abstractResponse;

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponseImage getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponseImage abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
