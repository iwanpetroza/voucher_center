package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class SubmitShareVoucherResp{

	@SerializedName("voucherSharingId")
	private String voucherSharingId;

	@SerializedName("voucherCodeBefore")
	private String voucherCodeBefore;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("memberIdFrom")
	private String memberIdFrom;

	@SerializedName("memberIdTo")
	private String memberIdTo;

	@SerializedName("voucherCodeAfter")
	private String voucherCodeAfter;

	@SerializedName("sharingDate")
	private String sharingDate;

	public void setVoucherSharingId(String voucherSharingId){
		this.voucherSharingId = voucherSharingId;
	}

	public String getVoucherSharingId(){
		return voucherSharingId;
	}

	public void setVoucherCodeBefore(String voucherCodeBefore){
		this.voucherCodeBefore = voucherCodeBefore;
	}

	public String getVoucherCodeBefore(){
		return voucherCodeBefore;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setMemberIdFrom(String memberIdFrom){
		this.memberIdFrom = memberIdFrom;
	}

	public String getMemberIdFrom(){
		return memberIdFrom;
	}

	public void setMemberIdTo(String memberIdTo){
		this.memberIdTo = memberIdTo;
	}

	public String getMemberIdTo(){
		return memberIdTo;
	}

	public void setVoucherCodeAfter(String voucherCodeAfter){
		this.voucherCodeAfter = voucherCodeAfter;
	}

	public String getVoucherCodeAfter(){
		return voucherCodeAfter;
	}

	public void setSharingDate(String sharingDate){
		this.sharingDate = sharingDate;
	}

	public String getSharingDate(){
		return sharingDate;
	}

	@Override
 	public String toString(){
		return 
			"SubmitShareVoucherResp{" + 
			"voucherSharingId = '" + voucherSharingId + '\'' + 
			",voucherCodeBefore = '" + voucherCodeBefore + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",memberIdFrom = '" + memberIdFrom + '\'' + 
			",memberIdTo = '" + memberIdTo + '\'' + 
			",voucherCodeAfter = '" + voucherCodeAfter + '\'' + 
			",sharingDate = '" + sharingDate + '\'' + 
			"}";
		}
}