package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/22/2018.
 */

public class VoucherCollectDetailResponse {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("merchantCode")
    @Expose
    private String merchantCode;
    @SerializedName("merchantName")
    @Expose
    private String merchantName;
    @SerializedName("rewardId")
    @Expose
    private String rewardId;
    @SerializedName("rewardCode")
    @Expose
    private String rewardCode;
    @SerializedName("rewardName")
    @Expose
    private String rewardName;
    @SerializedName("requiredPts")
    @Expose
    private int requiredPts;
    @SerializedName("rewardDescription")
    @Expose
    private String rewardDescription;
    @SerializedName("voucherTransactionId")
    @Expose
    private String voucherTransactionId;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("voucherExpiredDate")
    @Expose
    private String voucherExpiredDate;
    @SerializedName("productType")
    @Expose
    private int productType;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;
    @SerializedName("redeemedDate")
    @Expose
    private String redeemedDate;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("lotteryId")
    @Expose
    private String lotteryId;
    @SerializedName("lotteryCode")
    @Expose
    private String lotteryCode;
    @SerializedName("termAndCondition")
    @Expose
    private String termAndCondition;


    @SerializedName("voucherClass")
    @Expose
    private int voucherClass;

    /**
     * @return The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * @param memberId The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     * @return The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return The merchantCode
     */
    public String getMerchantCode() {
        return merchantCode;
    }

    /**
     * @param merchantCode The merchantCode
     */
    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    /**
     * @return The merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName The merchantName
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return The rewardId
     */
    public String getRewardId() {
        return rewardId;
    }

    /**
     * @param rewardId The rewardId
     */
    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    /**
     * @return The rewardCode
     */
    public String getRewardCode() {
        return rewardCode;
    }

    /**
     * @param rewardCode The rewardCode
     */
    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    /**
     * @return The rewardName
     */
    public String getRewardName() {
        return rewardName;
    }

    /**
     * @param rewardName The rewardName
     */
    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    /**
     *
     * @return
     * The requiredPts
     */
    public int getRequiredPts() {
        return requiredPts;
    }

    /**
     *
     * @param requiredPts
     * The requiredPts
     */
    public void setRequiredPts(int requiredPts) {
        this.requiredPts = requiredPts;
    }

    /**
     * @return The rewardDescription
     */
    public String getRewardDescription() {
        return rewardDescription;
    }

    /**
     * @param rewardDescription The rewardDescription
     */
    public void setRewardDescription(String rewardDescription) {
        this.rewardDescription = rewardDescription;
    }

    /**
     * @return The voucherTransactionId
     */
    public String getVoucherTransactionId() {
        return voucherTransactionId;
    }

    /**
     * @param voucherTransactionId The voucherTransactionId
     */
    public void setVoucherTransactionId(String voucherTransactionId) {
        this.voucherTransactionId = voucherTransactionId;
    }

    /**
     * @return The voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * @param voucherCode The voucherCode
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     * @return The voucherExpiredDate
     */
    public String getVoucherExpiredDate() {
        return voucherExpiredDate;
    }

    /**
     * @param voucherExpiredDate The voucherExpiredDate
     */
    public void setVoucherExpiredDate(String voucherExpiredDate) {
        this.voucherExpiredDate = voucherExpiredDate;
    }

    /**
     * @return The productType
     */
    public int getProductType() {
        return productType;
    }

    /**
     * @param productType The productType
     */
    public void setProductType(int productType) {
        this.productType = productType;
    }

    /**
     * @return The productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     * @param productImage The productImage
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    /**
     * @return The transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate The transactionDate
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return The redeemedDate
     */
    public String getRedeemedDate() {
        return redeemedDate;
    }

    /**
     * @param redeemedDate The redeemedDate
     */
    public void setRedeemedDate(String redeemedDate) {
        this.redeemedDate = redeemedDate;
    }

    /**
     * @return The pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin The pin
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * @return The lotteryId
     */
    public String getLotteryId() {
        return lotteryId;
    }

    /**
     * @param lotteryId The lotteryId
     */
    public void setLotteryId(String lotteryId) {
        this.lotteryId = lotteryId;
    }

    /**
     * @return The lotteryCode
     */
    public String getLotteryCode() {
        return lotteryCode;
    }

    /**
     * @param lotteryCode The lotteryCode
     */
    public void setLotteryCode(String lotteryCode) {
        this.lotteryCode = lotteryCode;
    }

    /**
     *
     * @return
     * The termAndCondition
     */
    public String getTermAndCondition() {
        return termAndCondition;
    }

    /**
     *
     * @param termAndCondition
     * The termAndCondition
     */
    public void setTermAndCondition(String termAndCondition) {
        this.termAndCondition = termAndCondition;
    }

    public int getVoucherClass() {
        return voucherClass;
    }

    public void setVoucherClass(int voucherClass) {
        this.voucherClass = voucherClass;
    }
}