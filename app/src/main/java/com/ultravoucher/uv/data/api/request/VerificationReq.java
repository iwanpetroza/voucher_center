package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class VerificationReq{

	@SerializedName("code")
	private String code;

	@SerializedName("username")
	private String username;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"VerificationReq{" + 
			"code = '" + code + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}