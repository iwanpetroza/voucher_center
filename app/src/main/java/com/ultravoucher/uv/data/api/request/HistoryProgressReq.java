package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/7/2017.
 */

public class HistoryProgressReq {

    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("nRecords")
    @Expose
    private int nRecords;
    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("sVoucherClass")
    @Expose
    private String sVoucherClass;

    /**
     *
     * @return
     * The page
     */
    public int getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     *
     * @return
     * The nRecords
     */
    public int getNRecords() {
        return nRecords;
    }

    /**
     *
     * @param nRecords
     * The nRecords
     */
    public void setNRecords(int nRecords) {
        this.nRecords = nRecords;
    }

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getsVoucherClass() {
        return sVoucherClass;
    }

    public void setsVoucherClass(String sVoucherClass) {
        this.sVoucherClass = sVoucherClass;
    }
}
