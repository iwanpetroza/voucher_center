package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class CheckMemberPointsReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("page")
	private int page;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("order")
	private String order;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"CheckMemberPointsReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",page = '" + page + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}