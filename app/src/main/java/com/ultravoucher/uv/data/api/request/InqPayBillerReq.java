package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class InqPayBillerReq {

    @SerializedName("billerId")
    @Expose
    public String billerId;
    @SerializedName("contractNo")
    @Expose
    public String contractNo;
    @SerializedName("memberId")
    @Expose
    public String memberId;
    @SerializedName("pin")
    @Expose
    public String pin;
    @SerializedName("paymentMethod")
    @Expose
    public Integer paymentMethod;
    @SerializedName("favorite")
    @Expose
    public Boolean favorite;

    @SerializedName("origin")
    @Expose
    public String origin;

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }
}
