package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.response.GenericResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 2/23/2018.
 */

public class UpdateDelMethodReq extends GenericResponse {

    @SerializedName("memberId")
    private String memberId;

    @SerializedName("orders")
    @Expose
    private List<UpdateDelMethod> orders = new ArrayList<UpdateDelMethod>();

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public List<UpdateDelMethod> getOrders() {
        return orders;
    }

    public void setOrders(List<UpdateDelMethod> orders) {
        this.orders = orders;
    }
}
