package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class VoucherMsgResp{

	@SerializedName("voucherCode")
	private Object voucherCode;

	@SerializedName("status")
	private Object status;

	public void setVoucherCode(Object voucherCode){
		this.voucherCode = voucherCode;
	}

	public Object getVoucherCode(){
		return voucherCode;
	}

	public void setStatus(Object status){
		this.status = status;
	}

	public Object getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"VoucherMsgResp{" + 
			"voucherCode = '" + voucherCode + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}