package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.SerializedName;

public class AutoRegistrationResponse extends GenericResponse{

	@SerializedName("isRegisteredPayment")
	private int isRegisteredPayment;

	@SerializedName("verificationCode")
	private String verificationCode;

	@SerializedName("memberId")
	private String memberId;

	public void setIsRegisteredPayment(int isRegisteredPayment){
		this.isRegisteredPayment = isRegisteredPayment;
	}

	public int getIsRegisteredPayment(){
		return isRegisteredPayment;
	}

	public void setVerificationCode(String verificationCode){
		this.verificationCode = verificationCode;
	}

	public String getVerificationCode(){
		return verificationCode;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	@Override
 	public String toString(){
		return 
			"AutoRegistrationResponse{" + 
			"isRegisteredPayment = '" + isRegisteredPayment + '\'' + 
			",verificationCode = '" + verificationCode + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}