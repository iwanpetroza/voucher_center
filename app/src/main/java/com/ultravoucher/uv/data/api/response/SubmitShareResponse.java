package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.SubmitShareVoucherResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/14/2017.
 */

public class SubmitShareResponse extends GenericResponse{

    @SerializedName("sharedVouchers")
    private List<SubmitShareVoucherResp> sharedVouchers = new ArrayList<SubmitShareVoucherResp>();

    public List<SubmitShareVoucherResp> getSharedVouchers() {
        return sharedVouchers;
    }

    public void setSharedVouchers(List<SubmitShareVoucherResp> sharedVouchers) {
        this.sharedVouchers = sharedVouchers;
    }
}
