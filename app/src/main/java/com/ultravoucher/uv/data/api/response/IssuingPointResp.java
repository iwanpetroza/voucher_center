package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.PromotionMessageResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class IssuingPointResp {

    @SerializedName("promotionMessageResponse")
    @Expose
    private List<PromotionMessageResponse> promotionMessageResponse = new ArrayList<>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    /**
     *
     * @return
     * The promotionMessageResponse
     */
    public List<PromotionMessageResponse> getPromotionMessageResponse() {
        return promotionMessageResponse;
    }

    /**
     *
     * @param promotionMessageResponse
     * The promotionMessageResponse
     */
    public void setPromotionMessageResponse(List<PromotionMessageResponse> promotionMessageResponse) {
        this.promotionMessageResponse = promotionMessageResponse;
    }

    /**
     *
     * @return
     * The abstractResponse
     */
    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    /**
     *
     * @param abstractResponse
     * The abstractResponse
     */
    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
