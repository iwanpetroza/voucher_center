package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class PromotionMessageResponse {
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("memberName")
    @Expose
    private String memberName;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("promotionCode")
    @Expose
    private String promotionCode;
    @SerializedName("promotionName")
    @Expose
    private String promotionName;
    @SerializedName("posTransactionId")
    @Expose
    private String posTransactionId;
    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;
    @SerializedName("trxPartialAmount")
    @Expose
    private Object trxPartialAmount;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("promotionNumberIssued")
    @Expose
    private PromotionNumberIssued promotionNumberIssued;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The memberName
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     *
     * @param memberName
     * The memberName
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The promotionCode
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     *
     * @param promotionCode
     * The promotionCode
     */
    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    /**
     *
     * @return
     * The promotionName
     */
    public String getPromotionName() {
        return promotionName;
    }

    /**
     *
     * @param promotionName
     * The promotionName
     */
    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    /**
     *
     * @return
     * The posTransactionId
     */
    public String getPosTransactionId() {
        return posTransactionId;
    }

    /**
     *
     * @param posTransactionId
     * The posTransactionId
     */
    public void setPosTransactionId(String posTransactionId) {
        this.posTransactionId = posTransactionId;
    }

    /**
     *
     * @return
     * The transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     *
     * @param transactionDate
     * The transactionDate
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     *
     * @return
     * The trxPartialAmount
     */
    public Object getTrxPartialAmount() {
        return trxPartialAmount;
    }

    /**
     *
     * @param trxPartialAmount
     * The trxPartialAmount
     */
    public void setTrxPartialAmount(Object trxPartialAmount) {
        this.trxPartialAmount = trxPartialAmount;
    }

    /**
     *
     * @return
     * The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     *
     * @param transactionId
     * The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     *
     * @return
     * The promotionNumberIssued
     */
    public PromotionNumberIssued getPromotionNumberIssued() {
        return promotionNumberIssued;
    }

    /**
     *
     * @param promotionNumberIssued
     * The promotionNumberIssued
     */
    public void setPromotionNumberIssued(PromotionNumberIssued promotionNumberIssued) {
        this.promotionNumberIssued = promotionNumberIssued;
    }
}


