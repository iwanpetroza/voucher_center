package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.VoucherListMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class VoucherListResponse extends GenericResponse {

    @SerializedName("products")
    @Expose
    private List<VoucherListMsgResp> products = new ArrayList<VoucherListMsgResp>();

    public List<VoucherListMsgResp> getProducts() {
        return products;
    }

    public void setProducts(List<VoucherListMsgResp> products) {
        this.products = products;
    }
}