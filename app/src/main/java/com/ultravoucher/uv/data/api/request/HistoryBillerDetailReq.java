package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerDetailReq {

    @SerializedName("transactionId")
    @Expose
    private String transactionId;

    @SerializedName("memberId")
    @Expose
    private String memberId;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
