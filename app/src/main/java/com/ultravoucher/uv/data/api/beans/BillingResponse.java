package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class BillingResponse implements Serializable {
    @SerializedName("billingId")
    @Expose
    private String billingId;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("storeName")
    @Expose
    private String storeName;
    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;
    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();
    @SerializedName("subTotal")
    @Expose
    private int subTotal;
    @SerializedName("taxes")
    @Expose
    private int taxes;
    @SerializedName("total")
    @Expose
    private int total;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("storeLogo")
    @Expose
    private String storeLogo;

    /**
     *
     * @return
     * The billingId
     */
    public String getBillingId() {
        return billingId;
    }

    /**
     *
     * @param billingId
     * The billingId
     */
    public void setBillingId(String billingId) {
        this.billingId = billingId;
    }

    /**
     *
     * @return
     * The transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     *
     * @param transactionId
     * The transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     *
     * @param storeName
     * The storeName
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     *
     * @param transactionDate
     * The transactionDate
     */
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     *
     * @return
     * The subTotal
     */
    public int getSubTotal() {
        return subTotal;
    }

    /**
     *
     * @param subTotal
     * The subTotal
     */
    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }

    /**
     *
     * @return
     * The taxes
     */
    public int getTaxes() {
        return taxes;
    }

    /**
     *
     * @param taxes
     * The taxes
     */
    public void setTaxes(int taxes) {
        this.taxes = taxes;
    }

    /**
     *
     * @return
     * The total
     */
    public int getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     *
     * @return
     * The status
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The storeLogo
     */
    public String getStoreLogo() {
        return storeLogo;
    }

    /**
     *
     * @param storeLogo
     * The storeLogo
     */
    public void setStoreLogo(String storeLogo) {
        this.storeLogo = storeLogo;
    }
}


