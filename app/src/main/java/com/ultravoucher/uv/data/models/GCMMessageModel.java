package com.ultravoucher.uv.data.models;

import java.io.Serializable;

/**
 * Created by Firwandi S Ramli on 03/09/18.
 */

public class GCMMessageModel implements Serializable {

    private String mTitle;
    private String mMessage;
    private String mType;
    private String mId;

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

}
