package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderVouchers implements Serializable{

	@SerializedName("isRedeemed")
	private int isRedeemed;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("redeemedDate")
	private String redeemedDate;

	@SerializedName("voucherCode")
	private String voucherCode;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("voucherUsage")
	private int voucherUsage;

	@SerializedName("voucherUsageDetails")
	private String voucherUsageDetails;

	public void setIsRedeemed(int isRedeemed){
		this.isRedeemed = isRedeemed;
	}

	public int getIsRedeemed(){
		return isRedeemed;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setRedeemedDate(String redeemedDate){
		this.redeemedDate = redeemedDate;
	}

	public String getRedeemedDate(){
		return redeemedDate;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	public int getVoucherClass() {
		return voucherClass;
	}

	public void setVoucherClass(int voucherClass) {
		this.voucherClass = voucherClass;
	}

	public int getVoucherUsage() {
		return voucherUsage;
	}

	public void setVoucherUsage(int voucherUsage) {
		this.voucherUsage = voucherUsage;
	}

	public String getVoucherUsageDetails() {
		return voucherUsageDetails;
	}

	public void setVoucherUsageDetails(String voucherUsageDetails) {
		this.voucherUsageDetails = voucherUsageDetails;
	}

	@Override
 	public String toString(){
		return 
			"OrderVouchers{" + 
			"isRedeemed = '" + isRedeemed + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",redeemedDate = '" + redeemedDate + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			"}";
		}
}