package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.CheckMemberReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/8/2017.
 */

public class CheckMemberExistReq extends GenericResponse {

    @SerializedName("listMember")
    @Expose
    private List<CheckMemberReq> listMember = new ArrayList<CheckMemberReq>();

    public List<CheckMemberReq> getListMember() {
        return listMember;
    }

    public void setListMember(List<CheckMemberReq> listMember) {
        this.listMember = listMember;
    }
}
