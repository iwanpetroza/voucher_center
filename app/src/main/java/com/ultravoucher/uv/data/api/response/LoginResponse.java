package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AuthenticationTokens;

/**
 * Created by firwandi.ramli on 10/4/2017.
 */

public class LoginResponse {

    @SerializedName("authenticationTokens")
    @Expose
    private AuthenticationTokens authenticationTokens;

    /**
     *
     * @return
     * The authenticationTokens
     */
    public AuthenticationTokens getAuthenticationTokens() {
        return authenticationTokens;
    }

    /**
     *
     * @param authenticationTokens
     * The authenticationTokens
     */
    public void setAuthenticationTokens(AuthenticationTokens authenticationTokens) {
        this.authenticationTokens = authenticationTokens;
    }


}
