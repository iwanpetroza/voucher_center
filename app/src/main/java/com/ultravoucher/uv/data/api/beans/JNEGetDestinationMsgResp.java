package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class JNEGetDestinationMsgResp{

	@SerializedName("code")
	private String code;

	@SerializedName("city")
	private String city;

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	@Override
 	public String toString(){
		return 
			"JNEGetDestinationMsgResp{" + 
			"code = '" + code + '\'' + 
			",city = '" + city + '\'' + 
			"}";
		}
}