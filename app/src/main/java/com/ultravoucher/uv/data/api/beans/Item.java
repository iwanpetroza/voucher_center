package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class Item implements Serializable {
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("itemQuantity")
    @Expose
    private int itemQuantity;
    @SerializedName("itemPrice")
    @Expose
    private int itemPrice;
    @SerializedName("itemTotalPrice")
    @Expose
    private int itemTotalPrice;
    @SerializedName("additionalTotalPrice")
    @Expose
    private int additionalTotalPrice;
    @SerializedName("discountTotalAmount")
    @Expose
    private int discountTotalAmount;

    /**
     *
     * @return
     * The itemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     *
     * @param itemName
     * The itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     *
     * @return
     * The itemQuantity
     */
    public int getItemQuantity() {
        return itemQuantity;
    }

    /**
     *
     * @param itemQuantity
     * The itemQuantity
     */
    public void setItemQuantity(int itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    /**
     *
     * @return
     * The itemPrice
     */
    public int getItemPrice() {
        return itemPrice;
    }

    /**
     *
     * @param itemPrice
     * The itemPrice
     */
    public void setItemPrice(int itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     *
     * @return
     * The itemTotalPrice
     */
    public int getItemTotalPrice() {
        return itemTotalPrice;
    }

    /**
     *
     * @param itemTotalPrice
     * The itemTotalPrice
     */
    public void setItemTotalPrice(int itemTotalPrice) {
        this.itemTotalPrice = itemTotalPrice;
    }

    /**
     *
     * @return
     * The additionalTotalPrice
     */
    public int getAdditionalTotalPrice() {
        return additionalTotalPrice;
    }

    /**
     *
     * @param additionalTotalPrice
     * The additionalTotalPrice
     */
    public void setAdditionalTotalPrice(int additionalTotalPrice) {
        this.additionalTotalPrice = additionalTotalPrice;
    }

    /**
     *
     * @return
     * The discountTotalAmount
     */
    public int getDiscountTotalAmount() {
        return discountTotalAmount;
    }

    /**
     *
     * @param discountTotalAmount
     * The discountTotalAmount
     */
    public void setDiscountTotalAmount(int discountTotalAmount) {
        this.discountTotalAmount = discountTotalAmount;
    }
}
