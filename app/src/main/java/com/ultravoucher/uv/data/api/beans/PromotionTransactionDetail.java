package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class PromotionTransactionDetail {

    @SerializedName("pointAmount")
    @Expose
    private String pointAmount;
    @SerializedName("totalTrxAmount")
    @Expose
    private String totalTrxAmount;
    @SerializedName("memberTier")
    @Expose
    private String memberTier;
    @SerializedName("visitNumber")
    @Expose
    private String visitNumber;
    @SerializedName("referralCode")
    @Expose
    private String referralCode;
    @SerializedName("stampNumber")
    @Expose
    private String stampNumber;
    @SerializedName("millageTotal")
    @Expose
    private String millageTotal;
    @SerializedName("socialSharingNumber")
    @Expose
    private String socialSharingNumber;

    /**
     *
     * @return
     * The pointAmount
     */
    public String getPointAmount() {
        return pointAmount;
    }

    /**
     *
     * @param pointAmount
     * The pointAmount
     */
    public void setPointAmount(String pointAmount) {
        this.pointAmount = pointAmount;
    }

    /**
     *
     * @return
     * The totalTrxAmount
     */
    public String getTotalTrxAmount() {
        return totalTrxAmount;
    }

    /**
     *
     * @param totalTrxAmount
     * The totalTrxAmount
     */
    public void setTotalTrxAmount(String totalTrxAmount) {
        this.totalTrxAmount = totalTrxAmount;
    }

    /**
     *
     * @return
     * The memberTier
     */
    public String getMemberTier() {
        return memberTier;
    }

    /**
     *
     * @param memberTier
     * The memberTier
     */
    public void setMemberTier(String memberTier) {
        this.memberTier = memberTier;
    }

    /**
     *
     * @return
     * The visitNumber
     */
    public String getVisitNumber() {
        return visitNumber;
    }

    /**
     *
     * @param visitNumber
     * The visitNumber
     */
    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    /**
     *
     * @return
     * The referralCode
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     *
     * @param referralCode
     * The referralCode
     */
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    /**
     *
     * @return
     * The stampNumber
     */
    public String getStampNumber() {
        return stampNumber;
    }

    /**
     *
     * @param stampNumber
     * The stampNumber
     */
    public void setStampNumber(String stampNumber) {
        this.stampNumber = stampNumber;
    }

    /**
     *
     * @return
     * The millageTotal
     */
    public String getMillageTotal() {
        return millageTotal;
    }

    /**
     *
     * @param millageTotal
     * The millageTotal
     */
    public void setMillageTotal(String millageTotal) {
        this.millageTotal = millageTotal;
    }

    /**
     *
     * @return
     * The socialSharingNumber
     */
    public String getSocialSharingNumber() {
        return socialSharingNumber;
    }

    /**
     *
     * @param socialSharingNumber
     * The socialSharingNumber
     */
    public void setSocialSharingNumber(String socialSharingNumber) {
        this.socialSharingNumber = socialSharingNumber;
    }
}
