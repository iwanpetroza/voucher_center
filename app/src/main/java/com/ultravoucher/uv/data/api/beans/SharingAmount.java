package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SharingAmount{

	@SerializedName("totalQuantity")
	private int totalQuantity;

	@SerializedName("voucherValue")
	private int voucherValue;

	@SerializedName("details")
	@Expose
	private List<SharingBalanceDetail> details = new ArrayList<SharingBalanceDetail>();

	public List<SharingBalanceDetail> getDetails() {
		return details;
	}

	public void setDetails(List<SharingBalanceDetail> details) {
		this.details = details;
	}

	public void setTotalQuantity(int totalQuantity){
		this.totalQuantity = totalQuantity;
	}

	public int getTotalQuantity(){
		return totalQuantity;
	}

	public void setVoucherValue(int voucherValue){
		this.voucherValue = voucherValue;
	}

	public int getVoucherValue(){
		return voucherValue;
	}


	@Override
 	public String toString(){
		return 
			"SharingAmount{" + 
			"totalQuantity = '" + totalQuantity + '\'' + 
			",voucherValue = '" + voucherValue + '\'' + 
			",details = '" + details + '\'' + 
			"}";
		}
}