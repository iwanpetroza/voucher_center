package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.JNEGetDestinationMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/9/2018.
 */

public class JNEGetDestinationResponse extends GenericResponse {

    @SerializedName("listCityCode")
    @Expose
    private List<JNEGetDestinationMsgResp> listCityCode = new ArrayList<JNEGetDestinationMsgResp>();

    public List<JNEGetDestinationMsgResp> getListCityCode() {
        return listCityCode;
    }

    public void setListCityCode(List<JNEGetDestinationMsgResp> listCityCode) {
        this.listCityCode = listCityCode;
    }
}
