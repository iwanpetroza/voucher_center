package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.BillerVoucherHistoryMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 25/07/18.
 */

public class BillerVoucherHistoryResponse extends GenericResponse{

    @SerializedName("orderHistory")
    @Expose
    private List<BillerVoucherHistoryMsgResp> orderHistory = new ArrayList<BillerVoucherHistoryMsgResp>();


    public List<BillerVoucherHistoryMsgResp> getOrderHistory() {
        return orderHistory;
    }

    public void setOrderHistory(List<BillerVoucherHistoryMsgResp> orderHistory) {
        this.orderHistory = orderHistory;
    }
}
