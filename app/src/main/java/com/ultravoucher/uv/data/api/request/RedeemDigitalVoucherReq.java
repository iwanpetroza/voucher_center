package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 23/04/18.
 */

public class RedeemDigitalVoucherReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("voucherId")
    @Expose
    private String voucherId;

    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
}
