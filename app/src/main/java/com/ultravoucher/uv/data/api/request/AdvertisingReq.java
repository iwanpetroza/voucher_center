package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/8/2017.
 */

public class AdvertisingReq {

    @SerializedName("advertisingType")
    @Expose
    private int advertisingType;

    @SerializedName("cityId")
    @Expose
    private String cityId;

    /**
     *
     * @return
     * The advertisingType
     */
    public int getAdvertisingType() {
        return advertisingType;
    }

    /**
     *
     * @param advertisingType
     * The advertisingType
     */
    public void setAdvertisingType(int advertisingType) {
        this.advertisingType = advertisingType;
    }


    /**
     *
     * @return
     * The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The cityId
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
