package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DealsDetailMsgResp implements Serializable{

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("productId")
	private String productId;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("storeId")
	private String storeId;

	@SerializedName("productName")
	private String productName;

	@SerializedName("sellingPrice")
	private int sellingPrice;

	@SerializedName("productImage")
	private String productImage;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("paymentDate")
	private String paymentDate;

	@SerializedName("productType")
	private String productType;

	@SerializedName("productDescription")
	private String productDescription;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("pickupExpiredDate")
	private String pickupExpiredDate;

	@SerializedName("isRedeemed")
	private int isRedeemed;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("redeemedDate")
	private String redeemedDate;

	@SerializedName("voucherCode")
	private String voucherCode;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("voucherUsage")
	private int voucherUsage;

	@SerializedName("voucherUsageDetails")
	private String voucherUsageDetails;

	@SerializedName("voucherExpiredDate")
	private String voucherExpiredDate;

	/*@SerializedName("orderVouchers")
	@Expose
	private List<OrderVouchers> orderVouchers = new ArrayList<OrderVouchers>();*/

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setSellingPrice(int sellingPrice){
		this.sellingPrice = sellingPrice;
	}

	public int getSellingPrice(){
		return sellingPrice;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}

	public String getPaymentDate(){
		return paymentDate;
	}

	public void setProductType(String productType){
		this.productType = productType;
	}

	public String getProductType(){
		return productType;
	}

	public void setProductDescription(String productDescription){
		this.productDescription = productDescription;
	}

	public String getProductDescription(){
		return productDescription;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public String getPickupExpiredDate() {
		return pickupExpiredDate;
	}

	public void setPickupExpiredDate(String pickupExpiredDate) {
		this.pickupExpiredDate = pickupExpiredDate;
	}

	public void setIsRedeemed(int isRedeemed){
		this.isRedeemed = isRedeemed;
	}

	public int getIsRedeemed(){
		return isRedeemed;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setRedeemedDate(String redeemedDate){
		this.redeemedDate = redeemedDate;
	}

	public String getRedeemedDate(){
		return redeemedDate;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	public int getVoucherClass() {
		return voucherClass;
	}

	public void setVoucherClass(int voucherClass) {
		this.voucherClass = voucherClass;
	}

	public int getVoucherUsage() {
		return voucherUsage;
	}

	public void setVoucherUsage(int voucherUsage) {
		this.voucherUsage = voucherUsage;
	}

	public String getVoucherUsageDetails() {
		return voucherUsageDetails;
	}

	public void setVoucherUsageDetails(String voucherUsageDetails) {
		this.voucherUsageDetails = voucherUsageDetails;
	}

	public String getVoucherExpiredDate() {
		return voucherExpiredDate;
	}

	public void setVoucherExpiredDate(String voucherExpiredDate) {
		this.voucherExpiredDate = voucherExpiredDate;
	}

	/*public List<OrderVouchers> getOrderVouchers() {
		return orderVouchers;
	}

	public void setOrderVouchers(List<OrderVouchers> orderVouchers) {
		this.orderVouchers = orderVouchers;
	}*/

	@Override
 	public String toString(){
		return 
			"DealsDetailMsgResp{" + 
			"orderNumber = '" + orderNumber + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",productId = '" + productId + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",productName = '" + productName + '\'' + 
			",sellingPrice = '" + sellingPrice + '\'' + 
			",productImage = '" + productImage + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",paymentDate = '" + paymentDate + '\'' + 
			",productType = '" + productType + '\'' + 
			",productDescription = '" + productDescription + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}