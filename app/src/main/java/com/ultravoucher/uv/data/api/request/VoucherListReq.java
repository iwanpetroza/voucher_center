package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class VoucherListReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("isDeals")
	private int isDeals;

	@SerializedName("page")
	private int page;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("order")
	private String order;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setIsDeals(int isDeals){
		this.isDeals = isDeals;
	}

	public int getIsDeals(){
		return isDeals;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setKeyword(String keyword){
		this.keyword = keyword;
	}

	public String getKeyword(){
		return keyword;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}

	@Override
 	public String toString(){
		return 
			"VoucherListReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",isDeals = '" + isDeals + '\'' + 
			",page = '" + page + '\'' + 
			",keyword = '" + keyword + '\'' + 
			",order = '" + order + '\'' + 
			"}";
		}
}