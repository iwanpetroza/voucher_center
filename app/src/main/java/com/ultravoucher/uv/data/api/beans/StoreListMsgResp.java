package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class StoreListMsgResp{

	@SerializedName("lastUpdatedBy")
	private String lastUpdatedBy;

	@SerializedName("storeType")
	private String storeType;

	@SerializedName("distance")
	private String distance;

	@SerializedName("storeTypeName")
	private String storeTypeName;

	@SerializedName("isReserve")
	private int isReserve;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("merchantLogo")
	private String merchantLogo;

	@SerializedName("storeId")
	private String storeId;

	@SerializedName("merchantName")
	private String merchantName;

	@SerializedName("lastUpdatedDate")
	private String lastUpdatedDate;

	@SerializedName("createdDate")
	private String createdDate;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("pin")
	private String pin;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("cityTown")
	private String cityTown;

	@SerializedName("storeLogo")
	private String storeLogo;

	@SerializedName("storeName")
	private String storeName;

	@SerializedName("backgroundPicture")
	private String backgroundPicture;

	@SerializedName("line1")
	private String line1;

	@SerializedName("storeCode")
	private String storeCode;

	@SerializedName("status")
	private int status;

	@SerializedName("longitude")
	private double longitude;

	public void setLastUpdatedBy(String lastUpdatedBy){
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLastUpdatedBy(){
		return lastUpdatedBy;
	}

	public void setStoreType(String storeType){
		this.storeType = storeType;
	}

	public String getStoreType(){
		return storeType;
	}

	public void setDistance(String distance){
		this.distance = distance;
	}

	public String getDistance(){
		return distance;
	}

	public void setStoreTypeName(String storeTypeName){
		this.storeTypeName = storeTypeName;
	}

	public String getStoreTypeName(){
		return storeTypeName;
	}

	public void setIsReserve(int isReserve){
		this.isReserve = isReserve;
	}

	public int getIsReserve(){
		return isReserve;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setMerchantLogo(String merchantLogo){
		this.merchantLogo = merchantLogo;
	}

	public String getMerchantLogo(){
		return merchantLogo;
	}

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setMerchantName(String merchantName){
		this.merchantName = merchantName;
	}

	public String getMerchantName(){
		return merchantName;
	}

	public void setLastUpdatedDate(String lastUpdatedDate){
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedDate(){
		return lastUpdatedDate;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setCityTown(String cityTown){
		this.cityTown = cityTown;
	}

	public String getCityTown(){
		return cityTown;
	}

	public void setStoreLogo(String storeLogo){
		this.storeLogo = storeLogo;
	}

	public String getStoreLogo(){
		return storeLogo;
	}

	public void setStoreName(String storeName){
		this.storeName = storeName;
	}

	public String getStoreName(){
		return storeName;
	}

	public void setBackgroundPicture(String backgroundPicture){
		this.backgroundPicture = backgroundPicture;
	}

	public String getBackgroundPicture(){
		return backgroundPicture;
	}

	public void setLine1(String line1){
		this.line1 = line1;
	}

	public String getLine1(){
		return line1;
	}

	public void setStoreCode(String storeCode){
		this.storeCode = storeCode;
	}

	public String getStoreCode(){
		return storeCode;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"StoreListMsgResp{" + 
			"lastUpdatedBy = '" + lastUpdatedBy + '\'' + 
			",storeType = '" + storeType + '\'' + 
			",distance = '" + distance + '\'' + 
			",storeTypeName = '" + storeTypeName + '\'' + 
			",isReserve = '" + isReserve + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",merchantLogo = '" + merchantLogo + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",merchantName = '" + merchantName + '\'' + 
			",lastUpdatedDate = '" + lastUpdatedDate + '\'' + 
			",createdDate = '" + createdDate + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",pin = '" + pin + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",cityTown = '" + cityTown + '\'' + 
			",storeLogo = '" + storeLogo + '\'' + 
			",storeName = '" + storeName + '\'' + 
			",backgroundPicture = '" + backgroundPicture + '\'' + 
			",line1 = '" + line1 + '\'' + 
			",storeCode = '" + storeCode + '\'' + 
			",status = '" + status + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}