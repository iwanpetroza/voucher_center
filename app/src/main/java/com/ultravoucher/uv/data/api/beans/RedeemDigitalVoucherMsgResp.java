package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 23/04/18.
 */

public class RedeemDigitalVoucherMsgResp {

    @SerializedName("voucherId")
    @Expose
    private String voucherId;

    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;

    @SerializedName("redemptionDate")
    @Expose
    private String redemptionDate;

    @SerializedName("isRedeemed")
    @Expose
    private String isRedeemed;


    public String getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(String redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    public String getIsRedeemed() {
        return isRedeemed;
    }

    public void setIsRedeemed(String isRedeemed) {
        this.isRedeemed = isRedeemed;
    }
}
