package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class MemberWaletDetail{

	@SerializedName("orderVoucherId")
	private String orderVoucherId;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("voucherBalanceId")
	private String voucherBalanceId;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("voucherValue")
	private int voucherValue;

	@SerializedName("expiredDate")
	private String expiredDate;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("status")
	private int status;

	public void setOrderVoucherId(String orderVoucherId){
		this.orderVoucherId = orderVoucherId;
	}

	public String getOrderVoucherId(){
		return orderVoucherId;
	}

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setVoucherBalanceId(String voucherBalanceId){
		this.voucherBalanceId = voucherBalanceId;
	}

	public String getVoucherBalanceId(){
		return voucherBalanceId;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setVoucherValue(int voucherValue){
		this.voucherValue = voucherValue;
	}

	public int getVoucherValue(){
		return voucherValue;
	}

	public void setExpiredDate(String expiredDate){
		this.expiredDate = expiredDate;
	}

	public String getExpiredDate(){
		return expiredDate;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MemberWaletDetail{" + 
			"orderVoucherId = '" + orderVoucherId + '\'' + 
			",voucherClass = '" + voucherClass + '\'' + 
			",voucherBalanceId = '" + voucherBalanceId + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",voucherValue = '" + voucherValue + '\'' + 
			",expiredDate = '" + expiredDate + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}