package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class UpdateProfileMsgResp {

    @SerializedName("motherName")
    @Expose
    private String motherName;

    @SerializedName("birthPlace")
    @Expose
    private String birthPlace;

    @SerializedName("idNumber")
    @Expose
    private String idNumber;

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("birthDate")
    @Expose
    private String birthDate;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("mobilePhone")
    @Expose
    private String mobilePhone;

    @SerializedName("interest")
    @Expose
    private List<MemberInterestMsgResp> interest;

    @SerializedName("memberStatus")
    @Expose
    private String memberStatus;

    @SerializedName("description1")
    @Expose
    private String description1;

    @SerializedName("description2")
    @Expose
    private String description2;

    @SerializedName("description3")
    @Expose
    private String description3;

    @SerializedName("allowNotification")
    @Expose
    private int allowNotification;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public List<MemberInterestMsgResp> getInterest() {
        return interest;
    }

    public void setInterest(List<MemberInterestMsgResp> interest) {
        this.interest = interest;
    }

    public String getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getDescription1() {
        return description1;
    }

    public void setDescription1(String description1) {
        this.description1 = description1;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getDescription3() {
        return description3;
    }

    public void setDescription3(String description3) {
        this.description3 = description3;
    }

    public int getAllowNotification() {
        return allowNotification;
    }

    public void setAllowNotification(int allowNotification) {
        this.allowNotification = allowNotification;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
