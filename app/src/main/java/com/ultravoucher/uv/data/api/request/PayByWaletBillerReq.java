package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class PayByWaletBillerReq {

    @SerializedName("trxId")
    @Expose
    public String trxId;
    @SerializedName("contractNo")
    @Expose
    public String contractNo;
    @SerializedName("memberId")
    @Expose
    public String memberId;
    @SerializedName("otp")
    @Expose
    public String otp;
    @SerializedName("pin")
    @Expose
    public String pin;

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
