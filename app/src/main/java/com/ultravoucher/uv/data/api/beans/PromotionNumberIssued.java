package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class PromotionNumberIssued {


    @SerializedName("basicRulePoint")
    @Expose
    private int basicRulePoint;
    @SerializedName("frequentVisitRulePoint")
    @Expose
    private Object frequentVisitRulePoint;
    @SerializedName("referralRulePoint")
    @Expose
    private Object referralRulePoint;
    @SerializedName("behaviourRulePoint")
    @Expose
    private Object behaviourRulePoint;
    @SerializedName("specialDayRulePoint")
    @Expose
    private Object specialDayRulePoint;
    @SerializedName("stampRulePoint")
    @Expose
    private Object stampRulePoint;
    @SerializedName("mileageRulePoint")
    @Expose
    private Object mileageRulePoint;
    @SerializedName("registerRulePoint")
    @Expose
    private Object registerRulePoint;
    @SerializedName("specialDayDiscount")
    @Expose
    private Object specialDayDiscount;
    @SerializedName("mileageDiscount")
    @Expose
    private Object mileageDiscount;
    @SerializedName("registerDiscount")
    @Expose
    private Object registerDiscount;

    /**
     *
     * @return
     * The basicRulePoint
     */
    public int getBasicRulePoint() {
        return basicRulePoint;
    }

    /**
     *
     * @param basicRulePoint
     * The basicRulePoint
     */
    public void setBasicRulePoint(int basicRulePoint) {
        this.basicRulePoint = basicRulePoint;
    }

    /**
     *
     * @return
     * The frequentVisitRulePoint
     */
    public Object getFrequentVisitRulePoint() {
        return frequentVisitRulePoint;
    }

    /**
     *
     * @param frequentVisitRulePoint
     * The frequentVisitRulePoint
     */
    public void setFrequentVisitRulePoint(Object frequentVisitRulePoint) {
        this.frequentVisitRulePoint = frequentVisitRulePoint;
    }

    /**
     *
     * @return
     * The referralRulePoint
     */
    public Object getReferralRulePoint() {
        return referralRulePoint;
    }

    /**
     *
     * @param referralRulePoint
     * The referralRulePoint
     */
    public void setReferralRulePoint(Object referralRulePoint) {
        this.referralRulePoint = referralRulePoint;
    }

    /**
     *
     * @return
     * The behaviourRulePoint
     */
    public Object getBehaviourRulePoint() {
        return behaviourRulePoint;
    }

    /**
     *
     * @param behaviourRulePoint
     * The behaviourRulePoint
     */
    public void setBehaviourRulePoint(Object behaviourRulePoint) {
        this.behaviourRulePoint = behaviourRulePoint;
    }

    /**
     *
     * @return
     * The specialDayRulePoint
     */
    public Object getSpecialDayRulePoint() {
        return specialDayRulePoint;
    }

    /**
     *
     * @param specialDayRulePoint
     * The specialDayRulePoint
     */
    public void setSpecialDayRulePoint(Object specialDayRulePoint) {
        this.specialDayRulePoint = specialDayRulePoint;
    }

    /**
     *
     * @return
     * The stampRulePoint
     */
    public Object getStampRulePoint() {
        return stampRulePoint;
    }

    /**
     *
     * @param stampRulePoint
     * The stampRulePoint
     */
    public void setStampRulePoint(Object stampRulePoint) {
        this.stampRulePoint = stampRulePoint;
    }

    /**
     *
     * @return
     * The mileageRulePoint
     */
    public Object getMileageRulePoint() {
        return mileageRulePoint;
    }

    /**
     *
     * @param mileageRulePoint
     * The mileageRulePoint
     */
    public void setMileageRulePoint(Object mileageRulePoint) {
        this.mileageRulePoint = mileageRulePoint;
    }

    /**
     *
     * @return
     * The registerRulePoint
     */
    public Object getRegisterRulePoint() {
        return registerRulePoint;
    }

    /**
     *
     * @param registerRulePoint
     * The registerRulePoint
     */
    public void setRegisterRulePoint(Object registerRulePoint) {
        this.registerRulePoint = registerRulePoint;
    }

    /**
     *
     * @return
     * The specialDayDiscount
     */
    public Object getSpecialDayDiscount() {
        return specialDayDiscount;
    }

    /**
     *
     * @param specialDayDiscount
     * The specialDayDiscount
     */
    public void setSpecialDayDiscount(Object specialDayDiscount) {
        this.specialDayDiscount = specialDayDiscount;
    }

    /**
     *
     * @return
     * The mileageDiscount
     */
    public Object getMileageDiscount() {
        return mileageDiscount;
    }

    /**
     *
     * @param mileageDiscount
     * The mileageDiscount
     */
    public void setMileageDiscount(Object mileageDiscount) {
        this.mileageDiscount = mileageDiscount;
    }

    /**
     *
     * @return
     * The registerDiscount
     */
    public Object getRegisterDiscount() {
        return registerDiscount;
    }

    /**
     *
     * @param registerDiscount
     * The registerDiscount
     */
    public void setRegisterDiscount(Object registerDiscount) {
        this.registerDiscount = registerDiscount;
    }
}
