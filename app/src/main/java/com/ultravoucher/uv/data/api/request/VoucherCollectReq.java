package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/22/2018.
 */

public class VoucherCollectReq {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("page")
    @Expose
    private int page;

    @SerializedName("nRecords")
    @Expose
    private int nRecords;

    @SerializedName("keyword")
    @Expose
    private String keyword;

    @SerializedName("order")
    @Expose
    private String order;

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     * The page
     */
    public int getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(int page) {
        this.page = page;
    }


    /**
     *
     * @return
     * The keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     *
     * @param keyword
     * The keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getnRecords() {
        return nRecords;
    }

    public void setnRecords(int nRecords) {
        this.nRecords = nRecords;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}

