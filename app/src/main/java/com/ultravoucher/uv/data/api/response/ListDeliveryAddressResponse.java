package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.ListDeliveryAddressMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/2/2017.
 */

public class ListDeliveryAddressResponse {

    @SerializedName("deliveryAddress")
    @Expose
    private List<ListDeliveryAddressMsgResp> deliveryAddress = new ArrayList<>();

    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    public List<ListDeliveryAddressMsgResp> getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(List<ListDeliveryAddressMsgResp> deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
