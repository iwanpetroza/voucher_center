package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/12/2017.
 */

public class SharingBalance implements Serializable{

    @SerializedName("voucherValue")
    @Expose
    private int voucherValue;

    @SerializedName("details")
    @Expose
    private List<SharingBalanceDetail> details = new ArrayList<SharingBalanceDetail>();


    public int getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(int voucherValue) {
        this.voucherValue = voucherValue;
    }

    public List<SharingBalanceDetail> getDetails() {
        return details;
    }

    public void setDetails(List<SharingBalanceDetail> details) {
        this.details = details;
    }
}
