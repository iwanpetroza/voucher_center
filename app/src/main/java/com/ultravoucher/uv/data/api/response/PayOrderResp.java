package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class PayOrderResp extends GenericResponse {

    @SerializedName("totalTrxAmount")
    @Expose
    private int totalTrxAmount;

    @SerializedName("orders")
    @Expose
    private List<Order> orders = new ArrayList<Order>();

    public int getTotalTrxAmount() {
        return totalTrxAmount;
    }

    public void setTotalTrxAmount(int totalTrxAmount) {
        this.totalTrxAmount = totalTrxAmount;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
