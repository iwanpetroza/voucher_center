package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.BillingResponse;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class BillingUpdateResp extends GenericResponse{
    @SerializedName("billingResponse")
    @Expose
    private BillingResponse billingResponse;

    /**
     *
     * @return
     * The billingResponse
     */
    public BillingResponse getBillingResponse() {
        return billingResponse;
    }

    /**
     *
     * @param billingResponse
     * The billingResponse
     */
    public void setBillingResponse(BillingResponse billingResponse) {
        this.billingResponse = billingResponse;
    }
}
