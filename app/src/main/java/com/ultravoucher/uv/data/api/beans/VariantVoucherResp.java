package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class VariantVoucherResp {

    @SerializedName("variantId")
    @Expose
    private String variantId;

    @SerializedName("variantName")
    @Expose
    private String variantName;

    @SerializedName("basePrice")
    @Expose
    private int basePrice;

    @SerializedName("sellingPrice")
    @Expose
    private int sellingPrice;

    @SerializedName("isChargeTaxes")
    @Expose
    private int isChargeTaxes;

    @SerializedName("sku")
    @Expose
    private String sku;

    @SerializedName("discountId")
    @Expose
    private int discountId;

    @SerializedName("discountType")
    @Expose
    private int discountType;

    @SerializedName("discountName")
    @Expose
    private String discountName;

    @SerializedName("discountValue")
    @Expose
    private int discountValue;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("variantImageMain")
    @Expose
    private String variantImageMain;

    @SerializedName("variantImageDetail1")
    @Expose
    private String variantImageDetail1;

    @SerializedName("variantImageDetail2")
    @Expose
    private String variantImageDetail2;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("vouchers")
    @Expose
    private VoucherMsgResp vouchers;

    public String getVariantId() {
        return variantId;
    }

    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    public String getVariantName() {
        return variantName;
    }

    public void setVariantName(String variantName) {
        this.variantName = variantName;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getIsChargeTaxes() {
        return isChargeTaxes;
    }

    public void setIsChargeTaxes(int isChargeTaxes) {
        this.isChargeTaxes = isChargeTaxes;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public int getDiscountId() {
        return discountId;
    }

    public void setDiscountId(int discountId) {
        this.discountId = discountId;
    }

    public int getDiscountType() {
        return discountType;
    }

    public void setDiscountType(int discountType) {
        this.discountType = discountType;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getVariantImageMain() {
        return variantImageMain;
    }

    public void setVariantImageMain(String variantImageMain) {
        this.variantImageMain = variantImageMain;
    }

    public String getVariantImageDetail1() {
        return variantImageDetail1;
    }

    public void setVariantImageDetail1(String variantImageDetail1) {
        this.variantImageDetail1 = variantImageDetail1;
    }

    public String getVariantImageDetail2() {
        return variantImageDetail2;
    }

    public void setVariantImageDetail2(String variantImageDetail2) {
        this.variantImageDetail2 = variantImageDetail2;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public VoucherMsgResp getVouchers() {
        return vouchers;
    }

    public void setVouchers(VoucherMsgResp vouchers) {
        this.vouchers = vouchers;
    }
}
