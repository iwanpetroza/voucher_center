package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PayByVoucherMsgResp{

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("city")
	private String city;

	@SerializedName("postalCode")
	private String postalCode;

	@SerializedName("paymentReferenceCode")
	private String paymentReferenceCode;

	@SerializedName("productName")
	private String productName;

	@SerializedName("vaNumber")
	private String vaNumber;

	@SerializedName("discountName")
	private String discountName;

	@SerializedName("sellingPrice")
	private int sellingPrice;

	@SerializedName("totalPayment")
	private int totalPayment;

	@SerializedName("productImage")
	private String productImage;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("deliveryAddress")
	private String deliveryAddress;

	@SerializedName("variantStock")
	private int variantStock;

	@SerializedName("discountType")
	private String discountType;

	@SerializedName("shippingAmount")
	private int shippingAmount;

	@SerializedName("variantId")
	private String variantId;

	@SerializedName("paymentStatus")
	private int paymentStatus;

	@SerializedName("email")
	private String email;

	@SerializedName("productType")
	private String productType;

	@SerializedName("shippingBy")
	private String shippingBy;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("basePrice")
	private int basePrice;

	@SerializedName("unitPrice")
	private int unitPrice;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("productId")
	private String productId;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("transactionRefNumber")
	private String transactionRefNumber;

	@SerializedName("storeId")
	private String storeId;

	@SerializedName("vouchers")
	private String vouchers;

	@SerializedName("paymentChannel")
	private String paymentChannel;

	@SerializedName("totalAmount")
	private int totalAmount;

	@SerializedName("checkoutDate")
	private String checkoutDate;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("pickupNote")
	private String pickupNote;

	@SerializedName("pickupDate")
	private String pickupDate;

	@SerializedName("paymentDate")
	private String paymentDate;

	@SerializedName("taxAmount")
	private int taxAmount;

	@SerializedName("variantName")
	private String variantName;

	@SerializedName("orderDate")
	private String orderDate;

	@SerializedName("discountValue")
	private int discountValue;

	@SerializedName("status")
	private int status;

	@SerializedName("variants")
	private List<Variant> variants = new ArrayList<Variant>();


	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setPostalCode(String postalCode){
		this.postalCode = postalCode;
	}

	public String getPostalCode(){
		return postalCode;
	}

	public void setPaymentReferenceCode(String paymentReferenceCode){
		this.paymentReferenceCode = paymentReferenceCode;
	}

	public String getPaymentReferenceCode(){
		return paymentReferenceCode;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setVaNumber(String vaNumber){
		this.vaNumber = vaNumber;
	}

	public String getVaNumber(){
		return vaNumber;
	}

	public void setDiscountName(String discountName){
		this.discountName = discountName;
	}

	public String getDiscountName(){
		return discountName;
	}

	public void setSellingPrice(int sellingPrice){
		this.sellingPrice = sellingPrice;
	}

	public int getSellingPrice(){
		return sellingPrice;
	}

	public void setTotalPayment(int totalPayment){
		this.totalPayment = totalPayment;
	}

	public int getTotalPayment(){
		return totalPayment;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setDeliveryAddress(String deliveryAddress){
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryAddress(){
		return deliveryAddress;
	}

	public void setVariantStock(int variantStock){
		this.variantStock = variantStock;
	}

	public int getVariantStock(){
		return variantStock;
	}

	public void setDiscountType(String discountType){
		this.discountType = discountType;
	}

	public String getDiscountType(){
		return discountType;
	}

	public void setShippingAmount(int shippingAmount){
		this.shippingAmount = shippingAmount;
	}

	public int getShippingAmount(){
		return shippingAmount;
	}

	public void setVariantId(String variantId){
		this.variantId = variantId;
	}

	public String getVariantId(){
		return variantId;
	}

	public void setPaymentStatus(int paymentStatus){
		this.paymentStatus = paymentStatus;
	}

	public int getPaymentStatus(){
		return paymentStatus;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setProductType(String productType){
		this.productType = productType;
	}

	public String getProductType(){
		return productType;
	}

	public void setShippingBy(String shippingBy){
		this.shippingBy = shippingBy;
	}

	public String getShippingBy(){
		return shippingBy;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setBasePrice(int basePrice){
		this.basePrice = basePrice;
	}

	public int getBasePrice(){
		return basePrice;
	}

	public void setUnitPrice(int unitPrice){
		this.unitPrice = unitPrice;
	}

	public int getUnitPrice(){
		return unitPrice;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setTransactionRefNumber(String transactionRefNumber){
		this.transactionRefNumber = transactionRefNumber;
	}

	public String getTransactionRefNumber(){
		return transactionRefNumber;
	}

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setVouchers(String vouchers){
		this.vouchers = vouchers;
	}

	public String getVouchers(){
		return vouchers;
	}

	public void setPaymentChannel(String paymentChannel){
		this.paymentChannel = paymentChannel;
	}

	public String getPaymentChannel(){
		return paymentChannel;
	}

	public void setTotalAmount(int totalAmount){
		this.totalAmount = totalAmount;
	}

	public int getTotalAmount(){
		return totalAmount;
	}

	public void setCheckoutDate(String checkoutDate){
		this.checkoutDate = checkoutDate;
	}

	public String getCheckoutDate(){
		return checkoutDate;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setPickupNote(String pickupNote){
		this.pickupNote = pickupNote;
	}

	public String getPickupNote(){
		return pickupNote;
	}

	public void setPickupDate(String pickupDate){
		this.pickupDate = pickupDate;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}

	public String getPaymentDate(){
		return paymentDate;
	}

	public void setTaxAmount(int taxAmount){
		this.taxAmount = taxAmount;
	}

	public int getTaxAmount(){
		return taxAmount;
	}

	public void setVariantName(String variantName){
		this.variantName = variantName;
	}

	public String getVariantName(){
		return variantName;
	}

	public void setOrderDate(String orderDate){
		this.orderDate = orderDate;
	}

	public String getOrderDate(){
		return orderDate;
	}

	public void setDiscountValue(int discountValue){
		this.discountValue = discountValue;
	}

	public int getDiscountValue(){
		return discountValue;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public List<Variant> getVariants() {
		return variants;
	}

	public void setVariants(List<Variant> variants) {
		this.variants = variants;
	}

	@Override
 	public String toString(){
		return 
			"PayByVoucherMsgResp{" + 
			"orderNumber = '" + orderNumber + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",city = '" + city + '\'' + 
			",postalCode = '" + postalCode + '\'' + 
			",paymentReferenceCode = '" + paymentReferenceCode + '\'' + 
			",productName = '" + productName + '\'' + 
			",vaNumber = '" + vaNumber + '\'' + 
			",discountName = '" + discountName + '\'' + 
			",sellingPrice = '" + sellingPrice + '\'' + 
			",totalPayment = '" + totalPayment + '\'' + 
			",productImage = '" + productImage + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",deliveryAddress = '" + deliveryAddress + '\'' + 
			",variantStock = '" + variantStock + '\'' + 
			",discountType = '" + discountType + '\'' + 
			",shippingAmount = '" + shippingAmount + '\'' + 
			",variantId = '" + variantId + '\'' + 
			",paymentStatus = '" + paymentStatus + '\'' + 
			",email = '" + email + '\'' + 
			",productType = '" + productType + '\'' + 
			",shippingBy = '" + shippingBy + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",basePrice = '" + basePrice + '\'' + 
			",unitPrice = '" + unitPrice + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",productId = '" + productId + '\'' + 
			",voucherClass = '" + voucherClass + '\'' + 
			",transactionRefNumber = '" + transactionRefNumber + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",vouchers = '" + vouchers + '\'' + 
			",paymentChannel = '" + paymentChannel + '\'' + 
			",totalAmount = '" + totalAmount + '\'' + 
			",checkoutDate = '" + checkoutDate + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",pickupNote = '" + pickupNote + '\'' + 
			",pickupDate = '" + pickupDate + '\'' + 
			",paymentDate = '" + paymentDate + '\'' + 
			",taxAmount = '" + taxAmount + '\'' + 
			",variantName = '" + variantName + '\'' + 
			",orderDate = '" + orderDate + '\'' + 
			",discountValue = '" + discountValue + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}