package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.ListPaymentMethodMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/30/2017.
 */

public class ListPaymentMethodResponse extends GenericResponse{

    @SerializedName("availablePaymentMethod")
    @Expose
    private List<ListPaymentMethodMsgResp> availablePaymentMethod = new ArrayList<ListPaymentMethodMsgResp>();

    public List<ListPaymentMethodMsgResp> getAvailablePaymentMethod() {
        return availablePaymentMethod;
    }

    public void setAvailablePaymentMethod(List<ListPaymentMethodMsgResp> availablePaymentMethod) {
        this.availablePaymentMethod = availablePaymentMethod;
    }
}
