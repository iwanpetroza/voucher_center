package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;
import com.ultravoucher.uv.data.api.beans.History;
import com.ultravoucher.uv.data.api.beans.RecordInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/7/2017.
 */

public class HistoryProgressResponse {

    @SerializedName("deals")
    @Expose
    private List<History> deals = new ArrayList<History>();
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;
    @SerializedName("recordInfo")
    @Expose
    private RecordInfo recordInfo;

    public List<History> getDeals() {
        return deals;
    }

    public void setDeals(List<History> deals) {
        this.deals = deals;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }
}
