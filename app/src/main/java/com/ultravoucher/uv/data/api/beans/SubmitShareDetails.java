package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class SubmitShareDetails {

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("voucherCode")
	private String voucherCode;

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	@Override
 	public String toString(){
		return 
			"SubmitShareDetails{" +
			"voucherId = '" + voucherId + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			"}";
		}
}