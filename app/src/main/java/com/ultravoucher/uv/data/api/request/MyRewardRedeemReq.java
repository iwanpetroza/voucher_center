package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class MyRewardRedeemReq{

	@SerializedName("redemptionDate")
	private String redemptionDate;

	@SerializedName("actor")
	private String actor;

	@SerializedName("pin")
	private String pin;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("voucherId")
	private String voucherId;

	@SerializedName("storeId")
	private String storeId;

	@SerializedName("voucherCode")
	private String voucherCode;

	@SerializedName("memberId")
	private String memberId;

	public void setRedemptionDate(String redemptionDate){
		this.redemptionDate = redemptionDate;
	}

	public String getRedemptionDate(){
		return redemptionDate;
	}

	public void setActor(String actor){
		this.actor = actor;
	}

	public String getActor(){
		return actor;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setVoucherId(String voucherId){
		this.voucherId = voucherId;
	}

	public String getVoucherId(){
		return voucherId;
	}

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setVoucherCode(String voucherCode){
		this.voucherCode = voucherCode;
	}

	public String getVoucherCode(){
		return voucherCode;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	@Override
 	public String toString(){
		return 
			"MyRewardRedeemReq{" + 
			"redemptionDate = '" + redemptionDate + '\'' + 
			",actor = '" + actor + '\'' + 
			",pin = '" + pin + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",voucherId = '" + voucherId + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",voucherCode = '" + voucherCode + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}