package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerHistoryReq {

	@SerializedName("memberId")
	private String memberId;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}