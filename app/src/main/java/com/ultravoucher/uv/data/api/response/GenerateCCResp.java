package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;

/**
 * Created by firwandi.ramli on 1/8/2018.
 */

public class GenerateCCResp {

    @SerializedName("abstractResponse")
    private AbstractResponse abstractResponse;

    @SerializedName("url")
    private String url;

    public void setAbstractResponse(AbstractResponse abstractResponse){
        this.abstractResponse = abstractResponse;
    }

    public AbstractResponse getAbstractResponse(){
        return abstractResponse;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    @Override
    public String toString(){
        return
                "GenerateCCResp{" +
                        "abstractResponse = '" + abstractResponse + '\'' +
                        ",url = '" + url + '\'' +
                        "}";
    }
}
