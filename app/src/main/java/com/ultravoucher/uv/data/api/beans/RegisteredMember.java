package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class RegisteredMember{

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("placeOfBirth")
	private String placeOfBirth;

	@SerializedName("gender")
	private String gender;

	@SerializedName("backgroundImage")
	private String backgroundImage;

	@SerializedName("mobileNumber")
	private String mobileNumber;

	@SerializedName("memberUsername")
	private String memberUsername;

	@SerializedName("middleName")
	private String middleName;

	@SerializedName("birthDate")
	private String birthDate;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("status")
	private int status;

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setPlaceOfBirth(String placeOfBirth){
		this.placeOfBirth = placeOfBirth;
	}

	public String getPlaceOfBirth(){
		return placeOfBirth;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setBackgroundImage(String backgroundImage){
		this.backgroundImage = backgroundImage;
	}

	public String getBackgroundImage(){
		return backgroundImage;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setMemberUsername(String memberUsername){
		this.memberUsername = memberUsername;
	}

	public String getMemberUsername(){
		return memberUsername;
	}

	public void setMiddleName(String middleName){
		this.middleName = middleName;
	}

	public String getMiddleName(){
		return middleName;
	}

	public void setBirthDate(String birthDate){
		this.birthDate = birthDate;
	}

	public String getBirthDate(){
		return birthDate;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"RegisteredMember{" + 
			"firstName = '" + firstName + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",placeOfBirth = '" + placeOfBirth + '\'' + 
			",gender = '" + gender + '\'' + 
			",backgroundImage = '" + backgroundImage + '\'' + 
			",mobileNumber = '" + mobileNumber + '\'' + 
			",memberUsername = '" + memberUsername + '\'' + 
			",middleName = '" + middleName + '\'' + 
			",birthDate = '" + birthDate + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}