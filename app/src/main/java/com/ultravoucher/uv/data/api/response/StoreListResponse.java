package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/19/2017.
 */

public class StoreListResponse extends GenericResponse {

    @SerializedName("storeResponse")
    @Expose
    private List<StoreListMsgResp> storeResponse = new ArrayList<StoreListMsgResp>();

    public List<StoreListMsgResp> getStoreResponse() {
        return storeResponse;
    }

    public void setStoreResponse(List<StoreListMsgResp> storeResponse) {
        this.storeResponse = storeResponse;
    }
}