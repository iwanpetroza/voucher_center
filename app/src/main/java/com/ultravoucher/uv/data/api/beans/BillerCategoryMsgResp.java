package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class BillerCategoryMsgResp{

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("billerCategoryId")
	private String billerCategoryId;

	@SerializedName("priority")
	private int priority;

	@SerializedName("status")
	private int status;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setBillerCategoryId(String billerCategoryId){
		this.billerCategoryId = billerCategoryId;
	}

	public String getBillerCategoryId(){
		return billerCategoryId;
	}

	public void setPriority(int priority){
		this.priority = priority;
	}

	public int getPriority(){
		return priority;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BillerCategoryMsgResp{" + 
			"image = '" + image + '\'' + 
			",name = '" + name + '\'' + 
			",billerCategoryId = '" + billerCategoryId + '\'' + 
			",priority = '" + priority + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}