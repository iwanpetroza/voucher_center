package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.MobileVersion;

/**
 * Created by Firwandi S Ramli on 11/05/18.
 */

public class CheckVersionResponse extends GenericResponse{

    @SerializedName("mobileVersion")
    @Expose
    private MobileVersion mobileVersion;

    public MobileVersion getMobileVersion() {
        return mobileVersion;
    }

    public void setMobileVersion(MobileVersion mobileVersion) {
        this.mobileVersion = mobileVersion;
    }
}
