package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class OrderRequest {

    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("variantId")
    @Expose
    private String variantId;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("voucherId")
    @Expose
    private String voucherId;

    @SerializedName("deliveryMethod")
    @Expose
    private int deliveryMethod;

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The productId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     *
     * @return
     * The variantId
     */
    public String getVariantId() {
        return variantId;
    }

    /**
     *
     * @param variantId
     * The variantId
     */
    public void setVariantId(String variantId) {
        this.variantId = variantId;
    }

    /**
     *
     * @return
     * The quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     *
     * @param quantity
     * The quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The voucherId
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     *
     * @param voucherId
     * The voucherId
     */
    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    public int getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(int deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }
}