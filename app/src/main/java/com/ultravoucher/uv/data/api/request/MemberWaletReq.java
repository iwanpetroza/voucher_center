package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class MemberWaletReq{

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("page")
	private int page;

	@SerializedName("memberId")
	private String memberId;

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	@Override
 	public String toString(){
		return 
			"MemberWaletReq{" + 
			"voucherClass = '" + voucherClass + '\'' + 
			",nRecords = '" + nRecords + '\'' + 
			",page = '" + page + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}