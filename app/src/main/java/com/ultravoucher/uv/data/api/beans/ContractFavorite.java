package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class ContractFavorite {

    @SerializedName("contractName")
    @Expose
    public String contractName;
    @SerializedName("contractNo")
    @Expose
    public String contractNo;

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }
}
