package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.Pockets;

/**
 * Created by firwandi.ramli on 1/22/2018.
 */

public class MemberPocketResponse extends GenericResponse{
    @SerializedName("pockets")
    @Expose
    private Pockets pockets;

    /**
     *
     * @return
     * The pockets
     */
    public Pockets getPockets() {
        return pockets;
    }

    /**
     *
     * @param pockets
     * The pockets
     */
    public void setPockets(Pockets pockets) {
        this.pockets = pockets;
    }
}
