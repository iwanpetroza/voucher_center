package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.MemberPointBalance;

import java.util.ArrayList;
import java.util.List;

public class MemberPointBalanceResponse extends GenericResponse{

	@SerializedName("totalPoints")
	private int totalPoints;

	@SerializedName("memberName")
	private String memberName;

	@SerializedName("memberBalanceResponse")
	@Expose
	private List<MemberPointBalance> memberBalanceResponse = new ArrayList<MemberPointBalance>();

	public void setTotalPoints(int totalPoints){
		this.totalPoints = totalPoints;
	}

	public int getTotalPoints(){
		return totalPoints;
	}

	public void setMemberName(String memberName){
		this.memberName = memberName;
	}

	public String getMemberName(){
		return memberName;
	}

	public List<MemberPointBalance> getMemberBalanceResponse() {
		return memberBalanceResponse;
	}

	public void setMemberBalanceResponse(List<MemberPointBalance> memberBalanceResponse) {
		this.memberBalanceResponse = memberBalanceResponse;
	}

	@Override
 	public String toString(){
		return 
			"MemberPointBalanceResponse{" + 
			"totalPoints = '" + totalPoints + '\'' + 
			",memberName = '" + memberName + '\'' + 
			"}";
		}
}