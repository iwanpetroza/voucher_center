package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class PayByVoucherAmountReq{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("voucherValue")
	private int voucherValue;

	@SerializedName("voucherOrigin")
	private int voucherOrigin;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setVoucherValue(int voucherValue){
		this.voucherValue = voucherValue;
	}

	public int getVoucherValue(){
		return voucherValue;
	}

	public void setVoucherOrigin(int voucherOrigin){
		this.voucherOrigin = voucherOrigin;
	}

	public int getVoucherOrigin(){
		return voucherOrigin;
	}

	@Override
 	public String toString(){
		return 
			"PayByVoucherAmountReq{" + 
			"quantity = '" + quantity + '\'' + 
			",voucherValue = '" + voucherValue + '\'' + 
			",voucherOrigin = '" + voucherOrigin + '\'' + 
			"}";
		}
}