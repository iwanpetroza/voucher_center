package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class UnRegisteredMember{

	@SerializedName("mobileNumber")
	private String mobileNumber;

	@SerializedName("username")
	private String username;

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"UnRegisteredMember{" + 
			"mobileNumber = '" + mobileNumber + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}