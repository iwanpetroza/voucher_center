package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.PromotionTransactionDetail;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class IssuingPointReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("promotionCode")
    @Expose
    private String promotionCode;
    @SerializedName("posTransactionId")
    @Expose
    private String posTransactionId;
    @SerializedName("promotionTransactionDetail")
    @Expose
    private PromotionTransactionDetail promotionTransactionDetail;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The promotionCode
     */
    public String getPromotionCode() {
        return promotionCode;
    }

    /**
     *
     * @param promotionCode
     * The promotionCode
     */
    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    /**
     *
     * @return
     * The posTransactionId
     */
    public String getPosTransactionId() {
        return posTransactionId;
    }

    /**
     *
     * @param posTransactionId
     * The posTransactionId
     */
    public void setPosTransactionId(String posTransactionId) {
        this.posTransactionId = posTransactionId;
    }

    /**
     *
     * @return
     * The promotionTransactionDetail
     */
    public PromotionTransactionDetail getPromotionTransactionDetail() {
        return promotionTransactionDetail;
    }

    /**
     *
     * @param promotionTransactionDetail
     * The promotionTransactionDetail
     */
    public void setPromotionTransactionDetail(PromotionTransactionDetail promotionTransactionDetail) {
        this.promotionTransactionDetail = promotionTransactionDetail;
    }

}
