package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/15/2018.
 */

public class OrderChangeAddress {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("deliveryAddressId")
    @Expose
    private String deliveryAddressId;
    @SerializedName("shippingBy")
    @Expose
    private String shippingBy;
    @SerializedName("shippingAmount")
    @Expose
    private int shippingAmount;
    @SerializedName("pickupDate")
    @Expose
    private String pickupDate;
    @SerializedName("pickupNote")
    @Expose
    private String pickupNote;
    @SerializedName("status")
    @Expose
    private int status;

    /**
     *
     * @return
     * The orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     *
     * @param orderId
     * The orderId
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     *
     * @return
     * The deliveryAddressId
     */
    public String getDeliveryAddressId() {
        return deliveryAddressId;
    }

    /**
     *
     * @param deliveryAddressId
     * The deliveryAddressId
     */
    public void setDeliveryAddressId(String deliveryAddressId) {
        this.deliveryAddressId = deliveryAddressId;
    }

    /**
     *
     * @return
     * The shippingBy
     */
    public String getShippingBy() {
        return shippingBy;
    }

    /**
     *
     * @param shippingBy
     * The shippingBy
     */
    public void setShippingBy(String shippingBy) {
        this.shippingBy = shippingBy;
    }

    /**
     *
     * @return
     * The shippingAmount
     */
    public int getShippingAmount() {
        return shippingAmount;
    }

    /**
     *
     * @param shippingAmount
     * The shippingAmount
     */
    public void setShippingAmount(int shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     *
     * @return
     * The pickupDate
     */
    public String getPickupDate() {
        return pickupDate;
    }

    /**
     *
     * @param pickupDate
     * The pickupDate
     */
    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    /**
     *
     * @return
     * The pickupNote
     */
    public String getPickupNote() {
        return pickupNote;
    }

    /**
     *
     * @param pickupNote
     * The pickupNote
     */
    public void setPickupNote(String pickupNote) {
        this.pickupNote = pickupNote;
    }

    /**
     *
     * @return
     * The status
     */
    public int getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(int status) {
        this.status = status;
    }
}
