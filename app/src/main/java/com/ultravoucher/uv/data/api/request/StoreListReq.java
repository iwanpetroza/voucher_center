package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class StoreListReq{

	@SerializedName("storeType")
	private String storeType;

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("page")
	private int page;

	@SerializedName("cityId")
	private String cityId;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("nearBy")
	private String nearBy;

	@SerializedName("order")
	private String order;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("sVoucherClass")
	private String voucherClass;

	public void setStoreType(String storeType){
		this.storeType = storeType;
	}

	public String getStoreType(){
		return storeType;
	}

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setCityId(String cityId){
		this.cityId = cityId;
	}

	public String getCityId(){
		return cityId;
	}

	public void setKeyword(String keyword){
		this.keyword = keyword;
	}

	public String getKeyword(){
		return keyword;
	}

	public void setNearBy(String nearBy){
		this.nearBy = nearBy;
	}

	public String getNearBy(){
		return nearBy;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public String getVoucherClass() {
		return voucherClass;
	}

	public void setVoucherClass(String voucherClass) {
		this.voucherClass = voucherClass;
	}

	@Override
 	public String toString(){
		return 
			"StoreListReq{" + 
			"storeType = '" + storeType + '\'' + 
			",nRecords = '" + nRecords + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",page = '" + page + '\'' + 
			",cityId = '" + cityId + '\'' + 
			",keyword = '" + keyword + '\'' + 
			",nearBy = '" + nearBy + '\'' + 
			",order = '" + order + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}