package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class InqPayBillerResponse {

    @SerializedName("vaNumber")
    @Expose
    public String vaNumber;
    @SerializedName("transactionId")
    @Expose
    public String transactionId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("contractNo")
    @Expose
    public String contractNo;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("fee")
    @Expose
    public String fee;
    @SerializedName("totalAmount")
    @Expose
    public String totalAmount;
    @SerializedName("abstractResponse")
    @Expose
    private AbstractResponse abstractResponse;

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
