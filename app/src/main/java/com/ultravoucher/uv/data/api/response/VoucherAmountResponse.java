package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.VoucherAmountMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firwandi S Ramli on 13/08/18.
 */

public class VoucherAmountResponse extends GenericResponse{

    @SerializedName("vouchers")
    @Expose
    private List<VoucherAmountMsgResp> vouchers = new ArrayList<VoucherAmountMsgResp>();

    public List<VoucherAmountMsgResp> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<VoucherAmountMsgResp> vouchers) {
        this.vouchers = vouchers;
    }
}
