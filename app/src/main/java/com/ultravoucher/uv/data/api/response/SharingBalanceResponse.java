package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.SharingBalance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 12/12/2017.
 */

public class SharingBalanceResponse extends GenericResponse {

    @SerializedName("sharingBalance")
    @Expose
    private List<SharingBalance> sharingBalance = new ArrayList<SharingBalance>();

    public List<SharingBalance> getSharingBalance() {
        return sharingBalance;
    }

    public void setSharingBalance(List<SharingBalance> sharingBalance) {
        this.sharingBalance = sharingBalance;
    }
}
