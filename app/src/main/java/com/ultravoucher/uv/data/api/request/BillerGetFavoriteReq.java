package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerGetFavoriteReq {

    @SerializedName("memberId")
    @Expose
    public String memberId;
    @SerializedName("category")
    @Expose
    public Integer category;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}