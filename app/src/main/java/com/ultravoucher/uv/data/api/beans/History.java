package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class History{

	@SerializedName("paymentStatus")
	private int paymentStatus;

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("productId")
	private String productId;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("storeId")
	private String storeId;

	@SerializedName("productName")
	private String productName;

	@SerializedName("sellingPrice")
	private int sellingPrice;

	@SerializedName("productImage")
	private String productImage;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("paymentDate")
	private String paymentDate;

	@SerializedName("productType")
	private String productType;

	@SerializedName("productDescription")
	private String productDescription;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("status")
	private int status;

	@SerializedName("transactionRefNumber")
	private String transactionRefNumber;

	@SerializedName("orderVouchers")
	private List<OrderVouchers> orderVouchers = new ArrayList<OrderVouchers>();

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setProductId(String productId){
		this.productId = productId;
	}

	public String getProductId(){
		return productId;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setStoreId(String storeId){
		this.storeId = storeId;
	}

	public String getStoreId(){
		return storeId;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setSellingPrice(int sellingPrice){
		this.sellingPrice = sellingPrice;
	}

	public int getSellingPrice(){
		return sellingPrice;
	}

	public void setProductImage(String productImage){
		this.productImage = productImage;
	}

	public String getProductImage(){
		return productImage;
	}

	public void setMerchantId(String merchantId){
		this.merchantId = merchantId;
	}

	public String getMerchantId(){
		return merchantId;
	}

	public void setPaymentDate(String paymentDate){
		this.paymentDate = paymentDate;
	}

	public String getPaymentDate(){
		return paymentDate;
	}

	public void setProductType(String productType){
		this.productType = productType;
	}

	public String getProductType(){
		return productType;
	}

	public void setProductDescription(String productDescription){
		this.productDescription = productDescription;
	}

	public String getProductDescription(){
		return productDescription;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public List<OrderVouchers> getOrderVouchers() {
		return orderVouchers;
	}

	public void setOrderVouchers(List<OrderVouchers> orderVouchers) {
		this.orderVouchers = orderVouchers;
	}

	public int getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTransactionRefNumber() {
		return transactionRefNumber;
	}

	public void setTransactionRefNumber(String transactionRefNumber) {
		this.transactionRefNumber = transactionRefNumber;
	}

	@Override
 	public String toString(){
		return 
			"History{" + 
			"orderNumber = '" + orderNumber + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",productId = '" + productId + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",productName = '" + productName + '\'' + 
			",sellingPrice = '" + sellingPrice + '\'' + 
			",productImage = '" + productImage + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",paymentDate = '" + paymentDate + '\'' + 
			",productType = '" + productType + '\'' + 
			",productDescription = '" + productDescription + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}