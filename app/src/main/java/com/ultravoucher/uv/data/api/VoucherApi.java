package com.ultravoucher.uv.data.api;

import com.ultravoucher.uv.data.api.request.AddToCartReq;
import com.ultravoucher.uv.data.api.request.AdvertisingReq;
import com.ultravoucher.uv.data.api.request.AutoRegistrationReq;
import com.ultravoucher.uv.data.api.request.BillerCategoryReq;
import com.ultravoucher.uv.data.api.request.BillerGetFavoriteReq;
import com.ultravoucher.uv.data.api.request.BillerHistoryReq;
import com.ultravoucher.uv.data.api.request.HistoryBillerDetailReq;
import com.ultravoucher.uv.data.api.request.ChangeAddressReq;
import com.ultravoucher.uv.data.api.request.ChangeForgotPassReq;
import com.ultravoucher.uv.data.api.request.ChangePasswordReq;
import com.ultravoucher.uv.data.api.request.CheckMemberExistReq;
import com.ultravoucher.uv.data.api.request.CheckMemberPointsReq;
import com.ultravoucher.uv.data.api.request.CreateAddressReq;
import com.ultravoucher.uv.data.api.request.DetailProductByIdReq;
import com.ultravoucher.uv.data.api.request.DigitalPocketReq;
import com.ultravoucher.uv.data.api.request.DiscardOrderReq;
import com.ultravoucher.uv.data.api.request.HistoryProductReq;
import com.ultravoucher.uv.data.api.request.HistoryProgressReq;
import com.ultravoucher.uv.data.api.request.HistoryRewardReq;
import com.ultravoucher.uv.data.api.request.HistoryVoucherDetailReq;
import com.ultravoucher.uv.data.api.request.HistoryWaletReq;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.request.InqPayOrderReq;
import com.ultravoucher.uv.data.api.request.InquiryShareReq;
import com.ultravoucher.uv.data.api.request.IssuingPointReq;
import com.ultravoucher.uv.data.api.request.JNECheckRatesReq;
import com.ultravoucher.uv.data.api.request.JNEGetDestinationReq;
import com.ultravoucher.uv.data.api.request.MemberPocketReq;
import com.ultravoucher.uv.data.api.request.MemberWaletReq;
import com.ultravoucher.uv.data.api.request.MyRewardRedeemReq;
import com.ultravoucher.uv.data.api.request.PayByVoucherAmountRequest;
import com.ultravoucher.uv.data.api.request.PayByVoucherReq;
import com.ultravoucher.uv.data.api.request.PayByWaletBillerReq;
import com.ultravoucher.uv.data.api.request.PayByWaletReq;
import com.ultravoucher.uv.data.api.request.PayOrderCCReq;
import com.ultravoucher.uv.data.api.request.PayOrderReq;
import com.ultravoucher.uv.data.api.request.RedeemDigitalVoucherReq;
import com.ultravoucher.uv.data.api.request.ReedemRewardReq;
import com.ultravoucher.uv.data.api.request.RewardDetailReq;
import com.ultravoucher.uv.data.api.request.RewardReq;
import com.ultravoucher.uv.data.api.request.SettingGCMReq;
import com.ultravoucher.uv.data.api.request.ShareAmountReq;
import com.ultravoucher.uv.data.api.request.SharingBalanceReq;
import com.ultravoucher.uv.data.api.request.SubmitShareReq;
import com.ultravoucher.uv.data.api.request.UpdateDelMethodReq;
import com.ultravoucher.uv.data.api.request.VoucherAmountReq;
import com.ultravoucher.uv.data.api.request.VoucherCollectReq;
import com.ultravoucher.uv.data.api.response.AddAddressResponse;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;
import com.ultravoucher.uv.data.api.response.AutoRegistrationResponse;
import com.ultravoucher.uv.data.api.response.BillerCategoryResponse;
import com.ultravoucher.uv.data.api.response.BillerGetFavoriteResponse;
import com.ultravoucher.uv.data.api.response.BillerHistoryResponse;
import com.ultravoucher.uv.data.api.response.HistoryBillerDetailResponse;
import com.ultravoucher.uv.data.api.response.BillerVoucherHistoryResponse;
import com.ultravoucher.uv.data.api.response.ChangeAddressOrderResponse;
import com.ultravoucher.uv.data.api.response.CheckVersionResponse;
import com.ultravoucher.uv.data.api.response.DealsDetailFisikResponse;
import com.ultravoucher.uv.data.api.response.DealsDetailResponse;
import com.ultravoucher.uv.data.api.response.DiscardOrderResp;
import com.ultravoucher.uv.data.api.response.FilterAreaResponse;
import com.ultravoucher.uv.data.api.request.ForgotPasswordReq;
import com.ultravoucher.uv.data.api.request.GetCityReq;
import com.ultravoucher.uv.data.api.request.GetDistrictReq;
import com.ultravoucher.uv.data.api.request.GetVillageReq;
import com.ultravoucher.uv.data.api.request.ListDeliveryAddressReq;
import com.ultravoucher.uv.data.api.request.ListPaymentMethodReq;
import com.ultravoucher.uv.data.api.request.LoginReq;
import com.ultravoucher.uv.data.api.request.MerchantListReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.RegistrationReq;
import com.ultravoucher.uv.data.api.request.StoreByIdReq;
import com.ultravoucher.uv.data.api.request.StoreListReq;
import com.ultravoucher.uv.data.api.request.UpdateProfileReq;
import com.ultravoucher.uv.data.api.request.VerificationReq;
import com.ultravoucher.uv.data.api.request.VoucherListReq;
import com.ultravoucher.uv.data.api.response.AddToCartResponse;
import com.ultravoucher.uv.data.api.response.ChangeForgotPassResponse;
import com.ultravoucher.uv.data.api.response.ChangePasswordResponse;
import com.ultravoucher.uv.data.api.response.ForgotPasswordResponse;
import com.ultravoucher.uv.data.api.response.GenerateCCResp;
import com.ultravoucher.uv.data.api.response.GetDistrictResponse;
import com.ultravoucher.uv.data.api.response.GetProvinceResponse;
import com.ultravoucher.uv.data.api.response.GetVillageResponse;
import com.ultravoucher.uv.data.api.response.HistoryProductResponse;
import com.ultravoucher.uv.data.api.response.HistoryProgressResponse;
import com.ultravoucher.uv.data.api.response.HistoryRewardResponse;
import com.ultravoucher.uv.data.api.response.HistoryVoucherDetailResponse;
import com.ultravoucher.uv.data.api.response.HistoryWaletResponse;
import com.ultravoucher.uv.data.api.response.InisiateBillerResp;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.api.response.InqPayOrderResponse;
import com.ultravoucher.uv.data.api.response.IssuingPointResp;
import com.ultravoucher.uv.data.api.response.JNECheckRatesResponse;
import com.ultravoucher.uv.data.api.response.JNEGetDestinationResponse;
import com.ultravoucher.uv.data.api.response.ListDeliveryAddressResponse;
import com.ultravoucher.uv.data.api.response.ListPaymentMethodResponse;
import com.ultravoucher.uv.data.api.response.LoginResponse;
import com.ultravoucher.uv.data.api.response.LoginWaletResponse;
import com.ultravoucher.uv.data.api.response.MemberExistResponse;
import com.ultravoucher.uv.data.api.response.MemberPocketResponse;
import com.ultravoucher.uv.data.api.response.MemberPointBalanceResponse;
import com.ultravoucher.uv.data.api.response.MemberWaletResponse;
import com.ultravoucher.uv.data.api.response.MerchantListResponse;
import com.ultravoucher.uv.data.api.response.PayBillerResp;
import com.ultravoucher.uv.data.api.response.PayByVoucherResponse;
import com.ultravoucher.uv.data.api.response.PayOrderResp;
import com.ultravoucher.uv.data.api.response.RedeemDigitalVoucherResponse;
import com.ultravoucher.uv.data.api.response.RedeemRewardResp;
import com.ultravoucher.uv.data.api.response.RegistrationResp;
import com.ultravoucher.uv.data.api.response.RewardDetailResp;
import com.ultravoucher.uv.data.api.response.RewardResponse;
import com.ultravoucher.uv.data.api.response.SettingGCMResp;
import com.ultravoucher.uv.data.api.response.SharingAmountResponse;
import com.ultravoucher.uv.data.api.response.SharingBalanceResponse;
import com.ultravoucher.uv.data.api.response.StoreListResponse;
import com.ultravoucher.uv.data.api.response.SubmitShareResponse;
import com.ultravoucher.uv.data.api.response.UpdateProfileResponse;
import com.ultravoucher.uv.data.api.response.UploadPhotoResp;
import com.ultravoucher.uv.data.api.response.VerificationResp;
import com.ultravoucher.uv.data.api.response.VoucherAmountResponse;
import com.ultravoucher.uv.data.api.response.VoucherCollectResponse;
import com.ultravoucher.uv.data.api.response.VoucherListResponse;
import com.ultravoucher.uv.data.api.response.VoucherRedemptionResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by firwandi.ramli on 9/25/2017.
 */

public interface VoucherApi {

    //LOGIN
    @POST("authentication/create")
    Call<LoginResponse> doLogin(@Body LoginReq request);

    //REGISTER
    @POST("member/profile/registration")
    Call<RegistrationResp> doRegistration(@Header("Content-Type") String contentType,
                                          @Body RegistrationReq request);

    //VERIFICATION REGISTER
    @POST("member/registration/verification")
    Call<VerificationResp> doVerification(@Header("Content-Type") String contentType,
                                          @Body VerificationReq request);

    //FORGOT PASS
    @POST("member/forgotPassword/verificationCode")
    Call<ForgotPasswordResponse> getResetCode(@Header("Content-Type") String contentType, @Body ForgotPasswordReq request);

    //FORGOT PASS VERIFICATION
    @POST("member/forgotPassword/confirmation")
    Call<ChangeForgotPassResponse> doChangePassword(@Header("Content-Type") String contentType,
                                                    @Body ChangeForgotPassReq request);

    //MERCHANT LIST
    @POST("merchant/list")
    Call<MerchantListResponse> getMerchant(@Header("Content-Type") String contentType,
                                           @Body MerchantListReq request);

    //STORE LIST
    @POST("merchant/store")
    Call<StoreListResponse> getStoreList(@Header("Content-Type") String contentType,
                                         @Body StoreListReq request);

    //PRODUCT LIST BY STORE ID
    @POST("product/getByStoreId")
    Call<VoucherListResponse> getStoreById(@Header("Content-Type") String contentType,
                                           @Body StoreByIdReq request);

    //PRODUCT LIST
    @POST("product/getProducts")
    Call<VoucherListResponse> getListProduct(@Header("Content-Type") String contentType,
                                             @Body VoucherListReq request);

    //PRODUCT DETAIL
    @POST("product/getByProductId")
    Call<VoucherListResponse> getDetailProduct(@Header("Content-Type") String contentType,
                                               @Body DetailProductByIdReq request);

    //LIST PAYMENT METHOD
    @POST("payment/method")
    Call<ListPaymentMethodResponse> getListPayMethod(@Header("Content-Type") String contentType,
                                                     @Body ListPaymentMethodReq request);

    //ADD TO CART
    @POST("order/create")
    Call<AddToCartResponse> addToCart(@Header("authToken") String authToken,
                                      @Header("deviceUniqueId") String deviceUniqueId,
                                      @Body AddToCartReq request);

    //GET ORDER NUMBER
    @POST("order/getOrderBy")
    Call<MyCartResponse> getMyCart(@Header("authToken") String authToken,
                                   @Header("deviceUniqueId") String deviceUniqueId,
                                   @Body MyCartReq request);

    //CHANGE PASSWORD
    @POST("member/changePassword")
    Call<ChangePasswordResponse> doChangePass(@Header("Content-Type") String contentType, @Header("authToken") String authToken,
                                              @Header("deviceUniqueId") String deviceUniqueId,
                                              @Body ChangePasswordReq request);

    //UPDATE PROFILE
    @POST("member/profile/update")
    Call<UpdateProfileResponse> updateProfile(@Header("Content-Type") String contentType, @Header("authToken") String authToken,
                                              @Header("deviceUniqueId") String deviceUniqueId,
                                              @Body UpdateProfileReq request);

    //GET LIST ADDRESS
    @POST("member/delivery/getByMemberId")
    Call<ListDeliveryAddressResponse> getListDeliveryAddress(@Header("Content-Type") String contentType,
                                                             @Header("authToken") String authToken,
                                                             @Header("deviceUniqueId") String deviceUniqueId,
                                                             @Body ListDeliveryAddressReq request);
    //GET LIST PROVINCE
    @POST("util/list/province")
    Call<GetProvinceResponse> getProvince();

    //GET LIST CITY
    @POST("util/list/city")
    Call<FilterAreaResponse> doFilterArea(@Body GetCityReq request);

    //GET LIST KECAMATAN
    @POST("util/list/district")
    Call<GetDistrictResponse> getDistrictByCity(@Body GetDistrictReq request);

    //GET LIST KELURAHAN
    @POST("util/list/village")
    Call<GetVillageResponse> getVillageByDistrict(@Body GetVillageReq request);

    //ADD DELIVERY ADDRESS
    @POST("member/delivery/create")
    Call<AddAddressResponse> doCreateAddress(@Header("authToken") String authToken,
                                             @Header("deviceUniqueId") String deviceUniqueId,
                                             @Body CreateAddressReq request);


    //HISTORY
    @POST("deals/history")
    Call<HistoryProgressResponse> getHistory(@Header("Content-Type") String contentType,
                                             @Header("authToken") String authToken,
                                             @Header("deviceUniqueId") String deviceUniqueId,
                                             @Body HistoryProgressReq request);

    //HISTORY BILLER DAN DIGITAL
    @POST("order/history/comp")
    Call<BillerVoucherHistoryResponse> getHistoryBV(@Header("Content-Type") String contentType,
                                                  @Header("authToken") String authToken,
                                                  @Header("deviceUniqueId") String deviceUniqueId,
                                                  @Body HistoryProgressReq request);

    //HISTORY DETAIL BILLER
    @POST("biller/history")
    Call<HistoryBillerDetailResponse> getHistoryBDetail(@Header("Content-Type") String contentType,
                                                         @Header("authToken") String authToken,
                                                         @Header("deviceUniqueId") String deviceUniqueId,
                                                         @Body HistoryBillerDetailReq request);

    //HISTORY DETAIL VOUCHER
    @POST("deals/history/details")
    Call<HistoryVoucherDetailResponse> getHistoryVDetail(@Header("Content-Type") String contentType,
                                                         @Header("authToken") String authToken,
                                                         @Header("deviceUniqueId") String deviceUniqueId,
                                                         @Body HistoryVoucherDetailReq request);

    //BANNER HOME
    @POST("advertising/details")
    Call<AdvertisingResponse> getDetailAds(@Body AdvertisingReq request);

    //LIST MYVOUCHER DIGITAL (GROUPING)
    @POST("deals/pocket/digital")
    Call<DealsDetailResponse> getListDigitalGrouping(@Header("Content-Type") String contentType,
                                                     @Header("authToken") String authToken,
                                                     @Header("deviceUniqueId") String deviceUniqueId,
                                                     @Body DigitalPocketReq request);

    //LIST MYVOUCHER FISIK
    @POST("deals/pocket")
    Call<DealsDetailFisikResponse> getListMyVoucherFisik(@Header("Content-Type") String contentType,
                                                         @Header("authToken") String authToken,
                                                         @Header("deviceUniqueId") String deviceUniqueId,
                                                         @Body DigitalPocketReq request);

    //CHECK MEMBER EXIST
    @POST("member/checkMemberExist")
    Call<MemberExistResponse> getMemberExist(@Header("Content-Type") String contentType,
                                             @Body CheckMemberExistReq request);

    //AUTO REGISTER
    @POST("member/profile/autoRegistration")
    Call<AutoRegistrationResponse> doAutoRegis(@Header("Content-Type") String contentType,
                                               @Body AutoRegistrationReq request);

    //GET LIST SHARING BALANCE (OUTDATED)
    @POST("deals/sharingBalance")
    Call<SharingBalanceResponse> getSharingBalance(@Header("Content-Type") String contentType,
                                                   @Header("authToken") String authToken,
                                                   @Header("deviceUniqueId") String deviceUniqueId,
                                                   @Body SharingBalanceReq request);

    //SUBMIT SHARING VOUCHER
    @POST("deals/share")
    Call<SubmitShareResponse> doShare(@Header("Content-Type") String contentType,
                                      @Header("authToken") String authToken,
                                      @Header("deviceUniqueId") String deviceUniqueId,
                                      @Body SubmitShareReq request);

    //MEMBER BALANCE
    @POST("deals/memberBalance")
    Call<MemberWaletResponse> getMemberWalet(@Header("Content-Type") String contentType,
                                             @Header("authToken") String authToken,
                                             @Header("deviceUniqueId") String deviceUniqueId,
                                             @Body MemberWaletReq request);

    //MEMBER UPLOAD PHOTO
    @Multipart
    @POST("member/upload")
    Call<UploadPhotoResp> doUploadPhoto(@Part("memberId") RequestBody memberId,
                                        @Part MultipartBody.Part image);

    //GENERATE VA NUMBER
    @POST("order/generateVirtualAccount")
    Call<PayOrderResp> doPayOrder(@Header("Content-Type") String contentType,
                                  @Header("authToken") String authToken,
                                  @Header("deviceUniqueId") String deviceUniqueId,
                                  @Body PayOrderReq request);

    //DISCARD ITEM CART
    @POST("order/discardOrder")
    Call<DiscardOrderResp> doDiscardOrder(@Header("authToken") String authToken,
                                          @Header("deviceUniqueId") String deviceUniqueId,
                                          @Body DiscardOrderReq request);

    //REWARD LIST
    @POST("point-transaction/redemption/reward/browse/all")
    Call<RewardResponse> doGetAllReward(@Header("authToken") String authToken,
                                        @Header("deviceUniqueId") String deviceUniqueId,
                                        @Body RewardReq request);


    //REWARD REEDEM
    @POST("point-transaction/redemption/reward/redeem")
    Call<RedeemRewardResp> getRedeemReward(@Header("authToken") String authToken,
                                           @Header("deviceUniqueId") String deviceUniqueId,
                                           @Body ReedemRewardReq request);

    //REWARD DETAIL BY ID
    @POST("point-transaction/redemption/reward/browse/id")
    Call<RewardDetailResp> getRewardDetailById(@Body RewardDetailReq request);


    //GENERATE CREDIT CARD
    @POST("payment/generate")
    Call<GenerateCCResp> doPayOrderCC(@Header("Content-Type") String contentType,
                                      @Header("authToken") String authToken,
                                      @Header("deviceUniqueId") String deviceUniqueId,
                                      @Body PayOrderCCReq request);

    //PAY BY VOUCHER
    @POST("payment/byVoucher")
    Call<PayByVoucherResponse> doPayByVoucher(@Header("Content-Type") String contentType,
                                              @Header("authToken") String authToken,
                                              @Header("deviceUniqueId") String deviceUniqueId,
                                              @Body PayByVoucherReq request);

    //GET DESTINATION JNE
    @POST("shipping/jne/getDestination")
    Call<JNEGetDestinationResponse> getDestination(@Header("Content-Type") String contentType,
                                                   @Header("authToken") String authToken,
                                                   @Header("deviceUniqueId") String deviceUniqueId,
                                                   @Body JNEGetDestinationReq request);

    //CHECK PACKAGE & PRICE JNE
    @POST("shipping/jne/checkRates")
    Call<JNECheckRatesResponse> getRates(@Header("Content-Type") String contentType,
                                         @Header("authToken") String authToken,
                                         @Header("deviceUniqueId") String deviceUniqueId,
                                         @Body JNECheckRatesReq request);

    //CHANGE ADDRESS
    @POST("order/updateDelivery")
    Call<ChangeAddressOrderResponse> doChangeAddressOrder(@Header("authToken") String authToken,
                                                          @Header("deviceUniqueId") String deviceUniqueId,
                                                          @Body ChangeAddressReq request);

    //LIST MY REWARD
    @POST("point-transaction/voucher/collect")
    Call<VoucherCollectResponse> getVoucherCollect(@Header("Content-Type") String contentType,
                                                   @Header("authToken") String authToken,
                                                   @Header("deviceUniqueId") String deviceUniqueId,
                                                   @Body VoucherCollectReq request);

    //WALET BALANCE
    @POST("member/pocket/balance")
    Call<MemberPocketResponse> getMemberPocket(@Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body MemberPocketReq request);

    //INQ WALET (PAY BY WALET)
    @POST("payment/wallet/inquiry")
    Call<InqPayOrderResponse> getInqPayOrder(@Header("authToken") String authToken,
                                             @Header("deviceUniqueId") String deviceUniqueId,
                                             @Body InqPayOrderReq request);

    //PAY BY WALET
    @POST("payment/wallet/pay")
    Call<PayOrderResp> payByWalet(@Header("Content-Type") String contentType,
                                  @Header("authToken") String authToken,
                                  @Header("deviceUniqueId") String deviceUniqueId,
                                  @Body PayByWaletReq request);

    //LOGIN WALET
    @POST("member/payment/login")
    Call<LoginWaletResponse> loginWalet(@Header("Content-Type") String contentType,
                                        @Body MemberPocketReq request);


    //ISSUING POIN
    @POST("promotion/posting")
    Call<IssuingPointResp> getPointIssuing(@Header("authToken") String authToken, @Header("deviceUniqueId") String deviceUniqueId, @Body IssuingPointReq request);


    //HISTORY WALET
    @POST("member/payment/history")
    Call<HistoryWaletResponse> getHistoryWalet(@Header("Content-Type") String contentType,
                                               @Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body HistoryWaletReq request);

    //HISTORY REWARD
    @POST("point-transaction/voucher/historyDompet")
    Call<HistoryRewardResponse> getHistoryReward(@Header("Content-Type") String contentType,
                                                 @Header("authToken") String authToken,
                                                 @Header("deviceUniqueId") String deviceUniqueId,
                                                 @Body HistoryRewardReq request);

    //UPDATE DELIVERY METHOD
    @POST("order/updateDelivery")
    Call<AddToCartResponse> updateDelMethod(@Header("Content-Type") String contentType,
                                                 @Header("authToken") String authToken,
                                                 @Header("deviceUniqueId") String deviceUniqueId,
                                                 @Body UpdateDelMethodReq request);

    //CHECK MEMBER POINT BALANCE
    @POST("member/points")
    Call<MemberPointBalanceResponse> checkMemberPoint(@Header("Content-Type") String contentType,
                                                     @Header("authToken") String authToken,
                                                     @Header("deviceUniqueId") String deviceUniqueId,
                                                     @Body CheckMemberPointsReq request);


    //HISTORY PRODUCT
    @POST("order/history/product")
    Call<HistoryProductResponse> listHistoryProduct(@Header("Content-Type") String contentType,
                                                  @Header("authToken") String authToken,
                                                  @Header("deviceUniqueId") String deviceUniqueId,
                                                  @Body HistoryProductReq request);

    //GET LIST SHARE AMOUNT
    @POST("deals/sharingAmount")
    Call<SharingAmountResponse> getShareAmount(@Header("Content-Type") String contentType,
                                               @Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body ShareAmountReq request);

    //GET LIST SHARE AMOUNT
    @POST("deals/sharing/inquiry")
    Call<SharingBalanceResponse> getInquiryShare(@Header("Content-Type") String contentType,
                                               @Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body InquiryShareReq request);

    //DO REDEEM VOUCHER DIGITAL
    @POST("deals/redeem/digital")
    Call<RedeemDigitalVoucherResponse> doShareDigitalVoucher(@Header("Content-Type") String contentType,
                                                       @Header("authToken") String authToken,
                                                       @Header("deviceUniqueId") String deviceUniqueId,
                                                       @Body RedeemDigitalVoucherReq request);

    //FORCE UPDATE
    @POST("util/mobile/version")
    Call<CheckVersionResponse> doCheckVersion();

    //REDEEM MY REWARD
    @POST("point-transaction/redemption/voucher/redeem")
    Call<VoucherRedemptionResponse> doRedeem(@Header("Content-Type") String contentType,
                                             @Header("authToken") String authToken,
                                             @Header("deviceUniqueId") String deviceUniqueId,
                                             @Body MyRewardRedeemReq request);

    /*BILLER*/
    //INQ WALET BILLER (PAY BY WALET)
//    @POST("biller/wallet/check")
    @POST("biller/check")
    Call<InqPayBillerResponse> getCheckBiller(@Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body InqPayBillerReq request);

    //INQ WALET BILLER (PAY BY WALET)
//    @POST("biller/wallet/inquiry")
    @POST("biller/inquiry")
    Call<InqPayBillerResponse> getInqPayBiller(@Header("authToken") String authToken,
                                               @Header("deviceUniqueId") String deviceUniqueId,
                                               @Body InqPayBillerReq request);

    //PAY BY WALET
//    @POST("biller/wallet/payment")
    @POST("biller/payment")
    Call<PayBillerResp> payByWaletBiller(@Header("Content-Type") String contentType,
                                         @Header("authToken") String authToken,
                                         @Header("deviceUniqueId") String deviceUniqueId,
                                         @Body PayByWaletBillerReq request);

    //INISIATE WALET BILLER
//    @POST("biller/wallet/fee")
    @POST("biller/fee")
    Call<InisiateBillerResp> inisiateBiller(@Header("authToken") String authToken,
                                            @Header("deviceUniqueId") String deviceUniqueId,
                                            @Body InqPayBillerReq request);

    //LIST PAYMENT METHOD BILLER
    @POST("biller/method")
    Call<ListPaymentMethodResponse> getListPayMethodBiller(@Header("Content-Type") String contentType,
                                                     @Body ListPaymentMethodReq request);

    //HISTORY BILLER
    @POST("biller/history")
    Call<BillerHistoryResponse> listHistoryBiller(@Header("Content-Type") String contentType,
                                                  @Header("authToken") String authToken,
                                                  @Header("deviceUniqueId") String deviceUniqueId,
                                                  @Body BillerHistoryReq request);

    //HISTORY BILLER
    @POST("biller/favorite")
    Call<BillerGetFavoriteResponse> getFavorite(@Header("Content-Type") String contentType,
                                                @Header("authToken") String authToken,
                                                @Header("deviceUniqueId") String deviceUniqueId,
                                                @Body BillerGetFavoriteReq request);

    // LIST VOUCHER (PAY BY VOUCHER (NEW))
    @POST("deals/voucher/amount")
    Call<VoucherAmountResponse> getListAmountVoucher(@Header("Content-Type") String contentType,
                                                     @Header("authToken") String authToken,
                                                     @Header("deviceUniqueId") String deviceUniqueId,
                                                     @Body VoucherAmountReq request);

    // PAY BY VOUCHER (PAY BY VOUCHER (NEW))
    @POST("payment/voucher/amount")
    Call<PayByVoucherResponse> doPayByVoucherAmount(@Header("Content-Type") String contentType,
                                                     @Header("authToken") String authToken,
                                                     @Header("deviceUniqueId") String deviceUniqueId,
                                                     @Body PayByVoucherAmountRequest request);

    // SET GCM ID
    @POST("google/checkRegKey")
    Call<SettingGCMResp> setGcmId(@Header("authToken") String authToken,
                                  @Header("deviceUniqueId") String deviceUniqueId,
                                  @Body SettingGCMReq request);


    /*========= START CODE PHASE 2 =============*/


    // Biller Menu (Home)
    @POST("biller/list/billerCategory")
    Call<BillerCategoryResponse> getBillerMenuHome(@Header("Content-Type") String contentType,
                                                   @Body BillerCategoryReq request);
}
