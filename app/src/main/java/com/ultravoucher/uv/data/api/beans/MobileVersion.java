package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 11/05/18.
 */

public class MobileVersion {

    @SerializedName("lastVersion")
    @Expose
    private String lastVersion;

    @SerializedName("description")
    @Expose
    private String description;

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
