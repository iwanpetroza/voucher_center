package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.MerchantListMsgResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class MerchantListResponse extends GenericResponse{

    @SerializedName("merchantResponse")
    @Expose
    private List<MerchantListMsgResp> merchantResponse = new ArrayList<MerchantListMsgResp>();

    public List<MerchantListMsgResp> getMerchantResponse() {
        return merchantResponse;
    }

    public void setMerchantResponse(List<MerchantListMsgResp> merchantResponse) {
        this.merchantResponse = merchantResponse;
    }
}
