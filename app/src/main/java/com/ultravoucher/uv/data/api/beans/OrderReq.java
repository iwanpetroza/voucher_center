package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/19/2017.
 */

public class OrderReq {

    @SerializedName("sortName")
    @Expose
    private String sortName;

    @SerializedName("sortCreatedDate")
    @Expose
    private String sortCreatedDate;

    @SerializedName("sortPrice")
    @Expose
    private String sortPrice;

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortCreatedDate() {
        return sortCreatedDate;
    }

    public void setSortCreatedDate(String sortCreatedDate) {
        this.sortCreatedDate = sortCreatedDate;
    }

    public String getSortPrice() {
        return sortPrice;
    }

    public void setSortPrice(String sortPrice) {
        this.sortPrice = sortPrice;
    }
}
