package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.SharingAmount;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 3/26/2018.
 */

public class SharingAmountResponse extends GenericResponse {

    @SerializedName("sharingBalance")
    @Expose
    private List<SharingAmount> sharingBalance = new ArrayList<SharingAmount>();

    public List<SharingAmount> getSharingBalance() {
        return sharingBalance;
    }

    public void setSharingBalance(List<SharingAmount> sharingBalance) {
        this.sharingBalance = sharingBalance;
    }
}
