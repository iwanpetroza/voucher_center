package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.UtilMessageResp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class GetVillageResponse extends GenericResponse{

    @SerializedName("utilMessageResponse")
    @Expose
    private List<UtilMessageResp> utilMessageResponse = new ArrayList<UtilMessageResp>();

    /**
     *
     * @return
     * The utilMessageResponse
     */
    public List<UtilMessageResp> getUtilMessageResponse() {
        return utilMessageResponse;
    }

    /**
     *
     * @param utilMessageResponse
     * The utilMessageResponse
     */
    public void setUtilMessageResponse(List<UtilMessageResp> utilMessageResponse) {
        this.utilMessageResponse = utilMessageResponse;
    }
}
