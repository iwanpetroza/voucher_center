package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class UpdateDelMethod {

	@SerializedName("deliveryAddressId")
	private String deliveryAddressId;

	@SerializedName("orderId")
	private String orderId;

	@SerializedName("deliveryMethod")
	private int deliveryMethod;

	@SerializedName("pickupNote")
	private String pickupNote;

	@SerializedName("shippingType")
	private String shippingType;

	@SerializedName("pickupDate")
	private String pickupDate;

	@SerializedName("shippingBy")
	private String shippingBy;

	@SerializedName("status")
	private int status;

	@SerializedName("shippingAmount")
	private int shippingAmount;

	@SerializedName("insuranceFee")
	private int insuranceFee;

	public void setDeliveryAddressId(String deliveryAddressId){
		this.deliveryAddressId = deliveryAddressId;
	}

	public String getDeliveryAddressId(){
		return deliveryAddressId;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setDeliveryMethod(int deliveryMethod){
		this.deliveryMethod = deliveryMethod;
	}

	public int getDeliveryMethod(){
		return deliveryMethod;
	}

	public void setPickupNote(String pickupNote){
		this.pickupNote = pickupNote;
	}

	public String getPickupNote(){
		return pickupNote;
	}

	public void setShippingType(String shippingType){
		this.shippingType = shippingType;
	}

	public String getShippingType(){
		return shippingType;
	}

	public void setPickupDate(String pickupDate){
		this.pickupDate = pickupDate;
	}

	public String getPickupDate(){
		return pickupDate;
	}

	public void setShippingBy(String shippingBy){
		this.shippingBy = shippingBy;
	}

	public String getShippingBy(){
		return shippingBy;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public int getShippingAmount() {
		return shippingAmount;
	}

	public void setShippingAmount(int shippingAmount) {
		this.shippingAmount = shippingAmount;
	}

	public int getInsuranceFee() {
		return insuranceFee;
	}

	public void setInsuranceFee(int insuranceFee) {
		this.insuranceFee = insuranceFee;
	}

	@Override
 	public String toString(){
		return 
			"UpdateDelMethod{" +
			"deliveryAddressId = '" + deliveryAddressId + '\'' + 
			",orderId = '" + orderId + '\'' + 
			",deliveryMethod = '" + deliveryMethod + '\'' + 
			",pickupNote = '" + pickupNote + '\'' + 
			",shippingType = '" + shippingType + '\'' + 
			",pickupDate = '" + pickupDate + '\'' + 
			",shippingBy = '" + shippingBy + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}