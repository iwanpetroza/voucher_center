package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class ListPaymentMethodReq{

	@SerializedName("purchasedVoucherClass")
	private int purchasedVoucherClass;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("orderType")
	private int orderType;

	public void setPurchasedVoucherClass(int purchasedVoucherClass){
		this.purchasedVoucherClass = purchasedVoucherClass;
	}

	public int getPurchasedVoucherClass(){
		return purchasedVoucherClass;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	@Override
 	public String toString(){
		return 
			"ListPaymentMethodReq{" + 
			"purchasedVoucherClass = '" + purchasedVoucherClass + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}