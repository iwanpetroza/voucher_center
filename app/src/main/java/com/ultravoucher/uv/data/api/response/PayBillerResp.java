package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.AbstractResponse;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class PayBillerResp {

    @SerializedName("transactionId")
    @Expose
    public String transactionId;
    @SerializedName("token")
    @Expose
    public String token;
    @SerializedName("abstractResponse")
    @Expose
    public AbstractResponse abstractResponse;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public AbstractResponse getAbstractResponse() {
        return abstractResponse;
    }

    public void setAbstractResponse(AbstractResponse abstractResponse) {
        this.abstractResponse = abstractResponse;
    }
}
