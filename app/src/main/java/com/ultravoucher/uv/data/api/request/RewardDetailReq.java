package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class RewardDetailReq {

        @SerializedName("rewardId")
        @Expose
        private String rewardId;

        /**
         *
         * @return
         * The rewardId
         */
        public String getRewardId() {
            return rewardId;
        }

        /**
         *
         * @param rewardId
         * The rewardId
         */
        public void setRewardId(String rewardId) {
            this.rewardId = rewardId;
        }

}
