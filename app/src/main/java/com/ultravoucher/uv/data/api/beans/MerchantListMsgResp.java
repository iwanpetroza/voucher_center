package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class MerchantListMsgResp{

	@SerializedName("merchantCode")
	private String merchantCode;

	@SerializedName("merchantWebsite")
	private String merchantWebsite;

	@SerializedName("distance")
	private double distance;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("merchantLogo")
	private String merchantLogo;

	@SerializedName("merchantName")
	private String merchantName;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("merchantPhone")
	private String merchantPhone;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("registeredName")
	private String registeredName;

	@SerializedName("cityTown")
	private String cityTown;

	@SerializedName("merchantAddress")
	private String merchantAddress;

	@SerializedName("brand")
	private String brand;

	@SerializedName("backgroundPicture")
	private String backgroundPicture;

	@SerializedName("line1")
	private String line1;

	@SerializedName("longitude")
	private double longitude;

	public String getMerchantCode() {
		return merchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		this.merchantCode = merchantCode;
	}

	public String getMerchantWebsite() {
		return merchantWebsite;
	}

	public void setMerchantWebsite(String merchantWebsite) {
		this.merchantWebsite = merchantWebsite;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getMerchantLogo() {
		return merchantLogo;
	}

	public void setMerchantLogo(String merchantLogo) {
		this.merchantLogo = merchantLogo;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMerchantPhone() {
		return merchantPhone;
	}

	public void setMerchantPhone(String merchantPhone) {
		this.merchantPhone = merchantPhone;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getRegisteredName() {
		return registeredName;
	}

	public void setRegisteredName(String registeredName) {
		this.registeredName = registeredName;
	}

	public String getCityTown() {
		return cityTown;
	}

	public void setCityTown(String cityTown) {
		this.cityTown = cityTown;
	}

	public String getMerchantAddress() {
		return merchantAddress;
	}

	public void setMerchantAddress(String merchantAddress) {
		this.merchantAddress = merchantAddress;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBackgroundPicture() {
		return backgroundPicture;
	}

	public void setBackgroundPicture(String backgroundPicture) {
		this.backgroundPicture = backgroundPicture;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
 	public String toString(){
		return 
			"MerchantListMsgResp{" + 
			"merchantCode = '" + merchantCode + '\'' + 
			",merchantWebsite = '" + merchantWebsite + '\'' + 
			",distance = '" + distance + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",merchantLogo = '" + merchantLogo + '\'' + 
			",merchantName = '" + merchantName + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",merchantPhone = '" + merchantPhone + '\'' + 
			",merchantId = '" + merchantId + '\'' + 
			",registeredName = '" + registeredName + '\'' + 
			",cityTown = '" + cityTown + '\'' + 
			",merchantAddress = '" + merchantAddress + '\'' + 
			",brand = '" + brand + '\'' + 
			",backgroundPicture = '" + backgroundPicture + '\'' + 
			",line1 = '" + line1 + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}