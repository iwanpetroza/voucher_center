package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class VoucherAmountMsgResp{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("voucherClass")
	private int voucherClass;

	@SerializedName("voucherValue")
	private int voucherValue;

	@SerializedName("expiredDate")
	private String expiredDate;

	@SerializedName("countExpiredDate")
	private int countExpiredDate;

	@SerializedName("voucherOrigin")
	private int voucherOrigin;

	@SerializedName("productName")
	private String productName;

	@SerializedName("image")
	private String image;

	@SerializedName("amountSum")
	private int amountSum;

	@SerializedName("quantityEt")
	private int quantityEt;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setVoucherClass(int voucherClass){
		this.voucherClass = voucherClass;
	}

	public int getVoucherClass(){
		return voucherClass;
	}

	public void setVoucherValue(int voucherValue){
		this.voucherValue = voucherValue;
	}

	public int getVoucherValue(){
		return voucherValue;
	}

	public void setExpiredDate(String expiredDate){
		this.expiredDate = expiredDate;
	}

	public String getExpiredDate(){
		return expiredDate;
	}

	public int getCountExpiredDate() {
		return countExpiredDate;
	}

	public void setCountExpiredDate(int countExpiredDate) {
		this.countExpiredDate = countExpiredDate;
	}

	public int getVoucherOrigin() {
		return voucherOrigin;
	}

	public void setVoucherOrigin(int voucherOrigin) {
		this.voucherOrigin = voucherOrigin;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getAmountSum() {
		return amountSum;
	}

	public void setAmountSum(int amountSum) {
		this.amountSum = amountSum;
	}

	public int getQuantityEt() {
		return quantityEt;
	}

	public void setQuantityEt(int quantityEt) {
		this.quantityEt = quantityEt;
	}

	@Override
 	public String toString(){
		return 
			"VoucherAmountMsgResp{" + 
			"quantity = '" + quantity + '\'' +
			",voucherClass = '" + voucherClass + '\'' + 
			",voucherValue = '" + voucherValue + '\'' + 
			",expiredDate = '" + expiredDate + '\'' + 
			",countExpiredDate = '" + countExpiredDate + '\'' + 
			"}";
		}
}