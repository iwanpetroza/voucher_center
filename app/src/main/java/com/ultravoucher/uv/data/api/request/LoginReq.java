package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class LoginReq{

	@SerializedName("password")
	private String password;

	@SerializedName("deviceUniqueId")
	private String deviceUniqueId;

	@SerializedName("username")
	private String username;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setDeviceUniqueId(String deviceUniqueId){
		this.deviceUniqueId = deviceUniqueId;
	}

	public String getDeviceUniqueId(){
		return deviceUniqueId;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"LoginReq{" + 
			"password = '" + password + '\'' + 
			",deviceUniqueId = '" + deviceUniqueId + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}