package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/3/2018.
 */

public class RewardReq {


    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("order")
    @Expose
    private String order;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("nRecords")
    @Expose
    private int nRecords;

    //   =========== Tambahan Baru =======================

    @SerializedName("memberId")
    @Expose
    private String memberId;

//   =========== End of Tambahan Baru =======================

    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("rewardCategory")
    @Expose
    private String rewardCategory;
    @SerializedName("cityId")
    @Expose
    private String cityId;

    /**
     *
     * @return
     * The keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     *
     * @param keyword
     * The keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     *
     * @return
     * The order
     */
    public String getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(String order) {
        this.order = order;
    }

    /**
     *
     * @return
     * The page
     */
    public int getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     *
     * @return
     * The nRecords
     */
    public int getNRecords() {
        return nRecords;
    }

    /**
     *
     * @param nRecords
     * The nRecords
     */
    public void setNRecords(int nRecords) {
        this.nRecords = nRecords;
    }


    //   =========== Tambahan Baru =======================
    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

//   =========== End of Tambahan Baru =======================

    /**
     *
     * @return
     * The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The rewardCategory
     */
    public String getRewardCategory() {
        return rewardCategory;
    }

    /**
     *
     * @param rewardCategory
     * The rewardCategory
     */
    public void setRewardCategory(String rewardCategory) {
        this.rewardCategory = rewardCategory;
    }

    /**
     *
     * @return
     * The cityId
     */
    public String getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * The cityId
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
}
