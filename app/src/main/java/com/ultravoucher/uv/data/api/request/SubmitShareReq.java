package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;
import com.ultravoucher.uv.data.api.beans.SubmitShareDetails;

import java.util.ArrayList;
import java.util.List;

public class SubmitShareReq{

	@SerializedName("memberIdFrom")
	private String memberIdFrom;

	@SerializedName("memberIdTo")
	private String memberIdTo;

	@SerializedName("merchantId")
	private String merchantId;

	@SerializedName("details")
	private List<SubmitShareDetails> details = new ArrayList<SubmitShareDetails>();

	public void setMemberIdFrom(String memberIdFrom){
		this.memberIdFrom = memberIdFrom;
	}

	public String getMemberIdFrom(){
		return memberIdFrom;
	}

	public void setMemberIdTo(String memberIdTo){
		this.memberIdTo = memberIdTo;
	}

	public String getMemberIdTo(){
		return memberIdTo;
	}

	public List<SubmitShareDetails> getDetails() {
		return details;
	}

	public void setDetails(List<SubmitShareDetails> details) {
		this.details = details;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	@Override
 	public String toString(){
		return 
			"SubmitShareReq{" + 
			"memberIdFrom = '" + memberIdFrom + '\'' + 
			",memberIdTo = '" + memberIdTo + '\'' + 
			"}";
		}
}