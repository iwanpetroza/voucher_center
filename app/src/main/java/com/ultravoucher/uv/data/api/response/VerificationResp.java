package com.ultravoucher.uv.data.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class VerificationResp extends GenericResponse{
    @SerializedName("verificationCode")
    @Expose
    private String verificationCode;

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("isRegisteredPayment")
    @Expose
    private int isRegisteredPayment;

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getIsRegisteredPayment() {
        return isRegisteredPayment;
    }

    public void setIsRegisteredPayment(int isRegisteredPayment) {
        this.isRegisteredPayment = isRegisteredPayment;
    }
}
