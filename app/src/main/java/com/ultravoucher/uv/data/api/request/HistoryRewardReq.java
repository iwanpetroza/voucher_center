package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class HistoryRewardReq {

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("page")
	private int page;

	@SerializedName("keyword")
	private String keyword;

	@SerializedName("order")
	private String order;

	@SerializedName("username")
	private String username;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setKeyword(String keyword){
		this.keyword = keyword;
	}

	public String getKeyword(){
		return keyword;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"HistoryRewardReq{" +
			"nRecords = '" + nRecords + '\'' + 
			",page = '" + page + '\'' + 
			",keyword = '" + keyword + '\'' + 
			",order = '" + order + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}