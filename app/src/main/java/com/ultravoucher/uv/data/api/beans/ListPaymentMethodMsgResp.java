package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class ListPaymentMethodMsgResp{

	@SerializedName("paymentCode")
	private int paymentCode;

	@SerializedName("paymentName")
	private String paymentName;

	@SerializedName("totalBalance")
	private int totalBalance;

	@SerializedName("availableVouchers")
	private int availableVouchers;

	@SerializedName("paymentLogo")
	private String paymentLogo;

	public void setPaymentCode(int paymentCode){
		this.paymentCode = paymentCode;
	}

	public int getPaymentCode(){
		return paymentCode;
	}

	public void setPaymentName(String paymentName){
		this.paymentName = paymentName;
	}

	public String getPaymentName(){
		return paymentName;
	}

	public int getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(int totalBalance) {
		this.totalBalance = totalBalance;
	}

	public int getAvailableVouchers() {
		return availableVouchers;
	}

	public void setAvailableVouchers(int availableVouchers) {
		this.availableVouchers = availableVouchers;
	}

	public String getPaymentLogo() {
		return paymentLogo;
	}

	public void setPaymentLogo(String paymentLogo) {
		this.paymentLogo = paymentLogo;
	}

	@Override
 	public String toString(){
		return 
			"ListPaymentMethodMsgResp{" + 
			"paymentCode = '" + paymentCode + '\'' + 
			",paymentName = '" + paymentName + '\'' + 
			"}";
		}
}