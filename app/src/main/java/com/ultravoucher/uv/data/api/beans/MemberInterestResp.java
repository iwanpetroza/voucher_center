package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

public class MemberInterestResp{

	@SerializedName("interestId")
	private String interestId;

	@SerializedName("interestName")
	private String interestName;

	public void setInterestId(String interestId){
		this.interestId = interestId;
	}

	public String getInterestId(){
		return interestId;
	}

	public void setInterestName(String interestName){
		this.interestName = interestName;
	}

	public String getInterestName(){
		return interestName;
	}

	@Override
 	public String toString(){
		return 
			"MemberInterestResp{" + 
			"interestId = '" + interestId + '\'' + 
			",interestName = '" + interestName + '\'' + 
			"}";
		}
}