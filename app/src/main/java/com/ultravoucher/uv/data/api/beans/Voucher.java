package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class Voucher {

    @SerializedName("voucherId")
    @Expose
    private String voucherId;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("voucherExpiredDate")
    @Expose
    private String voucherExpiredDate;
    @SerializedName("lotteryId")
    @Expose
    private Object lotteryId;
    @SerializedName("lotteryCode")
    @Expose
    private Object lotteryCode;

    /*Additional*/
    private String voucherName;

    /**
     *
     * @return
     * The voucherId
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     *
     * @param voucherId
     * The voucherId
     */
    public void setVoucherId(String voucherId) {
        this.voucherId = voucherId;
    }

    /**
     *
     * @return
     * The voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     *
     * @param voucherCode
     * The voucherCode
     */
    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     *
     * @return
     * The voucherExpiredDate
     */
    public String getVoucherExpiredDate() {
        return voucherExpiredDate;
    }

    /**
     *
     * @param voucherExpiredDate
     * The voucherExpiredDate
     */
    public void setVoucherExpiredDate(String voucherExpiredDate) {
        this.voucherExpiredDate = voucherExpiredDate;
    }

    /**
     *
     * @return
     * The lotteryId
     */
    public Object getLotteryId() {
        return lotteryId;
    }

    /**
     *
     * @param lotteryId
     * The lotteryId
     */
    public void setLotteryId(Object lotteryId) {
        this.lotteryId = lotteryId;
    }

    /**
     *
     * @return
     * The lotteryCode
     */
    public Object getLotteryCode() {
        return lotteryCode;
    }

    /**
     *
     * @param lotteryCode
     * The lotteryCode
     */
    public void setLotteryCode(Object lotteryCode) {
        this.lotteryCode = lotteryCode;
    }

    public String getVoucherName() {
        return voucherName;
    }

    public void setVoucherName(String voucherName) {
        this.voucherName = voucherName;
    }
}

