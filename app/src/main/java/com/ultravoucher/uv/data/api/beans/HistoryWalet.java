package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class HistoryWalet{

	@SerializedName("sourceAccount")
	private String sourceAccount;

	@SerializedName("transactionExpiryDate")
	private String transactionExpiryDate;

	@SerializedName("transactionNumber")
	private String transactionNumber;

	@SerializedName("description")
	private String description;

	@SerializedName("destAccount")
	private String destAccount;

	@SerializedName("transactionTimeStamp")
	private String transactionTimeStamp;

	@SerializedName("transactionTypeCode")
	private String transactionTypeCode;

	@SerializedName("enteredAmount")
	private int enteredAmount;

	@SerializedName("transactionType")
	private String transactionType;

	@SerializedName("feeAmount")
	private String feeAmount;

	@SerializedName("accountedAmount")
	private int accountedAmount;

	@SerializedName("drCr")
	private String drCr;

	@SerializedName("balanceAfterTrx")
	private BigDecimal balanceAfterTrx;

	@SerializedName("refNumber")
	private String refNumber;

	@SerializedName("status")
	private String status;

	public void setSourceAccount(String sourceAccount){
		this.sourceAccount = sourceAccount;
	}

	public String getSourceAccount(){
		return sourceAccount;
	}

	public void setTransactionExpiryDate(String transactionExpiryDate){
		this.transactionExpiryDate = transactionExpiryDate;
	}

	public String getTransactionExpiryDate(){
		return transactionExpiryDate;
	}

	public void setTransactionNumber(String transactionNumber){
		this.transactionNumber = transactionNumber;
	}

	public String getTransactionNumber(){
		return transactionNumber;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setDestAccount(String destAccount){
		this.destAccount = destAccount;
	}

	public String getDestAccount(){
		return destAccount;
	}

	public void setTransactionTimeStamp(String transactionTimeStamp){
		this.transactionTimeStamp = transactionTimeStamp;
	}

	public String getTransactionTimeStamp(){
		return transactionTimeStamp;
	}

	public void setTransactionTypeCode(String transactionTypeCode){
		this.transactionTypeCode = transactionTypeCode;
	}

	public String getTransactionTypeCode(){
		return transactionTypeCode;
	}

	public void setEnteredAmount(int enteredAmount){
		this.enteredAmount = enteredAmount;
	}

	public int getEnteredAmount(){
		return enteredAmount;
	}

	public void setTransactionType(String transactionType){
		this.transactionType = transactionType;
	}

	public String getTransactionType(){
		return transactionType;
	}

	public void setFeeAmount(String feeAmount){
		this.feeAmount = feeAmount;
	}

	public String getFeeAmount(){
		return feeAmount;
	}

	public void setAccountedAmount(int accountedAmount){
		this.accountedAmount = accountedAmount;
	}

	public int getAccountedAmount(){
		return accountedAmount;
	}

	public void setDrCr(String drCr){
		this.drCr = drCr;
	}

	public String getDrCr(){
		return drCr;
	}

	public void setBalanceAfterTrx(BigDecimal balanceAfterTrx){
		this.balanceAfterTrx = balanceAfterTrx;
	}

	public BigDecimal getBalanceAfterTrx(){
		return balanceAfterTrx;
	}

	public void setRefNumber(String refNumber){
		this.refNumber = refNumber;
	}

	public String getRefNumber(){
		return refNumber;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"HistoryWalet{" + 
			"sourceAccount = '" + sourceAccount + '\'' + 
			",transactionExpiryDate = '" + transactionExpiryDate + '\'' + 
			",transactionNumber = '" + transactionNumber + '\'' + 
			",description = '" + description + '\'' + 
			",destAccount = '" + destAccount + '\'' + 
			",transactionTimeStamp = '" + transactionTimeStamp + '\'' + 
			",transactionTypeCode = '" + transactionTypeCode + '\'' + 
			",enteredAmount = '" + enteredAmount + '\'' + 
			",transactionType = '" + transactionType + '\'' + 
			",feeAmount = '" + feeAmount + '\'' + 
			",accountedAmount = '" + accountedAmount + '\'' + 
			",drCr = '" + drCr + '\'' + 
			",balanceAfterTrx = '" + balanceAfterTrx + '\'' + 
			",refNumber = '" + refNumber + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}