package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class BillerVoucherHistoryMsgResp {

    @SerializedName("transactionId")
    private String transactionId;

    @SerializedName("productName")
    private String productName;

    @SerializedName("vaNumber")
    private String vaNumber;

    @SerializedName("amount")
    private int amount;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("transactionDate")
    private String transactionDate;

    @SerializedName("status")
    private int status;

    @SerializedName("type")
    private int type;

    @SerializedName("productImage")
    private String productImage;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVaNumber() {
        return vaNumber;
    }

    public void setVaNumber(String vaNumber) {
        this.vaNumber = vaNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}
