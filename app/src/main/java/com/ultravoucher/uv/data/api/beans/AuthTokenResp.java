package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthTokenResp{

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("gender")
	private String gender;

	@SerializedName("authDevice")
	private String authDevice;

	@SerializedName("userPic")
	private String userPic;

	@SerializedName("backgroundImage")
	private String backgroundImage;

	@SerializedName("authToken")
	private String authToken;

	@SerializedName("storeId")
	private Object storeId;

	@SerializedName("birthDate")
	private String birthDate;

	@SerializedName("point")
	private int point;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("expiryTime")
	private String expiryTime;

	@SerializedName("memberId")
	private String memberId;

	@SerializedName("status")
	private int status;

	@SerializedName("qrImage")
	private String qrImage;

	@SerializedName("vaNumber")
	private String vaNumber;

	@SerializedName("vaNumber2")
	private String vaNumber2;

	@SerializedName("birthPlace")
	@Expose
	private String birthPlace;

	@SerializedName("idNumber")
	@Expose
	private String idNumber;

	@SerializedName("motherName")
	@Expose
	private String motherName;

	@SerializedName("email")
	private String email;

	@SerializedName("isCorporateMember")
	private int isCorporateMember;

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setAuthDevice(String authDevice){
		this.authDevice = authDevice;
	}

	public String getAuthDevice(){
		return authDevice;
	}

	public void setUserPic(String userPic){
		this.userPic = userPic;
	}

	public String getUserPic(){
		return userPic;
	}

	public void setBackgroundImage(String backgroundImage){
		this.backgroundImage = backgroundImage;
	}

	public String getBackgroundImage(){
		return backgroundImage;
	}

	public void setAuthToken(String authToken){
		this.authToken = authToken;
	}

	public String getAuthToken(){
		return authToken;
	}

	public void setStoreId(Object storeId){
		this.storeId = storeId;
	}

	public Object getStoreId(){
		return storeId;
	}

	public void setBirthDate(String birthDate){
		this.birthDate = birthDate;
	}

	public String getBirthDate(){
		return birthDate;
	}

	public void setPoint(int point){
		this.point = point;
	}

	public int getPoint(){
		return point;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setExpiryTime(String expiryTime){
		this.expiryTime = expiryTime;
	}

	public String getExpiryTime(){
		return expiryTime;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public String getQrImage() {
		return qrImage;
	}

	public void setQrImage(String qrImage) {
		this.qrImage = qrImage;
	}

	public String getVaNumber() {
		return vaNumber;
	}

	public void setVaNumber(String vaNumber) {
		this.vaNumber = vaNumber;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIsCorporateMember() {
		return isCorporateMember;
	}

	public void setIsCorporateMember(int isCorporateMember) {
		this.isCorporateMember = isCorporateMember;
	}

	public String getVaNumber2() {
		return vaNumber2;
	}

	public void setVaNumber2(String vaNumber2) {
		this.vaNumber2 = vaNumber2;
	}

	@Override
 	public String toString(){
		return 
			"AuthTokenResp{" + 
			"lastName = '" + lastName + '\'' + 
			",gender = '" + gender + '\'' + 
			",authDevice = '" + authDevice + '\'' + 
			",userPic = '" + userPic + '\'' + 
			",backgroundImage = '" + backgroundImage + '\'' + 
			",authToken = '" + authToken + '\'' + 
			",storeId = '" + storeId + '\'' + 
			",birthDate = '" + birthDate + '\'' + 
			",point = '" + point + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",expiryTime = '" + expiryTime + '\'' + 
			",memberId = '" + memberId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}