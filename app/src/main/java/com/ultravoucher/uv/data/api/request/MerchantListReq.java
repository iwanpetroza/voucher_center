package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class MerchantListReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("merchantCategory")
	private String merchantCategory;

	@SerializedName("page")
	private int page;

	@SerializedName("cityId")
	private String cityId;

	@SerializedName("nearBy")
	private String nearBy;

	@SerializedName("order")
	private String order;

	@SerializedName("longitude")
	private String longitude;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setMerchantCategory(String merchantCategory){
		this.merchantCategory = merchantCategory;
	}

	public String getMerchantCategory(){
		return merchantCategory;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	public void setCityId(String cityId){
		this.cityId = cityId;
	}

	public String getCityId(){
		return cityId;
	}

	public void setNearBy(String nearBy){
		this.nearBy = nearBy;
	}

	public String getNearBy(){
		return nearBy;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"MerchantListReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",merchantCategory = '" + merchantCategory + '\'' + 
			",page = '" + page + '\'' + 
			",cityId = '" + cityId + '\'' + 
			",nearBy = '" + nearBy + '\'' + 
			",order = '" + order + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}