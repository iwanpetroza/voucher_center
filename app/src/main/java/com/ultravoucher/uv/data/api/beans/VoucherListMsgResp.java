package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class VoucherListMsgResp implements Serializable {

    @SerializedName("productId")
    @Expose
    private String productId;

    @SerializedName("productName")
    @Expose
    private String productName;

    @SerializedName("productCode")
    @Expose
    private String productCode;

    @SerializedName("productType")
    @Expose
    private String productType;

    @SerializedName("merchantId")
    @Expose
    private String merchantId;

    @SerializedName("merchantName")
    @Expose
    private String merchantName;

    @SerializedName("storeId")
    @Expose
    private String storeId;

    @SerializedName("storeName")
    @Expose
    private String storeName;

    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    @SerializedName("brandId")
    @Expose
    private String brandId;

    @SerializedName("brandName")
    @Expose
    private String brandName;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("isHotProduct")
    @Expose
    private int isHotProduct;

    @SerializedName("premiumPriority")
    @Expose
    private int premiumPriority;

    @SerializedName("voucherClass")
    @Expose
    private int voucherClass;

    @SerializedName("variants")
    @Expose
    private List<VariantVoucherResp> variants = new ArrayList<VariantVoucherResp>();

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsHotProduct() {
        return isHotProduct;
    }

    public void setIsHotProduct(int isHotProduct) {
        this.isHotProduct = isHotProduct;
    }

    public int getPremiumPriority() {
        return premiumPriority;
    }

    public void setPremiumPriority(int premiumPriority) {
        this.premiumPriority = premiumPriority;
    }

    public List<VariantVoucherResp> getVariants() {
        return variants;
    }

    public void setVariants(List<VariantVoucherResp> variants) {
        this.variants = variants;
    }

    public int getVoucherClass() {
        return voucherClass;
    }

    public void setVoucherClass(int voucherClass) {
        this.voucherClass = voucherClass;
    }
}
