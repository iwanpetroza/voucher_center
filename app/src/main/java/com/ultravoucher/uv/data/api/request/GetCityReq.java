package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class GetCityReq {

    @SerializedName("stateProvId")
    @Expose
    private String stateProvId;

    public String getStateProvId() {
        return stateProvId;
    }

    public void setStateProvId(String stateProvId) {
        this.stateProvId = stateProvId;
    }
}
