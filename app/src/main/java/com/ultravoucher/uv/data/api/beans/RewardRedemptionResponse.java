package com.ultravoucher.uv.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class RewardRedemptionResponse {

    @SerializedName("detailResponses")
    @Expose
    private List<DetailResponse> detailResponses = new ArrayList<DetailResponse>();
    @SerializedName("totalPointUsed")
    @Expose
    private int totalPointUsed;
    @SerializedName("currentBalance")
    @Expose
    private int currentBalance;
    @SerializedName("transactionIdReference")
    @Expose
    private String transactionIdReference;

    /**
     *
     * @return
     * The detailResponses
     */
    public List<DetailResponse> getDetailresponses() {
        return detailResponses;
    }

    /**
     *
     * @param detailResponses
     * The detailResponses
     */
    public void setDetailresponses(List<DetailResponse> detailResponses) {
        this.detailResponses = detailResponses;
    }

    /**
     *
     * @return
     * The totalPointUsed
     */
    public int getTotalPointUsed() {
        return totalPointUsed;
    }

    /**
     *
     * @param totalPointUsed
     * The totalPointUsed
     */
    public void setTotalPointUsed(int totalPointUsed) {
        this.totalPointUsed = totalPointUsed;
    }

    /**
     *
     * @return
     * The currentBalance
     */
    public int getCurrentBalance() {
        return currentBalance;
    }

    /**
     *
     * @param currentBalance
     * The currentBalance
     */
    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = currentBalance;
    }

    /**
     *
     * @return
     * The transactionIdReference
     */
    public String getTransactionIdReference() {
        return transactionIdReference;
    }

    /**
     *
     * @param transactionIdReference
     * The transactionIdReference
     */
    public void setTransactionIdReference(String transactionIdReference) {
        this.transactionIdReference = transactionIdReference;
    }
}
