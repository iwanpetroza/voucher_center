package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class BillerCategoryReq{

	@SerializedName("nRecords")
	private int nRecords;

	@SerializedName("page")
	private int page;

	public void setNRecords(int nRecords){
		this.nRecords = nRecords;
	}

	public int getNRecords(){
		return nRecords;
	}

	public void setPage(int page){
		this.page = page;
	}

	public int getPage(){
		return page;
	}

	@Override
 	public String toString(){
		return 
			"BillerCategoryReq{" + 
			"nRecords = '" + nRecords + '\'' + 
			",page = '" + page + '\'' + 
			"}";
		}
}