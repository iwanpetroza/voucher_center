package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by firwandi.ramli on 1/5/2018.
 */

public class ReedemRewardReq {

    @SerializedName("memberId")
    @Expose
    private String memberId;

    @SerializedName("merchantId")
    @Expose
    private String merchantId;

    @SerializedName("actor")
    @Expose
    private String actor;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("storeId")
    @Expose
    private String storeId;

    @SerializedName("rewardId")
    @Expose
    private String rewardId;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("posTransactionId")
    @Expose
    private String posTransactionId;

    /**
     *
     * @return
     * The memberId
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     *
     * @param memberId
     * The memberId
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     *
     * @return
     * The merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     *
     * @param merchantId
     * The merchantId
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     *
     * @return
     * The actor
     */
    public String getActor() {
        return actor;
    }

    /**
     *
     * @param actor
     * The actor
     */
    public void setActor(String actor) {
        this.actor = actor;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPosTransactionId() {
        return posTransactionId;
    }

    public void setPosTransactionId(String posTransactionId) {
        this.posTransactionId = posTransactionId;
    }
}
