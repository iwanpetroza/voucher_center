package com.ultravoucher.uv.data.api.request;

import com.google.gson.annotations.SerializedName;

public class PayByWaletReq{

	@SerializedName("orderNumber")
	private String orderNumber;

	@SerializedName("pin")
	private String pin;

	@SerializedName("transactionId")
	private String transactionId;

	@SerializedName("otp")
	private String otp;

	@SerializedName("memberId")
	private String memberId;

	public void setOrderNumber(String orderNumber){
		this.orderNumber = orderNumber;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public void setPin(String pin){
		this.pin = pin;
	}

	public String getPin(){
		return pin;
	}

	public void setOtp(String otp){
		this.otp = otp;
	}

	public String getOtp(){
		return otp;
	}

	public void setMemberId(String memberId){
		this.memberId = memberId;
	}

	public String getMemberId(){
		return memberId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@Override
 	public String toString(){
		return 
			"PayByWaletReq{" + 
			"orderNumber = '" + orderNumber + '\'' + 
			",pin = '" + pin + '\'' + 
			",otp = '" + otp + '\'' + 
			",memberId = '" + memberId + '\'' + 
			"}";
		}
}