package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VoucherListMsgResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.DetailProductActivity;
import com.ultravoucher.uv.ui.activities.LoginActivity;
import com.ultravoucher.uv.ui.adapters.DigitalVoucherAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/11/2017.
 */

public class DigitalVoucherFragment extends BaseFragment {

    private static final String TAG = DigitalVoucherFragment.class.getSimpleName();

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    protected int page = 0;
    private Handler mHandler;
    protected List<VoucherListMsgResp> mList = new ArrayList<>();
    String status, id;
    private static final String STATUS = "status";
    private static final String ID = "id";
    DigitalVoucherAdapter mAdapter;
    DigitalVoucherPresenter mPresenter;
    String keyword = "";

    public static void showFragment(BaseActivity sourceFragment, String status, String id) {
        DigitalVoucherFragment fragment = new DigitalVoucherFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(STATUS, status);
        fragmentExtras.putString(ID, id);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page= 0;

        status = getArguments().getString(STATUS);
        id = getArguments().getString(ID);
        mPresenter = new DigitalVoucherPresenter(this);

    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch.setHint("Cari nama voucher");

        initRVMyHistoryDeals();
        loadDigitalVoucherList();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String a = etSearch.getText().toString();

                if (a.equals("")) {
                    keyword = "";
                    rvList.setVisibility(View.GONE);
                    mPresenter.presentGetVoucherList(mPresenter.presentGetVoucherListReq());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        /*page= 0;
        initRVMyHistoryDeals();
        loadDigitalVoucherList();*/
    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new DigitalVoucherAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                VoucherListMsgResp product = (VoucherListMsgResp) childView.getTag();
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    getActivity().finish();
                    LoginActivity.startActivity((BaseActivity) getActivity(), false);
                } else {
                    DetailProductActivity.startActivity((BaseActivity) getActivity(), "", status, product.getProductId());
                }

            }

        }));

        /*mHandler = new Handler();

        mAdapter.setOnLoadMoreListener(new DigitalVoucherAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreDigitalVoucherList();
                    }
                }, 2000);

            }
        });*/
    }

    private void loadDigitalVoucherList() {
        mPresenter.presentGetVoucherList(mPresenter.presentGetVoucherListReq());
    }

    private void loadMoreDigitalVoucherList() {
        mPresenter.presentGetVoucherListLoadMore(mPresenter.presentGetVoucherListReq());
    }

    @OnClick(R.id.btnSearch)
    void clicked() {
        if (etSearch.getText().toString().equals("")) {
            showToast("Masukkan kata kunci");
        } else {
            page = 0;
            keyword = etSearch.getText().toString();
            rvList.setVisibility(View.GONE);
            mPresenter.presentGetVoucherList(mPresenter.presentGetVoucherListReq());
        }
    }
}
