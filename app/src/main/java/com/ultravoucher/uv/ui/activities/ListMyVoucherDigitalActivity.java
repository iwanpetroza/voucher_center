package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.myvoucher.ListMyVoucherDigitalFragment;
import com.ultravoucher.uv.ui.fragments.myvoucher.ListVoucherUVFragment;

import butterknife.BindView;

public class ListMyVoucherDigitalActivity extends BaseActivity {

    private static final String VCLASS = "vclass";
    private static final String POS = "pos";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;

//    DealsDetailResp deals;
    String position, vClass;

    public static void startActivity(BaseActivity sourceActivity, String pos, String vClass){
        Intent i = new Intent(sourceActivity, ListMyVoucherDigitalActivity.class);
        i.putExtra(POS, pos);
        i.putExtra(VCLASS, vClass);
        sourceActivity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*POS 3 = VOUCHER CLASS 1 KHUSUS UV*/
        position = getIntent().getStringExtra(POS);
        vClass = getIntent().getStringExtra(VCLASS);

        setupToolbar();
        /*if (vClass.equals("0")) {
            ListMyVoucherDigitalFragment.showFragment(this, "", "0");
        } else */

        if (vClass.equals("3")) {
            ListVoucherUVFragment.showFragment(this, "", "0");
        }
        else {
            ListMyVoucherDigitalFragment.showFragment(this, "", position);
        }

    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("List Voucher");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
