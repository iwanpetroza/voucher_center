package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.OrderReq;
import com.ultravoucher.uv.data.api.beans.VoucherListMsgResp;
import com.ultravoucher.uv.data.api.request.StoreByIdReq;
import com.ultravoucher.uv.data.api.response.VoucherListResponse;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/18/2017.
 */

public class DigitalVoucherPresenter {

    private DigitalVoucherFragment mFragment;

    public DigitalVoucherPresenter(DigitalVoucherFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetVoucherList(StoreByIdReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<VoucherListResponse> voucherListResponseCall = api.getStoreById(contentType, req);
        voucherListResponseCall.enqueue(new Callback<VoucherListResponse>() {
            @Override
            public void onResponse(Call<VoucherListResponse> call, Response<VoucherListResponse> response) {
                if (response.code() == 200) {
                    VoucherListResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("PROD101")) {
                        if (!resp.getProducts().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                initList(resp.getProducts());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                swapped(resp.getProducts());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            }


                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<VoucherListResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }


    protected void presentGetVoucherListLoadMore(final StoreByIdReq req) {
        /*mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));*/

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<VoucherListResponse> voucherListResponseCall = api.getStoreById(contentType, req);
        voucherListResponseCall.enqueue(new Callback<VoucherListResponse>() {
            @Override
            public void onResponse(Call<VoucherListResponse> call, Response<VoucherListResponse> response) {
                if (response.code() == 200) {
                    VoucherListResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("PROD101")) {
                        if (!resp.getProducts().isEmpty()) {
                            addToList(resp.getProducts());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mList.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<VoucherListResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);

                }
            }
        });
    }

    protected void initList(List<VoucherListMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void swapped(List<VoucherListMsgResp> data) {
        mFragment.mAdapter.swap(data);
    }

    protected void addToList(List<VoucherListMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (VoucherListMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected StoreByIdReq presentGetVoucherListReq() {
        StoreByIdReq req = new StoreByIdReq();
        OrderReq order = new OrderReq();
        req.setId(mFragment.id);
        req.setKeyword(mFragment.keyword);
        req.setPage(mFragment.page);
        order.setSortCreatedDate("");
        order.setSortName("");
        order.setSortPrice("");
        req.setOrder(order);
        req.setNRecords(100);
        req.setIsDeals(-1);
        req.setProductShownType("ANDROID");
        if (mFragment.status.equals("0")) {
            req.setVoucherClass("D");
        } else {
            req.setVoucherClass("P");
        }

        return req;
    }
}
