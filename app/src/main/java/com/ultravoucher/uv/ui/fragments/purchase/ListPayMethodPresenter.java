/*
package com.indivara.vouchercenter.ui.fragments.purchase;

import android.view.View;

import com.indivara.vouchercenter.R;
import com.indivara.vouchercenter.data.api.VoucherApi;
import com.indivara.vouchercenter.data.api.beans.ListPaymentMethodMsgResp;
import com.indivara.vouchercenter.data.api.request.ListPaymentMethodReq;
import com.indivara.vouchercenter.data.api.response.ListPaymentMethodResponse;
import com.indivara.vouchercenter.data.prefs.PrefHelper;
import com.indivara.vouchercenter.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

*/
/**
 * Created by firwandi.ramli on 10/30/2017.
 *//*


public class ListPayMethodPresenter {

    private ListPayMethodFragment mFragment;

    public ListPayMethodPresenter(ListPayMethodFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetListPayMethod(ListPaymentMethodReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListPaymentMethodResponse> listPaymentMethodResponseCall = api.getListPayMethod(contentType, req);
        listPaymentMethodResponseCall.enqueue(new Callback<ListPaymentMethodResponse>() {
            @Override
            public void onResponse(Call<ListPaymentMethodResponse> call, Response<ListPaymentMethodResponse> response) {
                if (response.code() == 200) {
                    ListPaymentMethodResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("MEM002")) {
                        if (!resp.getAvailablePaymentMethod().isEmpty()) {
                            initList(resp.getAvailablePaymentMethod());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListPaymentMethodResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void presentGetListPayMethodloadMore(ListPaymentMethodReq req) {

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListPaymentMethodResponse> listPaymentMethodResponseCall = api.getListPayMethod(contentType, req);
        listPaymentMethodResponseCall.enqueue(new Callback<ListPaymentMethodResponse>() {
            @Override
            public void onResponse(Call<ListPaymentMethodResponse> call, Response<ListPaymentMethodResponse> response) {
                if (response.code() == 200) {
                    ListPaymentMethodResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("MEM002")) {
                        if (!resp.getAvailablePaymentMethod().isEmpty()) {
                            addToList(resp.getAvailablePaymentMethod());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mList.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListPaymentMethodResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);

                }
            }
        });
    }

    protected void initList(List<ListPaymentMethodMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<ListPaymentMethodMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (ListPaymentMethodMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected ListPaymentMethodReq presentGetListReq() {
        ListPaymentMethodReq req = new ListPaymentMethodReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setPurchasedVoucherClass(Integer.parseInt(mFragment.vClass));
        return req;
    }
}
*/
