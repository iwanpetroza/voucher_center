package com.ultravoucher.uv.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ultravoucher.uv.ui.fragments.mainpage.HomePageFragment;

/**
 * Created by firwandi.ramli on 9/28/2017.
 */

public class PurchaseVoucherPagerAdapter extends FragmentStatePagerAdapter {

//    private static final String TAG = StoreDetailPagerAdapter.class.getSimpleName();

    private int mNumOfTabs;

    public PurchaseVoucherPagerAdapter(FragmentManager fm, int numOfTabs){
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment =null;
        switch (position){
            case 0:
                return HomePageFragment.showTabFragment();
            case 1:
                return HomePageFragment.showTabFragment();
            case 2:
                return HomePageFragment.showTabFragment();
            default:
                return fragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
