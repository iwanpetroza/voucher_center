package com.ultravoucher.uv.ui.fragments.myvoucher;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.DealsDetailMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GenerateQRCodeActivity;
import com.ultravoucher.uv.ui.adapters.ListMyVoucherDigitalAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 10/26/2017.
 */

public class ListMyVoucherDigitalFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ListMyVoucherDigitalFragment.class.getSimpleName();
    private static final String DEALS_PRODUCT = "deals_product";
    private static final String POS = "pos";

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    @BindView(R.id.ll)
    LinearLayout ll;

    List<DealsDetailMsgResp> mList = new ArrayList<>();
    ListMyVoucherDigitalPresenter mPresenter;
    ListMyVoucherDigitalAdapter mAdapter;
    String loc = "";
    String logoMerchant = "";
    //int position;
    protected int page = 0;
    private Handler handler;


    public static void showFragment(BaseActivity sourceFragment, String resp, String pos) {
        ListMyVoucherDigitalFragment fragment = new ListMyVoucherDigitalFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(POS, pos);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        loc = getArguments().getString(POS);
        mPresenter = new ListMyVoucherDigitalPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_myvoucher_digital;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);
        initRVMyVoucher();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                rvList.setVisibility(View.GONE);

            }
        });
        mPresenter.presentGetVoucherGrouping(mPresenter.presentGetGroupingList());
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
    }

    protected void initRVMyVoucher() {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new ListMyVoucherDigitalAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                DealsDetailMsgResp product = (DealsDetailMsgResp) childView.getTag();
                GenerateQRCodeActivity.startActivity((BaseActivity) getActivity(), logoMerchant, product.getProductName(), product.getVoucherId(), product.getVoucherCode(), product.getVoucherExpiredDate(), "D", "");
            }

        }));
    }

    private void loadMyVoucherDetail() {
        mPresenter.presentGetVoucherGrouping(mPresenter.presentGetGroupingList());
    }

    @Override
    public void onRefresh() {
        page = 0;
        rvList.setVisibility(View.GONE);
        mPresenter.presentGetVoucherGrouping(mPresenter.presentGetGroupingList());
    }
}
