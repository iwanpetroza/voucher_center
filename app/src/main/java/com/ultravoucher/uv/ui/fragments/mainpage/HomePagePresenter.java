package com.ultravoucher.uv.ui.fragments.mainpage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.AdvertisingReq;
import com.ultravoucher.uv.data.api.request.CheckMemberPointsReq;
import com.ultravoucher.uv.data.api.request.MemberPocketReq;
import com.ultravoucher.uv.data.api.request.MemberWaletReq;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;
import com.ultravoucher.uv.data.api.response.MemberPocketResponse;
import com.ultravoucher.uv.data.api.response.MemberPointBalanceResponse;
import com.ultravoucher.uv.data.api.response.MemberWaletResponse;
import com.ultravoucher.uv.data.api.response.UploadPhotoResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.utils.AmountFormatter2;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/8/2017.
 */

public class HomePagePresenter {

    private HomePageFragment mFragment;

    public HomePagePresenter(HomePageFragment mFragment) {
        this.mFragment = mFragment;
    }

    void presentAdvBanner() {

        AdvertisingReq req = new AdvertisingReq();
        req.setAdvertisingType(2);
        req.setCityId("");

        Call<AdvertisingResponse> advertisingResponseCall = mFragment.getVoucherApi().getDetailAds(req);
        advertisingResponseCall.enqueue(new Callback<AdvertisingResponse>() {
            @Override
            public void onResponse(Call<AdvertisingResponse> call, Response<AdvertisingResponse> response) {
                AdvertisingResponse resp = response.body();
                if (response.code() == 200) {
                    if(resp.getAbstractResponse().getResponseStatus().equals("INQ000"))
                    {
                        mFragment.setData(resp.getAdvertisingResponse());
                    }
                    else
                    {
                        mFragment.showToast("Eror Bukan INQ000");
                    }
                } else {
                    mFragment.showToast("Eror Bukan 200");
                }

                getMemberPocket(constructGetPocketReq());

            }

            @Override
            public void onFailure(Call<AdvertisingResponse> call, Throwable t) {
                mFragment.showToast("Eror Koneksi");
                getMemberPocket(constructGetPocketReq());
            }
        });
    }

    void getMemberPocket(final MemberPocketReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberPocketResponse> respCall = api.getMemberPocket(authToken, deviceUniqueId, req);
        respCall.enqueue(new Callback<MemberPocketResponse>() {
            @Override
            public void onResponse(Call<MemberPocketResponse> call, Response<MemberPocketResponse> response) {
                if(response.code() == 200){
                    mFragment.mycartPbLoading.setVisibility(View.INVISIBLE);
                    MemberPocketResponse resp = response.body();
                    int pocket;

                    if(resp.getAbstractResponse().getResponseStatus() != null) {
                        if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {

                            pocket = resp.getPockets().getBalance();
                            /*String walet = Integer.toString(pocket);
                            if (walet.equalsIgnoreCase("") || walet == null) {
                                mFragment.tvPocket.setText("Failed");
                            } else {*/
                            mFragment.tvWalet.setText("" + AmountFormatter2.format(pocket));
//                            }

//                    } else if(resp.getAbstractResponse().getResponseStatus().equals("MEM903")){
//                        mFragment.tvPocket.setText("Failed");
                        } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            mFragment.tvWalet.setText("Gagal");
                        }

                        else if (resp.getAbstractResponse().getResponseStatus().equals("ERR123")) {
                            mFragment.tvWalet.setText("Gagal");
//                    } else if(resp.getAbstractResponse().getResponseStatus().equals("MEM906")){
//                        mFragment.tvPocket.setText("Invalid PIN");
                        } else {
                            mFragment.tvWalet.setText("Gagal");
                        }
                    } else {
                        mFragment.showToast("Trouble with Our Payment System");
                        mFragment.tvWalet.setText("Gagal");
                    }
                } else if (response.code() == 400) {
//                    mFragment.showToast(mFragment.CONNECTION_ERROR);
                    mFragment.tvWalet.setText("Gagal");
                    /*try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                } else {
//                    if(mFragment!= null && mFragment.isAdded()) {
//                        String message = mFragment.getString(R.string.connection_error);
//                        mFragment.showToast(message);
                        mFragment.showToast("Trouble with Our Payment System");
                        mFragment.tvWalet.setText("Need PIN");
//                    }
                }
                mFragment.mycartPbLoading.setVisibility(View.INVISIBLE);
                mFragment.dismissProgressDialog();
                getmemberBalance(constructMemberBalance());
            }

            @Override
            public void onFailure(Call<MemberPocketResponse> call, Throwable t) {
                mFragment.mycartPbLoading.setVisibility(View.INVISIBLE);
                mFragment.tvWalet.setText("Error");
                mFragment.dismissProgressDialog();
                getmemberBalance(constructMemberBalance());
            }
        });
    }

    MemberPocketReq constructGetPocketReq() {
        String memberId = PrefHelper.getString(PrefKey.MEMBERID);
        MemberPocketReq request = new MemberPocketReq();
        request.setMemberId(memberId);
        return request;
    }

    /*void getmemberWalet(final MemberWaletReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberWaletResponse> memberWaletResponseCall = api.getMemberWalet(contentType, authToken, dui, req);
        memberWaletResponseCall.enqueue(new Callback<MemberWaletResponse>() {
            @Override
            public void onResponse(Call<MemberWaletResponse> call, Response<MemberWaletResponse> response) {

                if (response.code() == 200) {
                    MemberWaletResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        mFragment.tvWalet.setText("" + AmountFormatter2.format(String.valueOf(resp.getTotalBalance())));
                    } else {
                        mFragment.tvWalet.setText("GAGAL");
                    }
                } else {
                    mFragment.tvWalet.setText("GAGAL");
                    mFragment.showToast(mFragment.CONNECTION_ERROR);
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<MemberWaletResponse> call, Throwable t) {
                mFragment.tvWalet.setText("GAGAL");
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }

    MemberWaletReq constructMemberWalet() {
        MemberWaletReq req = new MemberWaletReq();
        req.setPage(0);
        req.setNRecords(10);
        req.setVoucherClass(2);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }*/

    void getmemberBalance(final MemberWaletReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);
        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberWaletResponse> memberWaletResponseCall = api.getMemberWalet(contentType, authToken, dui, req);
        memberWaletResponseCall.enqueue(new Callback<MemberWaletResponse>() {
            @Override
            public void onResponse(Call<MemberWaletResponse> call, Response<MemberWaletResponse> response) {

                if (response.code() == 200) {
                    MemberWaletResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
//                        if (String.valueOf(resp.getTotalBalance()) != null) {
                            mFragment.tvBalance.setText("" + AmountFormatter2.format(String.valueOf(resp.getTotalBalance())));
//                        }else{
//                            mFragment.tvBalance.setText("0");
//                        }

                    } else if (resp.getAbstractResponse().getResponseStatus().equals("MEM903")){
                        mFragment.tvBalance.setText("0");
                    } else {

                    }
                } else {
                    mFragment.tvBalance.setText("GAGAL");
//                    mFragment.showToast();
                }
                mFragment.dismissProgressDialog();
                checkmemberBalance(constructMemberPoint());
            }

            @Override
            public void onFailure(Call<MemberWaletResponse> call, Throwable t) {
                mFragment.tvBalance.setText("GAGAL");
//                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
                checkmemberBalance(constructMemberPoint());
            }
        });
    }

    MemberWaletReq constructMemberBalance() {
        MemberWaletReq req = new MemberWaletReq();
        req.setPage(0);
        req.setNRecords(10);
        req.setVoucherClass(1);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }

    void presentUploadProfilePic(File filedata){

        RequestBody memberId = RequestBody.create(MediaType.parse("multipart/form-data"), PrefHelper.getString(PrefKey.MEMBERID));
//        Log.d("Request memberId", ""+PrefHelper.getString(PrefKey.MEMBERID));
//        Log.d("Parse memberId", ""+memberId);
        //RequestBody requestfile = RequestBody.create(MediaType.parse("multipart/form-data"), filedata);
//        Log.d("Request File", ""+filedata);
        //Log.d("Parse File", ""+requestfile);

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filedata);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Bitmap bm = BitmapFactory.decodeStream(fis);
      //  Log.e("ImageString",""+data(bm));
//        long fileSize = filedata.length()/1024;
//        Log.d("File Size", "File size is "+fileSize+" Kb");
//        int maxSize = 1024;

        VoucherApi service = mFragment.getVoucherApi();
        Call<UploadPhotoResp> call = service.doUploadPhoto(memberId,
                constructPartFile(filedata, "image/*", "file"));
        call.enqueue(new Callback<UploadPhotoResp>() {
            @Override
            public void onResponse(Call<UploadPhotoResp> call, Response<UploadPhotoResp> response) {
                if(response.code() == 200){
                    UploadPhotoResp resp = response.body();
                    PrefHelper.setString(PrefKey.USER_PIC, resp.getAbstractResponse().getUrlImage());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(mFragment.getActivity().getApplicationContext()).load(PrefHelper.getString(PrefKey.USER_PIC))
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder).fit().centerCrop()
                                    .into(mFragment.civLogo);
                        }
                    }, 1500);
                    mFragment.showToast("Image upload success");
                }
            }

            @Override
            public void onFailure(Call<UploadPhotoResp> call, Throwable t) {
//                Log.d("Failure", "onFailure: fail = " + t.getMessage());
                mFragment.showToast(""+ t.getMessage());
            }
        });
    }
    public MultipartBody.Part constructPartFile(File file, String type, String tag) {
        RequestBody requestBody = RequestBody.create(MediaType.parse(type), file);
        return MultipartBody.Part.createFormData(tag, file.getName(), requestBody);
    }

    // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        Log.e("ImageString",""+stream.toByteArray());
        return stream.toByteArray();
    }

    // get the base 64 string
    public String data(Bitmap someImg)
    {
        String imgString = Base64.encodeToString(getBytesFromBitmap(someImg),
                Base64.NO_WRAP);

        return imgString;
    }

    void checkmemberBalance(CheckMemberPointsReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberPointBalanceResponse> memberPointBalanceResponseCall = api.checkMemberPoint(contentType, authToken, deviceUId, req);
        memberPointBalanceResponseCall.enqueue(new Callback<MemberPointBalanceResponse>() {
            @Override
            public void onResponse(Call<MemberPointBalanceResponse> call, Response<MemberPointBalanceResponse> response) {
                if (response.code() == 200) {
                    MemberPointBalanceResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        mFragment.tvPoin.setText("" + String.valueOf(resp.getTotalPoints()) + " Poin");
                    } else {
                        if (PrefHelper.getString(PrefKey.POIN).equals("") || PrefHelper.getString(PrefKey.POIN) == null) {
                            mFragment.tvPoin.setText("0 Poin");
                        } else {
                            mFragment.tvPoin.setText(PrefHelper.getString(PrefKey.POIN) + " Poin");
                        }
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<MemberPointBalanceResponse> call, Throwable t) {
                if (PrefHelper.getString(PrefKey.POIN).equals("") || PrefHelper.getString(PrefKey.POIN) == null) {
                    mFragment.tvPoin.setText("0 Poin");
                } else {
                    mFragment.tvPoin.setText(PrefHelper.getString(PrefKey.POIN) + " Poin");
                }

                mFragment.dismissProgressDialog();
            }
        });
    }

    CheckMemberPointsReq constructMemberPoint() {
        CheckMemberPointsReq req = new CheckMemberPointsReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(50);
        req.setOrder("");
        req.setPage(0);
        return req;
    }


}
