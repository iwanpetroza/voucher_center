package com.ultravoucher.uv.ui.fragments.biller;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.BillerHistory;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.api.request.BillerHistoryReq;
import com.ultravoucher.uv.data.api.request.HistoryProductReq;
import com.ultravoucher.uv.data.api.response.BillerHistoryResponse;
import com.ultravoucher.uv.data.api.response.HistoryProductResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.history.HistoryProductFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerHistoryPresenter {

    private BillerHistoryFragment mFragment;

    public BillerHistoryPresenter(BillerHistoryFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetHistoryProduct(BillerHistoryReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<BillerHistoryResponse> historyProductResponseCall = api.listHistoryBiller(contentType, authToken, deviceUId, req);
        historyProductResponseCall.enqueue(new Callback<BillerHistoryResponse>() {
            @Override
            public void onResponse(Call<BillerHistoryResponse> call, Response<BillerHistoryResponse> response) {
                if (response.code() == 200) {
                    BillerHistoryResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("BL001")) {
                        if (!resp.getBillerHistory().isEmpty()) {
                            initList(resp.getBillerHistory());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<BillerHistoryResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }

    protected void initList(List<BillerHistory> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected BillerHistoryReq constructGetListHistoryProduct() {
        BillerHistoryReq req = new BillerHistoryReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }

}
