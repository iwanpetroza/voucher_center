package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.fragments.history.HistoryProductFragment;
import com.ultravoucher.uv.utils.AmountFormatter2;
import com.ultravoucher.uv.utils.DateFormatter;
import com.ultravoucher.uv.utils.DateTimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.iwgang.countdownview.CountdownView;

/**
 * Created by firwandi.ramli on 3/13/2018.
 */

public class HistoryProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private HistoryProductFragment mContext;
    private List<Order> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private long different;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public HistoryProductAdapter(HistoryProductFragment context, List<Order> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_product, parent, false);

            vh = new ListHistoryProductHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListHistoryProductHolder) {
            final Order item = mDataset.get(position);

            if (item.getPaymentExpiredDate() == null || item.getPaymentExpiredDate().equals("") || item.getPaymentExpiredDate().equals("-")) {
                ((ListHistoryProductHolder) holder).countDown.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvCD.setText("Batas Waktu  :  -");
            } else {
                getMilis(item.getPaymentExpiredDate());
                ((ListHistoryProductHolder) holder).tvCD.setText("Batas Waktu  :  ");
                ((ListHistoryProductHolder) holder).countDown.start(different);
            }

            ((ListHistoryProductHolder) holder).itemView.setTag(item);
            ((ListHistoryProductHolder) holder).tvOrderNumber.setText("No. Order : " + item.getOrderNumber());

            if (item.getCheckoutDate().equals("") || item.getCheckoutDate().equals("-")) {
                ((ListHistoryProductHolder) holder).tvOrderDate.setText("Tgl Order : -");
            } else {
                ((ListHistoryProductHolder) holder).tvOrderDate.setText("Tgl Order : " + DateFormatter.format(item.getCheckoutDate()));
            }

            ((ListHistoryProductHolder) holder).tvTotalPayment.setText("Total Pembayaran : Rp. " + AmountFormatter2.format(item.getTotalAmount()));
            ((ListHistoryProductHolder) holder).tvVA.setText("Transfer ke nomor VA : " + item.getVaNumber());

            if (item.getStatus() == 5) {
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Menunggu Pembayaran");
                ((ListHistoryProductHolder) holder).tvStatus.setTextColor(mContext.getResources().getColor(R.color.green_600));
            } else if (item.getStatus() == 6) {
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Transaksi Expired - Gagal Bayar");
                ((ListHistoryProductHolder) holder).tvStatus.setTextColor(mContext.getResources().getColor(R.color.red_700));
            } else {
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : -");
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListHistoryProductHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvOrderNumber)
        TextView tvOrderNumber;

        @BindView(R.id.tvOrderDate)
        TextView tvOrderDate;

        @BindView(R.id.tvTotalPayment)
        TextView tvTotalPayment;

        @BindView(R.id.tvVA)
        TextView tvVA;

        @BindView(R.id.tvStatus)
        TextView tvStatus;

        @BindView(R.id.tvCD)
        TextView tvCD;

        @BindView(R.id.countDown)
        CountdownView countDown;


        public ListHistoryProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    //GET ELAPSED TIME IN MILIS

    public void getMilis(String tgl) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timePresent = df.format(c);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        try {

            Date date1 = simpleDateFormat.parse(tgl);
            Date date2 = simpleDateFormat.parse(timePresent);

            different = date1.getTime() - date2.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
