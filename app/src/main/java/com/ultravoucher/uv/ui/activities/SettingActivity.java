package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.a_setting)
    RelativeLayout aSetting;

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, SettingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_setting;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
    }

    @OnClick({R.id.tvRegisMerchant, R.id.tvChangeProfile, R.id.tvChangePass, R.id.tvLogout, R.id.tvPickVoucher, R.id.tvContactUs, R.id.tvCredit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvRegisMerchant:
                VerificationActivity.startActivity(this, "RM", "");
                break;
            case R.id.tvChangeProfile:
                GeneralActivity.startActivity(this, "profile");
                break;
            case R.id.tvChangePass:
                GeneralActivity.startActivity(this, "changepass");
                break;
            case R.id.tvLogout:
                PrefHelper.clearAllPreferences();
                finish();
                LoginActivity.startActivity(this, false);
                break;
            case R.id.tvPickVoucher:
                GeneralActivity.startActivity(this, "howto");
                break;
            case R.id.tvContactUs:
                ContactUsActivity.startActivity(this);
                break;
            case R.id.tvCredit:
//                GeneralActivity.startActivity(this, "credit");
//                break;
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Setting");
        toolbarTitle.setAllCaps(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;

            /*case R.id.action_cart:
                CheckOutActivity.startActivity(this);
                return true;*/
        }
        return super.onOptionsItemSelected(item);


    }
}
