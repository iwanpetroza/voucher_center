package com.ultravoucher.uv.ui.fragments.myvoucher;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.DealsDetailResp;
import com.ultravoucher.uv.data.api.request.DigitalPocketReq;
import com.ultravoucher.uv.data.api.response.DealsDetailResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/9/2017.
 */

public class ListDigitalGroupPresenter {

    private ListDigitalGroupFragment mFragment;

    public ListDigitalGroupPresenter(ListDigitalGroupFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetVoucherGrouping(DigitalPocketReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailResponse> dealsDetailResponseCall = api.getListDigitalGrouping(contentType, authToken, dui, req);
        dealsDetailResponseCall.enqueue(new Callback<DealsDetailResponse>() {
            @Override
            public void onResponse(Call<DealsDetailResponse> call, Response<DealsDetailResponse> response) {

                if (response.code() == 200) {
                    DealsDetailResponse resp = response.body();
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            initList(resp.getDeals());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                swapped(resp.getDeals());
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                    }

                    else {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }

                    mFragment.swipe_container.setRefreshing(false);

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                        mFragment.swipe_container.setRefreshing(false);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<DealsDetailResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }


    protected void presentGetVoucherGroupingLoadMore(DigitalPocketReq req) {

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailResponse> dealsDetailResponseCall = api.getListDigitalGrouping(contentType, authToken, dui, req);
        dealsDetailResponseCall.enqueue(new Callback<DealsDetailResponse>() {
            @Override
            public void onResponse(Call<DealsDetailResponse> call, Response<DealsDetailResponse> response) {

                if (response.code() == 200) {
                    DealsDetailResponse resp = response.body();
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            addToList(resp.getDeals());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
            }

            @Override
            public void onFailure(Call<DealsDetailResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });
    }

    protected void swapped(List<DealsDetailResp> data) {
        mFragment.mAdapter.swap(data);
    }

    protected void initList(List<DealsDetailResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<DealsDetailResp> list) {
        mFragment.mAdapter.notifyDataSetChanged();
        for (DealsDetailResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected DigitalPocketReq presentGetGroupingList() {
        DigitalPocketReq req = new DigitalPocketReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(5);
        req.setPage(mFragment.page);
        req.setSVoucherClass("");
        req.setVoucherClass(2);
        return req;
    }
}
