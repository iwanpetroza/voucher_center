package com.ultravoucher.uv.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.phase2.HomeActivity;

import butterknife.BindView;

public class SplashActivity extends BaseActivity {

    @BindView(R.id.splash_background)
    ImageView splashBackground;

    @BindView(R.id.tvVersion)
    TextView tvVersion;

    @Override
    protected int getLayout() {
        return R.layout.a_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        tvVersion.setText("v " + versionName + "." + versionCode);

        Picasso.with(getApplicationContext()).load(R.drawable.bg_splash).fit()
                .into(splashBackground, new Callback() {
                    @Override
                    public void onSuccess() {}

                    @Override
                    public void onError() {}
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PrefHelper.getBoolean(PrefKey.IS_LOGIN)) {
                    HomePageActivity.startActivity(SplashActivity.this, "");
//                    HomeActivity.startActivity(SplashActivity.this);
                } else {
//                    HomeActivity.startActivity(SplashActivity.this);
                    HomePageActivity.startActivity(SplashActivity.this, "");
                }
                finish();
            }
        }, 3000);

    }
}
