package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Firwandi S Ramli on 13/04/18.
 */

public class HowtoTopupDialogFragment extends BaseDialogFragment {

    private static final String TAG = HowtoTopupDialogFragment.class.getSimpleName();

    @BindView(R.id.img)
    ImageView img;

    public static void showDialog(BaseActivity fragment){
        HowtoTopupDialogFragment dialog = new HowtoTopupDialogFragment();
        dialog.show(fragment.getSupportFragmentManager(), TAG);
    }

    @Override
    protected int getLayout() {
        return R.layout.d_howto_pickup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        img.setVisibility(View.VISIBLE);
        Picasso.with(getContext()).load(R.drawable.cara_pickup).fit().into(img);
    }

    @OnClick(R.id.ivClose)
    void close() {

        getDialog().dismiss();

    }
}
