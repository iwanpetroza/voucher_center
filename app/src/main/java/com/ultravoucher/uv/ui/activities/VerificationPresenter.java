package com.ultravoucher.uv.ui.activities;

import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.ChangeForgotPassReq;
import com.ultravoucher.uv.data.api.request.LoginReq;
import com.ultravoucher.uv.data.api.request.VerificationReq;
import com.ultravoucher.uv.data.api.response.ChangeForgotPassResponse;
import com.ultravoucher.uv.data.api.response.VerificationResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class VerificationPresenter {

    private VerificationActivity mActivity;
    LoginPresenter loginPresenter;

    public VerificationPresenter(VerificationActivity mActivity) {
        this.mActivity = mActivity;
    }

    public void doVerification(final VerificationReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);
        String contentType = "application/json";

        VoucherApi api = mActivity.getVoucherApi();
        final Call<VerificationResp> verificationRespCall = api.doVerification(contentType, req);
        verificationRespCall.enqueue(new Callback<VerificationResp>() {
            @Override
            public void onResponse(Call<VerificationResp> call, Response<VerificationResp> response) {
                if (response.code() == 200) {
                    VerificationResp resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("MEM000")) {
//                        doLogin();
//                        HomePageActivity.startActivity(mActivity, "");
                        PrefHelper.setString(PrefKey.INITIAL_RUN, "first");
                        mActivity.finish();
                        IntroductionActivity.startActivity(mActivity, "a");
//                        doLogin();
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("")) {
                        //Todo change page
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                }else {
                    mActivity.showToast("Unstable Connection");
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<VerificationResp> call, Throwable t) {
                mActivity.dismissProgressDialog();
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });
    }

    VerificationReq constructVerificationReq() {
        VerificationReq req = new VerificationReq();
        req.setUsername(PrefHelper.getString(PrefKey.USERNAME));
        req.setCode(mActivity.etPin.getText().toString());
        return req;
    }

    /*void doLogin(){
        loginPresenter = new LoginPresenter(mActivity);

        final String deviceId = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String username = PrefHelper.getString(PrefKey.USERNAME);
        String password = PrefHelper.getString(PrefKey.PASSWORD);


        LoginReq loginReq = new LoginReq();
        loginReq.setDeviceUniqueId(deviceId);
        loginReq.setUsername(username);
        loginReq.setPassword(password);

        loginPresenter.login(loginReq);
        Log.d("Cek", "doLogin: " + "Login");
    }*/

    void doResetPassword(){

        String contentType = "application/json";
//        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
//        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mActivity.getVoucherApi();
        ChangeForgotPassReq request = new ChangeForgotPassReq();
        request.setUsername(PrefHelper.getString(PrefKey.USERNAME));
        request.setVerificationCode(mActivity.vercode);
        request.setPassword(mActivity.registrationEtPassword.getText()+"");

        Call<ChangeForgotPassResponse> respCall = api.doChangePassword(contentType, request);

        respCall.enqueue(new Callback<ChangeForgotPassResponse>() {
            @Override
            public void onResponse(Call<ChangeForgotPassResponse> call, Response<ChangeForgotPassResponse> response) {
                if (response.code() == 200) {
                    ChangeForgotPassResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("MEM005")){
                        PrefHelper.setString(PrefKey.PASSWORD, mActivity.registrationEtPassword.getText()+"");
                        Toast.makeText(mActivity,
                                "Reset Password Success", Toast.LENGTH_SHORT).show();
//                        mActivity.finish();
                        doLogin();

                    } else {
                        mActivity.showToast("Please Check Your Email");
                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ChangeForgotPassResponse> call, Throwable t) {
                mActivity.showToast("Unstable Connection");
            }
        });

    }

    void doReset() {
        if (mActivity.etPin.getText().toString().equals(mActivity.vercode)) {
            Log.d("Code pref hasil", PrefHelper.getString(PrefKey.VERIFICATION_CODE)+"");
            Log.d("Code pref hasil", mActivity.status+"");
            doResetPassword();
        } else {
            mActivity.showToast("Maaf, Kode verifikasi yang Anda masukkan salah.");
        }
    }

    void doLogin(){
        loginPresenter = new LoginPresenter(mActivity);

        final String deviceId = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String username = PrefHelper.getString(PrefKey.USERNAME);
        String password = PrefHelper.getString(PrefKey.PASSWORD);


        LoginReq loginReq = new LoginReq();
        loginReq.setDeviceUniqueId(deviceId);
        loginReq.setUsername(username);
        loginReq.setPassword(password);

        loginPresenter.login(loginReq);
        Log.d("Cek", "doLogin: " + "Login");
    }



}
