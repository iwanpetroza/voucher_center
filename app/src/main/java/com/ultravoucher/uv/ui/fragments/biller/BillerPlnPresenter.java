package com.ultravoucher.uv.ui.fragments.biller;

import com.google.gson.Gson;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.InisiateBillerResp;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tunggul.jati on 6/6/2018.
 */

public class BillerPlnPresenter {

    BillerPlnFragment mFragment;

    public BillerPlnPresenter(BillerPlnFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getInisiateData(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId("");
        request.setPin("");
        request.setContractNo("");
        request.setBillerId("");
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InisiateBillerResp> respCall = mFragment.getVoucherApi().inisiateBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InisiateBillerResp>() {
            @Override
            public void onResponse(Call<InisiateBillerResp> call, Response<InisiateBillerResp> response) {
                if(response.code()==200)
                {
                    InisiateBillerResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL003"))
                    {
                        mFragment.price20 = resp.getAmount().getIdPlnPre20();
                        mFragment.price50 = resp.getAmount().getIdPlnPre50();
                        mFragment.price100 = resp.getAmount().getIdPlnPre100();
                        mFragment.price200 = resp.getAmount().getIdPlnPre200();
                        mFragment.price500 = resp.getAmount().getIdPlnPre500();
                        mFragment.price1jt = resp.getAmount().getIdPlnPre1000();

                        mFragment.fee20 = resp.getFee().getIdPlnPre20();
                        mFragment.fee50 = resp.getFee().getIdPlnPre50();
                        mFragment.fee100 = resp.getFee().getIdPlnPre100();
                        mFragment.fee200 = resp.getFee().getIdPlnPre200();
                        mFragment.fee500 = resp.getFee().getIdPlnPre500();
                        mFragment.fee1jt = resp.getFee().getIdPlnPre1000();

                        mFragment.active20 = resp.getActive().getIdPlnPre20();
                        mFragment.active50 = resp.getActive().getIdPlnPre50();
                        mFragment.active100 = resp.getActive().getIdPlnPre100();
                        mFragment.active200 = resp.getActive().getIdPlnPre200();
                        mFragment.active500 = resp.getActive().getIdPlnPre500();
                        mFragment.active1jt = resp.getActive().getIdPlnPre1000();

                        mFragment.inisiateHit();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
//                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InisiateBillerResp> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }

    void getCheckBiller(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin("");
        /*test*/
        request.setContractNo(mFragment.etIdPel.getText().toString());
        request.setBillerId(mFragment.billerId);
        /*test*/
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayBillerResponse> respCall = mFragment.getVoucherApi().getCheckBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayBillerResponse>() {
            @Override
            public void onResponse(Call<InqPayBillerResponse> call, Response<InqPayBillerResponse> response) {
                if(response.code()==200)
                {
                    InqPayBillerResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL001"))
                    {
                        /*test*/
//                        PrefHelper.clearPreference(PrefKey.BILLER_ID);
//                        mFragment.orderNumber = resp.getTransactionId();
//                        PrefHelper.setString(PrefKey.TRX_ID, resp.getTransactionId());
                        mFragment.price = resp.getPrice();
                        mFragment.fee = resp.getFee();
                        mFragment.totalAmount = resp.getTotalAmount();
                        mFragment.atasNama = resp.getName();
                        mFragment.afterHit();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan"/*response.body().getAbstractResponse().getResponseMessage()*/);
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayBillerResponse> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }
}
