package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.HistoryWalet;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.history.HistoryWaletFragment;
import com.ultravoucher.uv.utils.AmountFormatter2;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firwandi.ramli on 2/9/2018.
 */

public class HistoryWaletAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private HistoryWaletFragment mContext;
    private List<HistoryWalet> mDataset;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public HistoryWaletAdapter(HistoryWaletFragment context, List<HistoryWalet> myDataSet, RecyclerView recyclerView) {

        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_walet, parent, false);

            vh = new ListHistoryWalet(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ListHistoryWalet) {
            final HistoryWalet item = mDataset.get(position);

            ((ListHistoryWalet) holder).itemView.setTag(item);

            if (item.getTransactionType().equals("FDTRFVAVA")) {
                SpannableString ss = new SpannableString("To : Ultra Voucher");
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan, 0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ListHistoryWalet) holder).tvUsage.setText(ss);
                ((ListHistoryWalet) holder).tvStatus.setText("PEMBAYARAN");

            } else if (item.getTransactionType().equals("SCPPAY")) {
                SpannableString ss = new SpannableString("From : " + " " +item.getDestAccount());
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan, 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ListHistoryWalet) holder).tvUsage.setText(ss);
                ((ListHistoryWalet) holder).tvStatus.setText("TOPUP");

            } else if (item.getTransactionType().equals("CRCTNBLNC")) {
                SpannableString ss = new SpannableString("From : Source Account");
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan, 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ListHistoryWalet) holder).tvUsage.setText(ss);
                ((ListHistoryWalet) holder).tvStatus.setText("TOPUP");
            } else if (item.getTransactionType().equals("TUPNOSCP")) {
                SpannableString ss = new SpannableString("From : " + PrefHelper.getString(PrefKey.VA_NUMBER));
                ClickableSpan clickableSpan = new ClickableSpan() {
                    @Override
                    public void onClick(View view) {

                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(false);
                    }
                };

                ss.setSpan(clickableSpan, 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ((ListHistoryWalet) holder).tvUsage.setText(ss);
                ((ListHistoryWalet) holder).tvStatus.setText("TOPUP");
            }

            ((ListHistoryWalet) holder).tvRefCode.setText("No. Referensi : " + item.getRefNumber());
            ((ListHistoryWalet) holder).tvDate.setText("Tanggal : " + item.getTransactionTimeStamp());
            ((ListHistoryWalet) holder).statusBar.setText("APPROVED");
            ((ListHistoryWalet) holder).tvTotal.setText("Rp. " + AmountFormatter2.format(item.getAccountedAmount()));
        }


    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class ListHistoryWalet extends RecyclerView.ViewHolder {

        @BindView(R.id.tvUsage)
        TextView tvUsage;

        @BindView(R.id.tvRefCode)
        TextView tvRefCode;

        @BindView(R.id.tvDate)
        TextView tvDate;

        @BindView(R.id.statusBar)
        TextView statusBar;

        @BindView(R.id.tvStatus)
        TextView tvStatus;

        @BindView(R.id.tvTotal)
        TextView tvTotal;

        public ListHistoryWalet(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}
