package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.data.api.request.StoreListReq;
import com.ultravoucher.uv.data.api.response.StoreListResponse;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class MerchantListFisikPresenter {

    private MerchantListFisikFragment mFragment;

    public MerchantListFisikPresenter(MerchantListFisikFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getListMerchant(final StoreListReq req) {

        String contentType = "application/json";
        VoucherApi api = mFragment.getVoucherApi();
        final Call<StoreListResponse> storeListResponseCall = api.getStoreList(contentType, req);
        storeListResponseCall.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                StoreListResponse resp = response.body();
                if (response.code() == 200) {
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getStoreResponse().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                mFragment.noDataState.setVisibility(View.GONE);
                                initListEvent(resp.getStoreResponse());
                                mFragment.swipe_container.setVisibility(View.VISIBLE);
                                mFragment.gridView.setVisibility(View.VISIBLE);
                            } else {
                                mFragment.noDataState.setVisibility(View.GONE);
                                swapped(resp.getStoreResponse());
                                mFragment.swipe_container.setVisibility(View.VISIBLE);
                                mFragment.gridView.setVisibility(View.VISIBLE);
                            }
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                swapped(resp.getStoreResponse());
                                mFragment.gridView.setVisibility(View.GONE);
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("RES001")) {
                        mFragment.gridView.setVisibility(View.GONE);
                        mFragment.swipe_container.setVisibility(View.GONE);
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.noDataState.setVisibility(View.VISIBLE);
                    mFragment.noDataMessage.setText(message);
                    mFragment.swipe_container.setRefreshing(false);
                    mFragment.showToast(message);
                }
            }
        });
    }

    void PresentGetAllMerchantLoadMore(StoreListReq req) {
        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        final Call<StoreListResponse> storeListResponseCall = api.getStoreList(contentType, req);
        storeListResponseCall.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                StoreListResponse resp = response.body();
                if (response.code() == 200) {
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getStoreResponse().isEmpty()) {
                            addToListEvent(resp.getStoreResponse());
                        } else {
                            mFragment.mData.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mData.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mData.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });
    }

    protected void initListEvent(List<StoreListMsgResp> list) {
        mFragment.mData.clear();
        mFragment.mData.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToListEvent(List<StoreListMsgResp> list) {
        mFragment.mData.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (StoreListMsgResp imr : list) {
            mFragment.mData.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void swapped(List<StoreListMsgResp> data) {
        mFragment.mAdapter.swap(data);
    }

    protected StoreListReq presentGetListMerchantReq() {
        StoreListReq req = new StoreListReq();
        req.setKeyword(mFragment.keyword);
        req.setCityId("");
        req.setLatitude("");
        req.setLongitude("");
        req.setPage(mFragment.page);
        req.setNearBy("");
        req.setMerchantId("");
        req.setNRecords(15);
        req.setStoreType("");
        req.setOrder("desc");
        req.setVoucherClass("P");
        return req;
    }
}
