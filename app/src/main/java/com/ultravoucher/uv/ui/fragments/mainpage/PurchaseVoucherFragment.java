package com.ultravoucher.uv.ui.fragments.mainpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.adapters.HomeViewPagerAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.purchase.MerchantListFisikFragment;
import com.ultravoucher.uv.ui.fragments.purchase.MerchantListFragment;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 9/28/2017.
 */

public class PurchaseVoucherFragment extends BaseFragment {
    private static final String TAG = "PurchaseVoucherFragment";

    @BindView(R.id.purchaseTablayout)
    TabLayout mTabLayout;

    @BindView(R.id.purchaseVP)
    ViewPager mViewPager;

    public static String keyword = "";

    PurchaseVoucherFragment mContext;

    private int[] tabIcons = {
            R.drawable.ic_voucher_digital_96px,
            R.drawable.ic_voucher_fisik_96px
    };

    public static PurchaseVoucherFragment showTabFragment() {
        PurchaseVoucherFragment tabStore = new PurchaseVoucherFragment();
        return tabStore;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_purchase_voucher;
    }

    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

        mContext = new PurchaseVoucherFragment();
        FragmentTransaction transaction = getChildFragmentManager()
                .beginTransaction().replace(R.id.container, mContext);
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

    }


    private void setupTabIcons() {
//        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
//        mTabLayout.getTabAt(2).setIcon(tabIcons[2]);

        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab2, null);
        view1.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_voucherdigital_300px);
        mTabLayout.getTabAt(0).setCustomView(view1);

        View view2 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab2, null);
        view2.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_voucherfisik_300px);
        mTabLayout.getTabAt(1).setCustomView(view2);
}

    private void setupViewPager(ViewPager viewPager) {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MerchantListFragment(), "Voucher Digital");
        adapter.addFragment(new MerchantListFisikFragment(), "Voucher Fisik");
//        adapter.addFragment(new HomePageFragment(), "Merchant");
        viewPager.setAdapter(adapter);
    }

    /*@Override
    public void onResume() {
        super.onResume();

        initTab();
        setuptabIcons();
        initPager();
    }*/

    /*private void initTab() {
        mTabLayout.removeAllTabs();
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.addTab(mTabLayout.newTab());
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setuptabIcons() {
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
        mTabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    private void initPager() {
        final PurchaseVoucherPagerAdapter mAdapter = new PurchaseVoucherPagerAdapter(getChildFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mAdapter);
        TabLayoutImage.applyFontedTab((BaseActivity) getActivity(), mViewPager, mTabLayout);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                mViewPager.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }*/


}
