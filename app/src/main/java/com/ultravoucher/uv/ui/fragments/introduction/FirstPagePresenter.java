package com.ultravoucher.uv.ui.fragments.introduction;

import android.provider.Settings;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.LoginReq;
import com.ultravoucher.uv.data.api.response.LoginResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.HomePageActivity;
import com.ultravoucher.uv.ui.activities.VerificationActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 3/20/2018.
 */

public class FirstPagePresenter {

    FirstPageFragment mFragment;

    public FirstPagePresenter(FirstPageFragment mFragment) {
        this.mFragment = mFragment;
    }

    public void login(final LoginReq request) {

//        mFragment.showProgressDialog(mFragment.LOADING);
        VoucherApi api = mFragment.getVoucherApi();
        Call<LoginResponse> loginResponseCall = api.doLogin(request);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse resp = response.body();
                if (response.code() == 200) {
                    if (resp.getAuthenticationTokens().getAuthtokenResponse() != null) {
                        if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                            final String authToken = resp.getAuthenticationTokens().getAuthtokenResponse().getAuthToken();
                            final String deviceUniqueId = resp.getAuthenticationTokens().getAuthtokenResponse().getAuthDevice();
                            final String memberId = resp.getAuthenticationTokens().getAuthtokenResponse().getMemberId();

                            PrefHelper.setString(PrefKey.AUTH_TOKEN, authToken);
                            PrefHelper.setString(PrefKey.DEVICE_UNIQUE_ID, deviceUniqueId);
                            PrefHelper.setString(PrefKey.MEMBERID, memberId);
                            PrefHelper.setString(PrefKey.FIRSTNAME, resp.getAuthenticationTokens().getAuthtokenResponse().getFirstName());
                            PrefHelper.setString(PrefKey.VA_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getVaNumber());
                            PrefHelper.setString(PrefKey.QR_IMAGE, resp.getAuthenticationTokens().getAuthtokenResponse().getQrImage());
                            PrefHelper.setString(PrefKey.INITIAL_RUN, "login");
                            PrefHelper.setString(PrefKey.PHONE_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getPhoneNumber());
                            PrefHelper.setString(PrefKey.GENDER, resp.getAuthenticationTokens().getAuthtokenResponse().getGender());
                            PrefHelper.setString(PrefKey.ID_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getIdNumber());
                            PrefHelper.setString(PrefKey.MOTHER_NAME, resp.getAuthenticationTokens().getAuthtokenResponse().getMotherName());
                            PrefHelper.setString(PrefKey.BIRTH_PLACE, resp.getAuthenticationTokens().getAuthtokenResponse().getBirthPlace());
                            PrefHelper.setString(PrefKey.BIRTH_DATE, resp.getAuthenticationTokens().getAuthtokenResponse().getBirthDate());
                            PrefHelper.setString(PrefKey.POIN, String.valueOf(resp.getAuthenticationTokens().getAuthtokenResponse().getPoint()));
                            PrefHelper.setString(PrefKey.USER_PIC, resp.getAuthenticationTokens().getAuthtokenResponse().getUserPic());
                            PrefHelper.setString(PrefKey.USERNAME, resp.getAuthenticationTokens().getAuthtokenResponse().getEmail());
                            PrefHelper.setString(PrefKey.PASSWORD, "");
                            mFragment.getActivity().finish();
                            HomePageActivity.startActivity((BaseActivity) mFragment.getActivity(), "login");
                        } else {

                        }
                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.showToast("Password tidak sesuai");

                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH002")) {
                        mFragment.showToast("Email/No.Hp belum terdaftar");

                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH005")) {
                        VerificationActivity.startActivity((BaseActivity) mFragment.getActivity(), "", "");
                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("ERR123")) {
                        mFragment.showToast("Sistem bermasalah, Silahkan coba lagi");
                    }

                    else {
                        mFragment.showToast(resp.getAuthenticationTokens().getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast("Kendala pada server, silahkan coba lagi");
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast(mFragment.CONNECTION_ERROR);
            }
        });
    }


    LoginReq constructLoginReq() {
        final String idDevice = Settings.Secure.getString(mFragment.getContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        LoginReq req = new LoginReq();
        req.setDeviceUniqueId(idDevice);
        req.setUsername(PrefHelper.getString(PrefKey.USERNAME));
        req.setPassword(PrefHelper.getString(PrefKey.PASSWORD));

        return req;
    }

}

