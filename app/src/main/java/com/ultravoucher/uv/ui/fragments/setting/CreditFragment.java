package com.ultravoucher.uv.ui.fragments.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 1/25/2018.
 */

public class CreditFragment extends BaseFragment {

    private static final String TAG = CreditFragment.class.getSimpleName();

    @BindView(R.id.tvCredit)
    TextView tvCredit;

    public static void showFragment(BaseActivity sourceFragment) {
        CreditFragment fragment = new CreditFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_credit;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvCredit.setText("Credit : \n Icon made by Freepik from www.flaticon.com");
    }
}
