package com.ultravoucher.uv.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.UtilMessageResp;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class AutoCompleteAdapter extends ArrayAdapter<UtilMessageResp>

{

    private final Context mContext;
    private final List<UtilMessageResp> mCities;
    private final List<UtilMessageResp> mCities_All;
    private final List<UtilMessageResp> mCities_Suggestion;
    private final int mLayoutResId;

    public AutoCompleteAdapter(Context mContext, int mLayoutResId, List<UtilMessageResp> mCities) {
        super(mContext, mLayoutResId, mCities);
        this.mContext = mContext;
        this.mCities = new ArrayList<>(mCities);
        this.mLayoutResId = mLayoutResId;
        this.mCities_All = mCities;
        this.mCities_Suggestion = new ArrayList<>();
    }

    public void commitChangeData() {
        mCities.clear();
        mCities.addAll(new ArrayList<>(mCities_All));
    }

    @Override
    public int getCount() {
        return mCities.size();
    }

    @Override
    public UtilMessageResp getItem(int position) {
        return mCities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(mLayoutResId, parent, false);
        }
        UtilMessageResp item = getItem(position);
        TextView name = (TextView) convertView.findViewById(R.id.textView);
        name.setText(item.getLabel());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((UtilMessageResp) resultValue).getLabel();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null){
                    mCities_Suggestion.clear();
                    for(UtilMessageResp city : mCities_All){
                        if(city.getLabel().toLowerCase().contains(constraint.toString().toLowerCase())){
                            mCities_Suggestion.add(city);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mCities_Suggestion;
                    filterResults.count = mCities_Suggestion.size();
                    return filterResults;
                }else{
                    return new FilterResults();
                }
            }

//            ==========METHOD BELOW WILL GIVE CONCURRENT_EXCEPTION================
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//                mCities.clear();
//                if(results != null && results.count > 0){
//                    List<?> result = (List<?>) results.values;
//                    for(Object object : result){
//                        if(object instanceof UtilMessageResponse){
//                            mCities.add((UtilMessageResponse) object);
//                        }
//                    }
//                }else if(constraint == null){
//                    mCities.addAll(mCities_All);
//                }
//                notifyDataSetChanged();
//            }
//            =============================================================

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<UtilMessageResp> filteredList = (ArrayList<UtilMessageResp>) results.values;
                ArrayList<UtilMessageResp> cityList = new ArrayList<UtilMessageResp>();
                if (results != null && results.count > 0) {
                    mCities.clear();
                    Iterator<UtilMessageResp> filteredListIterator = filteredList.iterator();
                    while (filteredListIterator.hasNext()){
                        UtilMessageResp cityItem = filteredListIterator.next();
                        cityList.add(cityItem);
                    }
                    Iterator<UtilMessageResp> customerIterator = cityList.iterator();
                    while (customerIterator.hasNext()) {
                        UtilMessageResp cityItem = customerIterator.next();
                        mCities.add(cityItem);
                    }
                    notifyDataSetChanged();
                }

            }
        };
    }}
