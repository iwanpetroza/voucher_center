package com.ultravoucher.uv.ui.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.VoucherApplication;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.LoginActivity;

import butterknife.BindString;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by firwandi.ramli on 9/25/2017.
 */

public abstract class BaseFragment extends Fragment {

    @BindString(R.string.loading)
    public String LOADING;

    @BindString(R.string.connection_error)
    public String CONNECTION_ERROR;

    private boolean mIsRunning = false;

    protected abstract int getLayout();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mIsRunning = true;
        View view = inflater.inflate(getLayout(), container, false);
        try {
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onDestroy() {
        dismissProgressDialog();
        mIsRunning = false;
        super.onDestroy();
    }

    @Override
    public void onAttach(Context newBase) {
        super.onAttach(CalligraphyContextWrapper.wrap(newBase));
    }

    public boolean isRunning() {
        return mIsRunning;
    }

    public void showProgressDialog(String message) {
        if (mIsRunning) {
            ((BaseActivity) getActivity()).showProgressDialog(message);
        }
    }

    public void showAlertDialog(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Information");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void dismissProgressDialog() {
        if (mIsRunning) {
            ((BaseActivity) getActivity()).dismissProgressDialog();
        }
    }

    public void showToast(String msg) {
        if (mIsRunning) {
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void doNeedRelogin() {
        PrefHelper.clearAllPreferences();
        LoginActivity.startActivity((BaseActivity) getActivity(), true);
    }

    public String getAndTrimValueFromEditText(EditText e) {
        return e.getText().toString().trim();
    }

    public VoucherApi getVoucherApi(){
        return VoucherApplication.getInstance().getVoucherApi();
    }

    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }
}