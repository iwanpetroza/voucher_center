package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.fragments.purchase.MyCartFragment;
import com.ultravoucher.uv.ui.fragments.purchase.MyCartPresenter;
import com.ultravoucher.uv.utils.AmountFormatter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firwandi.ramli on 12/29/2017.
 */

public class MyCartListAdapter extends RecyclerView.Adapter<MyCartListAdapter.CartHolder> {
    private MyCartFragment context;
    private List<Order> list;
    private MyCartPresenter mPresenter;

    public MyCartListAdapter(MyCartFragment context, List<Order> list, MyCartPresenter mPresenter) {
        this.context = context;
        this.list = list;
        this.mPresenter = mPresenter;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        return new CartHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CartHolder holder, int position) {
        final Order data = list.get(position);
        holder.tvProductName.setText(data.getProductName().toString());
        holder.tvDelMethod.setText(data.getDeliveryAddress());
        holder.tvSellPrice.setText("" + AmountFormatter.format(data.getSellingPrice()) + " x " + data.getQuantity());

        if (data.getDeliveryMethod() == -1) {
            holder.tvDelMethod.setText("Dikirim");
        } else if (data.getDeliveryMethod() == 1) {
            holder.tvDelMethod.setText("Ambil Sendiri");
        }


//        holder.tvDelMethodV.setText("");
        holder.subTotal.setText("Sub Total :");
        holder.subTotalV.setText("" + AmountFormatter.format(data.getSellingPrice() * data.getQuantity()));


        Picasso.with(context.getActivity()).load(data.getProductImage()).fit().centerCrop()
                .into(holder.civIcProduct, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        holder.pbLoading.setVisibility(View.GONE);
                        //  pbIntro.setVisibility(View.GONE);
                        // tvError.setVisibility(View.VISIBLE);
                    }
                });

        holder.imgDiscard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.discardOrder(data);
                //Toast.makeText(context, data.getProductName(), Toast.LENGTH_SHORT).show();
            }
        });

        /*holder.ll_btnQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeQuantityDialog changeQuantityDialog = new ChangeQuantityDialog(context,data);

            }
        });*/
                /*// custom dialog
                final Dialog dialog = new Dialog(context.getContext(), R.style.Ceploc_CustomDialog);
                dialog.setContentView(R.layout.d_change_quantity);

                // set the custom dialog components - text, image and button
               *//* TextView text = (TextView) dialog.findViewById(R.id.redeemreward_tv_message);
                text.setText(resp.getRewardRedemptionResponse().getDetailresponses().get(0).getDetailResponseMessage());*//*
                Button dialogButton = (Button) dialog.findViewById(R.id.change_quantity_btn_save);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        mPresenter.getmycartdata();
                    }
                });

                dialog.show();*/



    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class CartHolder extends RecyclerView.ViewHolder
    {

        @BindView(R.id.mycart_civ_product)
        ImageView civIcProduct;

        @BindView(R.id.tvDelMethod)
        TextView tvDelMethod;

        /*@BindView(R.id.tvDelMethodV)
        TextView tvDelMethodV;*/

        @BindView(R.id.subTotal)
        TextView subTotal;

        @BindView(R.id.subTotalV)
        TextView subTotalV;

        @BindView(R.id.mycart_tv_productname)
        TextView tvProductName;

        @BindView(R.id.mycart_tv_sellprice)
        TextView tvSellPrice;

        @BindView(R.id.mycart_img_discard)
        ImageView imgDiscard;

        @BindView(R.id.mycart_pb_loading)
        ProgressBar pbLoading;

        public CartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}