package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryBillerDetailFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryDetailDigitalFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryVoucherDetailFragment;

import butterknife.BindView;

public class GeneralHistoryActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    private static final String STATUS = "status";
    private static final String TRXID = "trxid";

    String status, pos, trxId;

    public static void startActivity(BaseActivity sourceActivity, String status, String trxId){
        Intent i = new Intent(sourceActivity, GeneralHistoryActivity.class);
        i.putExtra(STATUS, status);
        i.putExtra(TRXID, trxId);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        trxId = getIntent().getStringExtra(TRXID);
        setupToolbar();

        if (status.equalsIgnoreCase("historybiller")) {
            HistoryBillerDetailFragment.showFragment(this, trxId);
        } else if (status.equalsIgnoreCase("historydigital")) {
            HistoryVoucherDetailFragment.showFragment(this, trxId);
        }
//        HistoryDetailDigitalFragment.showFragment(this, pos);

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Detail Histori");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
