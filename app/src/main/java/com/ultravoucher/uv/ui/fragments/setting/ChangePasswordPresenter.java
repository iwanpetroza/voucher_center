package com.ultravoucher.uv.ui.fragments.setting;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.ChangePasswordReq;
import com.ultravoucher.uv.data.api.response.ChangePasswordResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class ChangePasswordPresenter {

    ChangePasswordFragment mFragment;

    public ChangePasswordPresenter(ChangePasswordFragment mFragment) {
        this.mFragment = mFragment;
    }

    public void doChangePass(ChangePasswordReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<ChangePasswordResponse> changePasswordResponseCall = api.doChangePass(contentType, authToken, deviceUniqueId, req);
        changePasswordResponseCall.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                if (response.code() == 200) {
                    ChangePasswordResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("VC001 Success")) {
                        mFragment.showToast("Your Password successfully changed");
                        mFragment.getActivity().finish();
                    } else if(resp.getAbstractResponse().getResponseStatus().equals("AUTH010")) {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    }
                    else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast("Problem when contacting server");
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast(mFragment.CONNECTION_ERROR);
            }
        });
    }

    ChangePasswordReq constructChangePass() {
        String oldPass = mFragment.etOldPass.getText().toString();
        String newPass = mFragment.etNewPass.getText().toString();
        String userName = PrefHelper.getString(PrefKey.USERNAME);

        ChangePasswordReq req = new ChangePasswordReq();
        req.setUsername(userName);
        req.setPasswordOld(oldPass);
        req.setPassword(newPass);
        return req;
    }
}
