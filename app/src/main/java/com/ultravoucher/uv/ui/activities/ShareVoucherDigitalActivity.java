package com.ultravoucher.uv.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.SharingBalance;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.utils.AmountFormatter2;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class ShareVoucherDigitalActivity extends BaseActivity implements Validator.ValidationListener {

    private static final int PICK_CONTACT = 1000;
    private static final String MERCHANT_ID = "merchant";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.etAmountShare)
    EditText etAmountShare;
    @BindView(R.id.etShareName)
    @NotEmpty
    EditText etShareName;
    @BindView(R.id.etSharePhone)
    @NotEmpty
    EditText etSharePhone;
    @BindView(R.id.etShareEmail)
    @NotEmpty
    EditText etShareEmail;

    @BindView(R.id.etShareQty)
    @NotEmpty
    EditText etShareQty;

    @BindView(R.id.tvQtySvr)
    @NotEmpty
    TextView tvQtySvr;

    @BindView(R.id.tvJmlhShare)
    @NotEmpty
    TextView tvJmlhShare;

    String merchantId;
    List<SharingBalance> datas = new ArrayList<>();
    List<String> listing = new ArrayList<>();
    List<String> listingb = new ArrayList<>();
    protected int variantPosition, amountFinal;
    ShareVoucherDigitalPresenter mPresenter;
    Validator validator;
    String voucherId, voucherCode, jmlhv, amount;
    boolean isSelected;

    public static void startActivity(BaseActivity sourceActivity, String merchantId){
        Intent i = new Intent(sourceActivity, ShareVoucherDigitalActivity.class);
        i.putExtra(MERCHANT_ID, merchantId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_share_voucher_digital;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        etShareQty.setEnabled(false);
        merchantId = getIntent().getStringExtra(MERCHANT_ID);
        mPresenter = new ShareVoucherDigitalPresenter(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
        etAmountShare.setInputType(InputType.TYPE_NULL);
        ShareVoucherDigitalActivityPermissionsDispatcher.getContactPermitWithPermissionCheck(this);


        /*etAmountShare.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    if (etAmountShare.getText().toString().equals("")) {
                        etShareQty.setEnabled(false);
                    } else {
                        etShareQty.setEnabled(true);
                    }
                }
            }
        });*/

        etShareQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

//                String amount = Integer.parseInt(etAmountShare.getText().toString());
                String a = String.valueOf(editable);
                String b = etAmountShare.getText().toString();
                int jmlhShre;


                if (a.equals("") && a != null || a.equals("0") || b.equals("") && b != null) {
                    showToast("Masukkan jumlah voucher yang akan dishare");
                } else {
                    if (Integer.parseInt(a) > Integer.parseInt(tvQtySvr.getText().toString())){
                        jmlhShre = Integer.parseInt(jmlhv);
                        etShareQty.setText("" + tvQtySvr.getText().toString());
                        showToast("Maksimal voucher yang dapat anda share adalah " + jmlhv + " voucher");
                    } else {
                        jmlhShre = Integer.parseInt(a);
                    }
                    tvJmlhShare.setText("" +
                            AmountFormatter2.format(Integer.parseInt(amount)*jmlhShre));
                    amountFinal = jmlhShre;
                }
            }
        });

        etAmountShare.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String edit = String.valueOf(editable);
                String ab = etShareQty.getText().toString();
                int jmlhshareB;

                if (edit.equals("") && edit != null|| ab.equals("") && ab != null) {
                    showToast("Masukkan Amount yang ingin Anda share");
                } else {
                    etShareQty.setEnabled(true);
                    String editB = edit.replaceAll("\\.", "");
                    if (Integer.parseInt(ab) > Integer.parseInt(jmlhv)){
                        jmlhshareB = Integer.parseInt(jmlhv);
                        etShareQty.setText("" + jmlhv);
                        showToast("Maksimal voucher yang dapat anda share adalah " + jmlhv + " voucher");
                    } else {
                        jmlhshareB = Integer.parseInt(ab);
                    }
                    tvJmlhShare.setText("" +
                            AmountFormatter2.format(Integer.parseInt(editB)*jmlhshareB));

                    amountFinal = Integer.parseInt(ab);
                }
            }
        });


//        etAmountShare.setOnClickListener(this);

    }

    @OnClick({R.id.btnContact, R.id.share_btn_simpan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContact:
//                ShareVoucherDigitalActivityPermissionsDispatcher.getContactPermitWithPermissionCheck(this);
                pickAContactNumber(view);
                break;
            case R.id.share_btn_simpan:
                if (etAmountShare.getText().toString().equals("") || etShareQty.getText().toString().equals("")) {
                    showToast("Masukkan data yang diperlukan");
                } else {
                    if (etShareQty.getText().toString().equals("0")) {
                        showToast("Jumlah share tidak boleh 0");
                    } else {
                        if (Integer.parseInt(etShareQty.getText().toString()) > Integer.parseInt(tvQtySvr.getText().toString())) {
                            showToast("Jumlah voucher Anda tidak mencukupi");
                        } else {
                            validator.validate();
                        }

                    }
                }

                break;
        }
    }


    //Todo when button is clicked
    public void pickAContactNumber(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String cNumber = null;
        String email = null;
        switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {

                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {


                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            cNumber = phones.getString(phones.getColumnIndex("data1"));
                        }

                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        Cursor emailCur = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id}, null);
                        while (emailCur.moveToNext()) {
                            // This would allow you get several email addresses
                            // if the email addresses were stored in an array
                            email = emailCur.getString(
                                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            String emailType = emailCur.getString(
                                    emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                        }
                        emailCur.close();

                        etShareName.setText(name);
                        etSharePhone.setText(cNumber);
                        etShareEmail.setText(email);
                    }
                }
                break;
        }
    }


    public void setVariantName(List<String> variantNames) {
        this.listing.clear();
        this.listing.addAll(variantNames);
    }

    public void setVariantQty(List<String> variantQty) {
        this.listingb.clear();
        this.listingb.addAll(variantQty);
    }

    @OnClick(R.id.etAmountShare)
    public void showAmount() {

        final CharSequence[] variantName = listing.toArray(new CharSequence[listing.size()]);
        final CharSequence[] variantQty = listingb.toArray(new CharSequence[listingb.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.GenericProgressDialogStyleAlert));
        builder.setTitle("Silahkan Pilih");
        builder.setItems(variantName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                etAmountShare.setText(AmountFormatter2.format(String.valueOf(variantName[i])));
                tvQtySvr.setText(String.valueOf(variantQty[i]));
                variantPosition = i;
                amount = String .valueOf(variantName[i]);
                jmlhv = String.valueOf(variantQty[i]);
                etShareQty.setEnabled(true);
                isSelected = true;

            }
        });
        builder.show();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Share Voucher");
        toolbarTitle.setAllCaps(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getShareAmount(mPresenter.constructSharingBalance());

    }

    @Override
    public void onValidationSucceeded() {

        String mail = etShareEmail.getText().toString();

        if (mail.equals(PrefHelper.getString(PrefKey.USERNAME))) {
            showToast("Anda tidak dapat melakukan share ke Akun Anda.");
        } else {
            confirmDialog();
//            mPresenter.doShareInq(mPresenter.constructShareInq());
//            mPresenter.CheckMember(mPresenter.constructMemberExist());
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /*switch (requestCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor phone = getContentResolver().query(contactData, null, null, null, null);
                    if (phone.moveToFirst()) {
                        String contactNumberName = phone.getString(phone.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String contactNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String contactEmail = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                        // Todo something when contact number selected
                        etShareName.setText(contactNumberName);
                        etSharePhone.setText(contactNumber);
                        etShareEmail.setText(contactEmail);
                    }
                    break;
                }
        }
    }*/



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ShareVoucherDigitalActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    //GET CONTACT PERMISSION
    @NeedsPermission({Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS})
    void getContactPermit() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        /*Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);*/
    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(this), false);

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
        txtView.setText("Apakah Anda yakin data yang Anda masukkan sudah benar?");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);

        final android.app.AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPresenter.doShareInq(mPresenter.constructShareInq());
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

}