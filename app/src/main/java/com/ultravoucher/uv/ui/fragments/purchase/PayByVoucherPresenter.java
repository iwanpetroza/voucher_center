package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.PayByVoucherAmountReq;
import com.ultravoucher.uv.data.api.beans.VoucherAmountMsgResp;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.PayByVoucherAmountRequest;
import com.ultravoucher.uv.data.api.request.VoucherAmountReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.PayByVoucherResponse;
import com.ultravoucher.uv.data.api.response.VoucherAmountResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 10/08/18.
 */

public class PayByVoucherPresenter {

    private PayByVoucherFragment mFragment;

    public PayByVoucherPresenter(PayByVoucherFragment mFragment) {

        this.mFragment = mFragment;

    }

    protected void presentGetVoucherList() {

        mFragment.mRvList.setVisibility(View.GONE);
        mFragment.progressBar.setVisibility(View.VISIBLE);
        mFragment.networkProblemState.setVisibility(View.GONE);

        VoucherAmountReq request = new VoucherAmountReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(-1);
        request.setVoucherClass(1);

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        Call<VoucherAmountResponse> voucherAmountResponseCall = mFragment.getVoucherApi().getListAmountVoucher(contentType, authToken, deviceUniqueId, request);
        voucherAmountResponseCall.enqueue(new Callback<VoucherAmountResponse>() {
            @Override
            public void onResponse(Call<VoucherAmountResponse> call, Response<VoucherAmountResponse> response) {

                if (response.code() == 200) {

                    VoucherAmountResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getVouchers().isEmpty()) {
                            initList(resp.getVouchers());
                            mFragment.mRvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        String message = mFragment.getString(R.string.need_relogin);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();

                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<VoucherAmountResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(mFragment.getString(R.string.connection_error));
                    mFragment.progressBar.setVisibility(View.GONE);
                }
            }
        });

    }

    protected void initList(List<VoucherAmountMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    void getOrder()
    {
        mFragment.showProgressDialog(mFragment.LOADING);
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        mFragment.tvTotalBayarV.setText("" + resp.getTotalTrxAmount());
                        mFragment.b = resp.getTotalTrxAmount();
                        mFragment.orderNumber = resp.getOrders().get(0).getOrderNumber();

                        mFragment.initRVPayByVoucher();
                        presentGetVoucherList();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {

                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {

                mFragment.dismissProgressDialog();
            }
        });
    }

    void doPayByVoucher() {

        mFragment.showProgressDialog(mFragment.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        PayByVoucherAmountRequest req = new PayByVoucherAmountRequest();

        List<PayByVoucherAmountReq> payByVoucherAmountReqs = new ArrayList<>();
        for (int i = 0; i < mFragment.orderDatas.size(); i++) {

            PayByVoucherAmountReq request = new PayByVoucherAmountReq();
            request.setQuantity(mFragment.orderDatas.get(i).getQuantityEt());
            request.setVoucherOrigin(mFragment.orderDatas.get(i).getVoucherOrigin());
            request.setVoucherValue(mFragment.orderDatas.get(i).getVoucherValue());
            payByVoucherAmountReqs.add(request);
        }

        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setVoucherClass(1);
        req.setOrderNumber(mFragment.orderNumber);
        req.setVouchers(payByVoucherAmountReqs);

        Call<PayByVoucherResponse> responseCall = mFragment.getVoucherApi().doPayByVoucherAmount(contentType, authToken, deviceUniqueId, req);
        responseCall.enqueue(new Callback<PayByVoucherResponse>() {
            @Override
            public void onResponse(Call<PayByVoucherResponse> call, Response<PayByVoucherResponse> response) {
                if (response.code() == 200) {
                    PayByVoucherResponse resp = response.body();
                    PrefHelper.setString(PrefKey.PAYCODE, "");
                    if (resp.getAbstractResponse().getResponseStatus().equals("OD003")) {
                        mFragment.getActivity().finish();
                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), "-", resp.getOrders().get(0).getOrderNumber(), String.valueOf(resp.getTotalVoucherPayment()), "vc");
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<PayByVoucherResponse> call, Throwable t) {
                String message = mFragment.getString(R.string.connection_error);
                mFragment.showToast(message);
                mFragment.dismissProgressDialog();
            }
        });

    }


}
