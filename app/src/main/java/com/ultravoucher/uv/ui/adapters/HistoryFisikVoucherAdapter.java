package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.History;
import com.ultravoucher.uv.ui.fragments.history.HistoryFisikFragment;
import com.ultravoucher.uv.utils.AmountFormatter;
import com.ultravoucher.uv.utils.DateFormatter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firwandi.ramli on 10/26/2017.
 */

public class HistoryFisikVoucherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private HistoryFisikFragment mContext;
    private List<History> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public HistoryFisikVoucherAdapter(HistoryFisikFragment context, List<History> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_fisik, parent, false);

            vh = new ListVoucherHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListVoucherHolder) {
            final History item = mDataset.get(position);

            if (item.getOrderVouchers().size() >= 1) {
                for (int i = 0; i < item.getOrderVouchers().size(); i++) {
                    ((ListVoucherHolder) holder).itemView.setTag(item);
                    ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);
                    ((ListVoucherHolder) holder).tvName.setText(item.getProductName());
                    ((ListVoucherHolder) holder).tvPrice.setText("" + AmountFormatter.format(item.getSellingPrice()) + " x " + item.getQuantity());
                    ((ListVoucherHolder) holder).tvCode.setText("Voucher Id : " + item.getOrderVouchers().get(i).getVoucherId());

                    if (item.getPaymentDate().equals("-") || item.getPaymentDate().equals("")) {
                        ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tanggal Beli : -");
                    } else {
                        ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tanggal Beli : " + DateFormatter.format(item.getPaymentDate()));
                    }

                    /*if (item.getOrderVouchers().get(i).getRedeemedDate().equals("-") || item.getOrderVouchers().get(i).getRedeemedDate().equals("")) {
                        ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : -");
                    } else {
                        ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : " + DateFormatter.format(item.getOrderVouchers().get(i).getRedeemedDate()));
                    }*/

                    //PAYMENT STATUS

                    if (item.getStatus() == -2) {
                        ((ListVoucherHolder) holder).paymentStatus.setText("Status : Ambil Sendiri" );
                        if (item.getOrderVouchers().get(i).getRedeemedDate().equals("-") || item.getOrderVouchers().get(i).getRedeemedDate().equals("")) {
                            ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : -");
                        } else {
                            ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : " + DateFormatter.format(item.getOrderVouchers().get(i).getRedeemedDate()));
                        }
                    } else if (item.getStatus() == -1) {
                        ((ListVoucherHolder) holder).paymentStatus.setText("Status : Sudah Dikirim" );
                        ((ListVoucherHolder) holder).tvUsageDate.setText("No. Resi : " + item.getTransactionRefNumber());

                    } else if (item.getStatus() == 1) {
                        ((ListVoucherHolder) holder).paymentStatus.setText("Status : Proses Pengiriman" );
                        if (item.getOrderVouchers().get(i).getRedeemedDate().equals("-") || item.getOrderVouchers().get(i).getRedeemedDate().equals("")) {
                            ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : -");
                        } else {
                            ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pickup : " + DateFormatter.format(item.getOrderVouchers().get(i).getRedeemedDate()));
                        }
                    }

                    //END OF PAYMENT STATUS


//                    ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pakai : " + item.getOrderVouchers().get(i).getRedeemedDate());

                    if (item.getProductImage() != null && !item.getProductImage().equals("")) {
                        Picasso.with(mContext.getContext()).load(item.getProductImage()).placeholder(R.drawable.image_placeholder)
                                .centerCrop().fit().into(((ListVoucherHolder) holder).civLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                                ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                            }
                        });
                    } else {
                        ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                    }
                }
            } else {
                ((ListVoucherHolder) holder).itemView.setTag(item);
                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);
                ((ListVoucherHolder) holder).tvName.setText(item.getProductName());
                ((ListVoucherHolder) holder).tvPrice.setText("" + AmountFormatter.format(item.getSellingPrice()));
                ((ListVoucherHolder) holder).tvCode.setText("Voucher Id : " + item.getOrderVouchers().get(0).getVoucherId());

                if (item.getPaymentDate().equals("-") || item.getPaymentDate().equals("")) {
                    ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tanggal Beli : -");
                } else {
                    ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tanggal Beli : " + DateFormatter.format(item.getPaymentDate()));
                }

                if (item.getOrderVouchers().get(0).getRedeemedDate().equals("-") || item.getOrderVouchers().get(0).getRedeemedDate().equals("")) {
                    ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pakai : -");
                } else {
                    ((ListVoucherHolder) holder).tvUsageDate.setText("Tanggal Pakai : " + DateFormatter.format(item.getOrderVouchers().get(0).getRedeemedDate()));
                }

//                ((ListVoucherHolder) holder).tvPurchaseDate.setText(item.getPaymentDate());
//                ((ListVoucherHolder) holder).tvUsageDate.setText(item.getOrderVouchers().get(0).getRedeemedDate());

                if (item.getProductImage() != null && !item.getProductImage().equals("")) {
                    Picasso.with(mContext.getContext()).load(item.getProductImage()).placeholder(R.drawable.image_placeholder)
                            .centerCrop().fit().into(((ListVoucherHolder) holder).civLogo, new Callback() {
                        @Override
                        public void onSuccess() {
                            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                            ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                        }
                    });
                } else {
                    ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                    ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                }
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListVoucherHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.paymentStatus)
        TextView paymentStatus;

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvPrice)
        TextView tvPrice;

        @BindView(R.id.tvCode)
        TextView tvCode;

        @BindView(R.id.tvPurchaseDate)
        TextView tvPurchaseDate;

        @BindView(R.id.tvUsageDate)
        TextView tvUsageDate;

        @BindView(R.id.imgView)
        ImageView civLogo;

        @BindView(R.id.vcLoading)
        ProgressBar pbLoading;

        /*@BindView(R.id.paymentStatus)
        TextView paymentStatus;*/

        public ListVoucherHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}