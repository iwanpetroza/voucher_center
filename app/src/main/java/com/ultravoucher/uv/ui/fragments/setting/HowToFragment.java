package com.ultravoucher.uv.ui.fragments.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 1/25/2018.
 */

public class HowToFragment extends BaseFragment {

    private static final String TAG = HowToFragment.class.getSimpleName();
    @BindView(R.id.img)
    ImageView img;

    public static void showFragment(BaseActivity sourceFragment) {
        HowToFragment fragment = new HowToFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_howto_pickup;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        img.setVisibility(View.VISIBLE);
        Picasso.with(getContext()).load(R.drawable.cara_pickup).fit().into(img);
    }



}

