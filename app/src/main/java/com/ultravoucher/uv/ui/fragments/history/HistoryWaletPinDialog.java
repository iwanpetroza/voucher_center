package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 2/14/2018.
 */

public class HistoryWaletPinDialog extends BaseDialogFragment {


    @BindView(R.id.insertotp_pb_loading)
    ProgressBar insertotpPbLoading;
    @BindView(R.id.insertotp_tv_error)
    TextView insertotpTvError;
    @BindView(R.id.insertotp_ll_load)
    LinearLayout insertotpLlLoad;
    @BindView(R.id.d_registration_info_tv1)
    TextView dRegistrationInfoTv1;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.insertotp_ll_form)
    LinearLayout insertotpLlForm;
    @BindView(R.id.cv)
    LinearLayout cv;
    @BindView(R.id.d_registration_linearLayout)
    LinearLayout dRegistrationLinearLayout;


    HistoryWaletFragment historyWaletFragment;

    HistoryWaletPinListener listener;
    String pass = "";

    /*public HistoryWaletPinDialog() {

    }

    public HistoryWaletPinDialog(String pass) {
        super();
        this.pass = pass;
    }*/

    public interface HistoryWaletPinListener {
        void onBtnClick(String pass);
    }

    public void setHistoryListener(HistoryWaletPinListener listener) {
        this.listener = listener;
    }


    @Override
    protected int getLayout() {
        return R.layout.d_input_pin2;
    }


    @OnClick({R.id.insertpin_btn_confirm, R.id.insertpin_btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.insertpin_btn_confirm:
                String password = etPassword.getText().toString();

                if (!password.equals("") && password != null) {
                    listener.onBtnClick(password);
                    dismiss();
                }else{
                    showToast("Masukkan Password Anda");
                }
                break;
            case R.id.insertpin_btn_cancel:

                dismiss();
                HistoryWaletPresenter.dismissHistory();
                break;
        }
    }
}
