package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.ListDeliveryAddressMsgResp;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.activities.AddressActivity;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.ListAddressAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class ListAddressFragment extends BaseFragment{

    private static final String TAG = ListAddressFragment.class.getSimpleName();

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    String deliveryId;
    protected List<ListDeliveryAddressMsgResp> mList = new ArrayList<ListDeliveryAddressMsgResp>();
    ListAddressAdapter mAdapter;
    ListAddressPresenter mPresenter;

    List<Order> datas = new ArrayList<Order>();

    public void setDatas(List<Order> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        mAdapter.notifyDataSetChanged();
    }

    public static void showFragment(BaseActivity sourceFragment) {
        ListAddressFragment fragment = new ListAddressFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        mPresenter = new ListAddressPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        page= 0;
        initRVListDelivery();
        loadListDelivery();
    }

    protected void initRVListDelivery() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new ListAddressAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                ListDeliveryAddressMsgResp product = (ListDeliveryAddressMsgResp) childView.getTag();
                deliveryId = product.getDeliveryAddressId();
                mPresenter.getOrder();
                //UpdateAddressActivity.startActivity((BaseActivity) getActivity(), product);
            }

        }));

        /*mHandler = new Handler();

        mAdapter.setOnLoadMoreListener(new ListAddressAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreListDelivery();
                    }
                }, 2000);

            }
        });*/
    }

    private void loadListDelivery() {
        mPresenter.presentGetListDeliveryAddress(mPresenter.presentGetListAddressReq());
    }

    private void loadMoreListDelivery() {
        mPresenter.presentGetListDeliveryAddressLoadMore(mPresenter.presentGetListAddressReq());
    }

    @OnClick(R.id.btnSimpan)
    public void doSave(){
        AddressActivity.startActivity((BaseActivity) getActivity(), "addaddress");

    }
}
