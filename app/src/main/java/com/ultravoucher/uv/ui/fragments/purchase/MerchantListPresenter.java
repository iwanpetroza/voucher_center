package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.data.api.request.StoreListReq;
import com.ultravoucher.uv.data.api.response.CheckVersionResponse;
import com.ultravoucher.uv.data.api.response.StoreListResponse;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class MerchantListPresenter {

    private MerchantListFragment mFragment;

    public MerchantListPresenter(MerchantListFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getListMerchant(final StoreListReq req) {

        String contentType = "application/json";
        VoucherApi api = mFragment.getVoucherApi();
        final Call<StoreListResponse> storeListResponseCall = api.getStoreList(contentType, req);
        storeListResponseCall.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                StoreListResponse resp = response.body();
                if (response.code() == 200) {
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getStoreResponse().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                mFragment.noDataState.setVisibility(View.GONE);
                                initListEvent(resp.getStoreResponse());
                                mFragment.swipe_container.setVisibility(View.VISIBLE);
                                mFragment.gridView.setVisibility(View.VISIBLE);
                            } else {

                                mFragment.noDataState.setVisibility(View.GONE);
                                swapped(resp.getStoreResponse());
                                mFragment.swipe_container.setVisibility(View.VISIBLE);
                                mFragment.gridView.setVisibility(View.VISIBLE);
                            }

                        } else {
                            if(mFragment!= null && mFragment.isAdded()) {
                                swapped(resp.getStoreResponse());
                                mFragment.gridView.setVisibility(View.GONE);
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("RES001")) {
                        if(mFragment!= null && mFragment.isAdded()) {
                            mFragment.gridView.setVisibility(View.GONE);
                            mFragment.swipe_container.setVisibility(View.GONE);
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }

                    else {
                        if(mFragment!= null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if(mFragment!= null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
//                getCheckVersion();
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.noDataState.setVisibility(View.VISIBLE);
                    mFragment.noDataMessage.setText(message);
                    mFragment.swipe_container.setRefreshing(false);
                    mFragment.showToast(message);
//                    getCheckVersion();
                }
            }
        });
    }

    void PresentGetAllMerchantLoadMore(StoreListReq req) {
        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        final Call<StoreListResponse> storeListResponseCall = api.getStoreList(contentType, req);
        storeListResponseCall.enqueue(new Callback<StoreListResponse>() {
            @Override
            public void onResponse(Call<StoreListResponse> call, Response<StoreListResponse> response) {
                StoreListResponse resp = response.body();
                if (response.code() == 200) {
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getStoreResponse().isEmpty()) {
                            addToListEvent(resp.getStoreResponse());
                        } else {
                            mFragment.mData.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }

                    else {
                        mFragment.mData.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mData.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
            }

            @Override
            public void onFailure(Call<StoreListResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });
    }

    protected void initListEvent(List<StoreListMsgResp> list) {
        mFragment.mData.clear();
        mFragment.mData.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void swapped(List<StoreListMsgResp> data) {
        mFragment.mAdapter.swap(data);
    }

    protected void addToListEvent(List<StoreListMsgResp> list) {
        mFragment.mData.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (StoreListMsgResp imr : list) {
            mFragment.mData.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected StoreListReq presentGetListMerchantReq() {
        StoreListReq req = new StoreListReq();
        req.setKeyword(mFragment.keyword);
        req.setCityId("");
        req.setLatitude("");
        req.setLongitude("");
        req.setPage(mFragment.page);
        req.setNearBy("");
        req.setMerchantId("");
        req.setNRecords(15);
        req.setStoreType("");
        req.setOrder("desc");
        req.setVoucherClass("D");
        return req;
    }

    void getCheckVersion () {

        VoucherApi api = mFragment.getVoucherApi();
        Call<CheckVersionResponse> checkVersionResponseCall = api.doCheckVersion();
        checkVersionResponseCall.enqueue(new Callback<CheckVersionResponse>() {
            @Override
            public void onResponse(Call<CheckVersionResponse> call, Response<CheckVersionResponse> response) {
                if(response.code() == 200) {
                    CheckVersionResponse resp = response.body();
                    int versionCode = BuildConfig.VERSION_CODE;
                    if (!String.valueOf(versionCode).equals(resp.getMobileVersion().getLastVersion())) {
                        LayoutInflater li = LayoutInflater.from(mFragment.getActivity());
                        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(mFragment.getActivity()), false);

                        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mFragment.getActivity());

                        builder.setView(promptsView);
                        builder.setCancelable(false);
                        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
                        txtView.setText("Silahkan update aplikasi Anda terlebih dahulu untuk melanjutkan");

                        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
                        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);
                        btnConfirm.setText("UPDATE");

                        final android.app.AlertDialog dialog = builder.create();

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                try{
                                    mFragment.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mFragment.getActivity().getPackageName())));
                                } catch (Exception e) {
                                    mFragment.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + mFragment.getActivity().getPackageName())));
                                }

                                dialog.dismiss();
                            }
                        });

                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                mFragment.getActivity().finish();

                            }
                        });

                        dialog.show();
                    }
//                    getCountMyCart();
                }


            }

            @Override
            public void onFailure(Call<CheckVersionResponse> call, Throwable t) {
//                getCountMyCart();
            }
        });

    }
}
