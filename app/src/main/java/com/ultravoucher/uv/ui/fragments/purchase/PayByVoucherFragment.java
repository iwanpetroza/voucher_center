package com.ultravoucher.uv.ui.fragments.purchase;

//import com.ultravoucher.uv.ui.adapters.PayByVoucherAdapter;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VoucherAmountMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GeneralHistoryActivity;
import com.ultravoucher.uv.ui.adapters.PayByVoucherAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.AmountFormatter;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Firwandi S Ramli on 10/08/18.
 */

public class PayByVoucherFragment extends BaseFragment implements PayByVoucherAdapter.AmountListener{

    private static final String TAG = PayByVoucherFragment.class.getSimpleName();

    @BindView(R.id.voucher_rv)
    RecyclerView mRvList;

    @BindView(R.id.progress)
    View progressBar;

    @BindView(R.id.progress_message)
    TextView progressMsg;


    @BindView(R.id.tvTotalBayarV)
    TextView tvTotalBayarV;

    @BindView(R.id.voucher_tv_checkout)
    TextView voucher_tv_checkout;

    @BindView(R.id.tvTotalVoucherV)
    TextView tvTotalVoucherV;

    @BindView(R.id.no_data_state)
    View noDataState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.network_problem_state)
    View networkProblemState;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.pay_voucher_btn_checkout)
    LinearLayout btnCheckOut;

    protected int page = 0;
    public int tot = 0;
    public int b = 0;
    protected List<VoucherAmountMsgResp> mList = new ArrayList<>();
    PayByVoucherAdapter mAdapter;
    PayByVoucherPresenter mPresenter;

    List<VoucherAmountMsgResp> orderDatas = new ArrayList<VoucherAmountMsgResp>();
    public void setVoucherData(List<VoucherAmountMsgResp> orderDatas) {
        this.orderDatas.clear();
        this.orderDatas.addAll(orderDatas);
    }

    protected String orderNumber = "";
    protected int totBayar = 0;
    public List<String> listAmount = new ArrayList<>();
    public List<String> listQty = new ArrayList<>();

    public static void showFragment(BaseActivity sourceFragment) {
        PayByVoucherFragment fragment = new PayByVoucherFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        mPresenter = new PayByVoucherPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_rv_pay_by_voucher;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    protected void initRVPayByVoucher() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRvList.setLayoutManager(linearLayoutManager);
        mAdapter = new PayByVoucherAdapter(this, mList, mRvList, this);
        mRvList.setItemAnimator(new DefaultItemAnimator());
        mRvList.setAdapter(mAdapter);
        mRvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                VoucherAmountMsgResp item = (VoucherAmountMsgResp) childView.getTag();

                mRvList.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        v.clearFocus();
                        return false;
                    }
                });


                }

        }));
    }


    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        tvTotalVoucherV.setText("0");
        mPresenter.getOrder();
    }

    public void setListAmount(List<String> amount) {
        this.listAmount.clear();
        this.listAmount.addAll(amount);
    }

    public void setListQty(List<String> value) {
        this.listQty.clear();
        this.listQty.addAll(value);
    }

    @Override
    public void onSelected(List<VoucherAmountMsgResp> voucherAmountMsgResp) {

        int a = 0;

        for (int j = 0; j < voucherAmountMsgResp.size(); j++) {

            a = a + voucherAmountMsgResp.get(j).getAmountSum();

            tot = a;

        }

        tvTotalVoucherV.setText("" + a);

        setVoucherData(voucherAmountMsgResp);

    }

    @OnClick(R.id.pay_voucher_btn_checkout)
    public void doPay() {


        if (!tvTotalBayarV.getText().toString().equals("") && tvTotalBayarV.getText().toString() != null) {
            int z = Integer.parseInt(tvTotalBayarV.getText().toString());
            int x = Integer.parseInt(tvTotalVoucherV.getText().toString());
            if (x >= z) {
                confirmDialog();
            } else {
                showToast("Total Voucher harus lebih besar atau sama dengan Total Bayar");
            }

        } else {
            showToast("Total Bayar Gagal didapat, silahkan reload ulang");
        }

    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(getActivity()), false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
        txtView.setText("Total Biaya " + AmountFormatter.format(b) + " akan dibayar menggunakan Total Voucher " + AmountFormatter.format(tot) + " ?");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);

        final AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mPresenter.doPayByVoucher();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
}
