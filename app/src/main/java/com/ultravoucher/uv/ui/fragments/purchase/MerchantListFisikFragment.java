package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.ui.adapters.MerchantListFisikAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class MerchantListFisikFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = "MerchantListFragment";

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.ll_grid)
    LinearLayout llGrid;

    @BindString(R.string.loading)
    String LOADING;

    @BindView(R.id.progress)
    View progressBar;
    @BindView(R.id.progress_message)
    TextView progressMsg;
    @BindView(R.id.no_data_state)
    View noDataState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;

    MerchantListFisikAdapter mAdapter;
    MerchantListFisikPresenter mPresenter;
    protected int page = 0;
    private Handler mHandler;
    String keyword = "";
    protected List<StoreListMsgResp> mData =  new ArrayList<>();

    public static void showFragment(BaseFragment sourceFragment) {
        MerchantListFragment fragment = new MerchantListFragment();
        FragmentTransaction transaction = sourceFragment.getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        transaction.commit();
    }


    @Override
    protected int getLayout() {
        return R.layout.f_merchant_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = 0;
        mPresenter = new MerchantListFisikPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String a = etSearch.getText().toString();

                if (a.equals("")) {
                    keyword = "";
                    gridView.setVisibility(View.GONE);
                    mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
                }
            }
        });

//        setupGridView();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                gridView.setVisibility(View.GONE);
//                mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
            }
        });
//        mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        setupGridView();
        mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
    }

    private void setupGridView() {
        mAdapter = new MerchantListFisikAdapter(getContext(), mData, gridView);
        gridView.setAdapter(mAdapter);

        mHandler = new Handler();
        mAdapter.setOnLoadMoreListener(new MerchantListFisikAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mData.removeAll(Collections.singleton(null));
                mData.add(null);
                mAdapter.notifyDataSetInvalidated();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreMerchantList();
                    }
                }, 5000);
            }
        });
    }

    private void loadMoreMerchantList() {
        mPresenter.PresentGetAllMerchantLoadMore(mPresenter.presentGetListMerchantReq());
    }

    @OnClick(R.id.btnSearch)
    void clicked() {
        if (etSearch.getText().toString().equals("")) {
            showToast("Masukkan kata kunci");
        } else {
            page = 0;
            keyword = etSearch.getText().toString();
            gridView.setVisibility(View.GONE);
            mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
        }
    }

    @Override
    public void onRefresh() {
        page = 0;
        gridView.setVisibility(View.GONE);
        mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser) {
                page = 0;
                setupGridView();
                mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
            }
        }
    }*/
}
