package com.ultravoucher.uv.ui.fragments.mainpage;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.AdvBannerMainPageAdapter2;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 1/24/2018.
 */

public class PopupAdvDialogFragment extends BaseDialogFragment {

    @BindView(R.id.homeViewpager)
    ViewPager homeViewpager;

    @BindView(R.id.circleMenuPageIndicator)
    CirclePageIndicator circlePageIndicator;

    /*@BindView(R.id.cpi)
    CirclePageIndicator mAdvIndicator;*/

    AdvBannerMainPageAdapter2 adapter;
    PopUpAdvDialogPresenter mPresenter;

    private Handler advSliderHandler;
    private final int SLIDER_DELAY_ADVERTISING_IN_SECOND = 5;
    private static int currentPage = 0;

    List<AdvertisingMsgResp> datas = new ArrayList<>();
    public void setData(List<AdvertisingMsgResp> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PrefHelper.setString(PrefKey.INITIAL_RUN, "");
        mPresenter = new PopUpAdvDialogPresenter(this);

    }

    @Override
    protected int getLayout() {
        return R.layout.a_popup;
    }

    @Override
    public void onResume() {
        super.onResume();
        initAdv();
    }

    @OnClick(R.id.ivClose)
    void close() {

        getDialog().dismiss();

    }

    public void initAdv() {
        mPresenter.presentAdvBanner();
        adapter = new AdvBannerMainPageAdapter2(datas, (BaseActivity) getActivity());
        homeViewpager.setAdapter(adapter);
        circlePageIndicator.setViewPager(homeViewpager);
//        mAdvIndicator.setViewPager(homeViewpager);
        final float density = getResources().getDisplayMetrics().density;

//        mAdvIndicator.setRadius(5 * density);
        advSliderHandler = new Handler();
        startPageAdvSwitchTimer();

        /*mAdvIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });*/
    }

    private void startPageAdvSwitchTimer() {
        advSliderHandler.postDelayed(advSliderRunnable, SLIDER_DELAY_ADVERTISING_IN_SECOND * 1000);
    }

    private void stopPageAdvSwitchTimer() {
        advSliderHandler.removeCallbacks(advSliderRunnable);
    }

    private Runnable advSliderRunnable = new Runnable() {
        @Override
        public void run() {

            if (currentPage == datas.size()) {
                currentPage = 0;
            }
            homeViewpager.setCurrentItem(currentPage++,true);
            Log.d("TAG", "run: runBabe! current = " + currentPage);
            startPageAdvSwitchTimer();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        stopPageAdvSwitchTimer();
    }
}

    /*@BindView(R.id.carouselView)
    CarouselView carouselView;

    List<String> postImages = new ArrayList<>();
//    List<PostListMsgResp> mData = new ArrayList<>();
    PopUpAdvDialogPresenter mPresenter;

    @Override
    protected int getLayout() {
        return R.layout.a_popup;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new PopUpAdvDialogPresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.presentAdvBanner();
    }
}*/
