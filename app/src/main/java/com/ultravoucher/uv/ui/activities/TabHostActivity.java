package com.ultravoucher.uv.ui.activities;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.ultravoucher.uv.R;

public class TabHostActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_tab_host);

        TabHost tabHost = getTabHost();

        TabHost.TabSpec homeSpec = tabHost.newTabSpec("Home");
        homeSpec.setIndicator("Home", getResources().getDrawable(R.drawable.ic_home_96px));
        Intent homeIntent = new Intent(this, LoginActivity.class);
        homeSpec.setContent(homeIntent);

        TabHost.TabSpec purchaseSpec = tabHost.newTabSpec("Purchase");
        purchaseSpec.setIndicator("Purchase", getResources().getDrawable(R.drawable.ic_home_96px));
        Intent purchaseIntent = new Intent(this, LoginActivity.class);
        homeSpec.setContent(purchaseIntent);

        tabHost.addTab(homeSpec);
        tabHost.addTab(purchaseSpec);
    }
}
