package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.introduction.FirstPageFragment;

import butterknife.BindView;

public class IntroductionActivity extends BaseActivity {

    private static final String STATUS = "status";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;

    String stat;

    public static void startActivity(BaseActivity sourceActivity, String status){
        Intent i = new Intent(sourceActivity, IntroductionActivity.class);
        i.putExtra(STATUS, status);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stat = getIntent().getStringExtra(STATUS);

        if (stat.equals("a")) {
            FirstPageFragment.showFragment(this, "first");
        } else if (stat.equals("b")) {
            FirstPageFragment.showFragment(this, "b");
        }


    }

    @Override
    protected int getLayout() {
        return R.layout.a_introduction;
    }

}
