package com.ultravoucher.uv.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements Validator.ValidationListener{

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.registration_et_email)
    @NotEmpty
    @Email
    EditText registrationEtEmail;

    @BindView(R.id.registration_et_phone)
    @NotEmpty
    EditText registrationEtPhone;

    @BindView(R.id.registration_et_password)
    @NotEmpty
    @Password(min = 6)
    EditText registrationEtPassword;

    @BindView(R.id.registration_et_confirm_password)
    @NotEmpty
    @Password(min = 6)
    EditText registrationEtConfirmPassword;

    @BindView(R.id.rbRegister)
    RadioButton rbRegister;

    @BindView(R.id.tvSyarat)
    TextView tvSyarat;

    @BindView(R.id.ll_register)
    LinearLayout linearLayoutRegis;

    @BindView(R.id.register_btn_simpan)
    LinearLayout linearLayout;

    @BindString(R.string.loading)
    String LOADING;

    private Validator mValidator;
    RegisterPresenter mPresenter;

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, RegisterActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_register;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        linearLayout.setClickable(false);
        mPresenter = new RegisterPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        setupToolbar();

        SpannableString ss = new SpannableString(getResources().getString(R.string.syarat_ketentuan));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {

                /*DEV*/
                /*SyaratDanKetentuanActivity.startActivity(RegisterActivity.this,
                        "http://52.76.117.11/vouchercenter/termsandconditions/true");*/

                /*PROD*/
                SyaratDanKetentuanActivity.startActivity(RegisterActivity.this,
                        "http://13.228.139.202/help/termsandconditions.html", "TAC");



            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableSpan, 49, 70, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvSyarat.setText(ss);
        tvSyarat.setMovementMethod(LinkMovementMethod.getInstance());
        tvSyarat.setHighlightColor(getResources().getColor(R.color.t0_white));

        rbRegister.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    linearLayout.setClickable(true);
                }
            }
        });

        final Context context = this;
        linearLayoutRegis.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (registrationEtEmail.isFocused()) {
                        Rect emailRect = new Rect();
                        registrationEtEmail.getGlobalVisibleRect(emailRect);
                        if (!emailRect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            registrationEtEmail.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    if (registrationEtPassword.isFocused()) {
                        Rect passRec = new Rect();
                        registrationEtPassword.getGlobalVisibleRect(passRec);
                        if (!passRec.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            registrationEtPassword.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    if (registrationEtConfirmPassword.isFocused()) {
                        Rect passRec = new Rect();
                        registrationEtConfirmPassword.getGlobalVisibleRect(passRec);
                        if (!passRec.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            registrationEtConfirmPassword.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    if (registrationEtPhone.isFocused()) {
                        Rect passRec = new Rect();
                        registrationEtPhone.getGlobalVisibleRect(passRec);
                        if (!passRec.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            registrationEtPhone.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                }
                return false;
            }
        });
    }

    @OnClick(R.id.register_btn_simpan)
    public void onViewClicked() {

        String pass = registrationEtPassword.getText().toString();
        String confirmPass = registrationEtConfirmPassword.getText().toString();
        String noHp = registrationEtPhone.getText().toString();
        if (noHp.trim().length() < 10) {
            showToast("Nomor Handphone minimum 10 digit");
        } else {
            if (pass.equals(confirmPass)) {
                mValidator.validate();
            } else {
                showToast("Ulangi password tidak sesuai");
            }
        }


    }

    @Override
    public void onValidationSucceeded() {
        PrefHelper.setString(PrefKey.PASSWORD, registrationEtPassword.getText().toString());
        mPresenter.signUp(mPresenter.constructRegistrationReq());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText){

                if(message.equalsIgnoreCase("invalid password")){
                    message = "Password Minimal 6 Karakter";
                }

                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Registrasi");
        toolbarTitle.setAllCaps(true);
    }
}
