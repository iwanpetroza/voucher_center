package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.DisOrder;
import com.ultravoucher.uv.data.api.beans.JNECheckRatesMsgResp;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.api.request.DiscardOrderReq;
import com.ultravoucher.uv.data.api.request.JNECheckRatesReq;
import com.ultravoucher.uv.data.api.request.JNEGetDestinationReq;
import com.ultravoucher.uv.data.api.request.ListDeliveryAddressReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.UpdateDelMethod;
import com.ultravoucher.uv.data.api.request.UpdateDelMethodReq;
import com.ultravoucher.uv.data.api.response.AddToCartResponse;
import com.ultravoucher.uv.data.api.response.DiscardOrderResp;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.JNECheckRatesResponse;
import com.ultravoucher.uv.data.api.response.JNEGetDestinationResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.PayMethodActivity;
import com.ultravoucher.uv.utils.AmountFormatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class MyCartPresenter {

    MyCartFragment mContext;

    String district, city, jneCode, addressId, addId;
    int var, ins;
    int z = 0;
    int c = 0;
    int ongkosKirim = 0;
    double d;
    Boolean addressStatus;

    public MyCartPresenter(MyCartFragment mContext) {
        this.mContext = mContext;
    }

    public void getmycartdata() {
        mContext.mRvList.setVisibility(View.GONE);
        mContext.progressBar.setVisibility(View.VISIBLE);
        mContext.networkProblemState.setVisibility(View.GONE);
        MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mContext.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {

                if (response.code() == 200) {
                    MyCartResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("OD004")) {
                        if (resp.getOrders().isEmpty()) {
                            String message = mContext.getString(R.string.no_data);
                            mContext.noDataState.setVisibility(View.VISIBLE);
                            mContext.noDataMessage.setText(message);
                            mContext.tvTotalBrgV.setText("0");
                            mContext.tvInsuranceV.setText("0");
                            mContext.qtyItem = 0;
                            mContext.tvCheckout.setText("Bayar");
                            mContext.ll_pengiriman.setVisibility(View.GONE);
                            mContext.llInsurance.setVisibility(View.GONE);
                        } else {
                            if (mContext != null && mContext.isAdded()) {

                                mContext.mRvList.setVisibility(View.VISIBLE);
                                mContext.setDatas(resp.getOrders());
                                mContext.vClass = String.valueOf(resp.getOrders().get(0).getVoucherClass());
                                mContext.progressBar.setVisibility(View.GONE);
                                mContext.totalTrx = resp.getTotalTrxAmount();
                                mContext.qtyItem = resp.getOrders().size();
                                mContext.totBrg = resp.getTotalTrxAmount();
                                mContext.highest = resp.getHighestVoucherClass();

                                if (resp.getOrders().size() > 0) {
                                    int s = 0;
                                    for (int j = 0; j < resp.getOrders().size(); j++) {
                                        if (s == 0) {
                                            s = resp.getOrders().get(j).getInsuranceFee();
                                        } else {
                                            s = s + resp.getOrders().get(j).getInsuranceFee();
                                        }

                                        mContext.totIns = s;
                                    }
                                } else {
                                    mContext.totIns = resp.getOrders().get(0).getInsuranceFee();
                                }

                                String t = mContext.tvOngkirV.getText().toString();


                                if (mContext.tvOngkirV.getVisibility() == View.VISIBLE
                                        /*&& !t.equals("0")*/) {
                                    if (resp.getOrders().size() > 0) {
                                        if (Integer.toString(resp.getOrders().get(0).getShippingAmount()) == null
                                                && Integer.toString(resp.getOrders().get(0).getShippingAmount()).equals("")) {
                                            for (int i = 0; i < resp.getOrders().size(); i++) {
                                                if (Integer.toString(resp.getOrders().get(i).getShippingAmount()) != null) {
                                                    c = 1;
                                                    ongkosKirim = resp.getOrders().get(i).getShippingAmount();
                                                    mContext.tvOngkirV.setText("" + AmountFormatter.format(ongkosKirim));
                                                    mContext.tvTotalBrgV.setText("" + AmountFormatter.format((resp.getTotalTrxAmount() - ongkosKirim)));
                                                } else {
                                                    if (c == 0)  {
                                                        c = 0;
                                                        ongkosKirim = 0;
                                                    }
                                                }
                                            }

                                        } else {
                                            c = 1;
                                            ongkosKirim = resp.getOrders().get(0).getShippingAmount();
                                            mContext.tvOngkirV.setText("" + AmountFormatter.format(ongkosKirim));
                                            mContext.tvTotalBrgV.setText("" + AmountFormatter.format((resp.getTotalTrxAmount() - ongkosKirim)));
                                        }
                                    } else {
                                        if (Integer.toString(resp.getOrders().get(0).getShippingAmount()) != null) {
                                            c = 1;
                                            ongkosKirim = resp.getOrders().get(0).getShippingAmount();
                                            mContext.tvOngkirV.setText("" + AmountFormatter.format(ongkosKirim));
                                            mContext.tvTotalBrgV.setText("" + AmountFormatter.format((resp.getTotalTrxAmount() - ongkosKirim)));
                                        } else {
                                            c = 0;
                                            district = "";
                                            city = "";
                                            ongkosKirim = 0;
                                        }
                                    }
                                }

                                int totAll = 0;
                                if (mContext.tvInsuranceV.getVisibility() == View.VISIBLE && !mContext.tvInsuranceV.getText().toString().equals("0")
                                        /*&& !mContext.tvOngkirV.getText().toString().equals("0")*/) {
                                    totAll = ((resp.getTotalTrxAmount() - ongkosKirim) - mContext.totIns) + mContext.totIns + ongkosKirim;
                                    mContext.tvInsuranceV.setText("" + AmountFormatter.format(mContext.totIns));
                                } else {
                                    totAll = resp.getTotalTrxAmount() + 0 + 0;
                                }

                                mContext.tvCheckout.setText("Bayar " + AmountFormatter.format(totAll));


                                if (resp.getOrders().size() > 0) {
                                    if (resp.getOrders().get(0).getDistrict() == null) {
                                        for (int i = 0; i < resp.getOrders().size(); i++) {
                                            if (resp.getOrders().get(i).getDistrict() != null) {
                                                z =1;
                                                addId = resp.getOrders().get(i).getDeliveryAddressId();
                                                district = "" +resp.getOrders().get(i).getDistrict();
                                                city = "" + resp.getOrders().get(i).getCity();
                                                mContext.tvAddress.setText(resp.getOrders().get(i).getDeliveryAddress() + ", " + resp.getOrders().get(i).getVillage() + ", "
                                                        + resp.getOrders().get(i).getDistrict() + ", " + resp.getOrders().get(i).getProvince() + " " + resp.getOrders().get(i).getPostalCode());

                                                getDest(constructGetListCity());
                                            } else {
                                                if (z == 0)  {
                                                    z = 0;
                                                    district = "";
                                                    city = "";
                                                    mContext.tvAddress.setText("Masukkan Alamat terlebih dahulu");
                                                }
                                            }
                                        }

                                    } else {
                                        z = 1;
                                        addId = resp.getOrders().get(0).getDeliveryAddressId();
                                        district = "" +resp.getOrders().get(0).getDistrict();
                                        city = "" + resp.getOrders().get(0).getCity();
                                        mContext.tvAddress.setText(resp.getOrders().get(0).getDeliveryAddress() + ", " + resp.getOrders().get(0).getVillage() + ", "
                                                + resp.getOrders().get(0).getDistrict() + ", " + resp.getOrders().get(0).getProvince() + " " + resp.getOrders().get(0).getPostalCode());
                                        getDest(constructGetListCity());
                                    }
                                } else {
                                    if (resp.getOrders().get(0).getDistrict() != null) {
                                        z = 1;
                                        addId = resp.getOrders().get(0).getDeliveryAddressId();
                                        district = "" +resp.getOrders().get(0).getDistrict();
                                        city = "" + resp.getOrders().get(0).getCity();
                                        mContext.tvAddress.setText(resp.getOrders().get(0).getDeliveryAddress() + ", " + resp.getOrders().get(0).getVillage() + ", "
                                                + resp.getOrders().get(0).getDistrict() + ", " + resp.getOrders().get(0).getProvince() + " " + resp.getOrders().get(0).getPostalCode());
                                        getDest(constructGetListCity());
                                    } else {
                                        z = 0;
                                        district = "";
                                        city = "";
                                        mContext.tvAddress.setText("Masukkan Alamat terlebih dahulu");
                                    }
                                }




                                int a = 0;
                                for(int i = 0; i < resp.getOrders().size(); i++) {


                                    if(resp.getOrders().get(i).getDeliveryMethod() == -1){
                                        a = 1;
                                    }

                                    if (resp.getOrders().get(i).getDeliveryMethod() == -1) {
                                        mContext.ll_pengiriman.setVisibility(View.VISIBLE);
                                    } else if (resp.getOrders().get(i).getDeliveryMethod() == 1) {
                                        mContext.ll_pengiriman.setVisibility(View.GONE);
                                    }
                                }

                                if(a == 1){
                                    mContext.ll_pengiriman.setVisibility(View.VISIBLE);
                                }else{
                                    mContext.ll_pengiriman.setVisibility(View.GONE);
                                }

                                mContext.tvTotalBrgV.setText("" + AmountFormatter.format((mContext.totBrg - ongkosKirim)));
                            }
                        }

                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mContext != null && mContext.isAdded()) {
                            String message = "Sesi Anda telah berakhir, Silahkan Login ulang";
                            mContext.showToast(message);
                            mContext.getActivity().finish();
                            mContext.doNeedRelogin();
                        }
                    }

                    else if (resp.getAbstractResponse().getResponseStatus().equals("OD904")) {

                        mContext.tvTotalBrgV.setText("" + (mContext.totBrg - ongkosKirim));

                    } else {
                        if (mContext != null && mContext.isAdded()) {
                            String message = mContext.getString(R.string.no_data);
                            mContext.qtyItem = 0;
                            mContext.noDataState.setVisibility(View.VISIBLE);
                            mContext.noDataMessage.setText(message);
                        }
                    }
                } else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if (mContext != null && mContext.isAdded()) {
                                ((BaseActivity) mContext.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mContext != null && mContext.isAdded()) {
                        mContext.networkProblemState.setVisibility(View.VISIBLE);
                        mContext.networkProblemMessage.setText(mContext.getString(R.string.connection_error));
                        mContext.progressBar.setVisibility(View.GONE);
                    }
                }
                mContext.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                if (mContext != null && mContext.isAdded()) {
                    mContext.networkProblemState.setVisibility(View.VISIBLE);
                    mContext.networkProblemMessage.setText(mContext.getString(R.string.connection_error));
                    mContext.progressBar.setVisibility(View.GONE);
                    mContext.qtyItem = 0;
                }
            }
        });
    }

    public void discardOrder(Order data) {
        mContext.showProgressDialog("Discard Order Send Request");
        DisOrder disOrder = new DisOrder();
        DiscardOrderReq request = new DiscardOrderReq();


        disOrder.setOrderId(data.getOrderId());
        disOrder.setVariantId(data.getVariantId());
        disOrder.setQuantity(data.getQuantity());
        disOrder.setProductType(data.getProductType());
        disOrder.setStatus("4");
        List<DisOrder> disOrders = new ArrayList<DisOrder>();
        disOrders.add(disOrder);
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setOrders(disOrders);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<DiscardOrderResp> discardOrderRespCall = mContext.getVoucherApi().doDiscardOrder(authKey, deviceUniq, request);
        discardOrderRespCall.enqueue(new Callback<DiscardOrderResp>() {
            @Override
            public void onResponse(Call<DiscardOrderResp> call, Response<DiscardOrderResp> response) {
                if (response.code() == 200) {
                    mContext.dismissProgressDialog();
                    DiscardOrderResp resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("OD002")) {
                        mContext.loadMyCart();

                        OutputDialogFragment viewDialog = new OutputDialogFragment(mContext, 6, 0);
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        String message = mContext.getString(R.string.need_relogin);
                        mContext.showToast(message);
                        mContext.getActivity().finish();
                        mContext.doNeedRelogin();
                    }

                    else {
                        mContext.showToast("Order has not Discard");
                    }
                } else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if (mContext != null && mContext.isAdded()) {
                                ((BaseActivity) mContext.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    mContext.dismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<DiscardOrderResp> call, Throwable t) {
                mContext.dismissProgressDialog();
                mContext.showToast("Connection is Eror");
            }
        });

    }

    protected void getDest(JNEGetDestinationReq req) {

        mContext.progressBar.setVisibility(View.VISIBLE);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        VoucherApi api = mContext.getVoucherApi();
        Call<JNEGetDestinationResponse> jneGetDestinationResponseCall = api.getDestination(contentType, authToken, deviceUniqueId, req);
        jneGetDestinationResponseCall.enqueue(new Callback<JNEGetDestinationResponse>() {
            @Override
            public void onResponse(Call<JNEGetDestinationResponse> call, Response<JNEGetDestinationResponse> response) {
                if (response.code() == 200) {
                    JNEGetDestinationResponse resp = response.body();

                    if(resp.getAbstractResponse().getResponseStatus().equals("JNE001")){

                        mContext.jneCodeV = "" + resp.getListCityCode().get(0).getCode();
                        checkRoutes(constructRatesReq());
                    } else if(resp.getAbstractResponse().getResponseStatus().equals("JNE002")) {
                        mContext.showToast("Ada kendala pada server JNE");
                    } else if(resp.getAbstractResponse().getResponseStatus().equals("JNE004")) {
                        mContext.datasJne.clear();
                        mContext.listCode.clear();
                        mContext.listPackage.clear();
                        mContext.etPacket.setText("");
                        mContext.showToast("Kecamatan tidak ditemukan");
                    }

                    else {
                        mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                } else {
                    mContext.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                mContext.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<JNEGetDestinationResponse> call, Throwable t) {
                try{
                    mContext.progressBar.setVisibility(View.GONE);
                    String message = mContext.getString(R.string.connection_error);
                    mContext.showToast(message);
                }catch(IllegalStateException ex){
                    ex.printStackTrace();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    JNEGetDestinationReq constructGetListCity() {
        JNEGetDestinationReq req = new JNEGetDestinationReq();
        String districtV = "";
        String cityV = "";

        if (district != null) {
            if (!district.equals("")) {

                districtV  = district;

            } else {
                districtV = "";
            }
        } else {
            districtV = "";
        }

        if (city != null) {
            if (!city.equals("")) {
                cityV  = city;
            } else {
                cityV = "";
            }
        } else {
            cityV = "";
        }



        req.setKeyInput(districtV);
        req.setCity(cityV);
        return req;
    }

    protected void checkRoutes(JNECheckRatesReq request) {

        mContext.progressBar.setVisibility(View.VISIBLE);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        VoucherApi api = mContext.getVoucherApi();
        Call<JNECheckRatesResponse> jneCheckRatesResponseCall = api.getRates(contentType, authToken, deviceUniqueId, request);
        jneCheckRatesResponseCall.enqueue(new Callback<JNECheckRatesResponse>() {
            @Override
            public void onResponse(Call<JNECheckRatesResponse> call, Response<JNECheckRatesResponse> response) {
                if (response.code() == 200) {
                    JNECheckRatesResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("JNE001")) {
                        if (!resp.getListPackage().equals("") && resp.getListPackage() != null) {
                            mContext.setDataJne(resp.getListPackage());
                            var = resp.getListPackage().size();

                            List<JNECheckRatesMsgResp> jneCheckRatesMsgResps = new ArrayList<>();

                            jneCheckRatesMsgResps = resp.getListPackage();

                            List<String> listVarCode = new ArrayList<>();
                            List<String> listVarName = new ArrayList<>();

                            JNECheckRatesMsgResp item;
                            for (int i = 0; i < var; i++) {
                                item = jneCheckRatesMsgResps.get(i);
                                listVarCode.add(String.valueOf(item.getPrice()));
                                listVarName.add(item.getServiceDisplay());
                            }

                            mContext.setDestCode(listVarCode);
                            mContext.setPacket(listVarName);
                            addressStatus = true;
                        } else {
                            mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("JNE002")) {
                        mContext.showToast("ada kendala pada server JNE");

                    }
                    else if (resp.getAbstractResponse().getResponseStatus().equals("JNE004")) {
                        mContext.showToast("Kecamatan tidak ditemukan");
                    } else {
                        mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mContext.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                mContext.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<JNECheckRatesResponse> call, Throwable t) {
                try {
                    mContext.progressBar.setVisibility(View.GONE);
                    String message = mContext.getString(R.string.connection_error);
                    mContext.showToast(message);
                }catch(IllegalStateException ex){
                    ex.printStackTrace();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    JNECheckRatesReq constructRatesReq() {
        JNECheckRatesReq req = new JNECheckRatesReq();
        req.setDestinationCode(mContext.jneCodeV);
        return req;
    }


    void getDelMethod(UpdateDelMethodReq req) {

        mContext.showProgressDialog(mContext.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mContext.getVoucherApi();

        Call<AddToCartResponse> addToCartResponseCall = api.updateDelMethod(contentType, authToken, deviceUniqueId, req);
        addToCartResponseCall.enqueue(new Callback<AddToCartResponse>() {
            @Override
            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {

                if (response.code() == 200) {
                    AddToCartResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("OD002")) {
                        if (mContext.ll_pengiriman.getVisibility() == View.VISIBLE) {
                            if (!mContext.etPacket.getText().toString().equals("") && mContext.etPacket.getText().toString() != null) {
                                PayMethodActivity.startActivity((BaseActivity) mContext.getActivity(), "", mContext.vClass, "purchase");
                            } else {
                                mContext.showToast("Silahkan pilih paket pengiriman Anda");
                            }
                        } else {
                            PayMethodActivity.startActivity((BaseActivity) mContext.getActivity(), "", mContext.vClass, "purchase");
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                        mContext.getActivity().finish();
                        mContext.doNeedRelogin();
                    }

                    else {
                        mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                } else {
                    mContext.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                mContext.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                mContext.dismissProgressDialog();
                mContext.showToast("Koneksi anda tidak stabil, silahkan coba lagi");
            }
        });
    }

    UpdateDelMethodReq constructDelMethod() {

        String paket = mContext.etPacket.getText().toString();
        String kurir = mContext.etKurir.getText().toString();
        String insurance = mContext.tvInsuranceV.getText().toString();

        UpdateDelMethodReq req = new UpdateDelMethodReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));

        List<UpdateDelMethod> orderPays = new ArrayList<>();
        for (int i = 0; i < mContext.datas.size(); i++) {

            UpdateDelMethod order = new UpdateDelMethod();
            order.setDeliveryMethod(mContext.datas.get(i).getDeliveryMethod());
            order.setDeliveryAddressId(addId);
            order.setOrderId(mContext.datas.get(i).getOrderId());
            if (!paket.equals("")) {
                order.setShippingType(paket);
            } else {
                order.setShippingType("");
            }

            if (!kurir.equals("")) {
                order.setShippingBy(kurir);
            } else {
                order.setShippingBy("");
            }

            order.setPickupDate("");
            order.setStatus(0);
            order.setPickupNote("");

            if (mContext.tvInsuranceV.getVisibility() == View.VISIBLE) {
                if (insurance.equals("0")) {
                    order.setInsuranceFee(0);
                } else {
                    order.setInsuranceFee(mContext.totIns);
                }
            } else {
                order.setInsuranceFee(0);
            }
            order.setShippingAmount(mContext.price);

            orderPays.add(order);
        }
        req.setOrders(orderPays);
        return req;
    }

}
