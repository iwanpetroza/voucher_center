package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;

import butterknife.BindView;
import butterknife.OnClick;

public class CCResultActivity extends BaseActivity {

    private static final String POST_ID = "postId";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.activity_ccresult)
    RelativeLayout activityCcresult;

    String order, on;

    public static void startActivity(BaseActivity sourceActivity, String url) {
        Intent i = new Intent(sourceActivity, CCResultActivity.class);
        i.putExtra(POST_ID, url);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_ccresult;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();

        Intent intent = getIntent();
        on = intent.getStringExtra("key");

        tv.setText(on);

    }

    @OnClick(R.id.btn)
    public void onViewClicked() {

        this.finish();
        HomePageActivity.startActivity(this, "");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        HomePageActivity.startActivity(this, "");
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar_title.setText("PAYMENT");
    }
}
