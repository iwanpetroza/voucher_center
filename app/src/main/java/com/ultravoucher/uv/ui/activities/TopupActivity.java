package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.ultravoucher.uv.R;

import butterknife.BindView;

public class TopupActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.tvTopup)
    TextView tvTopup;
    /*@BindView(R.id.tvVa)
    TextView tvVa;
    @BindView(R.id.topup_ic_arrow_bankpermata)
    ImageView topupIcArrowBankpermata;
    @BindView(R.id.topup_btn_permataBank)
    LinearLayout topupBtnPermataBank;
    @BindView(R.id.topup_desc_permataBank)
    LinearLayout topupDescPermataBank;

    @BindView(R.id.topup_ic_arrow_mobilePermata)
    ImageView topupIcArrowMobilePermata;
    @BindView(R.id.topup_desc_mobilePermata)
    LinearLayout topupDescMobilePermata;

    @BindView(R.id.topup_ic_arrow_inetPermata)
    ImageView topupIcArrowInetPermata;
    @BindView(R.id.topup_desc_inetPermata)
    LinearLayout topupDescInetPermata;

    @BindView(R.id.topup_ic_arrow_bankLain)
    ImageView topupIcArrowBankLain;
    @BindView(R.id.topup_desc_bankLain)
    LinearLayout topupDescBankLain;

    @BindView(R.id.topup_ic_arrow_inetBankLain)
    ImageView topupIcArrowInetBankLain;
    @BindView(R.id.topup_desc_inetBankLain)
    LinearLayout topupDescInetBankLain;

    @BindView(R.id.a_topup)
    RelativeLayout aTopup;

    ImageView iv;*/

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, TopupActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_topup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
//        tvVa.setText(PrefHelper.getString(PrefKey.VA_NUMBER));

        tvTopup.setText(
                "Via Bank Transfer BCA\n" +
                        "\n" +
                        "Silahkan transfer ke rekening BCA kami yaitu:\n" +
                        "No Rek: 4364080001\n" +
                        "Nama Akun: PT Trimegah Karya Pratama\n" +
                        "Cabang: BCA KCP Tebet \n" +
                        "Keterangan: 0812XXXXX\n" +
                        "\n" +
                        "Mohon masukan No. Handphone yang Anda gunakan untuk registrasi akun Ultra Voucher pada keterangan berita. Hal ini untuk mempercepat proses Topup anda.\n" +
                        "\n" +
                        "Proses Top Up akan di lakukan pada jam kerja 08:00 s/d 17:00. Untuk proses top up pada waktu weekend (Sabtu/Minggu) akan di proses di hari Senin.\n" +
                        "\n" +
                        "Apabila ada pertanyaan, Anda dapat menghubungi kami di:\n" +
                        "email : info@ultravoucher.co.id\n" +
                        "WhatsApp Chat : 08119276700 \n" +
                        "\n" +
                        "Terima kasih.");


    }

    /*@OnClick({R.id.topup_cv_permataBank, R.id.topup_cv_mobilePermata, R.id.topup_cv_inetPermata, R.id.topup_cv_bankLain, R.id.topup_cv_inetBankLain})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.topup_cv_permataBank:
                if (topupDescPermataBank.getVisibility() == View.GONE) {
                    topupDescPermataBank.setVisibility(View.VISIBLE);
                    topupIcArrowBankpermata.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    topupDescPermataBank.setVisibility(View.GONE);
                    topupIcArrowBankpermata.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                break;
            case R.id.topup_cv_mobilePermata:
                if (topupDescMobilePermata.getVisibility() == View.GONE) {
                    topupDescMobilePermata.setVisibility(View.VISIBLE);
                    topupIcArrowMobilePermata.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    topupDescMobilePermata.setVisibility(View.GONE);
                    topupIcArrowMobilePermata.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                break;
            case R.id.topup_cv_inetPermata:
                if (topupDescInetPermata.getVisibility() == View.GONE) {
                    topupDescInetPermata.setVisibility(View.VISIBLE);
                    topupIcArrowInetPermata.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    topupDescInetPermata.setVisibility(View.GONE);
                    topupIcArrowInetPermata.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }

                break;
            case R.id.topup_cv_bankLain:
                if (topupDescBankLain.getVisibility() == View.GONE) {
                    topupDescBankLain.setVisibility(View.VISIBLE);
                    topupIcArrowBankLain.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    topupDescBankLain.setVisibility(View.GONE);
                    topupIcArrowBankLain.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                break;
            case R.id.topup_cv_inetBankLain:
                if (topupDescInetBankLain.getVisibility() == View.GONE) {
                    topupDescInetBankLain.setVisibility(View.VISIBLE);
                    topupIcArrowInetBankLain.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                } else {
                    topupDescInetBankLain.setVisibility(View.GONE);
                    topupIcArrowInetBankLain.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
                break;
        }

    }*/

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Topup");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history_topup, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;

            case R.id.action_history:
//                CheckOutActivity.startActivity(this);
                GeneralActivity.startActivity(this, "historywalet");
                return true;
        }
        return super.onOptionsItemSelected(item);


    }
}
