package com.ultravoucher.uv.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.response.CheckVersionResponse;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import android.support.v7.app.AlertDialog.Builder;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 2/6/2018.
 */

public class HomePagePresenter {

    HomePageActivity mActivity;

    public HomePagePresenter (HomePageActivity mActivity) {
        this.mActivity = mActivity;
    }

    void getCountMyCart()
    {
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mActivity.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        int qty = 0;
                        if(resp.getOrders().size()>0){
                            for (int i = 0; i < resp.getOrders().size(); i++) {
                                if (qty == 0) {
                                    qty = resp.getOrders().get(i).getQuantity();
                                } else {
                                    qty = qty + resp.getOrders().get(i).getQuantity();
                                }
                            }

                            mActivity.tvCount.setVisibility(View.VISIBLE);
//                            mActivity.tvCount.setText(""+resp.getOrders().size());}
                            mActivity.tvCount.setText(""+qty);
                        } else {
                            mActivity.tvCount.setVisibility(View.GONE);
                        }
                    }
                    else
                    {

                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mActivity!= null) {
//                                (mActivity).doNeedRelogin();
                                mActivity.tvCount.setVisibility(View.GONE);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {

                }
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {

            }
        });
    }

    void getCheckVersion () {

        VoucherApi api = mActivity.getVoucherApi();
        Call<CheckVersionResponse> checkVersionResponseCall = api.doCheckVersion();
        checkVersionResponseCall.enqueue(new Callback<CheckVersionResponse>() {
            @Override
            public void onResponse(Call<CheckVersionResponse> call, Response<CheckVersionResponse> response) {
                if(response.code() == 200) {
                    CheckVersionResponse resp = response.body();
                    int versionCode = BuildConfig.VERSION_CODE;
                    if (!String.valueOf(versionCode).equals(resp.getMobileVersion().getLastVersion())) {
                        LayoutInflater li = LayoutInflater.from(mActivity);
                        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(mActivity), false);

                        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mActivity);

                        builder.setView(promptsView);
                        builder.setCancelable(false);

                        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
                        txtView.setText("Silahkan update aplikasi Anda terlebih dahulu untuk melanjutkan");

                        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
                        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);
                        btnConfirm.setText("UPDATE");

                        final android.app.AlertDialog dialog = builder.create();

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                try{
                                    mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mActivity.getPackageName())));
                                } catch (Exception e) {
                                    mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + mActivity.getPackageName())));
                                }

                                dialog.dismiss();
                            }
                        });

                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                mActivity.finish();

                            }
                        });

                        dialog.show();
                    }
                    getCountMyCart();
                }


            }

            @Override
            public void onFailure(Call<CheckVersionResponse> call, Throwable t) {
                getCountMyCart();
            }
        });

    }
}
