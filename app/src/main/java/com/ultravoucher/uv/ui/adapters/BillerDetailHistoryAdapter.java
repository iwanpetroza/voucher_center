package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.HistoryBillerDetailMsgResp;
import com.ultravoucher.uv.ui.fragments.history.HistoryBillerDetailFragment;
import com.ultravoucher.uv.utils.AmountFormatter2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.iwgang.countdownview.CountdownView;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class BillerDetailHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private HistoryBillerDetailFragment mContext;
    private List<HistoryBillerDetailMsgResp> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private long different;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public BillerDetailHistoryAdapter(HistoryBillerDetailFragment context, List<HistoryBillerDetailMsgResp> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_history_biller, parent, false);

            vh = new ListHistoryProductHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListHistoryProductHolder) {
            final HistoryBillerDetailMsgResp item = mDataset.get(position);

            if (item.getStatus().equalsIgnoreCase("Cancel")){
                ((ListHistoryProductHolder) holder).llCD.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvVA.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Dibatalkan");
            } else if (item.getStatus().equalsIgnoreCase("Paid")){
                ((ListHistoryProductHolder) holder).llCD.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvVA.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Pembayaran Sukses");
            } else if (item.getStatus().equalsIgnoreCase("Pending")){
                ((ListHistoryProductHolder) holder).llCD.setVisibility(View.VISIBLE);
                ((ListHistoryProductHolder) holder).tvVA.setVisibility(View.VISIBLE);
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Menunggu Pembayaran");
            } else {
                ((ListHistoryProductHolder) holder).llCD.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvVA.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvStatus.setText("Status : Menunggu Pembayaran");
            }

            if (item.getPaymentExpiredDate() == null || item.getPaymentExpiredDate().equals("") || item.getPaymentExpiredDate().equals("-")) {
                ((ListHistoryProductHolder) holder).countDown.setVisibility(View.GONE);
                ((ListHistoryProductHolder) holder).tvCD.setText("Batas Waktu  :  -");
            } else {
                getMilis(item.getPaymentExpiredDate());
                ((ListHistoryProductHolder) holder).tvCD.setText("Batas Waktu  :  ");
                ((ListHistoryProductHolder) holder).countDown.start(different);
            }

            ((ListHistoryProductHolder) holder).itemView.setTag(item);
            ((ListHistoryProductHolder) holder).tvProduct.setText("Nama Produk : " + item.getProductName());
            if (item.getToken() == null || item.getToken().equalsIgnoreCase("")) {
                ((ListHistoryProductHolder) holder).tvToken.setVisibility(View.GONE);
            } else {
                ((ListHistoryProductHolder) holder).tvToken.setText("Token : " + item.getToken());
            }
            ((ListHistoryProductHolder) holder).tvOrderNumber.setText("No Order : " + item.getTransactionId());

            if (item.getTransactionDate().equals("") || item.getTransactionDate().equals("-")) {
                ((ListHistoryProductHolder) holder).tvOrderDate.setText("Tgl Order : -");
            } else {
                ((ListHistoryProductHolder) holder).tvOrderDate.setText("Tgl Order : " + item.getTransactionDate());
            }

            if (item.getContractName() == null || item.getContractName().equals("") || item.getContractName().equals("-")) {
                ((ListHistoryProductHolder) holder).tvName.setVisibility(View.GONE);
            } else {
                ((ListHistoryProductHolder) holder).tvName.setText("Nama Pelanggan : " + item.getContractName());
            }

            ((ListHistoryProductHolder) holder).tvCustNo.setText("No Pelanggan : " + item.getContractNo());
            ((ListHistoryProductHolder) holder).tvTotalPayment.setText("Total Pembayaran : Rp. " + AmountFormatter2.format(item.getTotalAmount()));
            ((ListHistoryProductHolder) holder).tvVA.setText("Transfer ke nomor VA : " + item.getVaNumber());
            ((ListHistoryProductHolder) holder).tvPaymentMethod.setText("Metode Pembayaran : " + item.getPaymentMethod());

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListHistoryProductHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llCD)
        LinearLayout llCD;

        @BindView(R.id.tvProduct)
        TextView tvProduct;

        @BindView(R.id.tvToken)
        TextView tvToken;

        @BindView(R.id.tvOrderNumber)
        TextView tvOrderNumber;

        @BindView(R.id.tvOrderDate)
        TextView tvOrderDate;

        @BindView(R.id.tvCustNo)
        TextView tvCustNo;

        @BindView(R.id.tvTotalPayment)
        TextView tvTotalPayment;

        @BindView(R.id.tvPaymentMethod)
        TextView tvPaymentMethod;

        @BindView(R.id.tvVA)
        TextView tvVA;

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvStatus)
        TextView tvStatus;

        @BindView(R.id.tvCD)
        TextView tvCD;

        @BindView(R.id.countDown)
        CountdownView countDown;


        public ListHistoryProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    //GET ELAPSED TIME IN MILIS

    public void getMilis(String tgl) {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timePresent = df.format(c);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        try {

            Date date1 = simpleDateFormat.parse(tgl);
            Date date2 = simpleDateFormat.parse(timePresent);

            different = date1.getTime() - date2.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
