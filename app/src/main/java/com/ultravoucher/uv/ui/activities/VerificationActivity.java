package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.ultravoucher.uv.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class VerificationActivity extends BaseActivity implements Validator.ValidationListener{

    private static final String STATUS = "status";
    private static final String CODE = "code";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.etPin)
    @NotEmpty
    PinEntryEditText etPin;

    @BindView(R.id.tvText)
    TextView tvText;

    @BindView(R.id.registration_et_password)
    EditText registrationEtPassword;

    @BindView(R.id.registration_password)
    TextInputLayout registrationPassword;

    @BindView(R.id.registration_et_confirm_password)
    EditText registrationEtConfirmPassword;

    @BindView(R.id.ll_forgot)
    LinearLayout ll_forgot;

    VerificationPresenter mPresenter;
    private Validator mValidator;
    String status, vercode;

    public static void startActivity(BaseActivity sourceActivity, String status, String code){
        Intent i = new Intent(sourceActivity, VerificationActivity.class);
        i.putExtra(STATUS, status);
        i.putExtra(CODE, code);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_verification;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        status = getIntent().getStringExtra(STATUS);
        vercode = getIntent().getStringExtra(CODE);

        /*IF STATUS = "" -> REGISTRATION
        * IF STATUS = BUKAN "" -> FORGOT PASS*/

        if (!status.equals("")) {
            ll_forgot.setVisibility(View.VISIBLE);
        }

        if (status.equals("RM")) {
            ll_forgot.setVisibility(View.VISIBLE);
            tvText.setText("Masukkan Kode Merchant yang sudah Anda terima dari Customer Service kami. Umtuk Member yang ingin bergabung menjadi Member Merchant, silahkan hubungi Customer Service kami.");
        }

        if (status.equals("FP")) {
            ll_forgot.setVisibility(View.VISIBLE);
        }

        mPresenter = new VerificationPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        SmsRadar.initializeSmsRadarService(this, new SmsListener() {
            @Override
            public void onSmsSent(Sms sms) {

            }

            @Override
            public void onSmsReceived(Sms sms) {
                //showToast(sms.getMsg());
                String [] data = sms.getMsg().split(" ");
                etPin.setText(data[4]);
                Log.e("dataSms",""+sms.getMsg());
            }
        });
    }

    @OnClick(R.id.register_btn_simpan)
    public void onViewClicked() {
        mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        String code = etPin.getText().toString();

        if (!status.equals("") && status.equals("FP")) {
            String pass = registrationEtPassword.getText().toString();
            String confirm = registrationEtConfirmPassword.getText().toString();

//            if (vercode.equals(code)) {
            if (pass.equals("") || confirm.equals("")) {
                showToast("Silahkan masukkan Password baru Anda terlebih dahulu");
            } else {
                if (confirm.equals(pass)) {
                    mPresenter.doReset();
                } else {
                    showToast("Maaf, Konfirmasi Password Anda tidak sesuai");
                }
            }

        } else {
//            if (code.equals(PrefHelper.getString(PrefKey.VERIFICATION_CODE))) {
                mPresenter.doVerification(mPresenter.constructVerificationReq());
//            } else {
//                showToast("Maaf, Kode yang Anda masukkan salah");
//            }
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Kode Verifikasi");
    }
}
