package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.OrderVouchers;
import com.ultravoucher.uv.data.api.request.HistoryProgressReq;
import com.ultravoucher.uv.data.api.response.HistoryProgressResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 12/04/18.
 */

public class HistoryDetailDigitalPresenter {

    private HistoryDetailDigitalFragment mFragment;

    public HistoryDetailDigitalPresenter(HistoryDetailDigitalFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetDigitalHistory(HistoryProgressReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryProgressResponse> historyProgressResponseCall = api.getHistory(contentType, authToken, deviceUId, req);
        historyProgressResponseCall.enqueue(new Callback<HistoryProgressResponse>() {
            @Override
            public void onResponse(Call<HistoryProgressResponse> call, Response<HistoryProgressResponse> response) {
                if (response.code() == 200) {
                    HistoryProgressResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            initList(resp.getDeals().get(mFragment.pos).getOrderVouchers());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<HistoryProgressResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }

    protected void initList(List<OrderVouchers> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<OrderVouchers> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (OrderVouchers imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected HistoryProgressReq presentGetHistoryReq() {
        HistoryProgressReq req = new HistoryProgressReq();
        req.setPage(mFragment.page);
        req.setNRecords(80);
        req.setsVoucherClass("D");
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }
}
