package com.ultravoucher.uv.ui.fragments.myreward;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.RewardResp;
import com.ultravoucher.uv.data.api.request.RewardReq;
import com.ultravoucher.uv.data.api.response.RewardResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class ListRewardPresenter {

    private ListRewardFragment mFragment;

    public ListRewardPresenter(ListRewardFragment mFragment){
        this.mFragment = mFragment;
    }

    protected void presentGetRewardList(RewardReq req) {

        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<RewardResponse> rewardResponseCall = api.doGetAllReward(authToken, dui, req);
        rewardResponseCall.enqueue(new Callback<RewardResponse>() {
            @Override
            public void onResponse(Call<RewardResponse> call, Response<RewardResponse> response) {
                if (response.code() == 200) {
                    RewardResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getRewardResponse().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                initList(resp.getRewardResponse());
                                mFragment.noDataState.setVisibility(View.GONE);
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                if (mFragment != null && mFragment.isAdded()) {
                                    swapped(resp.getRewardResponse());
                                    mFragment.noDataState.setVisibility(View.GONE);
                                    mFragment.progress.setVisibility(View.GONE);
                                    mFragment.rvList.setVisibility(View.VISIBLE);
                                }
                            }


                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }

                    } else {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);

                    }

                    mFragment.swipe_container.setRefreshing(false);

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }

                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<RewardResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.noDataState.setVisibility(View.GONE);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }


    protected void initList(List<RewardResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void swapped(List<RewardResp> data) {
        mFragment.mAdapter.swap(data);
    }

    /*protected void addToList(List<RewardResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (RewardResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }*/

    protected RewardReq constructListReward() {
        RewardReq req = new RewardReq();
        req.setPage(mFragment.page);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(50);
        req.setOrder("");
        req.setCityId("");
        req.setKeyword(mFragment.keyword);
        req.setRewardCategory("");
        return req;
    }
}
