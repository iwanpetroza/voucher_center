package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.purchase.AddAddressFragment;
import com.ultravoucher.uv.ui.fragments.purchase.ListAddressFragment;
import butterknife.BindView;
import butterknife.OnClick;

public class AddressActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.btnSimpan)
    LinearLayout btnSimpan;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.tvBtnSimpan)
    TextView tvBtnSimpan;

    private static final String STATUS = "status";

    String status;

    public static void startActivity(BaseActivity sourceActivity, String status){
        Intent i = new Intent(sourceActivity, AddressActivity.class);
        i.putExtra(STATUS, status);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        setupToolbar();
        if (status.equalsIgnoreCase("addaddress")) {
            toolbarTitle.setText("Tambah Alamat");
            btnSimpan.setVisibility(View.GONE);
            AddAddressFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("listaddress")) {
            toolbarTitle.setText("Pilih Alamat");
            tvBtnSimpan.setText("TAMBAH ALAMAT");
            ListAddressFragment.showFragment(this);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (status.equalsIgnoreCase("addaddress")) {
            toolbarTitle.setText("Tambah Alamat");
        } else if (status.equalsIgnoreCase("listaddress")) {
            toolbarTitle.setText("Pilih Alamat");
        }
    }

    @OnClick(R.id.btnSimpan)
    public void doSave() {
        if (status.equalsIgnoreCase("addaddress")) {
            this.finish();
        } else if (status.equalsIgnoreCase("listaddress")) {
            this.finish();
            AddressActivity.startActivity(this, "addaddress");
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
