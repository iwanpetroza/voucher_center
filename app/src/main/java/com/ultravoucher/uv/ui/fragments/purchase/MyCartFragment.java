package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.JNECheckRatesMsgResp;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.activities.AddressActivity;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.CheckOutActivity;
import com.ultravoucher.uv.ui.activities.PayMethodActivity;
import com.ultravoucher.uv.ui.adapters.MyCartListAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.AmountFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

public class MyCartFragment extends BaseFragment {
    private static final String TAG = "MyCartFragment";

    @BindView(R.id.mycart_rv)
    RecyclerView mRvList;
    @BindView(R.id.progress)
    View progressBar;
    @BindView(R.id.progress_message)
    TextView progressMsg;

    @BindView(R.id.et_kurir)
    TextView etKurir;

    @BindView(R.id.tvAddress)
    TextView tvAddress;

    @BindView(R.id.et_packet)
    TextView etPacket;

    @BindView(R.id.tvTotalBrgV)
    TextView tvTotalBrgV;

    @BindView(R.id.mycart_tv_checkout)
    TextView tvCheckout;

    @BindView(R.id.tvOngkirV)
    TextView tvOngkirV;

    @BindView(R.id.tvInsuranceV)
    TextView tvInsuranceV;

    @BindView(R.id.no_data_state)
    View noDataState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.network_problem_state)
    View networkProblemState;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.mycart_btn_checkout)
    LinearLayout btnCheckOut;

    @BindView(R.id.llInsurance)
    LinearLayout llInsurance;

    @BindView(R.id.rg)
    RadioGroup rg;

    @BindView(R.id.rb1)
    RadioButton rb1;

    @BindView(R.id.rb2)
    RadioButton rb2;

    @BindView(R.id.ll_pengiriman)
    LinearLayout ll_pengiriman;

    int totalTrx=0;
    int qtyItem=0;
    MyCartListAdapter mAdapter;
    MyCartPresenter mPresenter;
    List<JNECheckRatesMsgResp> datasJne = new ArrayList<>();
    List<String> listCode = new ArrayList<>();
    List<String> listPackage = new ArrayList<>();
    String delPrice = "";
    String jneCodeV;
    int price, totIns = 0;
    int rgId;
    int totBrg;
    int highest;
    String vClass;



    List<Order> datas = new ArrayList<Order>();

    public void setDatas(List<Order> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_mycart;
    }

    public static void showFragment(BaseActivity sourceActivity) {
        FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, new MyCartFragment(), TAG);
        fragmentTransaction.commit();
        Log.d(TAG, "showFragment: masuk");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MyCartPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((CheckOutActivity)getActivity()).changeTitleBar("Cart");

        etKurir.setText("JNE");
//        llInsurance.setVisibility(View.GONE);
        tvInsuranceV.setVisibility(View.VISIBLE);
        tvOngkirV.setVisibility(View.VISIBLE);
        tvOngkirV.setText("0");
        tvInsuranceV.setText("0");
        /*tvInsuranceV.setVisibility(View.GONE);*/
        etPacket.setEnabled(true);
        etPacket.setInputType(InputType.TYPE_NULL);

        View.OnFocusChangeListener ofcListener = new MyFocusChangeListener();
        etPacket.setOnFocusChangeListener(ofcListener);


//        etPacket.setText("GRATIS");

        /*IT WILL USE IN NEXT PHASE*/
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getId()) {
                    case R.id.rg:
                        if (rg.getChildAt(0).getId() == i) {

                            tvInsuranceV.setVisibility(View.VISIBLE);
                            tvInsuranceV.setText("" + AmountFormatter.format(totIns));
                            int total;
                            int tt = totIns;
                            if (!tvOngkirV.getText().toString().equals("") && tvOngkirV.getText().toString() != null) {
                                if (tvInsuranceV.getVisibility() == View.VISIBLE) {
                                    total = price + tt + totBrg;
                                    tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                                } else {
                                    total = price + 0 + totBrg;
                                    tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                                }
                            } else {
                                total = 0 + 0 + totBrg;
                                tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                            }

                            rgId = rg.getChildAt(0).getId();

                        } else if (rg.getChildAt(0).getId() != i){
                            tvInsuranceV.setVisibility(View.GONE);

                            int total;
                            int tt = totIns;
                            if (!tvOngkirV.getText().toString().equals("") && tvOngkirV.getText().toString() != null) {
                                if (tvInsuranceV.getVisibility() == View.VISIBLE) {
                                    total = price + tt + totBrg;
                                    tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                                } else {
                                    total = price + 0 + totBrg;
                                    tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                                }
                            } else {
                                total = 0 + tt + totBrg;
                                tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                            }

                            rgId = rg.getChildAt(0).getId();
                        }
                        break;
                }
            }
        });

        etPacket.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                priceV = price;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
//                priceV = price;
                tvOngkirV.setText("" + AmountFormatter.format(price));

            }
        });

        tvOngkirV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int total;
                int tt = totIns;
                if (!tvOngkirV.getText().toString().equals("") && tvOngkirV.getText().toString() != null) {
                    if (!tvInsuranceV.getText().toString().equals("0")) {
                        total = price + tt + totBrg;
                        tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                    } else {
                        total = price + 0 + totBrg;
                        tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                    }
                } else {
                    total = 0 + tt + totBrg;
                    tvCheckout.setText("Bayar " + AmountFormatter.format(total));
                }

            }
        });

        /*END OF - IT WILL USE IN NEXT PHASE*/
    }

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus) {

            if (v.getId() == R.id.etPacket && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initMyCart();
        loadMyCart();
//        mPresenter.getListAddress(mPresenter.presentGetListAddressReq());
        if (!tvAddress.getText().toString().equals("Masukkan Alamat terlebih dahulu")) {
//            mPresenter.getDest(mPresenter.constructGetListCity());
        }
    }

    public void initMyCart()
    {
        LinearLayoutManager llManagerProducts = new LinearLayoutManager(getActivity());
        llManagerProducts.setOrientation(LinearLayoutManager.VERTICAL);
        mRvList.setLayoutManager(llManagerProducts);
        mAdapter = new MyCartListAdapter(this, datas, mPresenter);
        mRvList.setAdapter(mAdapter);
        /*mRvList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, final int position) {
                super.onItemClick(childView, position);
                //ChatRoomActivity.startActivity((BaseActivity) getActivity(),friends.get(position).getEmail());

                ImageView btnClick = (ImageView) childView.findViewById(R.id.mycart_img_discard);
                btnClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showToast(datas.get(position).getProductName());

                    }
                });
            }
        }));*/
    }

    public void loadMyCart()
    {
        //Log.d("dataSukses","1");
        mPresenter.getmycartdata();
        //Log.d("dataSukses","2");
        mAdapter.notifyDataSetChanged();
        //productsAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.mycart_btn_checkout)
    public void checkout()
    {
        if(qtyItem>0) {


            if (ll_pengiriman.getVisibility() == View.VISIBLE) {
                if (!etPacket.getText().toString().equals("") && etPacket.getText().toString() != null) {

                    if (mPresenter.z == 0 ) {
                        showToast("Masukkan alamat pengiriman Anda");
                    } else {
                        mPresenter.getDelMethod(mPresenter.constructDelMethod());
                    }

//                    PayMethodActivity.startActivity((BaseActivity) getActivity(), "", vClass);
                } else {
                    showToast("Silahkan pilih paket pengiriman Anda");
                }
            } else {
                PayMethodActivity.startActivity((BaseActivity) getActivity(), "", String.valueOf(highest), "purchase");
            }

//            DeliveryBillingFragment.showFragment((BaseActivity) getActivity());
        }
        else
        {
            OutputDialogFragment viewDialog = new OutputDialogFragment(this, 4,0);
        }
    }

    @OnClick(R.id.tvBtn)
    public void editAddress() {
        AddressActivity.startActivity((BaseActivity) getActivity(), "listaddress");
    }

    @OnClick(R.id.et_packet)
    public void getPacket() {

//        mPresenter.checkRoutes(mPresenter.constructRatesReq());

        if (mPresenter.addressStatus = true) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.GenericProgressDialogStyleAlert));
            final CharSequence[] listName = listPackage.toArray(new CharSequence[listPackage.size()]);
            final CharSequence[] listPrices = listCode.toArray(new CharSequence[listCode.size()]);
            if (listPackage.isEmpty()) {
                if (!mPresenter.city.equals("")) {
//                    mPresenter.getDest(mPresenter.constructGetListCity());
                    builder.setTitle("Ada kendala pada server JNE");
                } else {
                    builder.setTitle("Masukkan Alamat terlebih dahulu");
                }

            } else {
                builder.setTitle("Pilih Paket");
                builder.setItems(listName, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        etPacket.setText(listName[i]);
                        delPrice = String.valueOf(listPrices[i]);
                        price = Integer.parseInt(delPrice);
                        tvOngkirV.setText("" + AmountFormatter.format(price));
                    }
                });
            }
            builder.show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void setDataJne(List<JNECheckRatesMsgResp> datasJne) {
        this.datasJne.clear();
        this.datasJne.addAll(datasJne);
    }

    public void setDestCode(List<String> destCode) {
        this.listCode.clear();
        this.listCode.addAll(destCode);
    }

    public void setPacket(List<String> packetCode) {
        this.listPackage.clear();
        this.listPackage.addAll(packetCode);
    }


}