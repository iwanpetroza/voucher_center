package com.ultravoucher.uv.ui.activities;

import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ultravoucher.uv.VoucherApplication;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.LoginReq;
import com.ultravoucher.uv.data.api.request.MemberPocketReq;
import com.ultravoucher.uv.data.api.request.SettingGCMReq;
import com.ultravoucher.uv.data.api.response.LoginResponse;
import com.ultravoucher.uv.data.api.response.LoginWaletResponse;
import com.ultravoucher.uv.data.api.response.SettingGCMResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.phase2.HomeActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class LoginPresenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();

    BaseActivity mActivity;

    public LoginPresenter(LoginActivity mActivity) {
        this.mActivity = mActivity;
    }

    public LoginPresenter(IntroductionActivity mActivity) {
        this.mActivity = mActivity;
    }

    public LoginPresenter(VerificationActivity fragment) {
        this.mActivity = fragment;
    }

    public void loginWalet(MemberPocketReq req){

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";

        VoucherApi api = mActivity.getVoucherApi();
        Call<LoginWaletResponse> loginWaletResponseCall = api.loginWalet(contentType, req);
        loginWaletResponseCall.enqueue(new Callback<LoginWaletResponse>() {
            @Override
            public void onResponse(Call<LoginWaletResponse> call, Response<LoginWaletResponse> response) {
                LoginWaletResponse resp = response.body();

                if (response.code() == 200) {
                    if (resp.getAbstractResponse().getResponseStatus().equals("AUTH011")) {
                        HomePageActivity.startActivity(mActivity, "login");
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginWaletResponse> call, Throwable t) {
                mActivity.dismissProgressDialog();
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });

    }

    MemberPocketReq constructMemberWalet() {
        LoginActivity f = (LoginActivity) mActivity;
        String password = f.loginEtPassword.getText().toString();

        MemberPocketReq req = new MemberPocketReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }

    public void login(final LoginReq request) {

        mActivity.showProgressDialog(mActivity.LOADING);
//        mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        VoucherApi api = mActivity.getVoucherApi();
        Call<LoginResponse> loginResponseCall = api.doLogin(request);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse resp = response.body();
                if (response.code() == 200) {
                    if (resp.getAuthenticationTokens().getAuthtokenResponse() != null) {
                        if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH000") && resp.getAuthenticationTokens().getAuthtokenResponse().getIsCorporateMember() == -1) {
                            final String authToken = resp.getAuthenticationTokens().getAuthtokenResponse().getAuthToken();
                            final String deviceUniqueId = resp.getAuthenticationTokens().getAuthtokenResponse().getAuthDevice();
                            final String memberId = resp.getAuthenticationTokens().getAuthtokenResponse().getMemberId();

                            PrefHelper.setString(PrefKey.AUTH_TOKEN, authToken);
                            PrefHelper.setString(PrefKey.DEVICE_UNIQUE_ID, deviceUniqueId);
                            PrefHelper.setString(PrefKey.MEMBERID, memberId);
                            PrefHelper.setString(PrefKey.FIRSTNAME, resp.getAuthenticationTokens().getAuthtokenResponse().getFirstName());
                            PrefHelper.setString(PrefKey.VA_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getVaNumber());
                            PrefHelper.setString(PrefKey.VA_NUMBERPERMATA, resp.getAuthenticationTokens().getAuthtokenResponse().getVaNumber2());
                            PrefHelper.setString(PrefKey.QR_IMAGE, resp.getAuthenticationTokens().getAuthtokenResponse().getQrImage());
                            PrefHelper.setString(PrefKey.INITIAL_RUN, "login");
                            PrefHelper.setString(PrefKey.PHONE_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getPhoneNumber());
                            PrefHelper.setString(PrefKey.GENDER, resp.getAuthenticationTokens().getAuthtokenResponse().getGender());
                            PrefHelper.setString(PrefKey.ID_NUMBER, resp.getAuthenticationTokens().getAuthtokenResponse().getIdNumber());
                            PrefHelper.setString(PrefKey.MOTHER_NAME, resp.getAuthenticationTokens().getAuthtokenResponse().getMotherName());
                            PrefHelper.setString(PrefKey.BIRTH_PLACE, resp.getAuthenticationTokens().getAuthtokenResponse().getBirthPlace());
                            PrefHelper.setString(PrefKey.BIRTH_DATE, resp.getAuthenticationTokens().getAuthtokenResponse().getBirthDate());
                            PrefHelper.setString(PrefKey.POIN, String.valueOf(resp.getAuthenticationTokens().getAuthtokenResponse().getPoint()));
                            PrefHelper.setString(PrefKey.USER_PIC, resp.getAuthenticationTokens().getAuthtokenResponse().getUserPic());
                            PrefHelper.setString(PrefKey.USERNAME, resp.getAuthenticationTokens().getAuthtokenResponse().getEmail());
                            PrefHelper.setString(PrefKey.PASSWORD, "");

                            String updatedToken = FirebaseInstanceId.getInstance().getToken();
                            sendRegistrationToServer(updatedToken);
//                            mActivity.finish();
//                            HomePageActivity.startActivity(mActivity, "login");
                        } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("ERR123")) {
                            mActivity.showToast("Sistem sedang dalam pemeliharaan");
                        } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH005")) {
                            if (resp.getAuthenticationTokens().getAuthtokenResponse().getIsCorporateMember() == -1) {
                                VerificationActivity.startActivity((BaseActivity) mActivity, "", "");
                            } else {
                                mActivity.showToast("Anda sudah terdaftar sebagai Member Web Corporate");
                            }

                        }
                        else {
                            mActivity.showToast(resp.getAuthenticationTokens().getAbstractResponse().getResponseMessage());
                        }
                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mActivity.showToast("Password tidak sesuai");

                    } else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("AUTH002")) {
                        mActivity.showToast("Email/No.Hp belum terdaftar");

                    }  else if (resp.getAuthenticationTokens().getAbstractResponse().getResponseStatus().equals("ERR123")) {
                        mActivity.showToast("Sistem bermasalah, Silahkan coba lagi");
                    }

                    else {
                        mActivity.showToast(resp.getAuthenticationTokens().getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast("Kendala pada server, silahkan coba lagi");
                }

                mActivity.dismissProgressDialog();
//                mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                mActivity.dismissProgressDialog();
//                mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });
    }

    LoginReq constructLoginReq() {
        LoginActivity f = (LoginActivity) mActivity;
        final String idDevice = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String email = f.loginEtMail.getText().toString();
        String password = f.loginEtPassword.getText().toString();

        LoginReq req = new LoginReq();
        req.setDeviceUniqueId(idDevice);
        req.setUsername(email);
        req.setPassword(password);

        PrefHelper.setString(PrefKey.USERNAME, email);
        PrefHelper.setString(PrefKey.PASSWORD, password);

        return req;
    }

    private VoucherApi getBaseApi() {
        return VoucherApplication.getInstance().getVoucherApi();
    }

    private void sendRegistrationToServer(String updatedToken) {
        SettingGCMReq request = new SettingGCMReq();
        request.setDeviceUniqueId(PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID));
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setRegKey(updatedToken);

        VoucherApi api = getBaseApi();
        Call<SettingGCMResp> settingGcmResp = api.setGcmId(PrefHelper.getString(PrefKey.AUTH_TOKEN), PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID), request);
        settingGcmResp.enqueue(new Callback<SettingGCMResp>() {
            @Override
            public void onResponse(Call<SettingGCMResp> call, Response<SettingGCMResp> response) {
                if (response.code() == 200) {
                    SettingGCMResp resp = response.body();
                    if (resp.getResponseStatus().equals("003")) {
                        Log.d(TAG, "FCM UPDATE onResponseSuccess");
                        mActivity.finish();

                        HomePageActivity.startActivity(mActivity, "login");
//                        HomeActivity.startActivity(mActivity);
                    } else if (resp.getResponseStatus().equals("006")) {
                        mActivity.showToast("Member doesn't exist");
                    }
                }else{
                    Log.d(TAG, "FCM UPDATE onResponseError");
                }
            }

            @Override
            public void onFailure(Call<SettingGCMResp> call, Throwable t) {
                Log.d(TAG, "FCM UPDATE onResponseFailure");
            }
        });
    }


}
