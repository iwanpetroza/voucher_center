package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.ListDeliveryAddressMsgResp;
import com.ultravoucher.uv.data.api.request.ListDeliveryAddressReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.UpdateDelMethod;
import com.ultravoucher.uv.data.api.request.UpdateDelMethodReq;
import com.ultravoucher.uv.data.api.response.AddToCartResponse;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.ListDeliveryAddressResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/2/2017.
 */

public class ListAddressPresenter {

    private ListAddressFragment mFragment;

    public ListAddressPresenter(ListAddressFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetListDeliveryAddress(final ListDeliveryAddressReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListDeliveryAddressResponse> listDeliveryAddressResponseCall = api.getListDeliveryAddress(contentType, authToken,deviceUniqueId, req);
        listDeliveryAddressResponseCall.enqueue(new Callback<ListDeliveryAddressResponse>() {
            @Override
            public void onResponse(Call<ListDeliveryAddressResponse> call, Response<ListDeliveryAddressResponse> response) {
                if (response.code() == 200) {
                    ListDeliveryAddressResponse resp = response.body();

                    if (response.body() != null) {
                        if (resp.getAbstractResponse().getResponseStatus().equals("DL002")) {
                            if (!resp.getDeliveryAddress().isEmpty()) {
                                initList(resp.getDeliveryAddress());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                if (mFragment != null && mFragment.isAdded()) {
                                    String message = mFragment.getString(R.string.no_data);
                                    mFragment.noDataState.setVisibility(View.VISIBLE);
                                    mFragment.noDataMessage.setText(message);
                                }
                            }
                        } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.need_relogin);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }

                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }

                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListDeliveryAddressResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });

    }

    protected void presentGetListDeliveryAddressLoadMore(final ListDeliveryAddressReq req) {

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListDeliveryAddressResponse> listDeliveryAddressResponseCall = api.getListDeliveryAddress(contentType, authToken,deviceUniqueId, req);
        listDeliveryAddressResponseCall.enqueue(new Callback<ListDeliveryAddressResponse>() {
            @Override
            public void onResponse(Call<ListDeliveryAddressResponse> call, Response<ListDeliveryAddressResponse> response) {
                if (response.code() == 200) {
                    ListDeliveryAddressResponse resp = response.body();

                    if (response.body() != null) {
                        if (!resp.getDeliveryAddress().isEmpty()) {
                            addToList(resp.getDeliveryAddress());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mList.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
            }

            @Override
            public void onFailure(Call<ListDeliveryAddressResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });

    }

    protected void initList(List<ListDeliveryAddressMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<ListDeliveryAddressMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (ListDeliveryAddressMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected ListDeliveryAddressReq presentGetListAddressReq() {
        ListDeliveryAddressReq req = new ListDeliveryAddressReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }

    void getOrder()
    {
//        mFragment.pbLoadTrxMount.setVisibility(View.VISIBLE);
//        mFragment.tvTrxAmount.setVisibility(View.GONE);
        mFragment.showProgressDialog(mFragment.LOADING);
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
//                        vClass = resp.getOrders().get(0).getVoucherClass();
                        mFragment.setDatas(resp.getOrders());
                        doUpdateDeliveryAddress(constructDelMethod());
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
//                        mFragment.tvTrxAmount.setVisibility(View.VISIBLE);
//                        mFragment.tvTrxAmount.setText(AmountFormatter.format(resp.getTotalTrxAmount()));

                    }
                    else
                    {
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
//                    mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
//                mFragment.pbLoadTrxMount.setVisibility(View.GONE);
            }
        });
    }


    void doUpdateDeliveryAddress(UpdateDelMethodReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();

        Call<AddToCartResponse> addToCartResponseCall = api.updateDelMethod(contentType, authToken, deviceUniqueId, req);
        addToCartResponseCall.enqueue(new Callback<AddToCartResponse>() {
            @Override
            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {

                if (response.code() == 200) {
                    AddToCartResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("OD002")) {
                        mFragment.getActivity().finish();
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast("Koneksi anda tidak stabil, silahkan coba lagi");
            }
        });
    }




    UpdateDelMethodReq constructDelMethod() {

        UpdateDelMethodReq req = new UpdateDelMethodReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));

        List<UpdateDelMethod> orderPays = new ArrayList<>();
        for (int i = 0; i < mFragment.datas.size(); i++) {

            UpdateDelMethod order = new UpdateDelMethod();
            order.setDeliveryMethod(mFragment.datas.get(i).getDeliveryMethod());
            order.setDeliveryAddressId(mFragment.deliveryId);
            order.setOrderId(mFragment.datas.get(i).getOrderId());
            order.setShippingType("");
            order.setShippingBy("");
            order.setPickupDate("");
            order.setStatus(0);
            order.setPickupNote("");
            order.setShippingAmount(0);
            order.setInsuranceFee(0);
            orderPays.add(order);
        }

        req.setOrders(orderPays);
        return req;
    }


}
