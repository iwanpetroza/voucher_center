package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Html;
import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.OrderRequest;
import com.ultravoucher.uv.data.api.beans.VariantVoucherResp;
import com.ultravoucher.uv.data.api.request.AddToCartReq;
import com.ultravoucher.uv.data.api.request.DetailProductByIdReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.response.AddToCartResponse;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.VoucherListResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.utils.AmountFormatter;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/18/2017.
 */

public class DetailProductPresenter {

    DetailProductFragment mFragment;

    boolean delMethodStatus = false;

    String orderNumber = "";
    String vClass, wholeText;
    List<OrderRequest> orderRequest;
    List<VariantVoucherResp> datas = new ArrayList<>();
    String id;

    public DetailProductPresenter(DetailProductFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getdeliveryMethod()
    {
        final CharSequence[] delMethod = new CharSequence[2];
        delMethod[0] = "Kirim";
        delMethod[1] = "Ambil Sendiri";
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mFragment.getContext(), R.style.GenericProgressDialogStyleAlert));
        builder.setTitle("Pilih Metode Pengiriman");
        builder.setItems(delMethod, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                mFragment.etDelMethod.setText(delMethod[item]);
                if(delMethod[item] == delMethod[0]){
                    delMethodStatus = true;
                }
                else {
                    delMethodStatus = false;
                }
            }
        });
        builder.show();
    }

    public void getDetail(final DetailProductByIdReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);
        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<VoucherListResponse> voucherListResponseCall = api.getDetailProduct(contentType, req);
        voucherListResponseCall.enqueue(new Callback<VoucherListResponse>() {
            @Override
            public void onResponse(Call<VoucherListResponse> call, Response<VoucherListResponse> response) {
                if (response.code() == 200) {
                    VoucherListResponse resp = response.body();
                    datas = resp.getProducts().get(0).getVariants();
                    if (resp.getAbstractResponse().getResponseStatus().equals("PROD101")) {

                        vClass = String.valueOf(resp.getProducts().get(0).getVoucherClass());
                        mFragment.stockQty = datas.get(0).getQuantity();

                        id = resp.getProducts().get(0).getVariants().get(0).getVariantId();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            mFragment.tvDescription.setText(Html.fromHtml(resp.getProducts().get(0).getDescription().replace("\t", ""), Html.FROM_HTML_MODE_COMPACT));
                            wholeText = resp.getProducts().get(0).getDescription();
                        } else {
                            mFragment.tvDescription.setText(Html.fromHtml(resp.getProducts().get(0).getDescription().replace("\t", "")));
                            wholeText = resp.getProducts().get(0).getDescription();
                        }

                        mFragment.initView(wholeText);
                        mFragment.tvJumlah.setText("Total Harga : 0" );
                        mFragment.tvName.setText(resp.getProducts().get(0).getProductName());
                        mFragment.tvSaleprice.setText(AmountFormatter.format(resp.getProducts().get(0).getVariants().get(0).getSellingPrice()));
                        mFragment.price = resp.getProducts().get(0).getVariants().get(0).getSellingPrice();

                        if (resp.getProducts().get(0).getVariants().get(0).getVariantImageMain() != null && !resp.getProducts().get(0).getVariants().get(0).getVariantImageMain().equals("")) {
                            Picasso.with(mFragment.getContext()).load(resp.getProducts().get(0).getVariants().get(0).getVariantImageMain()).placeholder(R.drawable.image_placeholder)
                                    .centerCrop().fit().into(mFragment.detailLogo, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    mFragment.imgLoading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    mFragment.imgLoading.setVisibility(View.GONE);
                                    mFragment.detailLogo.setImageResource(R.drawable.image_placeholder);
                                }
                            });


                        } else {
                            mFragment.imgLoading.setVisibility(View.GONE);
                            mFragment.detailLogo.setImageResource(R.drawable.image_placeholder);
                        }

                        orderRequest = new ArrayList<>();
                        OrderRequest order = new OrderRequest();
                        order.setProductId(resp.getProducts().get(0).getProductId());
                        order.setMerchantId(resp.getProducts().get(0).getMerchantId());
                        order.setStoreId(resp.getProducts().get(0).getStoreId());

                        orderRequest.add(order);
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")){
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    }

                    else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast("Koneksi Tidak Stabil");
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<VoucherListResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast(mFragment.CONNECTION_ERROR);
            }
        });
    }

    DetailProductByIdReq constructDetailProduct() {
        DetailProductByIdReq req = new DetailProductByIdReq();
        req.setId(mFragment.id);
        req.setKeyword("");
        return req;
    }


    void getOrdernumber() {

        final String authToken, deviceUniqueId;

        authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        MyCartReq req = new MyCartReq();
        req.setStatus(0);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));

        Call<MyCartResponse> myCartResponseCall = mFragment.getVoucherApi().getMyCart(authToken, deviceUniqueId, req);
        myCartResponseCall.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if (response.code() == 200) {
                    MyCartResponse resp = response.body();

                    /*if (mFragment.status.equals("0")) {
                        presentAddToCart(constructAddToCartRequest());
                    } else {*/

                    if (resp != null && resp.getAbstractResponse().getResponseStatus().equals("OD004")) {
                        if (resp.getOrders().size() > 0)  {
                            orderNumber = resp.getOrders().get(0).getOrderNumber();
                        }
                        presentAddToCart(constructAddToCartRequest());
                    } else if (resp != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        String message = mFragment.getString(R.string.need_relogin);
                        mFragment.showToast(message);
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    String message = mFragment.CONNECTION_ERROR;
                    mFragment.showToast(message);
                }
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                String message = mFragment.CONNECTION_ERROR;
                mFragment.showToast(message);
            }
        });
    }

    void presentAddToCart(final AddToCartReq req) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        final String orderNumberD = String.valueOf(cal.get(Calendar.YEAR)) + String.valueOf(cal.get(Calendar.MONTH)+1) + String.valueOf(cal.get(Calendar.DATE)) + String.valueOf(cal.getTimeInMillis()) ;

        mFragment.showProgressDialog(mFragment.LOADING);

        final String authToken, deviceUniqueId;



        authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        req.setOrderNumber(orderNumber);
        VoucherApi service = mFragment.getVoucherApi();
        Call<AddToCartResponse> addToCartResponseCall = service.addToCart(authToken, deviceUniqueId, req);
        addToCartResponseCall.enqueue(new Callback<AddToCartResponse>() {
            @Override
            public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                if(response.code() == 200){
                    AddToCartResponse resp = response.body();

                    /*if (mFragment.status.equals("0")) {
                        PrefHelper.setString(PrefKey.ORDERNUMBER, orderNumberD);
                        PayMethodActivity.startActivity((BaseActivity) mFragment.getActivity(), "D", vClass);
//                        DetailProductActivity.startActivity((BaseActivity) mFragment.getActivity(), "D", mFragment.status, "");
                    } else {*/
                        if (resp.getAbstractResponse().getResponseStatus().equals("OD001")) {
//                            ProductDetailDialog dealsDetailDialog = new ProductDetailDialog(mFragment, 0);
                            mFragment.getActivity().finish();
                            PrefHelper.setString(PrefKey.TOTAL, "");
                            mFragment.showToast("Produk berhasil ditambahkan ke cart");
//                        dealsDetailDialog.showDialog(mContext);
//                        getCountMyCart();
                        } else if (resp.getAbstractResponse().getResponseStatus().equals("VA901")){
                            ProductDetailDialog dealsDetailDialog = new ProductDetailDialog(mFragment, 1);
                        } else if (resp.getAbstractResponse().getResponseStatus().equals("PROD902")) {
                            ProductDetailDialog dealsDetailDialog = new ProductDetailDialog(mFragment, 2);
                        } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            mFragment.showToast("Sesi Anda telah berakhir, Silahkan Login ulang");
                            mFragment.getActivity().finish();
                            mFragment.doNeedRelogin();
                        }
//                    }
                    // OutputDialogFragment viewdialog = new OutputDialogFragment(mContext, 3);
                }else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if (mFragment != null) {
                                mFragment.doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mFragment != null) {
                        String message = mFragment.getString(R.string.connection_error);
                        mFragment.showToast(message);
                    }
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                String message = mFragment.CONNECTION_ERROR;
                mFragment.showToast(message);
            }
        });
    }

    AddToCartReq constructAddToCartRequest(){

        String jml = mFragment.etQty.getText().toString();

        AddToCartReq request = new AddToCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setOrderNumber(orderNumber);
        request.setOrigin("ANDROID");

        for(OrderRequest or : orderRequest){
            OrderRequest order = new OrderRequest();
            order.setProductId(or.getProductId());
            order.setVariantId(id);
            order.setQuantity(Integer.parseInt(jml));

            order.setMerchantId(or.getMerchantId());
            order.setStoreId(or.getStoreId());
            order.setVoucherId("");

            if (!mFragment.status.equals("0")) {
                if (mFragment.method.equalsIgnoreCase("kirim")) {
                    order.setDeliveryMethod(-1);
                } else {
                    order.setDeliveryMethod(1);
                }
            } else {
                order.setDeliveryMethod(1);
            }



            orderRequest.clear();
            orderRequest.add(order);
        }

        request.setOrders(orderRequest);
        return request;
    }

}


