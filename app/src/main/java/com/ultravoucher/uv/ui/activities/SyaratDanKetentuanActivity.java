package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import butterknife.BindView;

/**
 * Created by asep.surahman on 16/03/2018.
 */

public class SyaratDanKetentuanActivity extends BaseActivity{


    private static final String POST_ID = "postId";
    private static final String CODE = "code";

    @BindView(R.id.toolbar)
    Toolbar toolBar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.my_webview)
    WebView wv;

    private VideoView videoView;
    private MediaController mController;
    private Uri uriYouTube;
    String webUrl, mTitle;
    boolean isTrue = false;

    @Override
    protected int getLayout() {
        return R.layout.a_creditcard;
    }


    String link;

    public static void startActivity(BaseActivity sourceActivity, String url, String code){
        Intent i = new Intent(sourceActivity, SyaratDanKetentuanActivity.class);
        i.putExtra(POST_ID, url);
        i.putExtra(CODE, code);
        sourceActivity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        link = getIntent().getStringExtra(POST_ID);
        mTitle = getIntent().getStringExtra(CODE);

        setupToolbar();



        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message,           JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });

        wv.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webUrl = url;

                if (webUrl.contains("true")) {

                } else if (webUrl.contains("false")) {

                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);

        if (mTitle.equals("TAC")) {
            wv.loadUrl(link);
        } else {
            wv.loadUrl("https://corporate.ultravoucher.co.id/help/topup.html?va_bca=" + PrefHelper.getString(PrefKey.VA_NUMBER) + "&va_permata=" + PrefHelper.getString(PrefKey.VA_NUMBERPERMATA));
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();

    }

    private void setupToolbar() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        if (mTitle.equals("TAC")) {
            toolbar_title.setText("Syarat dan Ketentuan");
        } else {
            toolbar_title.setText("Topup");
        }

        toolbar_title.setAllCaps(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
