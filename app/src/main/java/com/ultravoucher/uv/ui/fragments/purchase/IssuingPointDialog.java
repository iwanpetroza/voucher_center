package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.HomePageActivity;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 2/7/2018.
 */

public class IssuingPointDialog extends BaseDialogFragment {

    private static final String TAG = "IssuingPointDialog";

    @BindView(R.id.ll_point)
    LinearLayout ll_point;


    @BindView(R.id.tvPoint)
    TextView tvPoint;


    String dismissFrom="0";

    IssuingPointPresenter mPresenter;

    OutputDialogFragment mFragment;
    int totalTrx;

    public IssuingPointDialog(int totalTrx, OutputDialogFragment mFragment) {
        this.totalTrx = totalTrx;
        this.mFragment = mFragment;
        show(mFragment.getFragmentManager(), TAG);
    }

    @Override
    protected int getLayout() {
        return R.layout.d_get_point;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new IssuingPointPresenter(this);
//        this.setCancelable(false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.getPoint(mPresenter.constructRequest());
        this.setCancelable(false);

        //ll_load.setVisibility(View.GONE);
    }

    @OnClick(R.id.get_point_btn_close)
    void goDismiss()
    {
        dismissFrom="1";
        dismiss();
    }

    /*@OnClick(R.id.get_point_btn_rewards)
    void gotoReward()
    {
        dismissFrom="2";
        dismiss();
    }*/

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
/*//        //MainActivity.startActivity((BaseActivity) getActivity());
        mFragment.getDialog().dismiss();
        mFragment.mFragment.getActivity().finish();
        //getActivity().finish();
//        //getActivity().onBackPressed();
        //goDismiss();*/
        if(dismissFrom.equals("1"))
        {
            HomePageActivity.startActivity((BaseActivity) getActivity(), "");
        }
        else if(dismissFrom.equals("2"))
        {
            HomePageActivity.startActivity((BaseActivity) getActivity(), "3");
        }

    }
}
