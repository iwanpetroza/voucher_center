package com.ultravoucher.uv.ui.fragments.purchase;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.UtilMessageResp;
import com.ultravoucher.uv.data.api.request.CreateAddressReq;
import com.ultravoucher.uv.data.api.request.GetCityReq;
import com.ultravoucher.uv.data.api.request.GetDistrictReq;
import com.ultravoucher.uv.data.api.request.GetVillageReq;
import com.ultravoucher.uv.data.api.response.AddAddressResponse;
import com.ultravoucher.uv.data.api.response.FilterAreaResponse;
import com.ultravoucher.uv.data.api.response.GetDistrictResponse;
import com.ultravoucher.uv.data.api.response.GetProvinceResponse;
import com.ultravoucher.uv.data.api.response.GetVillageResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.AddressActivity;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class AddAddressPresenter {

    AddAddressFragment mFragment;

    public AddAddressPresenter(AddAddressFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getProvince() {

        VoucherApi api = mFragment.getVoucherApi();
        Call<GetProvinceResponse> getProvinceResponseCall = api.getProvince();
        getProvinceResponseCall.enqueue(new Callback<GetProvinceResponse>() {
            @Override
            public void onResponse(Call<GetProvinceResponse> call, Response<GetProvinceResponse> response) {
                if (response.code() == 200) {
                    GetProvinceResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getUtilMessageResponse().isEmpty()) {
                        /*mFragment.mListProvince.clear();
                        mFragment.mListProvince.addAll(resp.getUtilMessageResponse());
                        mFragment.mAdapter.commitChangeData();*/

                            mFragment.datasProvince = resp.getUtilMessageResponse();
                            mFragment.sizeProvince = resp.getUtilMessageResponse().size();

                            if (mFragment.sizeProvince > 0) {

                                List<String> listProvinceName = new ArrayList<>();
                                List<String> listProvinceProvinceId = new ArrayList<>();

                                UtilMessageResp item;
                                for (int i = 0; i< mFragment.sizeProvince; i++) {
                                    item = mFragment.datasProvince.get(i);
                                    listProvinceName.add(item.getLabel());
                                    listProvinceProvinceId.add(item.getId());
                                }

                                mFragment.setProvinceName(listProvinceName);
                                mFragment.setProvinceId(listProvinceProvinceId);
                            } else {
                                mFragment.showToast("Data belum tersedia");
                            }
                        } else {
                            mFragment.showToast("Data belum tersedia");
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.doNeedRelogin();
                    }

                    else if (resp.getAbstractResponse().getResponseStatus().equals("INQ902")) {
                        mFragment.showToast("Data belum tersedia");
                    }

                } else {
                    mFragment.showToast("Kontak dengan server gagal");
                }
            }

            @Override
            public void onFailure(Call<GetProvinceResponse> call, Throwable t) {
                mFragment.showToast("Kontak dengan server gagal");
            }
        });
    }

    void getCity() {

        mFragment.showProgressDialog(mFragment.LOADING);
        GetCityReq req = new GetCityReq();
        req.setStateProvId(mFragment.mProvinceId);

        Call<FilterAreaResponse> filterAreaResponseCall = mFragment.getVoucherApi().doFilterArea(req);
        filterAreaResponseCall.enqueue(new Callback<FilterAreaResponse>() {
            @Override
            public void onResponse(Call<FilterAreaResponse> call, Response<FilterAreaResponse> response) {
                if (response.code() == 200) {
                    FilterAreaResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getUtilMessageResponse().isEmpty()) {
                        /*mFragment.mListCity.clear();
                        mFragment.mListCity.addAll(resp.getUtilMessageResponse());
                        mFragment.mAdapterCity.commitChangeData();*/

                            mFragment.datasCity = resp.getUtilMessageResponse();
                            mFragment.sizeCity = resp.getUtilMessageResponse().size();

                            if (mFragment.sizeCity > 0) {

                                List<String> listCityName = new ArrayList<>();
                                List<String> listCityId = new ArrayList<>();

                                UtilMessageResp item;
                                for (int i = 0; i< mFragment.sizeCity; i++) {
                                    item = mFragment.datasCity.get(i);
                                    listCityName.add(item.getLabel());
                                    listCityId.add(item.getId());
                                }

                                mFragment.setCityNames(listCityName);
                                mFragment.setCityIds(listCityId);
                            } else {
                                mFragment.showToast("Data belum tersedia");
                            }

                        } else {
                            mFragment.showToast("Data belum tersedia");
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("INQ902")) {
                        mFragment.showToast("Data belum tersedia");
                    }

                } else {
                    mFragment.showToast("Kontak dengan server gagal");
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<FilterAreaResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast("Kontak dengan server gagal");
            }
        });
    }

    void getDistrict() {
        mFragment.showProgressDialog(mFragment.LOADING);
        GetDistrictReq req = new GetDistrictReq();
        req.setCityId(mFragment.mCityId);

        Call<GetDistrictResponse> districtByCity = mFragment.getVoucherApi().getDistrictByCity(req);
        districtByCity.enqueue(new Callback<GetDistrictResponse>() {
            @Override
            public void onResponse(Call<GetDistrictResponse> call, Response<GetDistrictResponse> response) {
                if (response.code() == 200) {
                    GetDistrictResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getUtilMessageResponse().isEmpty()) {
                        /*mFragment.mListDistrict.clear();
                        mFragment.mListDistrict.addAll(resp.getUtilMessageResponse());
                        mFragment.mAdapterDistrict.commitChangeData();*/

                            mFragment.datasDistrict = resp.getUtilMessageResponse();
                            mFragment.sizeDistrict = resp.getUtilMessageResponse().size();

                            if (mFragment.sizeDistrict > 0) {

                                List<String> listDistrictName = new ArrayList<>();
                                List<String> listDistrictId = new ArrayList<>();

                                UtilMessageResp item;
                                for (int i = 0; i< mFragment.sizeDistrict; i++) {
                                    item = mFragment.datasDistrict.get(i);
                                    listDistrictName.add(item.getLabel());
                                    listDistrictId.add(item.getId());
                                }

                                mFragment.setDistrictNames(listDistrictName);
                                mFragment.setDistrictIds(listDistrictId);
                            } else {
                                mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                            }

                        } else {
                            mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("INQ902")) {
                        mFragment.showToast("Data belum tersedia");
                    }

                } else {
                    mFragment.showToast(mFragment.CONNECTION_ERROR);
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<GetDistrictResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast("Kontak dengan server gagal");
            }
        });
    }

    /*void getVillage()
    {
        GetVillageReq request = new GetVillageReq();
        request.setDistrictId(mFragment.mDistrictId);
        Call<GetVillageResponse> getVillageRespCall = mFragment.getVoucherApi().getVillageByDistrict(request);
        getVillageRespCall.enqueue(new Callback<GetVillageResponse>() {
            @Override
            public void onResponse(Call<GetVillageResponse> call, Response<GetVillageResponse> response) {
                if (response.code() == 200) {
                    GetVillageResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getUtilMessageResponse().isEmpty()) {
                        *//*mFragment.mListVillage.clear();
                        mFragment.mListVillage.addAll(resp.getUtilMessageResponse());
                        mFragment.mAdapterVillage.commitChangeData();*//*

                            mFragment.datasVillage = resp.getUtilMessageResponse();
                            mFragment.sizeVillage = resp.getUtilMessageResponse().size();

                            if (mFragment.sizeVillage > 0) {

                                List<String> listVillageName = new ArrayList<>();
                                List<String> listVillageId = new ArrayList<>();

                                UtilMessageResp item;
                                for (int i = 0; i< mFragment.sizeVillage; i++) {
                                    item = mFragment.datasVillage.get(i);
                                    listVillageName.add(item.getLabel());
                                    listVillageId.add(item.getId());
                                }

                                mFragment.setVillageNames(listVillageName);
                                mFragment.setVillageIds(listVillageId);
                            } else {
                                mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                            }

                        } else {
                            mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("INQ902")) {
                        mFragment.showToast("Data belum tersedia");
                    }

                } else {
                    mFragment.showToast("Kontak dengan server gagal");
                }
            }

            @Override
            public void onFailure(Call<GetVillageResponse> call, Throwable t) {
                mFragment.showToast("Kontak dengan server gagal");
            }
        });
    }*/

    void createAddress(CreateAddressReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<AddAddressResponse> addAddressResponseCall = mFragment.getVoucherApi().doCreateAddress(authToken,dui, req);
        addAddressResponseCall.enqueue(new Callback<AddAddressResponse>() {
            @Override
            public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                if (response.code() == 200) {
                    AddAddressResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseMessage().equals("Create delivery success") || resp.getAbstractResponse().getResponseStatus().equals("DL001")) {
                        mFragment.showToast("Berhasil menambahkan alamat baru");
                        mFragment.getActivity().finish();
                        AddressActivity.startActivity((BaseActivity) mFragment.getActivity(), "listaddress");
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast("Koneksi anda tidak stabil, silahkan coba lagi");
                }
            }

            @Override
            public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                mFragment.showToast("Koneksi anda tidak stabil, silahkan coba lagi");
            }
        });
    }

    CreateAddressReq constructCreateDelAddress() {
        CreateAddressReq req = new CreateAddressReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setAddressAlias(mFragment.etAddress.getText().toString());
        req.setRecipientName(mFragment.etName.getText().toString());
        req.setStateProvId(mFragment.mProvinceId);
        req.setLine(mFragment.etDetailAddress.getText().toString());
        req.setCity(mFragment.etCity.getText().toString());
        req.setDistrict(mFragment.etDistrict.getText().toString());
        req.setVillage(mFragment.etVillage.getText().toString());
        req.setMobileNumber(mFragment.etPhoneNumber.getText().toString());
        req.setPostalCode(mFragment.etPostal.getText().toString());
        req.setEmail("-");
        return req;
    }
}
