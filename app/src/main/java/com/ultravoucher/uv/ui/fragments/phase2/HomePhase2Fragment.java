package com.ultravoucher.uv.ui.fragments.phase2;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.data.api.beans.BillerCategoryMsgResp;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GeneralActivity;
import com.ultravoucher.uv.ui.activities.ProfileDialog;
import com.ultravoucher.uv.ui.activities.QRMemberActivity;
import com.ultravoucher.uv.ui.activities.SyaratDanKetentuanActivity;
import com.ultravoucher.uv.ui.adapters.phase2.BillerMenuCategoryAdapter;
import com.ultravoucher.uv.ui.adapters.phase2.HotSalesAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Firwandi S Ramli on 10/09/18.
 */

@RuntimePermissions
public class HomePhase2Fragment extends BaseFragment {

    private static final String TAG = "HomePhase2Fragment";

    @BindView(R.id.civLogo)
    CircleImageView civLogo;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvWalet)
    TextView tvWalet;
    @BindView(R.id.tvVoucher)
    TextView tvVoucher;
    @BindView(R.id.tvPoint)
    TextView tvPoint;
    @BindView(R.id.tvTopup)
    TextView tvTopup;
    @BindView(R.id.llItem2)
    LinearLayout llItem2;
    @BindView(R.id.gridViewLainnya)
    GridView gridViewLainnya;
    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.no_data_state_message)
    TextView noDataStateMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;
    @BindView(R.id.ll_grid)
    RelativeLayout llGrid;
    @BindView(R.id.rvHotSales)
    RecyclerView rvHotSales;

    static final int CAMERA_CAPTURE = 1;
    final int PICK_IMAGE_REQUEST = 2;
    final int PIC_CROP = 3;
    final int PROFILE_PIC = 4;
    int page = 0;

    private Uri picUri;
    private Uri picUriCropped;
    private File picFile;

    boolean running = false;

    private int request;
    HomePhase2Presenter mPresenter;

    /*HOT SALES*/
    public HotSalesAdapter mAdapterHotSales;
    protected List<AdvertisingMsgResp> mHotSales = new ArrayList<>();

    /*BILLER MENU CATEGORY*/
    protected List<BillerCategoryMsgResp> mData = new ArrayList<>();
    protected BillerMenuCategoryAdapter mAdapter;

    Handler handler;


    @Override
    protected int getLayout() {
        return R.layout.f2_home_fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new HomePhase2Presenter(this);

        if(PrefHelper.getString(PrefKey.AUTH_TOKEN) == null || PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
            getActivity().finish();
            doNeedRelogin();
        }
    }


    @OnClick({R.id.civLogo, R.id.btnShare, R.id.btnQR, R.id.btnTopup, R.id.btnViewAll})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.civLogo:
                request = PROFILE_PIC;
                selectImage();
                break;
            case R.id.btnShare:
                GeneralActivity.startActivity((BaseActivity) getActivity(), "share");
                break;
            case R.id.btnQR:
                QRMemberActivity.startActivity((BaseActivity) getActivity());
                break;
            case R.id.btnTopup:
                SyaratDanKetentuanActivity.startActivity((BaseActivity) getActivity(),
                        "https://corporate.ultravoucher.co.id/help/topup.html?va_bca=" + PrefHelper.getString(PrefKey.VA_NUMBER) + "&va_permata=" + PrefHelper.getString(PrefKey.VA_NUMBERPERMATA), "TOPUP");
                break;
            case R.id.btnViewAll:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomePhase2FragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder((BaseActivity) getActivity());
        builder.setTitle("Change Profile Picture");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Photo")) {
                    HomePhase2FragmentPermissionsDispatcher.startCaptureWithPermissionCheck(HomePhase2Fragment.this);

                } else if (items[i].equals("Choose from Gallery")) {
                    HomePhase2FragmentPermissionsDispatcher.startSelectImageWithPermissionCheck(HomePhase2Fragment.this);
                }
            }
        });
        builder.show();
    }

    public static File createTemporaryFile(Context context, String folder_name, String ext) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode + ", " + resultCode);
        if (resultCode == RESULT_OK) {
            //user is returning from capturing an image using the camera
            if (requestCode == CAMERA_CAPTURE) {
                if (request == PROFILE_PIC) {
                    //cropImage();
                    HomePhase2FragmentPermissionsDispatcher.performCropWithPermissionCheck(HomePhase2Fragment.this);
                }
            } else if (requestCode == PICK_IMAGE_REQUEST) {
                picUri = data.getData();
                Log.d("uriGallery", picUri.toString());

                if (request == PROFILE_PIC) {
                    HomePhase2FragmentPermissionsDispatcher.performCropWithPermissionCheck(HomePhase2Fragment.this);
                }
            }

            //user is returning from cropping the image
            else if (requestCode == UCrop.REQUEST_CROP) {

                if (request == PROFILE_PIC) {
                    //get the returned data
                    picUriCropped = UCrop.getOutput(data);
                    if (picUriCropped != null) {
                        uploadProfilePic(new File(picUriCropped.getPath()));
                        //uploadProfilePic(picUriCropped);
                    } else {
                        // failed
                    }
                }
            }
        }
//        }
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    void startCapture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo;
        try {
            photo = createTemporaryFile(getContext(), "prof_pic", ".jpg");
            picFile = photo;
            picUri = FileProvider.getUriForFile(getContext(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            startActivityForResult(intent, CAMERA_CAPTURE);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE})
    void startSelectImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private void cropImage() {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");

        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", true);
        cropIntent.putExtra("outputX", 180);
        cropIntent.putExtra("outputY", 180);

        cropIntent.putExtra("aspectX", 3);
        cropIntent.putExtra("aspectY", 3);
        cropIntent.putExtra("scaleUpIfNeeded", true);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, PIC_CROP);

    }

    public UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.setMaxBitmapSize(1024);

        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
        options.setToolbarTitle("Edit Foto");
        options.setToolbarColor(ContextCompat.getColor(getContext(), android.R.color.black));
        options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        return uCrop.withOptions(options);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE})
    public void performCrop() {

        final String filename = Base64.encodeToString(UUID.randomUUID().toString().getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
        File target = new File(getContext().getCacheDir(), filename);
        UCrop uCrop = UCrop.of(picUri, Uri.fromFile(target));

        uCrop.withAspectRatio(1, 1);
        //uCrop.withMaxResultSize(max_width, max_height);

        advancedConfig(uCrop).start(getContext(), this);
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE})
    void pictureCrop(Bundle extrasPic){

        String filePath = Environment.getExternalStorageDirectory()
                + "/temporary_holder.jpg";

        Bitmap thePic = BitmapFactory.decodeFile(filePath);
        Log.d(TAG, "pictureCrop: ");
        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), thePic, "Title", null);
        uploadProfilePic(Uri.parse(path));
    }


    Uri uploadProfilePic(Uri fileUri){
        Log.d(TAG, "uploadProfilePic: " + fileUri.getPath());
//        uploadProfilePic(new File(FileUtils.getPath(getActivity().getApplicationContext(), fileUri)));
        uploadProfilePic(picFile);
        return fileUri;
    }

    void uploadProfilePic(File filedata) {

        long fileSize = filedata.length() / 1024;
        Log.d("File Size", "File size is " + fileSize + " Kb");
        int maxSize = 1024;

        if (fileSize > maxSize) {
            if (running = true) {
                ProfileDialog profileDialog = new ProfileDialog();
                profileDialog.showDialog((BaseActivity) getActivity());
            }
        } else {
            mPresenter.presentUploadProfilePic(filedata);
        }
    }
    //End method for change and crop profile picture/ background picture

    @Override
    public void onResume() {
        super.onResume();
        loadAdvertising();
        setupGVLainya();
        initRVPostEventList();
        mPresenter.getMemberPocket(mPresenter.constructGetPocketReq());
    }

    public void initRVPostEventList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvHotSales.setLayoutManager(linearLayoutManager);
        rvHotSales.setHasFixedSize(false);
        rvHotSales.setNestedScrollingEnabled(false);
        mAdapterHotSales = new HotSalesAdapter(this, mHotSales, rvHotSales);
        rvHotSales.setAdapter(mAdapterHotSales);
        rvHotSales.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {

                /*PostResponse item = (PostResponse) childView.getTag();
                PostDetailActivity.startActivity(MainActivity.this, item);*/

            }
        }));

        handler = new Handler();
        mAdapterHotSales.setOnLoadMoreListener(new HotSalesAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                rvHotSales.post(new Runnable() {
                    @Override
                    public void run() {
                        mHotSales.removeAll(Collections.singleton(null));
                        mHotSales.add(null);
                        mAdapterHotSales.notifyDataSetChanged();
                        Log.d(TAG, "onLoadMore: masuk");
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                loadMorePostEventList();
                            }
                        }, 2000);
                    }
                });

            }
        });
    }

    private void loadAdvertising() {
        mPresenter.presentAdvBanner();
    }


    private void setupGVLainya() {
        mAdapter = new BillerMenuCategoryAdapter(getContext(), mData, gridViewLainnya);
        gridViewLainnya.setAdapter(mAdapter);

        handler = new Handler();
        mAdapter.setOnLoadMoreListener(new BillerMenuCategoryAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mData.removeAll(Collections.singleton(null));
                mData.add(null);
                mAdapter.notifyDataSetInvalidated();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        loadMorePromoList();
                    }
                }, 2000);
            }
        });
    }

}
