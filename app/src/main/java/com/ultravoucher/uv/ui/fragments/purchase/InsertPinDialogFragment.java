package com.ultravoucher.uv.ui.fragments.purchase;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 1/31/2018.
 */

public class InsertPinDialogFragment extends BaseActivity implements Validator.ValidationListener {

    private static final String TAG = "InsertPinDialogFragment";

    PaymentMethodFragment mFragment;

    private static final String STATUS = "status";
    private static final String TYPE = "type";

    @BindView(R.id.insertotp_pb_loading)
    ProgressBar insertotpPbLoading;
    @BindView(R.id.insertotp_tv_error)
    TextView insertotpTvError;

    @BindView(R.id.d_registration_info_tv1)
    TextView dRegistrationInfoTv1;

    @BindView(R.id.etPassword)
    @NotEmpty
    EditText etPassword;


    @BindView(R.id.cv)
    LinearLayout cv;

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;

    private Validator mValidator;
    InsertPinDialogPresenter mPresenter;
    List<Order> orderDatas = new ArrayList<Order>();
    public void setOrderDatas(List<Order> orderDatas) {
        this.orderDatas.clear();
        this.orderDatas.addAll(orderDatas);
    }
    protected String orderNumber = "";
    protected String status = "";
    protected String type = "";

    public static void showFragment(BaseActivity sourceActivity, String status, String type) {

        Intent i = new Intent(sourceActivity, InsertPinDialogFragment.class);
        i.putExtra(STATUS, status);
        i.putExtra(TYPE, type);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);

        /*

        Intent i = new Intent(sourceFragment, InsertPinDialogFragment.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceFragment.startActivity(i);

        /*InsertPinDialogFragment fragment = new InsertPinDialogFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();*/
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        type = getIntent().getStringExtra(TYPE);

        mPresenter = new InsertPinDialogPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        setupToolbar();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (type.equalsIgnoreCase("purchase")) {
            mPresenter.getOrder();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.d_input_pin;
    }

    @Override
    public void onValidationSucceeded() {
        if (type.equalsIgnoreCase("purchase")) {
            mPresenter.getTransactionNumber();
        } else if (type.equalsIgnoreCase("biller")){
            mPresenter.getTransactionNumberBiller();
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.insertpin_btn_cancel)
    public void cancelPay() {
        finish();
    }

    @OnClick(R.id.insertpin_btn_confirm)
    public void confirmPay() {
        mValidator.validate();
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Payment Method");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
