package com.ultravoucher.uv.ui.adapters.phase2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.BillerCategoryMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import java.util.List;

/**
 * Created by Firwandi S Ramli on 26/09/18.
 */

public class BillerMenuCategoryAdapter extends BaseAdapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext;
    private List<BillerCategoryMsgResp> mDataSet;

    private int visibleTreshold = 3;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public BillerMenuCategoryAdapter(Context context, List<BillerCategoryMsgResp> mDataSet, GridView gridView) {
        this.mDataSet = mDataSet;
        this.mContext = context;
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!loading && (firstVisibleItem + visibleItemCount + visibleTreshold) >= totalItemCount ){
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    loading = true;
                }
            }
        });
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh = null;
        View gridView = convertView;
        vh = new ViewHolder();
        if (getItemViewType(position) == VIEW_ITEM) {
            gridView = LayoutInflater.from(mContext).inflate(R.layout.grid_home_menu, parent, false);
            final BillerCategoryMsgResp item = mDataSet.get(position);

            vh.iv = (ImageView) gridView.findViewById(R.id.grid_item_image);
            vh.tvTitle = (TextView) gridView.findViewById(R.id.tvTitle);
            Picasso.with(mContext).load(item.getImage()).placeholder(R.drawable.image_placeholder)
                    .centerCrop().fit().into(vh.iv);

            vh.tvTitle.setText(item.getName());

            vh.iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Todo Something
//                    VoucherListActivity.startActivity((BaseActivity) mContext, "1", item.getStoreId());
                }
            });
            gridView.setTag(vh);
        } else if (getItemViewType(position) == VIEW_PROG) {
            gridView = LayoutInflater.from(mContext).inflate(R.layout.progressbar_item_horizontal, parent, false);
            vh.progressBar = (ProgressBar) gridView.findViewById(R.id.progressBar);
            vh.progressBar.setIndeterminate(true);
        }

        return gridView;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class ViewHolder {
        ImageView iv;
        TextView tvTitle;
        ProgressBar progressBar;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;}

    public void setLoaded(boolean status) {
        loading = status;
    }
}