package com.ultravoucher.uv.ui.fragments.introduction;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.IntroductionActivity;
import com.ultravoucher.uv.ui.activities.LoginPresenter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 1/25/2018.
 */

public class FirstPageFragment extends BaseFragment {

    private static final String TAG = FirstPageFragment.class.getSimpleName();
    private static final String STATUS = "status";

    @BindView(R.id.img)
    ImageView img;

    @BindView(R.id.btnLanjut)
    Button btnLanjut;

    String stat;

    LoginPresenter loginPresenter;
    FirstPagePresenter mPresenter;

    public static void showFragment(BaseActivity sourceFragment, String status) {
        FirstPageFragment fragment = new FirstPageFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(STATUS, status);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new FirstPagePresenter(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stat = getArguments().getString(STATUS);

        if (stat.equals("first")) {
            btnLanjut.setText("Lanjut");
            Picasso.with(getContext()).load(R.drawable.how_to_use_a_1).fit().into(img);
        } else if (stat.equals("second")) {
            btnLanjut.setText("Lanjut");
            Picasso.with(getContext()).load(R.drawable.how_to_use_a_2).fit().into(img);
        } else if (stat.equals("third")) {
            btnLanjut.setText("Lanjut");
            Picasso.with(getContext()).load(R.drawable.how_to_use_a_3).fit().into(img);
        } else if (stat.equals("fourth")) {
            btnLanjut.setText("Tutup");
            Picasso.with(getContext()).load(R.drawable.how_to_use_a_4).fit().into(img);
        } else if (stat.equals("b")) {
            btnLanjut.setText("Lanjut");
            Picasso.with(getContext()).load(R.drawable.how_to_use_b_1).fit().into(img);
        } else if (stat.equals("b1")) {
            btnLanjut.setText("Lanjut");
            Picasso.with(getContext()).load(R.drawable.how_to_use_b_2).fit().into(img);
        } else if (stat.equals("b2")) {
            btnLanjut.setText("Tutup");
            Picasso.with(getContext()).load(R.drawable.how_to_use_b_3).fit().into(img);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_intro_page;
    }

    @OnClick(R.id.btnLanjut)
    void next() {
        if (stat.equals("first")) {
            FirstPageFragment.showFragment((BaseActivity) getActivity(), "second");
        } else if (stat.equals("second")) {
            FirstPageFragment.showFragment((BaseActivity) getActivity(), "third");
        } else if (stat.equals("third")) {
            FirstPageFragment.showFragment((BaseActivity) getActivity(), "fourth");
        } else if (stat.equals("fourth")) {
            IntroductionActivity.startActivity((BaseActivity) getActivity(), "b");
        } else if (stat.equals("b")) {
            FirstPageFragment.showFragment((BaseActivity) getActivity(), "b1");
        } else if (stat.equals("b1")) {
            FirstPageFragment.showFragment((BaseActivity) getActivity(), "b2");
        } else if (stat.equals("b2")) {
            PrefHelper.setString(PrefKey.INITIAL_RUN, "login");
            mPresenter.login(mPresenter.constructLoginReq());
//            doLogin();
//            HomePageActivity.startActivity((BaseActivity) getActivity(), "");
        }

    }




}
