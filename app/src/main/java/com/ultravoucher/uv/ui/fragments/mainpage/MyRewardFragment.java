package com.ultravoucher.uv.ui.fragments.mainpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.adapters.HomeViewPagerAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.myreward.ListMyRewardFragment;
import com.ultravoucher.uv.ui.fragments.myreward.ListRewardFragment;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class MyRewardFragment extends BaseFragment {
    private static final String TAG = "MyRewardFragment";

    @BindView(R.id.purchaseTablayout)
    TabLayout mTabLayout;

    @BindView(R.id.purchaseVP)
    ViewPager mViewPager;

    HistoryFragment mContext;

    private int[] tabIcons = {
            R.drawable.ic_reward_hijau_96px,
            R.drawable.ic_rewardku_96px
    };

    @Override
    protected int getLayout() {
        return R.layout.f_purchase_voucher;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

    }


    private void setupTabIcons() {
//        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
//        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab, null);
        view1.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_reward_hijau_96px);
        mTabLayout.getTabAt(0).setCustomView(view1);

        View view2 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab, null);
        view2.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_rewardku_300px);
        mTabLayout.getTabAt(1).setCustomView(view2);
    }

    private void setupViewPager(ViewPager viewPager) {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ListRewardFragment(), "Reward");
        adapter.addFragment(new ListMyRewardFragment(), "Rewardku");
//        adapter.addFragment(new HomePageFragment(), "Merchant");
        viewPager.setAdapter(adapter);
    }
}