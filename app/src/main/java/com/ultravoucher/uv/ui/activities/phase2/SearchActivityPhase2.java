package com.ultravoucher.uv.ui.activities.phase2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ultravoucher.uv.R;

public class SearchActivityPhase2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_search_phase2);
    }
}
