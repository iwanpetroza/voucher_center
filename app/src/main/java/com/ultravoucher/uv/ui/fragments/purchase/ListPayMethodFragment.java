/*
package com.indivara.vouchercenter.ui.fragments.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indivara.vouchercenter.R;
import com.indivara.vouchercenter.data.api.beans.ListPaymentMethodMsgResp;
import com.indivara.vouchercenter.ui.activities.BaseActivity;
import com.indivara.vouchercenter.ui.adapters.ListPaymentMethodAdapter;
import com.indivara.vouchercenter.ui.fragments.BaseFragment;
import com.indivara.vouchercenter.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

*/
/**
 * Created by firwandi.ramli on 10/30/2017.
 *//*


public class ListPayMethodFragment extends BaseFragment {

    private static final String TAG = ListPayMethodFragment.class.getSimpleName();

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    protected int page = 0;
    private Handler mHandler;
    private static final String VCLASS = "class";
    protected List<ListPaymentMethodMsgResp> mList = new ArrayList<>();
    ListPaymentMethodAdapter mAdapter;
    ListPayMethodPresenter mPresenter;
    String vClass;

    public static void showFragment(BaseActivity sourceFragment, String voucherClass) {
        ListPayMethodFragment fragment = new ListPayMethodFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(VCLASS, voucherClass);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        vClass = getArguments().getString(VCLASS);
        mPresenter = new ListPayMethodPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        page= 0;
        initRVListPayMethod();
        loadListPayMethod();
    }

    protected void initRVListPayMethod() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new ListPaymentMethodAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                ListPaymentMethodMsgResp product = (ListPaymentMethodMsgResp) childView.getTag();
                //Todo onClick
            }

        }));

        mHandler = new Handler();

        mAdapter.setOnLoadMoreListener(new ListPaymentMethodAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreListPayMethod();
                    }
                }, 2000);

            }
        });
    }

    private void loadListPayMethod() {
        mPresenter.presentGetListPayMethod(mPresenter.presentGetListReq());
    }

    private void loadMoreListPayMethod() {
        mPresenter.presentGetListPayMethodloadMore(mPresenter.presentGetListReq());
    }

}
*/
