package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.UtilMessageResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.AutoCompleteAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 11/3/2017.
 */

public class AddAddressFragment extends BaseFragment implements Validator.ValidationListener{

    private static final String TAG = AddAddressFragment.class.getSimpleName();

    @BindView(R.id.et_address)
    @NotEmpty
    EditText etAddress;

    @BindView(R.id.et_name)
    @NotEmpty
    EditText etName;

    @BindView(R.id.et_detail_address)
    @NotEmpty
    EditText etDetailAddress;

    @BindView(R.id.et_province)
    @NotEmpty
    EditText etProvince;

    @BindView(R.id.et_city)
    @NotEmpty
    EditText etCity;

    @BindView(R.id.et_district)
    @NotEmpty
    EditText etDistrict;

    @BindView(R.id.et_region)
    @NotEmpty
    EditText etVillage;

    @BindView(R.id.et_postal)
    @NotEmpty
    EditText etPostal;

    @BindView(R.id.et_phone_number)
    @NotEmpty
    EditText etPhoneNumber;

    AddAddressPresenter mPresenter;
    Validator mValidator;

    String mProvinceId, mCityId, mDistrictId, mVillageId;
    protected List<UtilMessageResp> mListProvince = new ArrayList<>();
    protected List<UtilMessageResp> mListCity = new ArrayList<>();
    protected List<UtilMessageResp> mListDistrict = new ArrayList<>();
    protected List<UtilMessageResp> mListVillage = new ArrayList<>();

    protected AutoCompleteAdapter mAdapter;
    protected AutoCompleteAdapter mAdapterCity;
    protected AutoCompleteAdapter mAdapterDistrict;
    protected AutoCompleteAdapter mAdapterVillage;

    /*NEW UPDATE*/

    List<UtilMessageResp> datasProvince = new ArrayList<>();
    List<String> listingProvince = new ArrayList<>();
    List<String> listingProvinceId = new ArrayList<>();
    int sizeProvince = 0;
    boolean isSelectedProv;

    List<UtilMessageResp> datasCity = new ArrayList<>();
    List<String> listingCity = new ArrayList<>();
    List<String> listingCityId = new ArrayList<>();
    int sizeCity = 0;
    boolean isSelectedCity;

    List<UtilMessageResp> datasDistrict = new ArrayList<>();
    List<String> listingDistrict = new ArrayList<>();
    List<String> listingDistrictId = new ArrayList<>();
    int sizeDistrict = 0;
    boolean isSelectedDistrict;

    List<UtilMessageResp> datasVillage = new ArrayList<>();
    List<String> listingVillage = new ArrayList<>();
    List<String> listingVillageId = new ArrayList<>();
    int sizeVillage = 0;
    boolean isSelectedVillage;

    /*END OF NEW UPDATE*/


    public static void showFragment(BaseActivity sourceFragment){
        AddAddressFragment fragment = new AddAddressFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_add_address;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new AddAddressPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadFilterArea();

        etProvince.setInputType(InputType.TYPE_NULL);
        etCity.setInputType(InputType.TYPE_NULL);
        etDistrict.setInputType(InputType.TYPE_NULL);

        View.OnFocusChangeListener ofcListener = new MyFocusChangeListener();
        etAddress.setOnFocusChangeListener(ofcListener);

        View.OnFocusChangeListener ofcListener1 = new MyFocusChangeListener();
        etDetailAddress.setOnFocusChangeListener(ofcListener1);

        View.OnFocusChangeListener ofcListener2 = new MyFocusChangeListener();
        etName.setOnFocusChangeListener(ofcListener2);

        /*View.OnFocusChangeListener ofcListener3 = new MyFocusChangeListener();
        etDistrict.setOnFocusChangeListener(ofcListener3);*/

        View.OnFocusChangeListener ofcListener4 = new MyFocusChangeListener();
        etVillage.setOnFocusChangeListener(ofcListener4);

        View.OnFocusChangeListener ofcListener5 = new MyFocusChangeListener();
        etPostal.setOnFocusChangeListener(ofcListener5);

        View.OnFocusChangeListener ofcListener6 = new MyFocusChangeListener();
        etPhoneNumber.setOnFocusChangeListener(ofcListener6);
    }

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus) {

            if (v.getId() == R.id.et_detail_address && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            } else if (v.getId() == R.id.et_address && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            } else if (v.getId() == R.id.et_name && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            } /*else if (v.getId() == R.id.et_district && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }*/ else if (v.getId() == R.id.et_region && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            } else if (v.getId() == R.id.et_postal && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            } else if (v.getId() == R.id.et_phone_number && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
        }
    }

    @OnClick(R.id.address_btn_simpan)
    public void onViewClicked() {

        String postCode = etPostal.getText().toString();
        if (postCode.trim().length() == 5) {
            mValidator.validate();
        } else {
            showToast("Kode Pos harus 5 karakter");
        }

    }

    @Override
    public void onValidationSucceeded() {
        mPresenter.createAddress(mPresenter.constructCreateDelAddress());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    void loadFilterArea() {
        mPresenter.getProvince();
    }


    void initCity() {
        mPresenter.getCity();
        /*mAdapterCity = new AutoCompleteAdapter(getContext(), R.layout.item_simple_textview, mListCity);
        etCity.setDropDownHeight(240);
        etCity.setThreshold(1);
        etCity.setAdapter(mAdapterCity);
        etCity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                UtilMessageResp umr = (UtilMessageResp) adapterView.getItemAtPosition(i);
                mCityId = umr.getId();
                initDistrict();
            }
        });*/
    }

    void initDistrict() {
        mPresenter.getDistrict();
        /*mAdapterDistrict = new AutoCompleteAdapter(getContext(), R.layout.item_simple_textview, mListDistrict);
        etDistrict.setDropDownHeight(240);
        etDistrict.setThreshold(1);
        etDistrict.setAdapter(mAdapterDistrict);
        etDistrict.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                UtilMessageResp umr = (UtilMessageResp) adapterView.getItemAtPosition(i);
                mDistrictId = umr.getId();
                initVillage();
            }
        });*/
    }

    void initVillage() {
//        mPresenter.getVillage();
        /*mAdapterVillage = new AutoCompleteAdapter(getContext(), R.layout.item_simple_textview, mListVillage);
        etRegion.setDropDownHeight(240);
        etRegion.setThreshold(1);
        etRegion.setAdapter(mAdapterVillage);
        etRegion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                UtilMessageResp umr = (UtilMessageResp) adapterView.getItemAtPosition(i);
                mVillageId = umr.getId();
            }
        });*/
    }

    /*NEW ADDED*/

    public void setProvinceName(List<String> provinceNames) {
        this.listingProvince.clear();
        this.listingProvince.addAll(provinceNames);
    }

    public void setProvinceId(List<String> provinceIds) {
        this.listingProvinceId.clear();
        this.listingProvinceId.addAll(provinceIds);
    }

    public void setCityNames(List<String> cityNames) {
        this.listingCity.clear();
        this.listingCity.addAll(cityNames);
    }

    public void setCityIds(List<String> cityIds) {
        this.listingCityId.clear();
        this.listingCityId.addAll(cityIds);
    }

    public void setDistrictNames(List<String> districtNames) {
        this.listingDistrict.clear();
        this.listingDistrict.addAll(districtNames);
    }

    public void setDistrictIds(List<String> districtIds) {
        this.listingDistrictId.clear();
        this.listingDistrictId.addAll(districtIds);
    }

    public void setVillageNames(List<String> villageNames) {
        this.listingVillage.clear();
        this.listingVillageId.addAll(villageNames);
    }

    public void setVillageIds(List<String> villageIds) {
        this.listingVillageId.clear();
        this.listingVillageId.addAll(villageIds);
    }



    @OnClick(R.id.et_province)
    public void showProvince() {

        final CharSequence[] provName = listingProvince.toArray(new CharSequence[listingProvince.size()]);
        final CharSequence[] provId = listingProvinceId.toArray(new CharSequence[listingProvinceId.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper((BaseActivity) getActivity(), R.style.GenericProgressDialogStyleAlert));
        builder.setTitle("Silahkan Pilih");

        builder.setItems(provName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                etProvince.setText(provName[i]);
                mProvinceId = provId[i].toString();
                isSelectedProv = true;
                initCity();

            }
        });
        builder.show();

    }

    @OnClick(R.id.et_city)
    public void showCities() {

        final CharSequence[] cityName = listingCity.toArray(new CharSequence[listingCity.size()]);
        final CharSequence[] cityId = listingCityId.toArray(new CharSequence[listingCityId.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper((BaseActivity) getActivity(), R.style.GenericProgressDialogStyleAlert));
        builder.setTitle("Silahkan Pilih");

        builder.setItems(cityName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                etCity.setText(cityName[i]);
                mCityId = cityId[i].toString();
                isSelectedCity = true;
                initDistrict();

            }
        });
        builder.show();
    }

    @OnClick(R.id.et_district)
    public void showDistricts() {

        final CharSequence[] districtName = listingDistrict.toArray(new CharSequence[listingDistrict.size()]);
        final CharSequence[] districtId = listingDistrictId.toArray(new CharSequence[listingDistrictId.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper((BaseActivity) getActivity(), R.style.GenericProgressDialogStyleAlert));
        builder.setTitle("Silahkan Pilih");

        builder.setItems(districtName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                etDistrict.setText(districtName[i]);
                mDistrictId = districtId[i].toString();
                isSelectedDistrict = true;

            }
        });
        builder.show();
    }
}
