package com.ultravoucher.uv.ui.activities;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.MyRewardRedeemReq;
import com.ultravoucher.uv.data.api.request.RedeemDigitalVoucherReq;
import com.ultravoucher.uv.data.api.response.RedeemDigitalVoucherResponse;
import com.ultravoucher.uv.data.api.response.VoucherRedemptionResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by Firwandi S Ramli on 23/04/18.
 */

public class GenerateQRCodePresenter {

    GenerateQRCodeActivity mActivity;

    public GenerateQRCodePresenter(GenerateQRCodeActivity mActivity) {
        this.mActivity = mActivity;
    }

    void redeemVoucher(final RedeemDigitalVoucherReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mActivity.getVoucherApi();
        Call<RedeemDigitalVoucherResponse> redeemDigitalVoucherResponseCall = api.doShareDigitalVoucher(contentType, authToken, dui, req);
        redeemDigitalVoucherResponseCall.enqueue(new Callback<RedeemDigitalVoucherResponse>() {
            @Override
            public void onResponse(Call<RedeemDigitalVoucherResponse> call, Response<RedeemDigitalVoucherResponse> response) {
                if (response.code() == 200) {
                    RedeemDigitalVoucherResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("TRX012")) {
                        mActivity.showToast("Redeem Voucher Berhasil");
                        mActivity.finish();
                        RedeemedDigitalVoucherActivity.startActivity(mActivity, mActivity.logo, mActivity.vouchercode, mActivity.exp, mActivity.name);
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast(mActivity.CONNECTION_ERROR);
                }

                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<RedeemDigitalVoucherResponse> call, Throwable t) {
                mActivity.showToast(mActivity.CONNECTION_ERROR);
                mActivity.dismissProgressDialog();
            }
        });
    }

    RedeemDigitalVoucherReq constructRedeemReq() {
        RedeemDigitalVoucherReq req = new RedeemDigitalVoucherReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setVoucherCode(mActivity.vouchercode);
        req.setVoucherId(mActivity.voucherId);
        return req;
    }

    void redeemRewardVoucher(final MyRewardRedeemReq req) {
        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mActivity.getVoucherApi();
        Call<VoucherRedemptionResponse> voucherRedemptionResponseCall = api.doRedeem(contentType, authToken, dui, req);
        voucherRedemptionResponseCall.enqueue(new Callback<VoucherRedemptionResponse>() {
            @Override
            public void onResponse(Call<VoucherRedemptionResponse> call, Response<VoucherRedemptionResponse> response) {
                if (response.code() == 200) {
                    VoucherRedemptionResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        mActivity.showToast("Redeem Voucher Berhasil");
                        mActivity.finish();
                        RedeemedDigitalVoucherActivity.startActivity(mActivity, mActivity.logo, mActivity.vouchercode, mActivity.exp, mActivity.name);
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<VoucherRedemptionResponse> call, Throwable t) {
                mActivity.showToast(mActivity.CONNECTION_ERROR);
                mActivity.dismissProgressDialog();
            }
        });

    }

    MyRewardRedeemReq constructDoRedeem() {

        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        MyRewardRedeemReq req = new MyRewardRedeemReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setActor(PrefHelper.getString(PrefKey.FIRSTNAME));
        req.setMerchantId(mActivity.mId);
        req.setStoreId("");
        req.setPin("123456");
        req.setRedemptionDate(formattedDate);
        req.setVoucherId(mActivity.voucherId);
        req.setVoucherCode(mActivity.vouchercode);
        return req;
    }
}
