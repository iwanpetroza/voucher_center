package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VoucherAmountMsgResp;
import com.ultravoucher.uv.ui.fragments.purchase.PayByVoucherFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Firwandi S Ramli on 10/08/18.
 */

public class PayByVoucherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    int totSize = 0;

    private PayByVoucherFragment mContext;
    private List<VoucherAmountMsgResp> mDataset;
    private List<PayByVoucherAdapter.ListVoucherHolder> listHolder;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private int totalFinal = 0;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    private AmountListener listener;
    VoucherAmountMsgResp voucherAmountMsgResp;
    List<String> listAmount2 = new ArrayList<>();
    List<String> listQty2 = new ArrayList<>();

    public interface AmountListener {
        void onSelected(List<VoucherAmountMsgResp> voucherAmountMsgResp);
    }

    public void getAmountListener(List<VoucherAmountMsgResp> voucherAmountMsgResp) {
        listener.onSelected(voucherAmountMsgResp);
    }

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public PayByVoucherAdapter(PayByVoucherFragment context, List<VoucherAmountMsgResp> myDataSet, RecyclerView recyclerView, AmountListener mListener) {
        mContext = context;
        mDataset = myDataSet;
        listener = mListener;
        listHolder = new ArrayList<>();


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_pay_by_voucher, parent, false);

            vh = new ListVoucherHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ListVoucherHolder) {
            final VoucherAmountMsgResp item = mDataset.get(position);
            totSize = mDataset.size();

            ((ListVoucherHolder) holder).itemView.setTag(item);
            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);

            ((ListVoucherHolder) holder).tvName.setText(item.getProductName());
            ((ListVoucherHolder) holder).tvTotalVoucher.setText("Total Voucher : " + item.getQuantity());

            if (item.getCountExpiredDate() == 0) {
                ((ListVoucherHolder) holder).tvExpired.setVisibility(View.GONE);
            } else {
                ((ListVoucherHolder) holder).tvExpired.setText("" + item.getCountExpiredDate() + " voucher expired sebelum : " + item.getExpiredDate());
            }


            ((ListVoucherHolder) holder).etTotalUsage.setFocusableInTouchMode(true);


            listHolder.add((ListVoucherHolder) holder);

            ((ListVoucherHolder) holder).etTotalUsage.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    String a = String.valueOf(s);

                    if (!((ListVoucherHolder) holder).etTotalUsage.getText().toString().equals("")
                            && ((ListVoucherHolder) holder).etTotalUsage.getText().toString() != null) {

                        if (Integer.parseInt(a) > item.getQuantity()) {
                            if (!((ListVoucherHolder) holder).etTotalUsage.getText().toString().equals("")
                                    && ((ListVoucherHolder) holder).etTotalUsage.getText().toString() != null) {
                                mContext.showToast("Jumlah voucher yang anda masukkan lebih dari voucher yang Anda miliki");
                                ((ListVoucherHolder) holder).etTotalUsage.setText("" + item.getQuantity());
                                getAmountListener(getList(((ListVoucherHolder) holder).etTotalUsage.getText().toString()));
                            } else {
                                getAmountListener(getList("0"));
                            }

                        } else {
                            getAmountListener(getList(((ListVoucherHolder) holder).etTotalUsage.getText().toString()));
                        }


                    } else {
                        getAmountListener(getList("0"));
                    }
                }
            });


            if (item.getImage() != null && !item.getImage().equals("")) {
                Picasso.with(mContext.getContext()).load(item.getImage()).placeholder(R.drawable.image_placeholder)
                        .centerCrop().fit().into(((ListVoucherHolder) holder).civLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                        ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                    }
                });
            } else {
                ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    public List<VoucherAmountMsgResp> getList(String et) {

        for (int i = 0; i < mDataset.size(); i++) {

            VoucherAmountMsgResp va = mDataset.get(i);
            PayByVoucherAdapter.ListVoucherHolder holders = listHolder.get(i);

            if (!holders.etTotalUsage.getText().toString().equals("0") && !holders.etTotalUsage.getText().toString().equals("") && holders.etTotalUsage.getText().toString() != null) {

                va.setAmountSum(Integer.parseInt(holders.etTotalUsage.getText().toString()) * va.getVoucherValue());
                va.setQuantityEt(Integer.parseInt(holders.etTotalUsage.getText().toString()));

            } else {
                va.setAmountSum(0);
                va.setQuantityEt(0);
            }

            mDataset.set(i, va);
        }


        return mDataset;

    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListVoucherHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvTotalVoucher)
        TextView tvTotalVoucher;

        @BindView(R.id.tvExpired)
        TextView tvExpired;

        @BindView(R.id.etTotalUsage)
        EditText etTotalUsage;

        @BindView(R.id.imgView)
        ImageView civLogo;

        @BindView(R.id.vcLoading)
        ProgressBar pbLoading;

        public ListVoucherHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            etTotalUsage = (EditText) itemView.findViewById(R.id.etTotalUsage);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

}
