package com.ultravoucher.uv.ui.fragments.setting;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class ChangeProfileFragment extends BaseFragment implements Validator.ValidationListener {


    private static final String TAG = ChangeProfileFragment.class.getSimpleName();
    @BindView(R.id.etName)
    @NotEmpty
    EditText etName;

    @BindView(R.id.etNumberId)
    @NotEmpty
    EditText etNumberId;

    @BindView(R.id.etBirthPlace)
    @NotEmpty
    EditText etBirthPlace;

    @BindView(R.id.etBirthDate)
    @NotEmpty
    EditText etBirthDate;

    @BindView(R.id.etBirthMonth)
    @NotEmpty
    @Length(min = 2, max = 2)
    EditText etBirthMonth;

    @BindView(R.id.etBirthYear)
    @NotEmpty
    @Length(min = 4, max = 4)
    EditText etBirthYear;

    @BindView(R.id.etMotherName)
    @NotEmpty
    EditText etMotherName;

    @BindView(R.id.etGender)
    @NotEmpty
    EditText etGender;

    @BindView(R.id.etphone)
    @NotEmpty
    EditText etphone;

    @BindView(R.id.etEmail)
    @NotEmpty
    EditText etEmail;

    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    private Calendar mCalendar = null;
    private DatePickerDialog.OnDateSetListener mDate;
    ChangeProfilePresenter mPresenter;
    Validator mValidator;
    int cal;

    public static void showFragment(BaseActivity sourceFragment) {
        ChangeProfileFragment fragment = new ChangeProfileFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }


    @Override
    protected int getLayout() {
        return R.layout.f_change_profile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new ChangeProfilePresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        mCalendar = Calendar.getInstance();
        cal = mCalendar.get(Calendar.YEAR);

        etGender.setText(PrefHelper.getString(PrefKey.GENDER));
        if(PrefHelper.getString(PrefKey.GENDER).equals("M")){
            etGender.setText("Pria");
        } else {
            etGender.setText("Wanita");
        }

//        etBirthDate.setText(PrefHelper.getString(PrefKey.BIRTH_DATE));


        if (!PrefHelper.getString(PrefKey.BIRTH_DATE).equals("")) {
            if (!PrefHelper.getString(PrefKey.BIRTH_DATE).equals("-")) {
                String days = PrefHelper.getString(PrefKey.BIRTH_DATE).toString();
                String tgl[] = days.split("-");
                etBirthDate.setText(tgl[2]);
                etBirthMonth.setText(tgl[1]);
                etBirthYear.setText(tgl[0]);
            }
        }

        etphone.setText(PrefHelper.getString(PrefKey.PHONE_NUMBER));
        etBirthPlace.setText(PrefHelper.getString(PrefKey.BIRTH_PLACE));
        etMotherName.setText(PrefHelper.getString(PrefKey.MOTHER_NAME));
        etNumberId.setText(PrefHelper.getString(PrefKey.ID_NUMBER));
        etName.setText(PrefHelper.getString(PrefKey.FIRSTNAME));
        etEmail.setText(PrefHelper.getString(PrefKey.USERNAME));


        etBirthDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                if (!hasFocus) {
                    if (etBirthDate.getText().toString().trim()
                            .length() != 2) {
                        etBirthDate.setText("0"+etBirthDate.getText().toString());
//                        showToast("Format yang Anda masukkan tidak sesuai");
                    }
                }
            }
        });

        etBirthMonth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                if (!hasFocus) {
                    if (etBirthMonth.getText().toString().trim()
                            .length() != 2) {
                        etBirthMonth.setText("0"+etBirthMonth.getText().toString());
//                        showToast("Format yang Anda masukkan tidak sesuai");
                    }
                }
            }
        });

        etBirthYear.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub

                if (!hasFocus) {
                    if (etBirthYear.getText().toString().trim()
                            .length() != 4) {

                        showToast("Format yang Anda masukkan tidak sesuai");
                    }
                }
            }
        });
    }

    @OnClick(R.id.profile_btn_simpan)
    public void onViewClicked() {

        String day = etBirthDate.getText().toString();
        String month = etBirthMonth.getText().toString();
        String year = etBirthYear.getText().toString();

        if (day.equals("") || month.equals("") || year.equals("")) {
            showToast("Mohon lengkapi data Anda");
        } else {
            if ((cal-10) > Integer.parseInt(year) && Integer.parseInt(year) < (cal - 10) && Integer.parseInt(year) != 0) {
                if (!day.equals("00") || !month.equals("00") || !year.equals("0000") || year.trim().length() != 4) {
                    if (month.equals("02")) {
                        if (Integer.parseInt(day) > 28) {
                            showToast("Maksimal tanggal pada bulan Februari adalah 28");
                        } else {
                            mValidator.validate();
                        }
                    } else {
                        if (Integer.parseInt(day) > 31) {
                            showToast("Maksimal tanggal adalah 31");
                        } else {
                            mValidator.validate();
                        }
                    }
                } else {
                    showToast("Input tanggal, bulan atau tahun tidak sesuai");
                }
            } else {
                showToast("Tahun minimum 10 tahun dari sekarang");
            }
        }







    }

    @Override
    public void onValidationSucceeded() {

        mPresenter.doUpdateProfile(mPresenter.constructProfile());


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage((BaseActivity) getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText((BaseActivity) getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    void init() {

        etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.selectGender();
            }
        });

        /*mCalendar = Calendar.getInstance();

        mDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, monthOfYear);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };*/
    }

    //Start method for set DateOfbirth
    /*private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etBirthDate.setText(sdf.format(mCalendar.getTime()));
    }*/

    /*@OnClick(R.id.etBirthDate)
    public void showDatePicker() {
        showDatePicker(true);
    }*/

    /*@OnFocusChange(R.id.etBirthDate)
    public void showDatePicker(boolean isFocus) {
        if (isFocus) {
            DatePickerDialog dpd = new DatePickerDialog(this.getContext(), mDate, mCalendar
                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                    mCalendar.get(Calendar.DAY_OF_MONTH));

            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            mCalendar.set(cal.get(Calendar.YEAR)-10, cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
            dpd.getDatePicker().setMaxDate(mCalendar.getTimeInMillis());
//            new DatePickerDialog(this.getContext(), mDate, mCalendar
//                    .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
//                    mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            dpd.show();
        }
    }*/
}
