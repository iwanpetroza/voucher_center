package com.ultravoucher.uv.ui.fragments.share;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.DealsDetailResp;
import com.ultravoucher.uv.data.api.request.DigitalPocketReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.PayByVoucherReq;
import com.ultravoucher.uv.data.api.response.DealsDetailResponse;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.PayByVoucherResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.purchase.VAConfirmFragment;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 12/12/2017.
 */

public class ListShareVoucherDigitalPresenter {

    int vClass;
    private ListShareVoucherDigitalFragment mFragment;

    public ListShareVoucherDigitalPresenter(ListShareVoucherDigitalFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetVoucherGrouping(DigitalPocketReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);


        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailResponse> dealsDetailResponseCall = api.getListDigitalGrouping(contentType, authToken, dui, req);
        dealsDetailResponseCall.enqueue(new Callback<DealsDetailResponse>() {
            @Override
            public void onResponse(Call<DealsDetailResponse> call, Response<DealsDetailResponse> response) {

                if (response.code() == 200) {
                    DealsDetailResponse resp = response.body();
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            initList(resp.getDeals());
                            mFragment.rvList.setVisibility(View.VISIBLE);

                            if (!mFragment.status.equals("share")) {
                                getOrder();
                            }

                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<DealsDetailResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }


    protected void presentGetVoucherGroupingLoadMore(DigitalPocketReq req) {

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailResponse> dealsDetailResponseCall = api.getListDigitalGrouping(contentType, authToken, dui, req);
        dealsDetailResponseCall.enqueue(new Callback<DealsDetailResponse>() {
            @Override
            public void onResponse(Call<DealsDetailResponse> call, Response<DealsDetailResponse> response) {

                if (response.code() == 200) {
                    DealsDetailResponse resp = response.body();
                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            addToList(resp.getDeals());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyItemRemoved(mFragment.mList.size());
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyItemRemoved(mFragment.mList.size());
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
            }

            @Override
            public void onFailure(Call<DealsDetailResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });
    }

    protected void initList(List<DealsDetailResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<DealsDetailResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyItemRemoved(mFragment.mList.size());
        mFragment.mAdapter.notifyDataSetChanged();
        for (DealsDetailResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyItemInserted(mFragment.mList.size());
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected DigitalPocketReq presentGetGroupingList() {
        String payCode2= String.valueOf(PrefHelper.getString(PrefKey.PAYCODE));

        DigitalPocketReq req = new DigitalPocketReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(10);
        req.setPage(mFragment.page);
        if (payCode2.equals("1")) {
            req.setVoucherClass(1);
        } else if (payCode2.equals("2")) {
            req.setVoucherClass(2);
        } else if (payCode2.equals("0")) {
            req.setVoucherClass(0);
        }
        req.setSVoucherClass("D");
        return req;
    }

    void getOrder()
    {
//        mFragment.pbLoadTrxMount.setVisibility(View.VISIBLE);
//        mFragment.tvTrxAmount.setVisibility(View.GONE);
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        vClass = resp.getOrders().get(0).getVoucherClass();
                        mFragment.setOrderDatas(resp.getOrders());
                        mFragment.orderNumber = resp.getOrders().get(0).getOrderNumber();
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
//                        mFragment.tvTrxAmount.setVisibility(View.VISIBLE);
//                        mFragment.tvTrxAmount.setText(AmountFormatter.format(resp.getTotalTrxAmount()));

                    }
                    else
                    {
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
//                    mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
//                mFragment.pbLoadTrxMount.setVisibility(View.GONE);
            }
        });
    }

    void doPayByVoucher() {

        String num = "";
        String odNumber = mFragment.status;
        String[] order = odNumber.split("\\|");

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";
        String payCode = String.valueOf(PrefHelper.getString(PrefKey.PAYCODE));

        if (mFragment.status.contains("pay")) {

            if (!order[1].equals("") && order[1] != null) {

                num = order[1];
            }
        } else if (mFragment.status.equals("fisik")) {
            num = mFragment.orderNumber;
        }

        PayByVoucherReq req = new PayByVoucherReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setOrderNumber(num);
        req.setMerchantId(mFragment.mId);

        if (payCode.equals("1")) {
            req.setVoucherClass(1);
        } else if (payCode.equals("2")) {
            req.setVoucherClass(2);
        }


        Call<PayByVoucherResponse> payByVoucherResponseCall = mFragment.getVoucherApi().doPayByVoucher(contentType, authToken, deviceUniqueId, req);
        payByVoucherResponseCall.enqueue(new Callback<PayByVoucherResponse>() {
            @Override
            public void onResponse(Call<PayByVoucherResponse> call, Response<PayByVoucherResponse> response) {
                if (response.code() == 200) {
                    PayByVoucherResponse resp = response.body();
                    PrefHelper.setString(PrefKey.PAYCODE, "");
                    if (resp.getAbstractResponse().getResponseStatus().equals("OD003")) {
                        mFragment.getActivity().finish();
                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), "-", resp.getOrders().get(0).getOrderNumber(), String.valueOf(resp.getTotalVoucherPayment()), "vc");
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
            }

            @Override
            public void onFailure(Call<PayByVoucherResponse> call, Throwable t) {
                String message = mFragment.getString(R.string.connection_error);
                mFragment.showToast(message);
            }
        });
    }

}
