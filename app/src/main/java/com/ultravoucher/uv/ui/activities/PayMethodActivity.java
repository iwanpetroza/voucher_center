package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.purchase.PaymentMethodFragment;

import butterknife.BindView;

public class PayMethodActivity extends BaseActivity {

    private static final String STATUS = "status";
    private static final String VCLASS = "vclass";
    private static final String TYPE = "type";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    String status = "";
    String vClass;
    String type;

    public static void startActivity(BaseActivity sourceActivity, String status, String vClass, String type){
        Intent i = new Intent(sourceActivity, PayMethodActivity.class);
        i.putExtra(STATUS, status);
        i.putExtra(VCLASS, vClass);
        i.putExtra(TYPE, type);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_pay_method;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        vClass = getIntent().getStringExtra(VCLASS);
        type = getIntent().getStringExtra(TYPE);
        setupToolbar();

        PaymentMethodFragment.showFragment(this, status, vClass, type);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Payment Method");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PrefHelper.setString(PrefKey.ORDERNUMBER, "");
                this.finish();
                onBackPressed();
                return true;

            /*case R.id.action_cart:
                CheckOutActivity.startActivity(this);
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        PrefHelper.setString(PrefKey.ORDERNUMBER, "");
        this.finish();
    }
}
