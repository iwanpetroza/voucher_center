package com.ultravoucher.uv.ui.fragments.mainpage;

import com.ultravoucher.uv.data.api.request.AdvertisingReq;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 1/24/2018.
 */

public class PopUpAdvDialogPresenter {

    private PopupAdvDialogFragment mFragment;

    public PopUpAdvDialogPresenter(PopupAdvDialogFragment mFragment) {
        this.mFragment = mFragment;
    }

    void presentAdvBanner() {

        /*mFragment.mCpiIndicator.setViewPager(mFragment.homeViewpager);
        final float density = mFragment.getResources().getDisplayMetrics().density;
        mFragment.mCpiIndicator.setRadius(5 * density);*/

        AdvertisingReq req = new AdvertisingReq();
        req.setAdvertisingType(1);
        req.setCityId("");

        Call<AdvertisingResponse> advertisingResponseCall = mFragment.getVoucherApi().getDetailAds(req);
        advertisingResponseCall.enqueue(new Callback<AdvertisingResponse>() {
            @Override
            public void onResponse(Call<AdvertisingResponse> call, Response<AdvertisingResponse> response) {
                AdvertisingResponse resp = response.body();
                if (response.code() == 200) {
                    if(resp.getAbstractResponse().getResponseStatus().equals("INQ000"))
                    {
                        mFragment.setData(resp.getAdvertisingResponse());
                    }
                    else
                    {
                        mFragment.showToast("Eror Bukan INQ000");
                    }
                } else {
                    mFragment.showToast("Eror Bukan 200");
                }
            }

            @Override
            public void onFailure(Call<AdvertisingResponse> call, Throwable t) {
                mFragment.showToast("Eror Koneksi");
            }
        });
    }
}