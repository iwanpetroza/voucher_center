package com.ultravoucher.uv.ui.fragments.share;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.DealsDetailResp;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.ShareVoucherDigitalActivity;
import com.ultravoucher.uv.ui.adapters.ShareVoucherDigitalListAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 12/12/2017.
 */

public class ListShareVoucherDigitalFragment extends BaseFragment {

    private static final String TAG = ListShareVoucherDigitalFragment.class.getSimpleName();

    private static final String STATUS = "status";

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    String status, mId;
    private Handler mHandler;
    protected List<DealsDetailResp> mList = new ArrayList<>();
    List<Order> orderDatas = new ArrayList<Order>();
    protected String orderNumber = "";
    ShareVoucherDigitalListAdapter mAdapter;
    ListShareVoucherDigitalPresenter mPresenter;

    public void setOrderDatas(List<Order> orderDatas) {
        this.orderDatas.clear();
        this.orderDatas.addAll(orderDatas);
    }

    public static void showFragment(BaseActivity sourceFragment, String status) {
        ListShareVoucherDigitalFragment fragment = new ListShareVoucherDigitalFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(STATUS, status);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getArguments().getString(STATUS);
        page = 0;
        mPresenter = new ListShareVoucherDigitalPresenter(this);

        if (status.equals("share")) {
            PrefHelper.setString(PrefKey.PAYCODE, "0");
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ll.setVisibility(View.GONE);
        page = 0;
        initRVListGruping();
        loadDigitalVoucherList();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*page = 0;
        initRVListGruping();
        loadDigitalVoucherList();*/
    }

    protected void initRVListGruping() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new ShareVoucherDigitalListAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                DealsDetailResp product = (DealsDetailResp) childView.getTag();

                if (status.equals("share")) {
                    PrefHelper.setString(PrefKey.ORDERNUMBER, "");
                    ShareVoucherDigitalActivity.startActivity((BaseActivity) getActivity(), product.getMerchantId());
                } else if (status.contains("pay")){
                    mId = product.getMerchantId();
                    confirmDialog();
//                    mPresenter.doPayByVoucher();
                } else if (status.contains("fisik")){
                    mId = product.getMerchantId();
                    confirmDialog();
//                    mPresenter.doPayByVoucher();
                }

//                ListMyVoucherDigitalActivity.startActivity((BaseActivity) getActivity(), product);
            }

        }));

        mHandler = new Handler();

        /*mAdapter.setOnLoadMoreListener(new ShareVoucherDigitalListAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreDigitalVoucherList();
                    }
                }, 2000);

            }
        });*/
    }

    private void loadDigitalVoucherList() {
        mPresenter.presentGetVoucherGrouping(mPresenter.presentGetGroupingList());
    }

    private void loadMoreDigitalVoucherList() {
        mPresenter.presentGetVoucherGroupingLoadMore(mPresenter.presentGetGroupingList());
    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(getActivity()), false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
        txtView.setText("Pastikan Nilai Transaksi Anda sudah sesuai dengan Nilai Voucher yang akan digunakan");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);

        final AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                mPresenter.doPayByVoucher();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
}
