package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.HistoryReward;
import com.ultravoucher.uv.data.api.request.HistoryRewardReq;
import com.ultravoucher.uv.data.api.response.HistoryRewardResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/7/2017.
 */

public class HistoryRewardPresenter {

    private HistoryRewardFragment mFragment;

    public HistoryRewardPresenter(HistoryRewardFragment mFragment) {

        this.mFragment = mFragment;
    }

    protected void presentGetHistoryReward(HistoryRewardReq req) {

        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryRewardResponse> historyRewardResponseCall = api.getHistoryReward(contentType, authToken, deviceUId, req);
        historyRewardResponseCall.enqueue(new Callback<HistoryRewardResponse>() {
            @Override
            public void onResponse(Call<HistoryRewardResponse> call, Response<HistoryRewardResponse> response) {
                if (response.code() == 200) {
                    HistoryRewardResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getDetailResponses().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                initList(resp.getDetailResponses());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                swapped(resp.getDetailResponses());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            }

                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<HistoryRewardResponse> call, Throwable t) {
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }
        });

    }

    protected void swapped(List<HistoryReward> data) {
        mFragment.mAdapter.swap(data);
    }

    protected void initList(List<HistoryReward> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected HistoryRewardReq constructRewardReq() {
        HistoryRewardReq req = new HistoryRewardReq();
        req.setPage(mFragment.page);
        req.setKeyword(mFragment.keyword);
        req.setOrder("DESC");
        req.setNRecords(50);
        req.setUsername(PrefHelper.getString(PrefKey.USERNAME));
        return req;
    }

}
