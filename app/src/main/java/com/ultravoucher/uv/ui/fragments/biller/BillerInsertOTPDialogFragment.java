package com.ultravoucher.uv.ui.fragments.biller;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.ultravoucher.uv.ui.fragments.purchase.InsertPinDialogFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class BillerInsertOTPDialogFragment extends BaseDialogFragment implements Validator.ValidationListener {

    private static final String TAG = "BillerInsertOTPDialogFragment";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    InsertPinDialogFragment mFragment;
    BillerInsertOTPDialogPresenter mPresenter;
    String pass, transactionId, status, totalTrxAmount;

    public BillerInsertOTPDialogFragment() {
    }

    public BillerInsertOTPDialogFragment(InsertPinDialogFragment mFragment, String pass, String transactionId, String status, String totalTrxAmount) {
        this.mFragment = mFragment;
        this.pass = pass;
        this.status = status;
        this.transactionId = transactionId;
        this.totalTrxAmount = totalTrxAmount;
        show(mFragment.getSupportFragmentManager(), TAG);
    }

    @BindView(R.id.insertotp_ll_form)
    LinearLayout llForm;

    @BindView(R.id.insertotp_ll_load)
    LinearLayout llLoad;

    @BindView(R.id.insertotp_pb_loading)
    ProgressBar pbLoad;

    @BindView(R.id.insertotp_tv_error)
    TextView tvError;

    @BindView(R.id.insertotp_et_otp)
    @NotEmpty
    EditText etOTP;

    @BindView(R.id.viewResendOTP)
    TextView viewResendOTP;

    String inqPayOrder;

    private Validator mValidator;

    @Override
    public void onStart() {
        super.onStart();
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.d_input_otp;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new BillerInsertOTPDialogPresenter(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewResendOTP.setVisibility(View.VISIBLE);
        viewResendOTP.setText("Silahkan tunggu 3 menit, \n sebelum klik 'KIRIM ULANG'");
        SmsRadar.initializeSmsRadarService(getContext(), new SmsListener() {
            @Override
            public void onSmsSent(Sms sms) {

            }

            @Override
            public void onSmsReceived(Sms sms) {
                //showToast(sms.getMsg());
                String [] data = sms.getMsg().split(" ");
                etOTP.setText(data[3]);
                Log.e("dataSms",""+sms.getMsg());
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        SmsRadar.stopSmsRadarService(getContext());
    }

    @OnClick(R.id.insertotp_btn_confirm)
    public void payordersend()
    {
        mValidator.validate();
    }

    @OnClick(R.id.insertotp_btn_resend)
    public void resendOTP()
    {
        mPresenter.getTransactionNumberBiller();
    }

    void reOpenDialog(){
        BillerInsertOTPDialogFragment.this.dismiss();
        BillerInsertOTPDialogFragment viewDialog = new BillerInsertOTPDialogFragment(mFragment, pass, transactionId, status, totalTrxAmount);
    }

    @OnClick(R.id.insertotp_btn_cancel)
    public void cancelOTP()
    {
        dismiss();
        mFragment.finish();

    }

    @Override
    public void onValidationSucceeded() {
        mPresenter.doPayOrderWalet();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
