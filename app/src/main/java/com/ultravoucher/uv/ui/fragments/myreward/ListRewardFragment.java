package com.ultravoucher.uv.ui.fragments.myreward;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.RewardResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.RewardListAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class ListRewardFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ListRewardFragment.class.getSimpleName();

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    protected int page = 0;
    private Handler mHandler;
    RewardListAdapter mAdapter;
    List<RewardResp> mList = new ArrayList<>();
    ListRewardPresenter mPresenter;
    String keyword = "";

    private boolean _hasLoadedOnce= false;

    public static void showFragment(BaseActivity sourceFragment) {
        ListRewardFragment fragment = new ListRewardFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }


    @Override
    protected int getLayout() {
        return R.layout.f_reword_listview;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ListRewardPresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        page = 0;
        initRVRewardList();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                rvList.setVisibility(View.GONE);
                mPresenter.presentGetRewardList(mPresenter.constructListReward());
            }
        });



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etSearch.setHint("Cari Nama Reward");

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String a = etSearch.getText().toString();

                if (a.equals("")) {
                    keyword = "";
                    rvList.setVisibility(View.GONE);
                    mPresenter.presentGetRewardList(mPresenter.constructListReward());
                }
            }
        });

    }

    protected void initRVRewardList() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new RewardListAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                RewardResp rewardResp = (RewardResp) childView.getTag();
                RewardDetailFragment.startActivity((BaseActivity) getActivity(), rewardResp);
            }

            @Override
            public void onItemLongPress(View childView, int position) {
                super.onItemLongPress(childView, position);
            }
        }));
    }

    private void loadRewardList() {
        mPresenter.presentGetRewardList(mPresenter.constructListReward());
    }


    @OnClick(R.id.btnSearch)
    void clicked() {
        if (etSearch.getText().toString().equals("")) {
            showToast("Masukkan kata kunci");
        } else {
            page = 0;
            keyword = etSearch.getText().toString();
            rvList.setVisibility(View.GONE);
            mPresenter.presentGetRewardList(mPresenter.constructListReward());
        }
    }


    //new
    @Override
    public void onRefresh(){
        page = 0;
        rvList.setVisibility(View.GONE);
        mPresenter.presentGetRewardList(mPresenter.constructListReward());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser) {


                //new
                swipe_container.setOnRefreshListener(this);
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);

                        page = 0;
                        rvList.setVisibility(View.GONE);
                        mPresenter.presentGetRewardList(mPresenter.constructListReward());
                    }
                });
                _hasLoadedOnce = true;
            }
        }
    }

}
