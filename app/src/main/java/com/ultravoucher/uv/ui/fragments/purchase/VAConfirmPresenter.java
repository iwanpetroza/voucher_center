package com.ultravoucher.uv.ui.fragments.purchase;

import com.ultravoucher.uv.data.api.beans.PromotionTransactionDetail;
import com.ultravoucher.uv.data.api.request.IssuingPointReq;
import com.ultravoucher.uv.data.api.response.IssuingPointResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 2/22/2018.
 */

public class VAConfirmPresenter {

    public VAConfirmFragment mContext;

    public VAConfirmPresenter(VAConfirmFragment mContext) {
        this.mContext = mContext;
    }

    void getPoint(IssuingPointReq request)
    {
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        Call<IssuingPointResp> respCall = mContext.getVoucherApi().getPointIssuing(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<IssuingPointResp>() {
            @Override
            public void onResponse(Call<IssuingPointResp> call, Response<IssuingPointResp> response) {
                if(response.code()==200)
                {
                    IssuingPointResp resp = response.body();

                    if(resp.getAbstractResponse().getResponseStatus().equals("INQ001"))
                    {
                        int test = resp.getPromotionMessageResponse().size();
                        int point = 0;
                        for (int i=0; i<test; i++) {
                            if (!String.valueOf(resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint()).equalsIgnoreCase("null")) {
                                if (point == 0) {
                                    point = resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint();
                                } else {
                                    point = point + resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint();
                                }
                            } else {
                                point = point + 0;
                            }
                        }
                        mContext.tvPoint.setText("Anda Mendapatkan poin sebanyak : " + String.valueOf(point));
                    }

                    else if(resp.getAbstractResponse().getResponseStatus().equals("MIS901"))
                    {
//                        mContext.ll_point.setVisibility(View.VISIBLE);
//                        mContext.tvPoint.setText(""+resp.getPromotionMessageResponse().get(0).getPromotionNumberIssued().getBasicRulePoint());

                        int test = resp.getPromotionMessageResponse().size();
                        int point = 0;
                        for (int i=0; i<test; i++) {
                            if (!String.valueOf(resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint()).equalsIgnoreCase("null")) {
                                if (point ==  0) {
                                    point = resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint();
                                } else {
                                    point = point + resp.getPromotionMessageResponse().get(i).getPromotionNumberIssued().getBasicRulePoint();
                                }
                            } else {
                                point = point + 0;
                            }
                        }

                        if(mContext.status.equalsIgnoreCase("va")) {
                            mContext.tvPoint.setText("");
                            mContext.tvPoint2.setText("");

                        } else {
                            mContext.tvPoint.setText("Selamat Anda Mendapatkan poin");
                            mContext.tvPoint2.setText(String.valueOf(point) + " Poin");

                        }

                    } else if(resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mContext.showToast("Sesi Anda telah berakhir. Silahkan login ulang");
                        mContext.finish();
                        mContext.doNeedRelogin();
                    }

                    else
                    {
                        mContext.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                }
                else if (response.code() == 400) {
                    mContext.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
                else
                {
                    mContext.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
            }

            @Override
            public void onFailure(Call<IssuingPointResp> call, Throwable t) {

            }
        });
    }

    IssuingPointReq constructRequest()
    {

        PromotionTransactionDetail nestedData = new PromotionTransactionDetail();
        nestedData.setMemberTier("3EE4F9D1-CBB5-456F-B457-6958D9C2E099");
        nestedData.setMillageTotal("0");
        nestedData.setPointAmount("0");
        nestedData.setReferralCode("");
        nestedData.setSocialSharingNumber("0");
        nestedData.setVisitNumber("0");
        nestedData.setTotalTrxAmount(""+mContext.trx);
        nestedData.setStampNumber("0");

        IssuingPointReq data = new IssuingPointReq();
        data.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
//        data.setMerchantId("02C3E9EA-DD5F-47A8-B9FC-548D8E5ECCF4"); // PROD
//        data.setStoreId("2D55E768-4415-4052-90DF-38BF8B0A1D67"); // PROD

        data.setMerchantId("BED85014-FD3F-4183-8D08-0AAFE3F52AB3"); // DEV
        data.setStoreId("39A5139E-6C59-48AB-BF7A-3FA88FDBC277"); // DEV
        data.setPosTransactionId("CEPLOC001");
        data.setPromotionCode("");
        data.setPromotionTransactionDetail(nestedData);

        return data;
    }
}
