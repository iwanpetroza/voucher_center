package com.ultravoucher.uv.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.DetailProductActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by firwandi.ramli on 11/8/2017.
 */

public class AdvBannerMainPageAdapter extends PagerAdapter {
    private List<AdvertisingMsgResp> advertises =new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    public AdvBannerMainPageAdapter(List<AdvertisingMsgResp> advertises, Context context) {
        this.advertises = advertises;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return advertises.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.item_introduction, view, false);
        assert imageLayout != null;

        final DotProgressBar pbIntro = (DotProgressBar)imageLayout.findViewById(R.id.introduction_pbImg);
        final TextView tvError = (TextView)imageLayout.findViewById(R.id.introduction_tverror);
        final ImageView imgIntro = (ImageView) imageLayout
                .findViewById(R.id.introduction_image);

        Picasso.with(context).load(advertises.get(position).getAdvertisingImage().toString()).fit()
                .into(imgIntro, new Callback() {
                    @Override
                    public void onSuccess() {
                        pbIntro.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        pbIntro.setVisibility(View.GONE);
                        tvError.setVisibility(View.VISIBLE);
                    }
                });
        imgIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (advertises.get(position).getUrl() != null){
                    //new
                    String url = advertises.get(position).getUrl();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    context.startActivity(i);

                } else if (advertises.get(position).getProductId() != null) {
                    DetailProductActivity.startActivity((BaseActivity) context, "", "", advertises.get(position).getProductId());
                } else {
                    Toast.makeText(context.getApplicationContext(), "Adv "+ position, Toast.LENGTH_SHORT).show();
                }
        }
    });
        view.addView(imageLayout, 0);


        return imageLayout;
}

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}
