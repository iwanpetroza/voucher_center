package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.biller.BillerHistoryFragment;
import com.ultravoucher.uv.ui.fragments.biller.BillerPlnFragment;

import butterknife.BindView;

/**
 * Created by tunggul.jati on 7/9/2018.
 */

public class BillerHistoryActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    private static final String STATUS = "status";

    String status;

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, BillerHistoryActivity.class);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        setupToolbar();
        BillerHistoryFragment.showFragment(this);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Histori Biller");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
