package com.ultravoucher.uv.ui.fragments.purchase;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 10/31/2017.
 */

@SuppressLint("ValidFragment")
public class ProductDetailDialog extends BaseDialogFragment {

    @BindView(R.id.order_success_tv_desc)
    TextView txtDesc;

    int pos;

    private static final String TAG = ProductDetailDialog.class.getSimpleName();

    DetailProductFragment mActivity;

    public ProductDetailDialog(){
        // Required empty public constructor
    }

    public ProductDetailDialog(DetailProductFragment activity, int pos){
        this.mActivity = activity;
        this.pos = pos;
        show(mActivity.getFragmentManager(), TAG);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (pos == 0) {
            txtDesc.setText("Produk berhasil ditambahkan ke Cart");
        } else  if (pos == 1) {
            txtDesc.setText("Stok Tidak Cukup");
        } else  if (pos == 2) {
            txtDesc.setText("Produk tidak ditemukan");
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.d_order_success;
    }

    //    public void showDialog(BaseActivity activity){
//        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.d_order_success);
//
//        dialog.show();
//    }
}
