package com.ultravoucher.uv.ui.activities.phase2;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.CheckOutActivity;
import com.ultravoucher.uv.ui.activities.HomePageActivity;
import com.ultravoucher.uv.ui.activities.LoginActivity;
import com.ultravoucher.uv.ui.activities.SettingActivity;
import com.ultravoucher.uv.ui.fragments.mainpage.HistoryFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.MyRewardFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.MyVoucherFragment;
import com.ultravoucher.uv.ui.fragments.phase2.HomePhase2Fragment;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.bnve)
    BottomNavigationViewEx navigation;


    public static void startActivity(BaseActivity sourceActivity) {
        Intent i = new Intent(sourceActivity, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a2_home;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.enableAnimation(false);
        navigation.enableItemShiftingMode(false);
        navigation.enableShiftingMode(false);
        navigation.setTextVisibility(true);
        navigation.setCurrentItem(0);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.support.v4.app.Fragment fragment;
            switch (item.getItemId()) {
                case R.id.nav_home:

                    if(PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
                        doNeedRelogin();
                    } else {
                        fragment = new HomePhase2Fragment();
                        loadFragment(fragment);
                        return true;
                    }
                case R.id.nav_voucherku:
                    if(PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
                        doNeedRelogin();
                    } else {
                        fragment = new MyVoucherFragment();
                        loadFragment(fragment);
                        return true;
                    }
                case R.id.nav_reward:

                    if(PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
                        doNeedRelogin();
                    } else {
                        fragment = new MyRewardFragment();
                        loadFragment(fragment);
                        return true;
                    }

                case R.id.nav_history:

                    if(PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
                        doNeedRelogin();
                    } else {
                        fragment = new HistoryFragment();
                        loadFragment(fragment);
                        return true;
                    }

                case R.id.nav_akun:
                    if(PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("")) {
                        doNeedRelogin();
                    } else {
                        fragment = new MyRewardFragment();
                        loadFragment(fragment);
                        return true;
                    }

            }
            return false;
        }
    };

    @OnClick({R.id.imgBtnCart, R.id.imgBtnSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtnCart:
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    LoginActivity.startActivity(this, false);
                } else {
                    CheckOutActivity.startActivity(this);
                }
                break;
            case R.id.imgBtnSearch:
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    LoginActivity.startActivity(this, false);
                } else {
                    SettingActivity.startActivity(this);
                }
                break;
        }
    }

    private void loadFragment(android.support.v4.app.Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
