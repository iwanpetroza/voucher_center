package com.ultravoucher.uv.ui.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.ultravoucher.uv.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class GenerateQRCodeActivity extends BaseActivity {

    private static final String LOGO = "logo";
    private static final String NAME = "name";
    private static final String VOUCHERID = "voucherid";
    private static final String VOUCHERCODE = "vouchercode";
    private static final String EXP = "exp";
    private static final String FLAGGING = "flagging";
    private static final String MID = "mid";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.tvExp)
    TextView tvExp;

    @BindView(R.id.tvName)
    TextView tvName;

    /*@BindView(R.id.tvVoucherId)
    TextView tvVoucherId;*/

    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    String logo, name, voucherId, vouchercode, exp, flag, mId = "";
    Bitmap bitmap;
    public final static int QRcodeWidth = 500 ;
    GenerateQRCodePresenter mPresenter;

    public static void startActivity(BaseActivity sourceActivity, String logo, String name, String vId, String vCode, String expired, String flagging, String merchantId){
        Intent i = new Intent(sourceActivity, GenerateQRCodeActivity.class);
        i.putExtra(LOGO, logo);
        i.putExtra(NAME, name);
        i.putExtra(VOUCHERID, vId);
        i.putExtra(EXP, expired);
        i.putExtra(VOUCHERCODE, vCode);
        i.putExtra(FLAGGING, flagging);
        i.putExtra(MID, merchantId);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_generate_qrcode2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        logo = getIntent().getStringExtra(LOGO);
        name = getIntent().getStringExtra(NAME);
        exp = getIntent().getStringExtra(EXP);
        mId = getIntent().getStringExtra(MID);

        vouchercode = getIntent().getStringExtra(VOUCHERCODE);
        voucherId = getIntent().getStringExtra(VOUCHERID);
        flag = getIntent().getStringExtra(FLAGGING);
        mPresenter = new GenerateQRCodePresenter(this);

        Picasso.with(this).load(logo).fit().centerCrop().into(ivLogo);
        tvName.setText(name);
//        tvVoucherId.setText("Voucher Id : " + voucherId);
        tvExp.setText("Tanggal Expired : " + exp);

    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Detail Voucher");

    }

    @OnClick(R.id.btnRedeem)
    public void onViewClicked() {
        confirmDialog();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.d_redeem_layout, new LinearLayout(this), false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.d_redeem_info_tv);
        final TextView txtView2 = (TextView) promptsView.findViewById(R.id.d_redeem_info_tv2);

        /*SPANNABLE*/

        SpannableString ss = new SpannableString(getResources().getString(R.string.redeem_warning2));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        ss.setSpan(clickableSpan, 21, 28, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtView.setText(ss);
        txtView.setMovementMethod(LinkMovementMethod.getInstance());
        txtView.setHighlightColor(getResources().getColor(R.color.t0_white));

        /*END OF SPANNABLE*/

        txtView2.setText("Voucher yang sudah digunakan, tidak menjadi tanggung jawab Ultra Voucher.");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btn_redeem_confirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btn_redeem_cancel);

        final AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (flag.equals("D")) {
                    mPresenter.redeemVoucher(mPresenter.constructRedeemReq());
                } else if (flag.equals("R")) {
                    mPresenter.redeemRewardVoucher(mPresenter.constructDoRedeem());
                }

                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
}
