package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VariantVoucherResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.HowtoActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.AmountFormatter;
import com.ultravoucher.uv.utils.MySpannable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/18/2017.
 */

public class DetailProductFragment extends BaseFragment implements Validator.ValidationListener{

    private static final String TAG = DetailProductFragment.class.getSimpleName();

    @BindView(R.id.imgLoading)
    ProgressBar imgLoading;
    @BindView(R.id.detailLogo)
    ImageView detailLogo;
    @BindView(R.id.rlImage)
    RelativeLayout rlImage;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSaleprice)
    TextView tvSaleprice;
    @BindView(R.id.rltvName)
    RelativeLayout rltvName;
    @BindView(R.id.rl_voucherDetail)
    RelativeLayout rlVoucherDetail;
    @BindView(R.id.ll_tvDesc)
    LinearLayout llTvDesc;

    @BindView(R.id.tvDescription)
    TextView tvDescription;

    @BindView(R.id.ll)
    LinearLayout ll;

    @BindView(R.id.etQty)
    @NotEmpty
    EditText etQty;

    @BindView(R.id.qty)
    TextInputLayout qty;

    @BindView(R.id.ll_jumlah)
    LinearLayout llJumlah;
    @BindView(R.id.etDelMethod)
    EditText etDelMethod;
    @BindView(R.id.delMethod)
    TextInputLayout delMethod;
    @BindView(R.id.ll_delMethod)
    LinearLayout llDelMethod;
    @BindView(R.id.tvInfo)
    TextView tvInfo;
    @BindView(R.id.tvJumlah)
    TextView tvJumlah;

    @BindView(R.id.tvBtn)
    TextView tvBtn;

    @BindView(R.id.btnInfo)
    ImageButton btnInfo;

    @BindView(R.id.ll_detail)
    LinearLayout ll_detail;

    @BindString(R.string.loading)
    String LOADING;

    boolean open, expandable;
    int total;
    int price, stockQty;
    String status, id, method;
    private static final String STATUS = "status";
    private static final String ID = "id";
    DetailProductPresenter mPresenter;
    Validator mValidator;
    List<VariantVoucherResp> datas = new ArrayList<>();

    private static final int MAX_LINES = 10;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getArguments().getString(STATUS);
        id = getArguments().getString(ID);
        mPresenter = new DetailProductPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

    }

    public static void showFragment(BaseActivity sourceFragment, String status, String id) {
        DetailProductFragment fragment = new DetailProductFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(STATUS, status);
        fragmentExtras.putString(ID, id);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_purchase_voucher_detail;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnInfo.setVisibility(View.GONE);

//        makeTextViewResizable(tvDescription, 3, "View More", true);
        if (!PrefHelper.getString(PrefKey.TOTAL).equals("")) {
          tvJumlah.setText("Total Harga : " + PrefHelper.getString(PrefKey.TOTAL));
        }

        if (status.equals("0")) {
            etDelMethod.setVisibility(View.GONE);
            tvBtn.setText("Tambah ke Cart");
            tvInfo.setText("(Voucher digital yang sudah terbayar, akan disimpan dimenu Voucherku > Voucher Digital)");
            tvInfo.setVisibility(View.GONE);
            btnInfo.setVisibility(View.GONE);

        } else {
            etDelMethod.setVisibility(View.VISIBLE);
            etDelMethod.setInputType(InputType.TYPE_NULL);
            tvInfo.setText("");
            tvBtn.setText("Tambah ke Cart");
            tvInfo.setVisibility(View.VISIBLE);
            etDelMethod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPresenter.getdeliveryMethod();
                }
            });

        }

        etDelMethod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                method = etDelMethod.getText().toString();
                if (method.equalsIgnoreCase("kirim")) {
                    tvInfo.setText("Pengisian alamat pada saat Bayar");
                    btnInfo.setVisibility(View.GONE);
                } else {
                    btnInfo.setVisibility(View.VISIBLE);
                    tvInfo.setText("Untuk mengetahui tata cara pengambilan Voucher Fisik, silahkan klik tombol disamping");
                }
            }
        });

        etQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String qty = etQty.getText().toString();
                if (!qty.equals("")) {
                    int a = Integer.parseInt(qty);
                    total = a * price;
                    tvJumlah.setText("Total Harga :    " + AmountFormatter.format(total));
                    PrefHelper.setString(PrefKey.TOTAL, String.valueOf(total));
                } else {
                    tvJumlah.setText("Total Harga :    0");
                }
            }
        });
    }

    @OnClick({R.id.ll_detail, R.id.btnPurchase})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.ll_detail:
                if (open) {
                    ll_detail.setVisibility(View.GONE);
                } else {
                    ll_detail.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btnPurchase:
                String qty = etQty.getText().toString();

                if (!etQty.getText().toString().equals("") || qty.length() > 0) {
                    if (qty.equals("0")) {
                        showToast("Pembelian minimum 1 Buah");
                    } else if (Integer.parseInt(qty) > 100) {
                        showToast("Pembelian maksimum 100 Buah");
                    } else {
                        mValidator.validate();
                    }
                } else {
                    showToast("Masukkan jumlah pembelian");
                }
                break;

            case R.id.btnInfo:
                HowtoActivity.startActivity((BaseActivity) getActivity());
                break;
        }



//        addReadLess(tvDescription.getText().toString(), tvDescription);
//        addReadMore(tvDescription.getText().toString(), tvDescription);
    }

    @Override
    public void onResume() {
        super.onResume();
        etQty.setText("");
        mPresenter.getDetail(mPresenter.constructDetailProduct());
    }

    @Override
    public void onValidationSucceeded() {
        /*if (status.equals("0")) {
            mPresenter.presentAddToCart(mPresenter.constructAddToCartRequest());
        } else {*/

        int txtQty = Integer.parseInt(etQty.getText().toString());
        if (txtQty > stockQty) {
            showToast("Stok hanya tersedia " + stockQty + " voucher");
        } else {

            if(status.equals("0")){
                mPresenter.getOrdernumber();
            }else{
                if(etDelMethod.getText().toString().equalsIgnoreCase("kirim") || etDelMethod.getText().toString().equalsIgnoreCase("ambil sendiri")){
                    mPresenter.getOrdernumber();
                }else{
                    showToast("Metode pengambilan tidak boleh kosong!");
                }
            }

        }



//        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.btnInfo)
    public void info() {
        HowtoTopupDialogFragment.showDialog((BaseActivity) getActivity());
    }

    public void initView(String txt) {
        MySpannable.doResizeTextView(tvDescription, MAX_LINES, " ...Read More", true, txt);
    }
}
