package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.response.BillingUpdateResp;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class OutputDialogFragment extends BaseDialogFragment {

    private static final String TAG = "OutputDialogFragment";

    @BindView(R.id.d_checkout_info_tv1)
    TextView tv1;
    @BindView(R.id.d_checkout_info_tv2)
    TextView tv2;
    @BindView(R.id.d_checkout_info_tv3)
    TextView tv3;
    @BindView(R.id.d_checkout_ll_btn)
    LinearLayout ll_btn;


    BaseFragment mFragment;
    int typeOutput;
    int nominal;
    BillingUpdateResp resp;
    public OutputDialogFragment(BaseFragment mFragment, int typeOutput, int nominal) {
        this.mFragment = mFragment;
        this.typeOutput = typeOutput;
        this.nominal = nominal;
        show(mFragment.getFragmentManager(), TAG);
    }

    public OutputDialogFragment(BaseFragment mFragment, int typeOutput, int nominal, BillingUpdateResp resp) {
        this.mFragment = mFragment;
        this.typeOutput = typeOutput;
        this.nominal = nominal;
        this.resp=resp;
        show(mFragment.getFragmentManager(), TAG);
    }


    @Override
    protected int getLayout() {
        return R.layout.d_checkout_output;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(typeOutput==0)
        {
            //Money Tidak Cukup
            tv1.setText("Sorry");
            tv2.setText("Your Wallet Balance is Insufficient");
            tv3.setText("Please Top Up First");
            ll_btn.setVisibility(View.VISIBLE);
        }
        else if(typeOutput==1)
        {
            //OTP Error
            tv1.setText("Sorry");
            tv2.setText("Your OTP Code is Incorrect");
            tv3.setText("Please Try Again");
            ll_btn.setVisibility(View.GONE);
        }
        else if(typeOutput==2)
        {
            //Success
            tv1.setText("Congratulations");
            tv2.setText("Your Payment Successful");
            tv3.setText("Thank You");
            ll_btn.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            },4000);
        }
        else if(typeOutput==3)
        {
            //Success
            tv1.setText("Congratulations");
            tv2.setText("You have successfully Add To Cart");
            tv3.setText("Thank You");
            ll_btn.setVisibility(View.GONE);
        }
        else if(typeOutput==4)
        {
            //Success
            tv1.setText("Maaf,");
            tv2.setText("Anda tidak memiliki barang");
            tv3.setText("di dalam cart");
            ll_btn.setVisibility(View.GONE);
        }
        /*else if(typeOutput==5)
        {
            //Success
            tv1.setText("Sorry");
            tv2.setText("Please Select Delivery Method First");
            tv3.setText("In your product ordered");
            ll_btn.setVisibility(View.GONE);
        }*/
        else if(typeOutput==5)
        {
            //Success
            tv1.setText("");
            tv2.setText("Please Input Pick Up Date");
            tv3.setText("");
            ll_btn.setVisibility(View.GONE);
        }
        else if(typeOutput==6)
        {
            //Success
            tv1.setText("");
            tv2.setText("Pesanan Anda berhasil dihapus");
            tv3.setText("");
            ll_btn.setVisibility(View.GONE);
        }
        else if(typeOutput==7)
        {
            //Success
            tv1.setText("Congratulations");
            tv2.setText("Your Payment Successful");
            tv3.setText("Thank You");
            ll_btn.setVisibility(View.GONE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            },4000);
        }

    }

    @OnClick(R.id.d_checkout_btnClose)
    public void btnCLoseClick()
    {
        dismiss();
    }
}
