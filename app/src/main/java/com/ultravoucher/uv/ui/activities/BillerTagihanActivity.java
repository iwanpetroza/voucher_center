package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.biller.BillerMenuFragment;
import com.ultravoucher.uv.ui.fragments.biller.BillerMenuTagihanFragment;

import butterknife.BindView;

public class BillerTagihanActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    private static final String STATUS = "status";

    String status;

    public static void startActivity(BaseActivity sourceActivity, String status){
        Intent i = new Intent(sourceActivity, BillerTagihanActivity.class);
        i.putExtra(STATUS, status);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        setupToolbar();
        if (status.equalsIgnoreCase("tagihan")) {
            BillerMenuTagihanFragment.showFragment(this);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (status.equalsIgnoreCase("tagihan")) {
            toolbarTitle.setText("Bayar Tagihan");
        }
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_biller, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            /*case R.id.menu_bill:
                BillerHistoryActivity.startActivity(this);
                return true;*/
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
