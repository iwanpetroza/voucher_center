package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.R;

import butterknife.BindView;
import butterknife.OnClick;

public class QRUVVoucherActivity extends BaseActivity {

    private static final String LOGO = "logo";
    private static final String NAME = "name";
    private static final String VOUCHERID = "voucherid";
    private static final String VOUCHERCODE = "vouchercode";
    private static final String EXP = "exp";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.tv)
    TextView tv;
    @BindView(R.id.ivQR)
    ImageView ivQR;

    @BindView(R.id.tvCode)
    TextView tvCode;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.tvExp)
    TextView tvExp;

    @BindView(R.id.tvVoucherId)
    TextView tvVoucherId;

    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    @BindView(R.id.tvError)
    TextView tvError;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    String logo, name, voucherId, vouchercode, exp = "";
    Bitmap bitmap;
    public final static int QRcodeWidth = 500 ;

    public static void startActivity(BaseActivity sourceActivity, String logo, String name, String vId, String vCode, String expired ){
        Intent i = new Intent(sourceActivity, QRUVVoucherActivity.class);
        i.putExtra(LOGO, logo);
        i.putExtra(NAME, name);
        i.putExtra(VOUCHERID, vId);
        i.putExtra(EXP, expired);
        i.putExtra(VOUCHERCODE, vCode);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_qruvvoucher;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        logo = getIntent().getStringExtra(LOGO);
        name = getIntent().getStringExtra(NAME);
        exp = getIntent().getStringExtra(EXP);
        vouchercode = getIntent().getStringExtra(VOUCHERCODE);
        voucherId = getIntent().getStringExtra(VOUCHERID);


        try {
            bitmap = TextToImageEncode(vouchercode);

            ivQR.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        Picasso.with(this).load(logo).fit().centerCrop().into(ivLogo);
        tvCode.setText("Voucher Code : " + vouchercode);
        tvName.setText(name);
        tvExp.setText("Tgl Expired : " + exp);
        tvVoucherId.setText("Voucher Id : " + voucherId);

    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        ContextCompat.getColor(this, R.color.black): ContextCompat.getColor(this, R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("QR Code Voucher");
    }

    @OnClick(R.id.btnBack)
    public void onViewClicked() {

        this.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
