package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.PayByWaletReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.PayOrderResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 1/23/2018.
 */

public class InsertOTPDialogPresenter {

    InsertOTPDialogFragment mFragment;

    public InsertOTPDialogPresenter(InsertOTPDialogFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getOrder()
    {
        mFragment.showProgressDialog(mFragment.LOADING);
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        mFragment.orderNumber = resp.getOrders().get(0).getOrderNumber();

                    }
                    else
                    {
                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mFragment.llForm.setVisibility(View.GONE);
                    mFragment.llLoad.setVisibility(View.VISIBLE);
                    mFragment.pbLoad.setVisibility(View.GONE);
                    mFragment.tvError.setVisibility(View.VISIBLE);
                    mFragment.tvError.setPadding(0,100,0,100);
                    mFragment.tvError.setText("Server Is Down");
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
                mFragment.llForm.setVisibility(View.GONE);
                mFragment.llLoad.setVisibility(View.VISIBLE);
                mFragment.pbLoad.setVisibility(View.GONE);
                mFragment.tvError.setVisibility(View.VISIBLE);
                mFragment.tvError.setPadding(0,100,0,100);
                mFragment.tvError.setText("Connection is Error");
                mFragment.dismissProgressDialog();
            }
        });
    }

    /*void getTransactionNumber()
    {
        mFragment.llForm.setVisibility(View.GONE);
        mFragment.llLoad.setVisibility(View.VISIBLE);
        mFragment.pbLoad.setVisibility(View.VISIBLE);
        mFragment.tvError.setVisibility(View.GONE);
        InqPayOrderReq request = new InqPayOrderReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(PrefHelper.getString(PrefKey.IS_PIN));
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayOrderResponse> respCall = mFragment.getVoucherApi().getInqPayOrder(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayOrderResponse>() {
            @Override
            public void onResponse(Call<InqPayOrderResponse> call, Response<InqPayOrderResponse> response) {
                if(response.code()==200)
                {
                    InqPayOrderResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD005"))
                    {
                        mFragment.llForm.setVisibility(View.VISIBLE);
                        mFragment.llLoad.setVisibility(View.GONE);
                        mFragment.orderNumber = resp.getTransactionId();
                    }
                    else
                    {
                        mFragment.llForm.setVisibility(View.GONE);
                        mFragment.llLoad.setVisibility(View.VISIBLE);
                        mFragment.pbLoad.setVisibility(View.GONE);
                        mFragment.tvError.setVisibility(View.VISIBLE);
                        mFragment.tvError.setText(resp.getAbstractResponse().getResponseMessage());
                        mFragment.tvError.setPadding(0,100,0,100);
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mFragment.llForm.setVisibility(View.GONE);
                    mFragment.llLoad.setVisibility(View.VISIBLE);
                    mFragment.pbLoad.setVisibility(View.GONE);
                    mFragment.tvError.setVisibility(View.VISIBLE);
                    mFragment.tvError.setPadding(0,100,0,100);
                    mFragment.tvError.setText("Server Is Down");
                }
            }

            @Override
            public void onFailure(Call<InqPayOrderResponse> call, Throwable t) {
                mFragment.llForm.setVisibility(View.GONE);
                mFragment.llLoad.setVisibility(View.VISIBLE);
                mFragment.pbLoad.setVisibility(View.GONE);
                mFragment.tvError.setVisibility(View.VISIBLE);
                mFragment.tvError.setPadding(0,100,0,100);
                mFragment.tvError.setText("Connection is Error");
            }
        });
    }*/

    void doPayOrderWalet() {

        mFragment.showProgressDialog(mFragment.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        PayByWaletReq request = new PayByWaletReq();
        request.setTransactionId(mFragment.transactionId);

        if (mFragment.status.equals("D")) {
            request.setOrderNumber(PrefHelper.getString(PrefKey.ORDERNUMBER));
        } else {
            request.setOrderNumber(mFragment.orderNumber);
        }

        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(mFragment.pass);
        request.setOtp(mFragment.etOTP.getText().toString());

        Call<PayOrderResp> respCall = mFragment.getVoucherApi().payByWalet(contentType, authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<PayOrderResp>() {
            @Override
            public void onResponse(Call<PayOrderResp> call, Response<PayOrderResp> response) {
                if(response.code()==200)
                {
                    PayOrderResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD003"))
                    {
                        mFragment.getDialog().dismiss();
                        mFragment.getActivity().finish();
                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), resp.getOrders().get(0).getPaymentReferenceCode(), mFragment.orderNumber , String.valueOf(resp.getTotalTrxAmount()), "walet");
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());

                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getDialog().dismiss();
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());

                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());

                    }
                }
                else if (response.code() == 400) {

                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());

                }
                else
                {
                    mFragment.showToast("Error Data");
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<PayOrderResp> call, Throwable t) {
                mFragment.showToast("Failed to Connect");
                mFragment.dismissProgressDialog();
            }
        });
    }


    /*void doPayOrder() {
    }
        mFragment.llForm.setVisibility(View.GONE);
        mFragment.llLoad.setVisibility(View.VISIBLE);
        mFragment.pbLoad.setVisibility(View.VISIBLE);
        mFragment.tvError.setVisibility(View.GONE);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
        PayOrderReq request = new PayOrderReq();
        List<OrderPay> orderPays = new ArrayList<>();
        for(int i=0; i<mFragment.orderDatas.size(); i++)
        {
            OrderPay order = new OrderPay();
            order.setOrderId(mFragment.orderDatas.get(i).getOrderId());
            order.setDiscountValue(mFragment.orderDatas.get(i).getDiscountValue());
            order.setShippingAmount(mFragment.orderDatas.get(i).getShippingAmount());
            order.setTaxAmount(mFragment.orderDatas.get(i).getTaxAmount());
            order.setTotalAmount(mFragment.orderDatas.get(i).getTotalAmount());
            orderPays.add(order);
        }
        request.setOrderNumber(mFragment.orderNumber);
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setOtp(mFragment.etOTP.getText().toString());
        request.setPin(PrefHelper.getString(PrefKey.IS_PIN));
        request.setPaymentChannel("");
        request.setPaymentDate(df.format(new Date()));
        request.setOrders(orderPays);

        Call<PayOrderResp> respCall = mFragment.getLoyaltyApi().doPayOrder(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<PayOrderResp>() {
            @Override
            public void onResponse(Call<PayOrderResp> call, Response<PayOrderResp> response) {
                if(response.code()==200)
                {
                    PayOrderResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD003"))
                    {
                        SmsRadar.stopSmsRadarService(mFragment.getContext());
                        mFragment.dismiss();
                        OutputDialogFragment viewDialog = new OutputDialogFragment((BaseFragment) mFragment.mFragment, 2, resp.getTotalTrxAmount());
                    }
                    else
                    {
                        SmsRadar.stopSmsRadarService(mFragment.getContext());
                        mFragment.dismiss();
                        OutputDialogFragment viewDialog = new OutputDialogFragment((BaseFragment) mFragment.mFragment, 1, 0);

                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mFragment.showToast("Error Data");
                }
            }

            @Override
            public void onFailure(Call<PayOrderResp> call, Throwable t) {
                mFragment.showToast("Failed to Connect");
            }
        });
    }*/
}
