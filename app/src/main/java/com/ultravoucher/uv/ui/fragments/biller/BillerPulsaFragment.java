package com.ultravoucher.uv.ui.fragments.biller;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.PayMethodActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.AmountFormatter2;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tunggul.jati on 5/18/2018.
 */

public class BillerPulsaFragment extends BaseFragment implements Validator.ValidationListener {

    private static final String TAG = BillerPulsaFragment.class.getSimpleName();

    @BindView(R.id.btn_list_fav)
    Button btn_list_fav;

    @BindView(R.id.cbFav)
    CheckBox cbFav;

    @BindView(R.id.rl_all)
    RelativeLayout rl_all;

    @BindView(R.id.tvPilihan)
    TextView tvPilihan;

    @BindView(R.id.tvNominalAxis)
    TextView tvNominalAxis;

    @BindView(R.id.tvNominalIndosat)
    TextView tvNominalIndosat;

    @BindView(R.id.tvNominalTsel)
    TextView tvNominalTsel;

    @BindView(R.id.tvNominalXl)
    TextView tvNominalXl;

    @BindView(R.id.tvNominalSmart)
    TextView tvNominalSmart;

    @BindView(R.id.tvHarga)
    TextView tvHarga;

    @BindView(R.id.tvFee)
    TextView tvFee;

    @BindView(R.id.mycart_tv_checkout)
    TextView mycart_tv_checkout;

    @BindView(R.id.mycart_btn_checkout)
    LinearLayout mycart_btn_checkout;

    @BindView(R.id.llNominalAxis)
    LinearLayout llNominalAxis;

    @BindView(R.id.llNominalIndosat)
    LinearLayout llNominalIndosat;

    @BindView(R.id.llNominalTsel)
    LinearLayout llNominalTsel;

    @BindView(R.id.llNominalXl)
    LinearLayout llNominalXl;

    @BindView(R.id.llNominalSmart)
    LinearLayout llNominalSmart;

    @BindView(R.id.llIdPel)
    LinearLayout llIdPel;

    @BindView(R.id.llIdPelPasca)
    LinearLayout llIdPelPasca;

    @BindView(R.id.etIdPel)
    @Length(min = 10, max = 15, message = "Harap isi data valid")
    EditText etIdPel;

    @BindView(R.id.etIdPelPasca)
    @Length(min = 10, max = 15, message = "Harap isi data valid")
    EditText etIdPelPasca;

    @BindView(R.id.llLogoOperator)
    LinearLayout llLogoOperator;

    @BindView(R.id.ivLogoOperator)
    ImageView ivLogoOperator;

    @BindView(R.id.llCekTagihan)
    LinearLayout llCekTagihan;

    @BindView(R.id.llTagihan)
    LinearLayout llTagihan;

    @BindView(R.id.tvTagihan)
    TextView tvTagihan;

    @BindView(R.id.tvNominalThree)
    TextView tvNominalThree;

    @BindView(R.id.llNominalThree)
    LinearLayout llNominalThree;


    private Validator mValidator;
    BillerPulsaPresenter mPresenter;

    /**
     * PRICE
     */
    /*LIST PRICE PULSA AXIS*/
    public int price_Axis_5 = 0;
    public int price_Axis_10 = 0;
    public int price_Axis_15 = 0;
    public int price_Axis_25 = 0;
    public int price_Axis_50 = 0;
    public int price_Axis_100 = 0;

    /*LIST PRICE PULSA INDOSAT*/
    public int price_Indosat_5 = 0;
    public int price_Indosat_10 = 0;
    public int price_Indosat_25 = 0;
    public int price_Indosat_50 = 0;
    public int price_Indosat_100 = 0;

    /*LIST PRICE PULSA XL*/
    public int price_XL_5 = 0;
    public int price_XL_10 = 0;
    public int price_XL_25 = 0;
    public int price_XL_50 = 0;
    public int price_XL_100 = 0;

    /*LIST PRICE PULSA TELKOMSEL*/
    public int price_Telkomsel_5 = 0;
    public int price_Telkomsel_10 = 0;
    public int price_Telkomsel_25 = 0;
    public int price_Telkomsel_50 = 0;
    public int price_Telkomsel_100 = 0;

    /*LIST PRICE PULSA SMARTFREN*/
    public int price_Smartfren_5 = 0;
    public int price_Smartfren_10 = 0;
    public int price_Smartfren_20 = 0;
    public int price_Smartfren_25 = 0;
    public int price_Smartfren_50 = 0;
    public int price_Smartfren_100 = 0;

    /*LIST PRICE PULSA THREE*/
    public int price_three_5 = 0;
    public int price_three_10 = 0;
    public int price_three_20 = 0;
    public int price_three_50 = 0;
    public int price_three_100 = 0;

    /**
     * FEE
     */
    /*LIST FEE PULSA AXIS*/
    public int fee_Axis_5 = 0;
    public int fee_Axis_10 = 0;
    public int fee_Axis_15 = 0;
    public int fee_Axis_25 = 0;
    public int fee_Axis_50 = 0;
    public int fee_Axis_100 = 0;

    /*LIST FEE PULSA INDOSAT*/
    public int fee_Indosat_5 = 0;
    public int fee_Indosat_10 = 0;
    public int fee_Indosat_25 = 0;
    public int fee_Indosat_50 = 0;
    public int fee_Indosat_100 = 0;

    /*LIST FEE PULSA XL*/
    public int fee_XL_5 = 0;
    public int fee_XL_10 = 0;
    public int fee_XL_25 = 0;
    public int fee_XL_50 = 0;
    public int fee_XL_100 = 0;

    /*LIST FEE PULSA TELKOMSEL*/
    public int fee_Telkomsel_5 = 0;
    public int fee_Telkomsel_10 = 0;
    public int fee_Telkomsel_25 = 0;
    public int fee_Telkomsel_50 = 0;
    public int fee_Telkomsel_100 = 0;

    /*LIST FEE PULSA SMARTFREN*/
    public int fee_Smartfren_5 = 0;
    public int fee_Smartfren_10 = 0;
    public int fee_Smartfren_20 = 0;
    public int fee_Smartfren_25 = 0;
    public int fee_Smartfren_50 = 0;
    public int fee_Smartfren_100 = 0;

    /*LIST FEE PULSA THREE*/
    public int fee_three_5 = 0;
    public int fee_three_10 = 0;
    public int fee_three_20 = 0;
    public int fee_three_50 = 0;
    public int fee_three_100 = 0;

    /**
     * ACTIVE
     */
    /*LIST ACTIVE PULSA AXIS*/
    public int active_Axis_5;
    public int active_Axis_10;
    public int active_Axis_15;
    public int active_Axis_25;
    public int active_Axis_50;
    public int active_Axis_100;

    /*LIST ACTIVE PULSA INDOSAT*/
    public int active_Indosat_5;
    public int active_Indosat_10;
    public int active_Indosat_25;
    public int active_Indosat_50;
    public int active_Indosat_100;

    /*LIST ACTIVE PULSA XL*/
    public int active_XL_5;
    public int active_XL_10;
    public int active_XL_25;
    public int active_XL_50;
    public int active_XL_100;

    /*LIST ACTIVE PULSA TELKOMSEL*/
    public int active_Telkomsel_5;
    public int active_Telkomsel_10;
    public int active_Telkomsel_25;
    public int active_Telkomsel_50;
    public int active_Telkomsel_100;

    /*LIST ACTIVE PULSA SMARTFREN*/
    public int active_Smartfren_5;
    public int active_Smartfren_10;
    public int active_Smartfren_20;
    public int active_Smartfren_25;
    public int active_Smartfren_50;
    public int active_Smartfren_100;

    /*LIST ACTIVE PULSA THREE*/
    public int active_three_5;
    public int active_three_10;
    public int active_three_20;
    public int active_three_50;
    public int active_three_100;

    /**
     * BILLER ID
     */
    /*LIST BILLER ID PULSA PASCA*/
    private String idPulsaPasca_Axis = "394";
    private String idPulsaPasca_Indosat = "11";
    private String idPulsaPasca_XL = "12";
    private String idPulsaPasca_Telkomsel = "10";
    private String idPulsaPasca_Smartfren = "928";
    private String idPulsaPasca_Three = "45";

    /*LIST BILLER ID PULSA PRE AXIS*/
    private String idPulsaPre_Axis_5 = "34";
    private String idPulsaPre_Axis_10 = "35";
    private String idPulsaPre_Axis_15 = "36";
    private String idPulsaPre_Axis_25 = "37";
    private String idPulsaPre_Axis_50 = "38";
    private String idPulsaPre_Axis_100 = "39";

    /*LIST BILLER ID PULSA PRE INDOSAT*/
    private String idPulsaPre_Indosat_5 = "18";
    private String idPulsaPre_Indosat_10 = "19";
    private String idPulsaPre_Indosat_25 = "20";
    private String idPulsaPre_Indosat_50 = "21";
    private String idPulsaPre_Indosat_100 = "22";

    /*LIST BILLER ID PULSA PRE XL*/
    private String idPulsaPre_XL_5 = "23";
    private String idPulsaPre_XL_10 = "24";
    private String idPulsaPre_XL_25 = "25";
    private String idPulsaPre_XL_50 = "26";
    private String idPulsaPre_XL_100 = "27";

    /*LIST BILLER ID PULSA PRE TELKOMSEL*/
    private String idPulsaPre_Telkomsel_5 = "13";
    private String idPulsaPre_Telkomsel_10 = "14";
    private String idPulsaPre_Telkomsel_25 = "15";
    private String idPulsaPre_Telkomsel_50 = "16";
    private String idPulsaPre_Telkomsel_100 = "17";

    /*LIST BILLER ID PULSA PRE SMARTFREN*/
    private String idPulsaPre_Smartfren_5 = "28";
    private String idPulsaPre_Smartfren_10 = "29";
    private String idPulsaPre_Smartfren_20 = "30";
    private String idPulsaPre_Smartfren_25 = "31";
    private String idPulsaPre_Smartfren_50 = "32";
    private String idPulsaPre_Smartfren_100 = "33";

    /*LIST BILLER ID PULSA PRE THREE*/
    private String idPulsaPre_Three_5 = "40";
    private String idPulsaPre_Three_10 = "41";
    private String idPulsaPre_Three_20 = "42";
    private String idPulsaPre_Three_50 = "43";
    private String idPulsaPre_Three_100 = "44";

    /**
     * NOMINAL / PRODUCT VALUE
     */
    /*LIST FEE PULSA AXIS*/
    public int pv_Axis_5 = 0;
    public int pv_Axis_10 = 0;
    public int pv_Axis_15 = 0;
    public int pv_Axis_25 = 0;
    public int pv_Axis_50 = 0;
    public int pv_Axis_100 = 0;

    /*LIST FEE PULSA INDOSAT*/
    public int pv_Indosat_5 = 0;
    public int pv_Indosat_10 = 0;
    public int pv_Indosat_25 = 0;
    public int pv_Indosat_50 = 0;
    public int pv_Indosat_100 = 0;

    /*LIST FEE PULSA XL*/
    public int pv_XL_5 = 0;
    public int pv_XL_10 = 0;
    public int pv_XL_25 = 0;
    public int pv_XL_50 = 0;
    public int pv_XL_100 = 0;

    /*LIST FEE PULSA TELKOMSEL*/
    public int pv_Telkomsel_5 = 0;
    public int pv_Telkomsel_10 = 0;
    public int pv_Telkomsel_25 = 0;
    public int pv_Telkomsel_50 = 0;
    public int pv_Telkomsel_100 = 0;

    /*LIST FEE PULSA SMARTFREN*/
    public int pv_Smartfren_5 = 0;
    public int pv_Smartfren_10 = 0;
    public int pv_Smartfren_20 = 0;
    public int pv_Smartfren_25 = 0;
    public int pv_Smartfren_50 = 0;
    public int pv_Smartfren_100 = 0;

    /*LIST FEE PULSA THREE*/
    public int pv_three_5 = 0;
    public int pv_three_10 = 0;
    public int pv_three_20 = 0;
    public int pv_three_50 = 0;
    public int pv_three_100 = 0;


    /*LIST NOMINAL*/
    /*private int limaRb = 5000;
    private int sepuluhRb = 10000;
    private int limaBelasRb = 15000;
    private int duaPuluhRb = 20000;
    private int duaLimaRb = 25000;
    private int limaPuluhRb = 50000;
    private int seratusRb = 100000;*/

    /*CHOOSED NOMINAL BY SPINNER*/
    private int choosedNominalAxis = pv_Axis_5 /*limaRb*/; //inisiate default spinner data
    private int choosedNominalIndosat = pv_Indosat_5 /*limaRb*/; //inisiate default spinner data
    private int choosedNominalTsel = pv_Telkomsel_5 /*limaRb*/; //inisiate default spinner data
    private int choosedNominalXl = pv_XL_5 /*limaRb*/; //inisiate default spinner data
    private int choosedNominalSmart = pv_Smartfren_5 /*limaRb*/; //inisiate default spinner data
    private int choosedNominalThree = pv_three_5 /*limaRb*/; //inisiate default spinner data

    private int choosedPriceAxis = price_Axis_5; //inisiate default spinner data
    private int choosedPriceIndosat = price_Indosat_5; //inisiate default spinner data
    private int choosedPriceTsel = price_Telkomsel_5; //inisiate default spinner data
    private int choosedPriceXl = price_XL_5; //inisiate default spinner data
    private int choosedPriceSmart = price_Smartfren_5; //inisiate default spinner data
    private int choosedPriceThree = price_three_5; //inisiate default spinner data

    private int choosedFeeAxis = fee_Axis_5; //inisiate default spinner data
    private int choosedFeeIndosat = fee_Indosat_5; //inisiate default spinner data
    private int choosedFeeTsel = fee_Telkomsel_5; //inisiate default spinner data
    private int choosedFeeXl = fee_XL_5; //inisiate default spinner data
    private int choosedFeeSmart = fee_Smartfren_5; //inisiate default spinner data
    private int choosedFeeThree = fee_three_5; //inisiate default spinner data


    public int choosedNominal = 0; //inisiate default spinner data
    public String billerId = ""; //inisiate default spinner data

    private String operator = ""; //inisiate default spinner data
    public String praPascaBayar = "prabayar";
    public String price = "";
    public String fee = "";
    public String totalAmount = "";
    private boolean isChecked = false;
    private boolean isChoosed = false;
    private boolean isFav = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mValidator = new Validator(this);
        mPresenter = new BillerPulsaPresenter(this);
        mValidator.setValidationListener(this);
        PrefHelper.setBoolean(PrefKey.IS_FAV, false);
    }

    public static void showFragment(BaseActivity sourceFragment) {
        BillerPulsaFragment fragment = new BillerPulsaFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
//        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_biller_pulsa;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cbFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isFav = true;
                } else {
                    isFav = false;
                }
                Log.d(TAG, "onCheckedChanged: " + isFav);
            }
        });
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
        rl_all.setVisibility(View.GONE);
        mPresenter.getInisiateData();

//        etIdPelPasca.setVisibility(View.GONE);

        etIdPelPasca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etIdPelPasca.getText().toString().startsWith("0831")
                        || etIdPelPasca.getText().toString().startsWith("0832")
                        || etIdPelPasca.getText().toString().startsWith("0838")
                        || etIdPelPasca.getText().toString().startsWith("0833")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_axis);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_axis";
                    billerId = idPulsaPasca_Axis;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                } else if (etIdPelPasca.getText().toString().startsWith("0855")
                        || etIdPelPasca.getText().toString().startsWith("0856")
                        || etIdPelPasca.getText().toString().startsWith("0857")
                        || etIdPelPasca.getText().toString().startsWith("0858")
                        || etIdPelPasca.getText().toString().startsWith("0814")
                        || etIdPelPasca.getText().toString().startsWith("0815")
                        || etIdPelPasca.getText().toString().startsWith("0816")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_indosat_96px);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_indosat";
                    billerId = idPulsaPasca_Indosat;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                }   else if (etIdPelPasca.getText().toString().startsWith("0821")
                        || etIdPelPasca.getText().toString().startsWith("0822")
                        || etIdPelPasca.getText().toString().startsWith("0812")
                        || etIdPelPasca.getText().toString().startsWith("0813")
                        || etIdPelPasca.getText().toString().startsWith("0811")
                        || etIdPelPasca.getText().toString().startsWith("0852")
                        || etIdPelPasca.getText().toString().startsWith("0823")
                        || etIdPelPasca.getText().toString().startsWith("0853")
                        || etIdPelPasca.getText().toString().startsWith("0851")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_telkomsel_96px);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_tsel";
                    billerId = idPulsaPasca_Telkomsel;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                } else if (etIdPelPasca.getText().toString().startsWith("0878")
                        || etIdPelPasca.getText().toString().startsWith("0817")
                        || etIdPelPasca.getText().toString().startsWith("0818")
                        || etIdPelPasca.getText().toString().startsWith("0819")
                        || etIdPelPasca.getText().toString().startsWith("0859")
                        || etIdPelPasca.getText().toString().startsWith("0877")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_xl_96px);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_xl";
                    billerId = idPulsaPasca_XL;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                } else if (etIdPelPasca.getText().toString().startsWith("0881")
                        || etIdPelPasca.getText().toString().startsWith("0882")
                        || etIdPelPasca.getText().toString().startsWith("0883")
                        || etIdPelPasca.getText().toString().startsWith("0884")
                        || etIdPelPasca.getText().toString().startsWith("0885")
                        || etIdPelPasca.getText().toString().startsWith("0886")
                        || etIdPelPasca.getText().toString().startsWith("0887")
                        || etIdPelPasca.getText().toString().startsWith("0888")
                        || etIdPelPasca.getText().toString().startsWith("0889")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_smartfren);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_smart";
                    billerId = idPulsaPasca_Smartfren;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                } else if (etIdPelPasca.getText().toString().startsWith("0895")
                        || etIdPelPasca.getText().toString().startsWith("0896")
                        || etIdPelPasca.getText().toString().startsWith("0897")
                        || etIdPelPasca.getText().toString().startsWith("0898")
                        || etIdPelPasca.getText().toString().startsWith("0899")) {
                    Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_three);
                    ivLogoOperator.setImageDrawable(dr);
                    llLogoOperator.setVisibility(View.VISIBLE);
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    operator = "pasca_three";
                    billerId = idPulsaPasca_Three;
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                } else {
                    llLogoOperator.setVisibility(View.GONE);
                    billerId = "";
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                    operator = "";
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etIdPel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    if (etIdPel.getText().toString().equalsIgnoreCase("0856")){

                if (etIdPel.getText().toString().startsWith("0831")
                        || etIdPel.getText().toString().startsWith("0832")
                        || etIdPel.getText().toString().startsWith("0838")
                        || etIdPel.getText().toString().startsWith("0833")) {

                    if (active_Axis_5 == -1 ||
                            active_Axis_15 == -1 ||
                            active_Axis_25 == -1 ||
                            active_Axis_50 == -1 ||
                            active_Axis_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_axis);
                        ivLogoOperator.setImageDrawable(dr);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "axis";
                        /*choosedNominalAxis = limaRb;
                        choosedPriceAxis = price_Axis_5;
                        choosedFeeAxis = fee_Axis_5;*/
                        choosedNominalAxis = 0;
                        choosedPriceAxis = 0;
                        choosedFeeAxis = 0;
                        choosedNominal = choosedNominalAxis;
                        tvNominalAxis.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceAxis));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeAxis));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceAxis + choosedFeeAxis));
//                        billerId = idPulsaPre_Axis_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalAxis.setVisibility(View.VISIBLE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }
                } else if (etIdPel.getText().toString().startsWith("0821")
                        || etIdPel.getText().toString().startsWith("0822")
                        || etIdPel.getText().toString().startsWith("0812")
                        || etIdPel.getText().toString().startsWith("0813")
                        || etIdPel.getText().toString().startsWith("0811")
                        || etIdPel.getText().toString().startsWith("0852")
                        || etIdPel.getText().toString().startsWith("0823")
                        || etIdPel.getText().toString().startsWith("0853")
                        || etIdPel.getText().toString().startsWith("0851")) {

                    if (active_Telkomsel_5 == -1 ||
                            active_Telkomsel_10 == -1 ||
                            active_Telkomsel_25 == -1 ||
                            active_Telkomsel_50 == -1 ||
                            active_Telkomsel_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_telkomsel_96px);
                        ivLogoOperator.setImageDrawable(dr);
//                    ivLogoOperator.setBackgroundResource(R.drawable.ic_pulsa_telkomsel_96px);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "tsel";
                        /*choosedNominalTsel = limaRb;
                        choosedPriceTsel = price_Telkomsel_5;
                        choosedFeeTsel = fee_Telkomsel_5;*/
                        choosedNominalTsel = 0;
                        choosedPriceTsel = 0;
                        choosedFeeTsel = 0;
                        choosedNominal = 0;
                        tvNominalTsel.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceTsel));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeTsel));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceTsel + choosedFeeTsel));
//                        billerId = idPulsaPre_Telkomsel_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalTsel.setVisibility(View.VISIBLE);
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);

                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }

                } else if (etIdPel.getText().toString().startsWith("0878")
                        || etIdPel.getText().toString().startsWith("0817")
                        || etIdPel.getText().toString().startsWith("0818")
                        || etIdPel.getText().toString().startsWith("0819")
                        || etIdPel.getText().toString().startsWith("0859")
                        || etIdPel.getText().toString().startsWith("0877")) {

                    if (active_XL_5 == -1 ||
                            active_XL_10 == -1 ||
                            active_XL_25 == -1 ||
                            active_XL_50 == -1 ||
                            active_XL_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_xl_96px);
                        ivLogoOperator.setImageDrawable(dr);
//                    ivLogoOperator.setBackgroundResource(R.drawable.ic_pulsa_xl_96px);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "xl";
                        /*choosedNominalXl = limaRb;
                        choosedPriceXl = price_XL_5;
                        choosedFeeXl = fee_XL_5;*/
                        choosedNominalXl = 0;
                        choosedPriceXl = 0;
                        choosedFeeXl = 0;
                        choosedNominal = choosedNominalXl;
                        tvNominalXl.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceXl));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeXl));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceXl + choosedFeeXl));
//                        billerId = idPulsaPre_XL_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalXl.setVisibility(View.VISIBLE);
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }
                } else if (etIdPel.getText().toString().startsWith("0881")
                        || etIdPel.getText().toString().startsWith("0882")
                        || etIdPel.getText().toString().startsWith("0883")
                        || etIdPel.getText().toString().startsWith("0884")
                        || etIdPel.getText().toString().startsWith("0885")
                        || etIdPel.getText().toString().startsWith("0886")
                        || etIdPel.getText().toString().startsWith("0887")
                        || etIdPel.getText().toString().startsWith("0888")
                        || etIdPel.getText().toString().startsWith("0889")) {

                    if (active_Smartfren_5 == -1 ||
                            active_Smartfren_10 == -1 ||
                            active_Smartfren_20 == -1 ||
                            active_Smartfren_25 == -1 ||
                            active_Smartfren_50 == -1 ||
                            active_Smartfren_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_smartfren);
                        ivLogoOperator.setImageDrawable(dr);
//                    ivLogoOperator.setBackgroundResource(R.drawable.ic_pulsa_xl_96px);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "smartfren";
                        /*choosedNominalSmart = limaRb;
                        choosedPriceSmart = price_Smartfren_5;
                        choosedFeeSmart = fee_Smartfren_5;*/
                        choosedNominalSmart = 0;
                        choosedPriceSmart = 0;
                        choosedFeeSmart = 0;
                        choosedNominal = choosedNominalSmart;
                        tvNominalSmart.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceSmart));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeSmart));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceSmart + choosedFeeSmart));
//                        billerId = idPulsaPre_Smartfren_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalSmart.setVisibility(View.VISIBLE);
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }
                } else if (etIdPel.getText().toString().startsWith("0855")
                        || etIdPel.getText().toString().startsWith("0856")
                        || etIdPel.getText().toString().startsWith("0857")
                        || etIdPel.getText().toString().startsWith("0858")
                        || etIdPel.getText().toString().startsWith("0814")
                        || etIdPel.getText().toString().startsWith("0815")
                        || etIdPel.getText().toString().startsWith("0816")) {

                    if (active_Indosat_5 == -1 ||
                            active_Indosat_10 == -1 ||
                            active_Indosat_25 == -1 ||
                            active_Indosat_50 == -1 ||
                            active_Indosat_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_pulsa_indosat_96px);
                        ivLogoOperator.setImageDrawable(dr);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "indosat";
                        /*choosedNominalIndosat = limaRb;
                        choosedPriceIndosat = price_Indosat_5;
                        choosedFeeIndosat = fee_Indosat_5;*/
                        choosedNominalIndosat = 0;
                        choosedPriceIndosat = 0;
                        choosedFeeIndosat = 0;
                        choosedNominal = choosedNominalIndosat;
                        tvNominalIndosat.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceIndosat));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeIndosat));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceIndosat + choosedFeeIndosat));
//                        billerId = idPulsaPre_Indosat_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalIndosat.setVisibility(View.VISIBLE);
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }
                } else if (etIdPel.getText().toString().startsWith("0895")
                        || etIdPel.getText().toString().startsWith("0896")
                        || etIdPel.getText().toString().startsWith("0897")
                        || etIdPel.getText().toString().startsWith("0898")
                        || etIdPel.getText().toString().startsWith("0899")) {

                    if (active_three_5 == -1 ||
                            active_three_10 == -1 ||
                            active_three_20 == -1 ||
                            active_three_50 == -1 ||
                            active_three_100 == -1) {
                        Drawable dr = ContextCompat.getDrawable(getActivity(), R.drawable.ic_three);
                        ivLogoOperator.setImageDrawable(dr);
//                    ivLogoOperator.setBackgroundResource(R.drawable.ic_pulsa_xl_96px);
                        llLogoOperator.setVisibility(View.VISIBLE);
                        operator = "three";
                        /*choosedNominalXl = limaRb;
                        choosedPriceXl = price_XL_5;
                        choosedFeeXl = fee_XL_5;*/
                        choosedNominalThree = 0;
                        choosedPriceThree = 0;
                        choosedFeeThree = 0;
                        choosedNominal = choosedNominalThree;
                        tvNominalThree.setText("Rp " + AmountFormatter2.format(choosedNominal));
                        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceThree));
                        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeThree));
                        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceXl + choosedFeeXl));
//                        billerId = idPulsaPre_XL_5;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        llNominalThree.setVisibility(View.VISIBLE);
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                    } else {
                        llLogoOperator.setVisibility(View.GONE);
                        choosedNominal = 0;
                        billerId = "";
                        isChoosed = false;
                        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                        operator = "";
                        llNominalAxis.setVisibility(View.GONE);
                        llNominalIndosat.setVisibility(View.GONE);
                        llNominalTsel.setVisibility(View.GONE);
                        llNominalXl.setVisibility(View.GONE);
                        llNominalSmart.setVisibility(View.GONE);
                        llNominalThree.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void inisiateHit() {
        llIdPelPasca.setVisibility(View.GONE);
        llLogoOperator.setVisibility(View.GONE);
        llCekTagihan.setVisibility(View.GONE);
        llTagihan.setVisibility(View.GONE);
        tvNominalAxis.setText("Rp " + AmountFormatter2.format(choosedNominalAxis));
        tvNominalIndosat.setText("Rp " + AmountFormatter2.format(choosedNominalIndosat));
        tvNominalTsel.setText("Rp " + AmountFormatter2.format(choosedNominalTsel));
        tvNominalXl.setText("Rp " + AmountFormatter2.format(choosedNominalXl));
        tvNominalSmart.setText("Rp " + AmountFormatter2.format(choosedNominalSmart));
        tvNominalThree.setText("Rp " + AmountFormatter2.format(choosedNominalThree));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedNominal));
        tvFee.setText("Rp " + AmountFormatter2.format(0));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedNominal + 0));
        rl_all.setVisibility(View.VISIBLE);
    }

    public void afterHit() {
        tvTagihan.setText("Rp " + AmountFormatter2.format(Double.parseDouble(price)));
        tvHarga.setText("Rp " + AmountFormatter2.format(Double.parseDouble(price)));
        tvFee.setText("Rp " + AmountFormatter2.format(Double.parseDouble(fee)));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(Double.parseDouble(totalAmount)));
        llTagihan.setVisibility(View.VISIBLE);
        isChecked = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
    }

    @OnClick(R.id.btn_list_fav)
    public void getFav() {
        PrefHelper.setInt(PrefKey.CATEGORY, 2); //PLN=1 PULSA=2
        BillerFavoritePulsaDialogFragment.showFragment(this);
    }

    public void afterFav() {
        if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
            etIdPelPasca.setText(PrefHelper.getString(PrefKey.CONTRACT_NO_FAV));
        } else {
            etIdPel.setText(PrefHelper.getString(PrefKey.CONTRACT_NO_FAV));
        }
    }

    @OnClick(R.id.btnTagihan)
    public void doCheck() {
        mValidator.validate();

//        mPresenter.getCheckBiller();
    }

    @OnClick(R.id.mycart_btn_checkout)
    public void doBayar() {
        if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
            if (isChecked) {
                PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
                PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPelPasca.getText().toString());
                PrefHelper.setString(PrefKey.BILLER_ID, billerId);
                PrefHelper.setBoolean(PrefKey.IS_FAV, isFav);
                isChecked = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                Log.d(TAG, "doBayar: " + isFav);
            }
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")) {
            if (isChoosed) {
                mValidator.validate();
                isChoosed = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
            }
        }

//        mValidator.validate();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
            llTagihan.setVisibility(View.GONE);
        }
        setNol();
        tvNominalAxis.setText("Rp " + AmountFormatter2.format(choosedNominalAxis));
        tvNominalIndosat.setText("Rp " + AmountFormatter2.format(choosedNominalIndosat));
        tvNominalTsel.setText("Rp " + AmountFormatter2.format(choosedNominalTsel));
        tvNominalXl.setText("Rp " + AmountFormatter2.format(choosedNominalXl));
        tvNominalSmart.setText("Rp " + AmountFormatter2.format(choosedNominalSmart));
        tvNominalThree.setText("Rp " + AmountFormatter2.format(choosedNominalThree));
        tvHarga.setText("Rp " + AmountFormatter2.format(price));
        tvFee.setText("Rp " + AmountFormatter2.format(fee));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(totalAmount));
        isChecked = false;
        isChoosed = false;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
        cbFav.setChecked(false);
        isFav = false;
    }

    private void setNol() {
        choosedNominalAxis = 0;
        choosedNominalIndosat = 0;
        choosedNominalTsel = 0;
        choosedNominalXl = 0;
        choosedNominalSmart = 0;
        choosedNominalThree = 0;
        choosedPriceAxis = 0;
        choosedPriceIndosat = 0;
        choosedPriceTsel = 0;
        choosedPriceXl = 0;
        choosedPriceSmart = 0;
        choosedPriceThree = 0;
        choosedFeeAxis = 0;
        choosedFeeIndosat = 0;
        choosedFeeTsel = 0;
        choosedFeeXl = 0;
        choosedFeeSmart = 0;
        choosedFeeThree = 0;
        price = "0";
        fee = "0";
        totalAmount = "0";
    }

    @Override
    public void onValidationSucceeded() {
        if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
            mPresenter.getCheckBiller();
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")) {
            PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
            PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
            PrefHelper.setString(PrefKey.BILLER_ID, billerId);
            PrefHelper.setBoolean(PrefKey.IS_FAV, isFav);
            Log.d(TAG, "onValidationSucceeded: " + isFav);
        }

        /*if (praPascaBayar.equalsIgnoreCase("pascabayar")){
            if (isChecked) {
                PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
                PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPelPasca.getText().toString());
                PrefHelper.setString(PrefKey.BILLER_ID, billerId);
                isChecked = false;
            }
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")){
            PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
            PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
            PrefHelper.setString(PrefKey.BILLER_ID, billerId);
        }*/
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.llPilihan)
    public void showDialogPilihan() {
        dialogPilihan();
    }

    @OnClick(R.id.llNominalAxis)
    public void showDialogNominalAxis() {
        if (active_Axis_5 != -1 &&
                active_Axis_15 != -1 &&
                active_Axis_25 != -1 &&
                active_Axis_50 != -1 &&
                active_Axis_100 != -1) {
            //do nothing
        } else {
            dialogNominalAxis();
        }
    }

    @OnClick(R.id.llNominalIndosat)
    public void showDialogNominalIndosat() {
        if (active_Indosat_5 != -1 &&
                active_Indosat_10 != -1 &&
                active_Indosat_25 != -1 &&
                active_Indosat_50 != -1 &&
                active_Indosat_100 != -1) {
            //do nothing
        } else {
            dialogNominalIndosat();
        }
    }

    @OnClick(R.id.llNominalTsel)
    public void showDialogNominalTsel() {
        if (active_Telkomsel_5 != -1 &&
                active_Telkomsel_10 != -1 &&
                active_Telkomsel_25 != -1 &&
                active_Telkomsel_50 != -1 &&
                active_Telkomsel_100 != -1) {
            //do nothing
        } else {
            dialogNominalTsel();
        }
    }

    @OnClick(R.id.llNominalXl)
    public void showDialogNominalXl() {
        if (active_XL_5 != -1 &&
                active_XL_10 != -1 &&
                active_XL_25 != -1 &&
                active_XL_50 != -1 &&
                active_XL_100 != -1) {
            //do nothing
        } else {
            dialogNominalXl();
        }
    }

    @OnClick(R.id.llNominalSmart)
    public void showDialogNominalSmart() {
        if (active_Smartfren_5 != -1 &&
                active_Smartfren_10 != -1 &&
                active_Smartfren_20 != -1 &&
                active_Smartfren_25 != -1 &&
                active_Smartfren_50 != -1 &&
                active_Smartfren_100 != -1) {
            //do nothing
        } else {
            dialogNominalSmart();
        }
    }

    @OnClick(R.id.llNominalThree)
    public void showDialogNominalThree() {
        if (active_three_5 != -1 &&
                active_three_10 != -1 &&
                active_three_20 != -1 &&
                active_three_50 != -1 &&
                active_three_100 != -1) {
            //do nothing
        } else {
            dialogNominalThree();
        }
    }

    private void dialogPilihan() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_pilihan);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btnToken = (LinearLayout) dialog.findViewById(R.id.llToken);
        LinearLayout btnTagihan = (LinearLayout) dialog.findViewById(R.id.llTagihan);
        TextView tvPra = (TextView) dialog.findViewById(R.id.tv1);
        TextView tvPasca = (TextView) dialog.findViewById(R.id.tv2);
        tvPra.setText(R.string.prabayar);
        tvPasca.setText(R.string.pascabayar);
        btnToken.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                tvPilihan.setText(R.string.prabayar);
//                llNominal.setVisibility(View.VISIBLE);
                tvHarga.setText("Rp " + AmountFormatter2.format(0));
                tvFee.setText("Rp " + AmountFormatter2.format(0));
                mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                billerId = "";
                isChoosed = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                praPascaBayar = "prabayar";
                etIdPel.setText("");
                etIdPelPasca.setText("");
                llIdPelPasca.setVisibility(View.GONE);
                llIdPel.setVisibility(View.VISIBLE);
                llCekTagihan.setVisibility(View.GONE);
                llTagihan.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
        btnTagihan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                tvPilihan.setText(R.string.pascabayar);
//                llNominal.setVisibility(View.GONE);
//                choosedNominal = 0;
                choosedNominalAxis = pv_Axis_5 /*limaRb*/;
                choosedNominalIndosat = pv_Indosat_5 /*limaRb*/;
                choosedNominalTsel = pv_Telkomsel_5 /*limaRb*/;
                choosedNominalXl = pv_XL_5 /*limaRb*/;
                choosedNominalSmart = pv_Smartfren_5 /*limaRb*/;
                choosedNominalThree = pv_three_5 /*limaRb*/;
                choosedPriceAxis = price_Axis_5;
                choosedPriceIndosat = price_Indosat_5;
                choosedPriceTsel = price_Telkomsel_5;
                choosedPriceXl = price_XL_5;
                choosedPriceSmart = price_Smartfren_5;
                choosedPriceThree = price_three_5;
                choosedFeeAxis = fee_Axis_5;
                choosedFeeIndosat = fee_Indosat_5;
                choosedFeeTsel = fee_Telkomsel_5;
                choosedFeeXl = fee_XL_5;
                choosedFeeSmart = fee_Smartfren_5;
                choosedFeeThree = fee_three_5;
                tvNominalAxis.setText("Rp " + AmountFormatter2.format(choosedNominalAxis));
                tvNominalIndosat.setText("Rp " + AmountFormatter2.format(choosedNominalIndosat));
                tvNominalTsel.setText("Rp " + AmountFormatter2.format(choosedNominalTsel));
                tvNominalXl.setText("Rp " + AmountFormatter2.format(choosedNominalXl));
                tvNominalSmart.setText("Rp " + AmountFormatter2.format(choosedNominalSmart));
                tvNominalThree.setText("Rp " + AmountFormatter2.format(choosedNominalThree));
                tvHarga.setText("Rp " + AmountFormatter2.format(0));
                tvFee.setText("Rp " + AmountFormatter2.format(0));
                mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                billerId = "";
                praPascaBayar = "pascabayar";
                etIdPel.setText("");
                etIdPelPasca.setText("");
                llIdPelPasca.setVisibility(View.VISIBLE);
                llIdPel.setVisibility(View.GONE);
                llCekTagihan.setVisibility(View.VISIBLE);
                llTagihan.setVisibility(View.GONE);
                llNominalAxis.setVisibility(View.GONE);
                llNominalIndosat.setVisibility(View.GONE);
                llNominalSmart.setVisibility(View.GONE);
                llNominalTsel.setVisibility(View.GONE);
                llNominalThree.setVisibility(View.GONE);
                llNominalXl.setVisibility(View.GONE);
                //test
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominalAxis() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn15rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn25rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll6);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        TextView tv6 = (TextView) dialog.findViewById(R.id.nom6);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp15);
        tv4.setText(R.string.rp25);
        tv5.setText(R.string.rp50);
        tv6.setText(R.string.rp100);
        if (active_Axis_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_Axis_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_Axis_15 == -1) {
            btn15rb.setVisibility(View.VISIBLE);
        } else {
            btn15rb.setVisibility(View.GONE);
        }
        if (active_Axis_25 == -1) {
            btn25rb.setVisibility(View.VISIBLE);
        } else {
            btn25rb.setVisibility(View.GONE);
        }
        if (active_Axis_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_Axis_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_5/*limaRb*/, price_Axis_5, fee_Axis_5, idPulsaPre_Axis_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_10/*sepuluhRb*/, price_Axis_10, fee_Axis_10, idPulsaPre_Axis_10);
                dialog.dismiss();
            }
        });
        btn15rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_15/*limaBelasRb*/, price_Axis_15, fee_Axis_15, idPulsaPre_Axis_15);
                dialog.dismiss();
            }
        });
        btn25rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_25/*duaLimaRb*/, price_Axis_25, fee_Axis_25, idPulsaPre_Axis_25);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_50/*limaPuluhRb*/, price_Axis_50, fee_Axis_50, idPulsaPre_Axis_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerAxis(pv_Axis_100/*seratusRb*/, price_Axis_100, fee_Axis_100, idPulsaPre_Axis_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominalIndosat() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn25rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btnLebih = (LinearLayout) dialog.findViewById(R.id.ll6);
        btnLebih.setVisibility(View.GONE);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp25);
        tv4.setText(R.string.rp50);
        tv5.setText(R.string.rp100);
        if (active_Indosat_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_Indosat_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_Indosat_25 == -1) {
            btn25rb.setVisibility(View.VISIBLE);
        } else {
            btn25rb.setVisibility(View.GONE);
        }
        if (active_Indosat_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_Indosat_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerIndosat(pv_Indosat_5/*limaRb*/, price_Indosat_5, fee_Indosat_5, idPulsaPre_Indosat_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerIndosat(pv_Indosat_10/*sepuluhRb*/, price_Indosat_10, fee_Indosat_10, idPulsaPre_Indosat_10);
                dialog.dismiss();
            }
        });
        btn25rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerIndosat(pv_Indosat_25/*duaLimaRb*/, price_Indosat_25, fee_Indosat_25, idPulsaPre_Indosat_25);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerIndosat(pv_Indosat_50/*limaPuluhRb*/, price_Indosat_50, fee_Indosat_50, idPulsaPre_Indosat_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerIndosat(pv_Indosat_100/*seratusRb*/, price_Indosat_100, fee_Indosat_100, idPulsaPre_Indosat_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominalTsel() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn25rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btnLebih = (LinearLayout) dialog.findViewById(R.id.ll6);
        btnLebih.setVisibility(View.GONE);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp25);
        tv4.setText(R.string.rp50);
        tv5.setText(R.string.rp100);
        if (active_Telkomsel_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_Telkomsel_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_Telkomsel_25 == -1) {
            btn25rb.setVisibility(View.VISIBLE);
        } else {
            btn25rb.setVisibility(View.GONE);
        }
        if (active_Telkomsel_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_Telkomsel_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerTsel(pv_Telkomsel_5/*limaRb*/, price_Telkomsel_5, fee_Telkomsel_5, idPulsaPre_Telkomsel_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerTsel(pv_Telkomsel_10/*sepuluhRb*/, price_Telkomsel_10, fee_Telkomsel_10, idPulsaPre_Telkomsel_10);
                dialog.dismiss();
            }
        });
        btn25rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerTsel(pv_Telkomsel_25/*duaLimaRb*/, price_Telkomsel_25, fee_Telkomsel_25, idPulsaPre_Telkomsel_25);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerTsel(pv_Telkomsel_50/*limaPuluhRb*/, price_Telkomsel_50, fee_Telkomsel_50, idPulsaPre_Telkomsel_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerTsel(pv_Telkomsel_100/*seratusRb*/, price_Telkomsel_100, fee_Telkomsel_100, idPulsaPre_Telkomsel_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominalXl() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn25rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btnLebih = (LinearLayout) dialog.findViewById(R.id.ll6);
        btnLebih.setVisibility(View.GONE);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp25);
        tv4.setText(R.string.rp50);
        tv5.setText(R.string.rp100);
        if (active_XL_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_XL_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_XL_25 == -1) {
            btn25rb.setVisibility(View.VISIBLE);
        } else {
            btn25rb.setVisibility(View.GONE);
        }
        if (active_XL_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_XL_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerXL(pv_XL_5/*limaRb*/, price_XL_5, fee_XL_5, idPulsaPre_XL_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerXL(pv_XL_10/*sepuluhRb*/, price_XL_10, fee_XL_10, idPulsaPre_XL_10);
                dialog.dismiss();
            }
        });
        btn25rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerXL(pv_XL_25/*duaLimaRb*/, price_XL_25, fee_XL_25, idPulsaPre_XL_25);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerXL(pv_XL_50/*limaPuluhRb*/, price_XL_50, fee_XL_50, idPulsaPre_XL_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerXL(pv_XL_100/*seratusRb*/, price_XL_100, fee_XL_100, idPulsaPre_XL_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominalSmart() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn20rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn25rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll6);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        TextView tv6 = (TextView) dialog.findViewById(R.id.nom6);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp20);
        tv4.setText(R.string.rp25);
        tv5.setText(R.string.rp50);
        tv6.setText(R.string.rp100);
        if (active_Smartfren_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_Smartfren_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_Smartfren_20 == -1) {
            btn20rb.setVisibility(View.VISIBLE);
        } else {
            btn20rb.setVisibility(View.GONE);
        }
        if (active_Smartfren_25 == -1) {
            btn25rb.setVisibility(View.VISIBLE);
        } else {
            btn25rb.setVisibility(View.GONE);
        }
        if (active_Smartfren_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_Smartfren_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_5/*limaRb*/, price_Smartfren_5, fee_Smartfren_5, idPulsaPre_Smartfren_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_10/*sepuluhRb*/, price_Smartfren_10, fee_Smartfren_10, idPulsaPre_Smartfren_10);
                dialog.dismiss();
            }
        });
        btn20rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_20/*duaPuluhRb*/, price_Smartfren_20, fee_Smartfren_20, idPulsaPre_Smartfren_20);
                dialog.dismiss();
            }
        });
        btn25rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_25/*duaLimaRb*/, price_Smartfren_25, fee_Smartfren_25, idPulsaPre_Smartfren_25);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_50/*limaPuluhRb*/, price_Smartfren_50, fee_Smartfren_50, idPulsaPre_Smartfren_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerSmart(pv_Smartfren_100/*seratusRb*/, price_Smartfren_100, fee_Smartfren_100, idPulsaPre_Smartfren_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void dialogNominalThree() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn5rb = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn10rb = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn20rb = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn50rb = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn100rb = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btnLebih = (LinearLayout) dialog.findViewById(R.id.ll6);
        btnLebih.setVisibility(View.GONE);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        tv1.setText(R.string.rp5);
        tv2.setText(R.string.rp10);
        tv3.setText(R.string.rp20);
        tv4.setText(R.string.rp50);
        tv5.setText(R.string.rp100);
        if (active_three_5 == -1) {
            btn5rb.setVisibility(View.VISIBLE);
        } else {
            btn5rb.setVisibility(View.GONE);
        }
        if (active_three_10 == -1) {
            btn10rb.setVisibility(View.VISIBLE);
        } else {
            btn10rb.setVisibility(View.GONE);
        }
        if (active_three_20 == -1) {
            btn20rb.setVisibility(View.VISIBLE);
        } else {
            btn20rb.setVisibility(View.GONE);
        }
        if (active_three_50 == -1) {
            btn50rb.setVisibility(View.VISIBLE);
        } else {
            btn50rb.setVisibility(View.GONE);
        }
        if (active_three_100 == -1) {
            btn100rb.setVisibility(View.VISIBLE);
        } else {
            btn100rb.setVisibility(View.GONE);
        }
        btn5rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerThree(pv_three_5/*limaRb*/, price_three_5, fee_three_5, idPulsaPre_Three_5);
                dialog.dismiss();
            }
        });
        btn10rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerThree(pv_three_10/*sepuluhRb*/, price_three_10, fee_three_10, idPulsaPre_Three_10);
                dialog.dismiss();
            }
        });
        btn20rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerThree(pv_three_20/*duaPuluhRb*/, price_three_20, fee_three_20, idPulsaPre_Three_20);
                dialog.dismiss();
            }
        });
        btn50rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerThree(pv_three_50/*limaPuluhRb*/, price_three_50, fee_three_50, idPulsaPre_Three_50);
                dialog.dismiss();
            }
        });
        btn100rb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dataSpinnerThree(pv_three_100/*seratusRb*/, price_three_100, fee_three_100, idPulsaPre_Three_100);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dataSpinnerAxis(int nominal, int price, int fee, String biller) {
        choosedNominalAxis = nominal;
        choosedPriceAxis = price;
        choosedFeeAxis = fee;
        tvNominalAxis.setText("Rp " + AmountFormatter2.format(choosedNominalAxis));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceAxis));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeAxis));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceAxis + choosedFeeAxis));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalAxis;
    }

    private void dataSpinnerIndosat(int nominal, int price, int fee, String biller) {
        choosedNominalIndosat = nominal;
        choosedPriceIndosat = price;
        choosedFeeIndosat = fee;
        tvNominalIndosat.setText("Rp " + AmountFormatter2.format(choosedNominalIndosat));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceIndosat));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeIndosat));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceIndosat + choosedFeeIndosat));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalIndosat;
    }

    private void dataSpinnerTsel(int nominal, int price, int fee, String biller) {
        choosedNominalTsel = nominal;
        choosedPriceTsel = price;
        choosedFeeTsel = fee;
        tvNominalTsel.setText("Rp " + AmountFormatter2.format(choosedNominalTsel));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceTsel));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeTsel));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceTsel + choosedFeeTsel));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalTsel;
    }

    private void dataSpinnerXL(int nominal, int price, int fee, String biller) {
        choosedNominalXl = nominal;
        choosedPriceXl = price;
        choosedFeeXl = fee;
        tvNominalXl.setText("Rp " + AmountFormatter2.format(choosedNominalXl));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceXl));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeXl));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceXl + choosedFeeXl));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalXl;
    }

    private void dataSpinnerSmart(int nominal, int price, int fee, String biller) {
        choosedNominalSmart = nominal;
        choosedPriceSmart = price;
        choosedFeeSmart = fee;
        tvNominalSmart.setText("Rp " + AmountFormatter2.format(choosedNominalSmart));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceSmart));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeSmart));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceSmart + choosedFeeSmart));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalSmart;
    }

    private void dataSpinnerThree(int nominal, int price, int fee, String biller) {
        choosedNominalThree = nominal;
        choosedPriceThree = price;
        choosedFeeThree = fee;
        tvNominalThree.setText("Rp " + AmountFormatter2.format(choosedNominalThree));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPriceThree));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFeeThree));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedPriceThree + choosedFeeThree));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
        choosedNominal = choosedNominalThree;
    }
}
