package com.ultravoucher.uv.ui.fragments.myvoucher;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.DealsDetailFisikMsgResp;
import com.ultravoucher.uv.data.api.request.DigitalPocketReq;
import com.ultravoucher.uv.data.api.response.DealsDetailFisikResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/26/2017.
 */

public class ListMyVoucherFisikPresenter {

    private ListMyVoucherFisikFragment mFragment;

    public ListMyVoucherFisikPresenter(ListMyVoucherFisikFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetVoucherFisikList(DigitalPocketReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailFisikResponse> dealsDetailFisikResponseCall = api.getListMyVoucherFisik(contentType, authToken, dui, req);
        dealsDetailFisikResponseCall.enqueue(new Callback<DealsDetailFisikResponse>() {
            @Override
            public void onResponse(Call<DealsDetailFisikResponse> call, Response<DealsDetailFisikResponse> response) {

                if (response.code() == 200) {
                    DealsDetailFisikResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            initList(resp.getDeals());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
//                            mFragment.getActivity().finish();
//                            mFragment.doNeedRelogin();

                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }

                    mFragment.swipe_container.setRefreshing(false);

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }

                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<DealsDetailFisikResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }

    protected void presentGetVoucherFisikListLoadMore(DigitalPocketReq req) {

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<DealsDetailFisikResponse> dealsDetailFisikResponseCall = api.getListMyVoucherFisik(contentType, authToken, dui, req);
        dealsDetailFisikResponseCall.enqueue(new Callback<DealsDetailFisikResponse>() {
            @Override
            public void onResponse(Call<DealsDetailFisikResponse> call, Response<DealsDetailFisikResponse> response) {

                if (response.code() == 200) {
                    DealsDetailFisikResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getDeals().isEmpty()) {
                            addToList(resp.getDeals());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mList.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
            }

            @Override
            public void onFailure(Call<DealsDetailFisikResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void initList(List<DealsDetailFisikMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<DealsDetailFisikMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (DealsDetailFisikMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected DigitalPocketReq presentGetGroupingList() {
        DigitalPocketReq req = new DigitalPocketReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(100);
        req.setPage(mFragment.page);
        req.setSVoucherClass("");
        req.setVoucherClass(3);
        return req;
    }

}
