package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.ListDeliveryAddressMsgResp;
import com.ultravoucher.uv.ui.fragments.purchase.ListAddressFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firwandi.ramli on 11/2/2017.
 */

public class ListAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private ListAddressFragment mContext;
    private List<ListDeliveryAddressMsgResp> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public ListAddressAdapter(ListAddressFragment context, List<ListDeliveryAddressMsgResp> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }


    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_address, parent, false);

            vh = new ListVoucherHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListVoucherHolder) {
            final ListDeliveryAddressMsgResp item = mDataset.get(position);
            ((ListVoucherHolder) holder).itemView.setTag(item);
//            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);
            ((ListVoucherHolder) holder).addressName.setText(item.getAddressAlias());
            ((ListVoucherHolder) holder).name.setText(item.getRecipientName());
            ((ListVoucherHolder) holder).addressDetail.setText(item.getLine());
            ((ListVoucherHolder) holder).addressRegion.setText(item.getDistrict() + ", " + item.getCity() + ", " + item.getPostalCode());
            ((ListVoucherHolder) holder).addressProvince.setText(item.getProvince());
            ((ListVoucherHolder) holder).addressPhone.setText(item.getMobileNumber());


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListVoucherHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.btnEdit)
        ImageButton btnEdit;

        @BindView(R.id.addressName)
        TextView addressName;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.addressDetail)
        TextView addressDetail;

        @BindView(R.id.addressRegion)
        TextView addressRegion;

        @BindView(R.id.addressProvince)
        TextView addressProvince;

        @BindView(R.id.addressPhone)
        TextView addressPhone;

//        @BindView(R.id.vcLoading)
//        ProgressBar pbLoading;

        public ListVoucherHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}
