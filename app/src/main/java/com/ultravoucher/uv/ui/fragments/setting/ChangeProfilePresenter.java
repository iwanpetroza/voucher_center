package com.ultravoucher.uv.ui.fragments.setting;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.UpdateProfileReq;
import com.ultravoucher.uv.data.api.response.UpdateProfileResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class ChangeProfilePresenter {

    ChangeProfileFragment mFragment;
    private String[] mGender = {"Pria", "Wanita"};
    private String mGenderVal;

    public ChangeProfilePresenter(ChangeProfileFragment mFragment) {
        this.mFragment = mFragment;
    }

    public void doUpdateProfile(final UpdateProfileReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<UpdateProfileResponse> updateProfileResponseCall = api.updateProfile(contentType, authToken, deviceUniqueId, req);
        updateProfileResponseCall.enqueue(new Callback<UpdateProfileResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                if (response.code() == 200) {
                    UpdateProfileResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        Log.d("test 200", "-----------------");
                        PrefHelper.setString(PrefKey.FIRSTNAME, resp.getMemberResponse().getFirstName());
                        PrefHelper.setString(PrefKey.BIRTH_DATE, resp.getMemberResponse().getBirthDate());
                        PrefHelper.setString(PrefKey.MOTHER_NAME, resp.getMemberResponse().getMotherName());
                        PrefHelper.setString(PrefKey.BIRTH_PLACE, resp.getMemberResponse().getBirthPlace());
                        PrefHelper.setString(PrefKey.ID_NUMBER, resp.getMemberResponse().getIdNumber());

                        mFragment.showToast("Your setting have been changed");
                        mFragment.getActivity().finish();
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mFragment.showToast("Problem when contacting server");
                }
                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                mFragment.dismissProgressDialog();
                mFragment.showToast(mFragment.CONNECTION_ERROR);
            }
        });
    }

    /*Dialog Jenis Kelamin*/
    void selectGender() {
        final CharSequence[] items = mGender;

        AlertDialog.Builder builder = new AlertDialog.Builder(mFragment.getContext());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(mGender[0])) {
                    mFragment.etGender.setText(mGender[0]);
                    PrefHelper.setString(PrefKey.GENDER, "M");
                } else if (items[item].equals(mGender[1])) {
                    mFragment.etGender.setText(mGender[1]);
                    PrefHelper.setString(PrefKey.GENDER, "F");
                }
            }
        });
        builder.show();
    }

    UpdateProfileReq constructProfile() {

        if (mFragment.etGender.getText().toString().equals("Pria")) {
            mGenderVal = "M";
        } else {
            mGenderVal = "F";
        }

        String name = mFragment.etName.getText().toString();
        String memberId = PrefHelper.getString(PrefKey.MEMBERID);
        String noId = mFragment.etNumberId.getText().toString();
        String birthDate = mFragment.etBirthDate.getText().toString();
        String birthPlace = mFragment.etBirthPlace.getText().toString();
        String mother = mFragment.etMotherName.getText().toString();

        UpdateProfileReq req = new UpdateProfileReq();
        req.setMemberId(memberId);
        req.setFirstName(name);
        req.setLastName("-");
        req.setUserName(PrefHelper.getString(PrefKey.USERNAME));
        req.setBirthDate(mFragment.etBirthYear.getText().toString() + "-" + mFragment.etBirthMonth.getText().toString() + "-" + mFragment.etBirthDate.getText().toString());
        req.setGender(mGenderVal);
        req.setMobilePhone(PrefHelper.getString(PrefKey.PHONE_NUMBER));
        req.setAllowNotification(0);
        req.setBirthPlace(birthPlace);
        req.setIdNumber(noId);
        req.setMotherName(mother);
        return req;
    }
}
