package com.ultravoucher.uv.ui.fragments.forgotpassword;

import android.util.Log;
import android.view.View;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.ForgotPasswordReq;
import com.ultravoucher.uv.data.api.response.ForgotPasswordResponse;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.VerificationActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/30/2017.
 */

public class ForgotPasswordPresenter {

    private ForgotPasswordFragment mDialog;

    public static final String VERIFICATION_CODE = "verificationCode";

    public ForgotPasswordPresenter(ForgotPasswordFragment dialog) {
        this.mDialog = dialog;
    }

    void VerificatioCodeInReset(){
        mDialog.llView.setVisibility(View.GONE);
        mDialog.pbLoading.setVisibility(View.VISIBLE);
        String email = mDialog.etEmail.getText()+"";
        Log.d("email", email+"");
        VoucherApi api = mDialog.getVoucherApi();
        ForgotPasswordReq request = new ForgotPasswordReq();

        request.setEmail(email);
        String contentType = "application/json";

        Call<ForgotPasswordResponse> respCall = api.getResetCode(contentType, request);

        respCall.enqueue(new Callback<ForgotPasswordResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                if(response.code()==200) {
                    mDialog.pbLoading.setVisibility(View.GONE);
                    ForgotPasswordResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("INQ000")){
                        mDialog.verificationCode = resp.getCode();
                        VerificationActivity.startActivity((BaseActivity) mDialog.getActivity(), "FP", mDialog.verificationCode);
                        /*Intent result = new Intent();
                        result.putExtra(VERIFICATION_CODE, mDialog.verificationCode);
                        mDialog.onActivityResult(
                                mDialog.getTargetRequestCode(),
                                LoginActivity.RESET_DIALOG_REQ_CODE_OK,
                                result);*/
                        mDialog.dismiss();
                        mDialog.getActivity().finish();
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH002")) {
                        mDialog.llView.setVisibility(View.VISIBLE);
                        mDialog.showToast("Email ini belum terdaftar");
                    }
                    else {
                        mDialog.llView.setVisibility(View.VISIBLE);
                        mDialog.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mDialog.pbLoading.setVisibility(View.GONE);
                    mDialog.llView.setVisibility(View.VISIBLE);
                    mDialog.showToast("Connection Error, Try again");
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                mDialog.pbLoading.setVisibility(View.GONE);
                mDialog.llView.setVisibility(View.VISIBLE);
                mDialog.showToast(mDialog.CONNECTION_ERROR);
            }
        });
    }
}
