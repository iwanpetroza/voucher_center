package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class QRMemberActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.ivQR)
    ImageView ivQR;

    @BindView(R.id.tvError)
    TextView tvError;

    private QRMemberActivity mContext;

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, QRMemberActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_qrmember;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        String imgUri = PrefHelper.getString(PrefKey.QR_IMAGE);

        try {
            Picasso.with(this).load(imgUri).fit().into(ivQR, new Callback() {
                @Override
                public void onSuccess() {
                    ivQR.setVisibility(View.VISIBLE);
                    tvError.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    ivQR.setVisibility(View.GONE);
                    tvError.setVisibility(View.VISIBLE);
                    tvError.setText("Cannot Load QRCode");
                }
            });
        } catch (Exception e) {
            ivQR.setVisibility(View.GONE);
            tvError.setVisibility(View.VISIBLE);
            tvError.setText("Cannot Load QRCode");
        }


    }

    @OnClick(R.id.btnBack)
    public void onViewClicked() {
        this.finish();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Terima");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;

            /*case R.id.action_cart:
                CheckOutActivity.startActivity(this);
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

}
