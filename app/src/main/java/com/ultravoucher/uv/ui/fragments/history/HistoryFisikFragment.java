package com.ultravoucher.uv.ui.fragments.history;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.History;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.HistoryFisikVoucherAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 10/26/2017.
 */

public class HistoryFisikFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = HistoryFisikFragment.class.getSimpleName();

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;
    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    private Handler mHandler;
    protected List<History> mList = new ArrayList<>();
    HistoryFisikVoucherAdapter mAdapter;
    HistoryFisikPresenter mPresenter;

    public static void showFragment(BaseActivity sourceFragment) {
        HistoryFisikFragment fragment = new HistoryFisikFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = 0;
        mPresenter = new HistoryFisikPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);
        initRVMyHistoryDeals();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                rvList.setVisibility(View.GONE);
                mPresenter.presentGetFisikHistory(mPresenter.presentGetHistoryReq());
            }
        });

    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new HistoryFisikVoucherAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);

            }

        }));

        /*mHandler = new Handler();

        mAdapter.setOnLoadMoreListener(new HistoryFisikVoucherAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreHistoryDigitalList();
                    }
                }, 2000);

            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        /*initRVMyHistoryDeals();
        loadHistoryDigitalList();*/
    }

    private void loadHistoryDigitalList() {
        mPresenter.presentGetFisikHistory(mPresenter.presentGetHistoryReq());
    }

    private void loadMoreHistoryDigitalList() {
        mPresenter.presentGetFisikHistoryLoadMore(mPresenter.presentGetHistoryReq());
    }


    @Override
    public void onRefresh() {
        page = 0;
        rvList.setVisibility(View.GONE);
        mPresenter.presentGetFisikHistory(mPresenter.presentGetHistoryReq());
    }
}