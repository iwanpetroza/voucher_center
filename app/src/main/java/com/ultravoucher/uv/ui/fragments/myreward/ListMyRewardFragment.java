package com.ultravoucher.uv.ui.fragments.myreward;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VoucherCollectDetailResponse;
import com.ultravoucher.uv.data.api.beans.VoucherListMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GenerateQRCodeActivity;
import com.ultravoucher.uv.ui.adapters.MyRewardListAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class ListMyRewardFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ListMyRewardFragment.class.getSimpleName();

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    protected int page = 0;
    List<VoucherCollectDetailResponse> mList = new ArrayList<>();
    ListMyRewardPresenter mPresenter;
    MyRewardListAdapter mAdapter;
    String keyword = "";

    private boolean _hasLoadedOnce= false;

    public static void showFragment(BaseActivity sourceFragment) {
        ListMyRewardFragment fragment = new ListMyRewardFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_reword_listview;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        mPresenter = new ListMyRewardPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        page = 0;
        initRVMyReward();

        etSearch.setHint("Cari Nama Reward");

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String a = etSearch.getText().toString();

                if (a.equals("")) {
                    keyword = "";
                    rvList.setVisibility(View.GONE);
                    mPresenter.getListMyReward(mPresenter.construct());
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    protected void initRVMyReward() {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new MyRewardListAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                VoucherCollectDetailResponse product = (VoucherCollectDetailResponse) childView.getTag();
                if (product.getVoucherClass() == 2) {
                    GenerateQRCodeActivity.startActivity((BaseActivity) getActivity(), product.getProductImage(), product.getRewardName(), product.getVoucherTransactionId(), product.getVoucherCode(), product.getVoucherExpiredDate(), "R", product.getMerchantId());
                }
            }

        }));
    }

    private void loadMyRewardList() {
        mPresenter.getListMyReward(mPresenter.construct());
    }

    @OnClick(R.id.btnSearch)
    void clicked() {
        if (etSearch.getText().toString().equals("")) {
            showToast("Masukkan kata kunci");
        } else {
            page = 0;
            keyword = etSearch.getText().toString();
            rvList.setVisibility(View.GONE);
            mPresenter.getListMyReward(mPresenter.construct());
        }
    }


    //new
    @Override
    public void onRefresh() {
        page = 0;
        rvList.setVisibility(View.GONE);
        mPresenter.getListMyReward(mPresenter.construct());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isVisibleToUser) {
            //new
                swipe_container.setOnRefreshListener(this);
                swipe_container.post(new Runnable() {
                    @Override
                    public void run() {
                        swipe_container.setRefreshing(true);

                        page = 0;
                        rvList.setVisibility(View.GONE);
                        mPresenter.getListMyReward(mPresenter.construct());
                    }
                });
                _hasLoadedOnce = true;
            }
        }
    }
}
