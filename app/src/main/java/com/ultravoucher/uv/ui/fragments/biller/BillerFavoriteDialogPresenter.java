package com.ultravoucher.uv.ui.fragments.biller;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.BillerHistory;
import com.ultravoucher.uv.data.api.beans.ContractFavorite;
import com.ultravoucher.uv.data.api.request.AdvertisingReq;
import com.ultravoucher.uv.data.api.request.BillerGetFavoriteReq;
import com.ultravoucher.uv.data.api.request.BillerHistoryReq;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;
import com.ultravoucher.uv.data.api.response.BillerGetFavoriteResponse;
import com.ultravoucher.uv.data.api.response.BillerHistoryResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.mainpage.PopupAdvDialogFragment;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tunggul.jati on 7/11/2018.
 */

public class BillerFavoriteDialogPresenter {

    private BillerFavoriteDialogFragment mFragment;

    public BillerFavoriteDialogPresenter(BillerFavoriteDialogFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void getFavoriteList(BillerGetFavoriteReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<BillerGetFavoriteResponse> historyProductResponseCall = api.getFavorite(contentType, authToken, deviceUId, req);
        historyProductResponseCall.enqueue(new Callback<BillerGetFavoriteResponse>() {
            @Override
            public void onResponse(Call<BillerGetFavoriteResponse> call, Response<BillerGetFavoriteResponse> response) {
                if (response.code() == 200) {
                    BillerGetFavoriteResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("BL001")) {
                        if (!resp.getContractFavorite().isEmpty()) {
                            initList(resp.getContractFavorite());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BillerGetFavoriteResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void initList(List<ContractFavorite> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected BillerGetFavoriteReq constructBillerGetFavorite() {
        BillerGetFavoriteReq req = new BillerGetFavoriteReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setCategory(PrefHelper.getInt(PrefKey.CATEGORY)); //PLN=1 PULSA=2
        return req;
    }

}