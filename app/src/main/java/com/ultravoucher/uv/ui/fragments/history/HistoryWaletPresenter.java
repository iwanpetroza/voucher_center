package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.HistoryWalet;
import com.ultravoucher.uv.data.api.request.HistoryWaletReq;
import com.ultravoucher.uv.data.api.response.HistoryWaletResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 2/9/2018.
 */

public class HistoryWaletPresenter {

    private static HistoryWaletFragment mFragment;

    Calendar calendar;

    public HistoryWaletPresenter(HistoryWaletFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetHistoryWalet(HistoryWaletReq req) {

        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryWaletResponse> historyWaletResponseCall = api.getHistoryWalet(contentType, authToken, deviceUId, req);
        historyWaletResponseCall.enqueue(new Callback<HistoryWaletResponse>() {
            @Override
            public void onResponse(Call<HistoryWaletResponse> call, Response<HistoryWaletResponse> response) {
                if (response.code() == 200) {
                    HistoryWaletResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("H0001")) {
                        if (!resp.getHistory().isEmpty()) {
                            initList(resp.getHistory());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getActivity().finish();
                        mFragment.doNeedRelogin();
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("OD908")) {
                        mFragment.showToast("Password salah!");
                        dismissHistory();
                    }

                    else {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);

                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<HistoryWaletResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void initList(List<HistoryWalet> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
//        mFragment.page++;
    }

    protected HistoryWaletReq presentGetHistoryWalet() {
        Date curDate = calendar.getInstance().getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String DateToStr = format.format(curDate);


        HistoryWaletReq req = new HistoryWaletReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setDateFrom("2018-01-01 12:00:00 AM");
        req.setDateTo(DateToStr);
        req.setPin(mFragment.pass);
        return req;
    }

    public static void dismissHistory(){
        mFragment.getActivity().finish();
    }

}
