package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.ultravoucher.uv.R;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 1/8/2018.
 */

public class CCActivity extends BaseActivity {


    private static final String POST_ID = "postId";

    @BindView(R.id.toolbar)
    Toolbar toolBar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.my_webview)
    WebView wv;

    private VideoView videoView;
    private MediaController mController;
    private Uri uriYouTube;
    String webUrl;
    boolean isTrue = false;

    @Override
    protected int getLayout() {
        return R.layout.a_creditcard;
    }

    String link;

    public static void startActivity(BaseActivity sourceActivity, String url){
        Intent i = new Intent(sourceActivity, CCActivity.class);
        i.putExtra(POST_ID, url);
        sourceActivity.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        link = getIntent().getStringExtra(POST_ID);

        setupToolbar();

        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message,           JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });
        wv.loadUrl(link);

        wv.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webUrl = url;


                if (webUrl.contains("true")) {
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Pembayaran Anda berhasil");
                    startActivity(intent);
                    finish();
                } else if (webUrl.contains("false")) {
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Pembayaran Anda Gagal \n Silahkan Coba Lagi");
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        this.finish();
        HomePageActivity.startActivity(this, "");

    }

    private void setupToolbar() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar_title.setText("PAYMENT");
        toolbar_title.setAllCaps(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
