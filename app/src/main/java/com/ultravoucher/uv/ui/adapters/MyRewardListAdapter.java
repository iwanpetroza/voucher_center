package com.ultravoucher.uv.ui.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.VoucherCollectDetailResponse;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GenerateQRCodeActivity;
import com.ultravoucher.uv.ui.fragments.myreward.ListMyRewardFragment;
import com.ultravoucher.uv.utils.DateFormatter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by firwandi.ramli on 1/22/2018.
 */

public class MyRewardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private ListMyRewardFragment mContext;
    private List<VoucherCollectDetailResponse> mDataset;

    // The minimum amount of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public MyRewardListAdapter(ListMyRewardFragment context, List<VoucherCollectDetailResponse> myDataSet, RecyclerView recyclerView) {
        mContext = context;
        mDataset = myDataSet;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mDataset.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_my_reward, parent, false);

            vh = new ListVoucherHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListVoucherHolder) {
            final VoucherCollectDetailResponse item = mDataset.get(position);

            ((ListVoucherHolder) holder).itemView.setTag(item);
            ((ListVoucherHolder) holder).pbLoading.setVisibility(View.VISIBLE);
            ((ListVoucherHolder) holder).tvName.setText(item.getRewardName());

            if (item.getRedeemedDate() == null) {
                if (item.getTransactionDate().equals("") || item.getTransactionDate().equals("-")) {
                    ((ListVoucherHolder) holder).tvExpiredDate.setText("Tgl Redeem : - ");
                } else {
                    ((ListVoucherHolder) holder).tvExpiredDate.setText("Tgl Redeem : " + DateFormatter.format(item.getTransactionDate()));
                }
            } else {
                ((ListVoucherHolder) holder).tvExpiredDate.setText("Tgl Redeem : - ");
            }



            if (item.getTransactionDate().equals("") || item.getTransactionDate().equals("-")) {
                ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tgl Expired : -");
            } else {
                ((ListVoucherHolder) holder).tvPurchaseDate.setText("Tgl Expired : " + DateFormatter.format(item.getVoucherExpiredDate()));
            }




            if (item.getProductImage() != null && !item.getProductImage().equals("")) {
                Picasso.with(mContext.getContext()).load(item.getProductImage()).placeholder(R.drawable.image_placeholder)
                        .centerCrop().fit().into(((ListVoucherHolder) holder).civLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
                        ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                    }
                });
            } else {
                ((ListVoucherHolder) holder).civLogo.setImageResource(R.drawable.image_placeholder);
                ((ListVoucherHolder) holder).pbLoading.setVisibility(View.GONE);
            }

            if (item.getVoucherClass() == 2) {
                ((ListVoucherHolder) holder).btnQR.setVisibility(View.VISIBLE);
            } else {
                ((ListVoucherHolder) holder).btnQR.setVisibility(View.GONE);
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    public void swap(List<VoucherCollectDetailResponse> datas) {
        mDataset.clear();
        mDataset.addAll(datas);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void setLoaded(boolean status) {
        loading = status;
    }

    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class ListVoucherHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;

        @BindView(R.id.tvPurchaseDate)
        TextView tvPurchaseDate;

        @BindView(R.id.tvExpiredDate)
        TextView tvExpiredDate;

        @BindView(R.id.imgView)
        ImageView civLogo;

        @BindView(R.id.vcLoading)
        ProgressBar pbLoading;

        @BindView(R.id.btnQR)
        ImageView btnQR;

        public ListVoucherHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }
}
