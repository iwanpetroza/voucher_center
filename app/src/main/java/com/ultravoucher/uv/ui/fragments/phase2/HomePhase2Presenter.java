package com.ultravoucher.uv.ui.fragments.phase2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.data.api.beans.BillerCategoryMsgResp;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.data.api.request.AdvertisingReq;
import com.ultravoucher.uv.data.api.request.BillerCategoryReq;
import com.ultravoucher.uv.data.api.request.CheckMemberPointsReq;
import com.ultravoucher.uv.data.api.request.MemberPocketReq;
import com.ultravoucher.uv.data.api.request.MemberWaletReq;
import com.ultravoucher.uv.data.api.response.AdvertisingResponse;
import com.ultravoucher.uv.data.api.response.BillerCategoryResponse;
import com.ultravoucher.uv.data.api.response.MemberPocketResponse;
import com.ultravoucher.uv.data.api.response.MemberPointBalanceResponse;
import com.ultravoucher.uv.data.api.response.MemberWaletResponse;
import com.ultravoucher.uv.data.api.response.UploadPhotoResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.utils.AmountFormatter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 10/09/18.
 */

public class HomePhase2Presenter {

    private HomePhase2Fragment mFragment;

    public HomePhase2Presenter(HomePhase2Fragment mFragment) {
        this.mFragment = mFragment;
    }

    void presentUploadProfilePic(File filedata) {

        RequestBody memberId = RequestBody.create(MediaType.parse("multipart/form-data"), PrefHelper.getString(PrefKey.MEMBERID));

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filedata);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Bitmap bm = BitmapFactory.decodeStream(fis);


        VoucherApi service = mFragment.getVoucherApi();
        Call<UploadPhotoResp> call = service.doUploadPhoto(memberId,
                constructPartFile(filedata, "image/*", "file"));
        call.enqueue(new Callback<UploadPhotoResp>() {
            @Override
            public void onResponse(Call<UploadPhotoResp> call, Response<UploadPhotoResp> response) {
                if (response.code() == 200) {
                    UploadPhotoResp resp = response.body();
                    PrefHelper.setString(PrefKey.USER_PIC, resp.getAbstractResponse().getUrlImage());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(mFragment.getActivity().getApplicationContext()).load(PrefHelper.getString(PrefKey.USER_PIC))
                                    .placeholder(R.drawable.image_placeholder)
                                    .error(R.drawable.image_placeholder).fit().centerCrop()
                                    .into(mFragment.civLogo);
                        }
                    }, 1500);
                    mFragment.showToast("Image upload success");
                }
            }

            @Override
            public void onFailure(Call<UploadPhotoResp> call, Throwable t) {
                mFragment.showToast("" + t.getMessage());
            }
        });
    }

    public MultipartBody.Part constructPartFile(File file, String type, String tag) {
        RequestBody requestBody = RequestBody.create(MediaType.parse(type), file);
        return MultipartBody.Part.createFormData(tag, file.getName(), requestBody);
    }

    // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        Log.e("ImageString", "" + stream.toByteArray());
        return stream.toByteArray();
    }

    // get the base 64 string
    public String data(Bitmap someImg) {
        String imgString = Base64.encodeToString(getBytesFromBitmap(someImg),
                Base64.NO_WRAP);

        return imgString;
    }


    void getMemberPocket(final MemberPocketReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberPocketResponse> respCall = api.getMemberPocket(authToken, deviceUniqueId, req);
        respCall.enqueue(new Callback<MemberPocketResponse>() {
            @Override
            public void onResponse(Call<MemberPocketResponse> call, Response<MemberPocketResponse> response) {
                if (response.code() == 200) {
                    mFragment.pbLoading.setVisibility(View.INVISIBLE);
                    MemberPocketResponse resp = response.body();
                    int pocket;

                    if (resp.getAbstractResponse().getResponseStatus() != null) {
                        if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {

                            pocket = resp.getPockets().getBalance();
                            mFragment.tvWalet.setText("" + AmountFormatter.format(pocket));

                        } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            mFragment.tvWalet.setText("Gagal");
                        } else if (resp.getAbstractResponse().getResponseStatus().equals("ERR123")) {
                            mFragment.tvWalet.setText("Gagal");
//
                        } else {
                            mFragment.tvWalet.setText("Gagal");
                        }
                    } else {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                        mFragment.tvWalet.setText("Gagal");
                    }
                } else if (response.code() == 400) {
                    mFragment.tvWalet.setText("Gagal");
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                    mFragment.tvWalet.setText("Need PIN");
                }
                mFragment.pbLoading.setVisibility(View.INVISIBLE);
                mFragment.dismissProgressDialog();
                getmemberBalance(constructMemberBalance());
            }

            @Override
            public void onFailure(Call<MemberPocketResponse> call, Throwable t) {
                mFragment.pbLoading.setVisibility(View.INVISIBLE);
                mFragment.tvWalet.setText("Error");
                mFragment.dismissProgressDialog();
                getmemberBalance(constructMemberBalance());
            }
        });
    }

    MemberPocketReq constructGetPocketReq() {
        String memberId = PrefHelper.getString(PrefKey.MEMBERID);
        MemberPocketReq request = new MemberPocketReq();
        request.setMemberId(memberId);
        return request;
    }

    void getmemberBalance(final MemberWaletReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);
        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberWaletResponse> memberWaletResponseCall = api.getMemberWalet(contentType, authToken, dui, req);
        memberWaletResponseCall.enqueue(new Callback<MemberWaletResponse>() {
            @Override
            public void onResponse(Call<MemberWaletResponse> call, Response<MemberWaletResponse> response) {

                if (response.code() == 200) {
                    MemberWaletResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
//                        if (String.valueOf(resp.getTotalBalance()) != null) {
                        mFragment.tvVoucher.setText("" + resp.getDealsDetailBalance().size() + " Vouchers");
//                        }else{
//                            mFragment.tvBalance.setText("0");
//                        }

                    } else if (resp.getAbstractResponse().getResponseStatus().equals("MEM903")) {
                        mFragment.tvVoucher.setText("0");
                    } else {

                    }
                } else {
                    mFragment.tvVoucher.setText("GAGAL");
//                    mFragment.showToast();
                }
                mFragment.dismissProgressDialog();
                checkmemberBalance(constructMemberPoint());
            }

            @Override
            public void onFailure(Call<MemberWaletResponse> call, Throwable t) {
                mFragment.tvVoucher.setText("GAGAL");
//                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
                checkmemberBalance(constructMemberPoint());
            }
        });
    }

    MemberWaletReq constructMemberBalance() {
        MemberWaletReq req = new MemberWaletReq();
        req.setPage(0);
        req.setNRecords(10);
        req.setVoucherClass(1);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }

    void checkmemberBalance(CheckMemberPointsReq req) {

        mFragment.showProgressDialog(mFragment.LOADING);

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<MemberPointBalanceResponse> memberPointBalanceResponseCall = api.checkMemberPoint(contentType, authToken, deviceUId, req);
        memberPointBalanceResponseCall.enqueue(new Callback<MemberPointBalanceResponse>() {
            @Override
            public void onResponse(Call<MemberPointBalanceResponse> call, Response<MemberPointBalanceResponse> response) {
                if (response.code() == 200) {
                    MemberPointBalanceResponse resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        mFragment.tvPoint.setText("" + String.valueOf(resp.getTotalPoints()) + " Poin");
                        getBillerCategoryHome(getBillerMenu());
                    } else {
                        if (PrefHelper.getString(PrefKey.POIN).equals("") || PrefHelper.getString(PrefKey.POIN) == null) {
                            mFragment.tvPoint.setText("0 Poin");
                        } else {
                            mFragment.tvPoint.setText(PrefHelper.getString(PrefKey.POIN) + " Poin");
                        }
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();

            }

            @Override
            public void onFailure(Call<MemberPointBalanceResponse> call, Throwable t) {
                if (PrefHelper.getString(PrefKey.POIN).equals("") || PrefHelper.getString(PrefKey.POIN) == null) {
                    mFragment.tvPoint.setText("0 Poin");
                } else {
                    mFragment.tvPoint.setText(PrefHelper.getString(PrefKey.POIN) + " Poin");
                }

                mFragment.dismissProgressDialog();
            }
        });
    }

    CheckMemberPointsReq constructMemberPoint() {
        CheckMemberPointsReq req = new CheckMemberPointsReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(50);
        req.setOrder("");
        req.setPage(0);
        return req;
    }

    void presentAdvBanner() {

        AdvertisingReq req = new AdvertisingReq();
        req.setAdvertisingType(2);
        req.setCityId("");

        Call<AdvertisingResponse> advertisingResponseCall = mFragment.getVoucherApi().getDetailAds(req);
        advertisingResponseCall.enqueue(new Callback<AdvertisingResponse>() {
            @Override
            public void onResponse(Call<AdvertisingResponse> call, Response<AdvertisingResponse> response) {
                AdvertisingResponse resp = response.body();
                if (response.code() == 200) {
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000"))
                        if (!resp.getAdvertisingResponse().isEmpty()) {
                            initList(resp.getAdvertisingResponse());
                        } else {


                    } else {
                        mFragment.showToast("Eror Bukan INQ000");
                    }
                } else {
                    mFragment.showToast("Eror Bukan 200");
                }

                getMemberPocket(constructGetPocketReq());

            }

            @Override
            public void onFailure(Call<AdvertisingResponse> call, Throwable t) {
                mFragment.showToast("Eror Koneksi");
                getMemberPocket(constructGetPocketReq());
            }
        });
    }

    protected void initList(List<AdvertisingMsgResp> list) {
        mFragment.mHotSales.clear();
        mFragment.mHotSales.addAll(list);
        mFragment.mAdapterHotSales.notifyDataSetChanged();
        mFragment.mAdapterHotSales.setLoaded(false);
        mFragment.page++;
    }

    void getBillerCategoryHome(final BillerCategoryReq req) {

        String contentType = "application/json";
        VoucherApi api = mFragment.getVoucherApi();
        final  Call<BillerCategoryResponse> billerCategoryResponseCall = api.getBillerMenuHome(contentType, req);
        billerCategoryResponseCall.enqueue(new Callback<BillerCategoryResponse>() {
            @Override
            public void onResponse(Call<BillerCategoryResponse> call, Response<BillerCategoryResponse> response) {
                if (response.code() == 200) {
                    if (response.body() != null && response.body().getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        BillerCategoryResponse resp = response.body();
                        if (!resp.getBillerCategoryMsgResponseList().isEmpty()) {
                            initListBillerMenu(resp.getBillerCategoryMsgResponseList());
                        } else {
                            mFragment.mData.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.setLoaded(false);
                        }
                    } else {
                        mFragment.mData.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.setLoaded(false);
                    }
                } else {
                    mFragment.mData.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.setLoaded(false);
                }
            }

            @Override
            public void onFailure(Call<BillerCategoryResponse> call, Throwable t) {
                String message = mFragment.getString(R.string.connection_error);
                mFragment.showToast(message);
            }
        });
    }

    protected BillerCategoryReq getBillerMenu() {
        BillerCategoryReq req = new BillerCategoryReq();
        req.setNRecords(2);
        req.setPage(0);
        return req;
    }

    protected void initListBillerMenu(List<BillerCategoryMsgResp> list) {
        mFragment.mData.clear();
        mFragment.mData.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

}
