package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class HowtoActivity extends BaseActivity {

    private static final String TAG = HowtoActivity.class.getSimpleName();

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.img)
    ImageView img;

    public static void startActivity(BaseActivity sourceActivity){
        Intent i = new Intent(sourceActivity, HowtoActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_howto;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        img.setVisibility(View.VISIBLE);
        Picasso.with(this).load(R.drawable.cara_pickup).fit().into(img);

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Tata Cara Pickup");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
