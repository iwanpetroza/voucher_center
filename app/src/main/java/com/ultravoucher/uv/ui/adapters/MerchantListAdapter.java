package com.ultravoucher.uv.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.VoucherListActivity;
import com.ultravoucher.uv.utils.PicassoTrustAll;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by firwandi.ramli on 10/11/2017.
 */

/*=======NOTE : Replace ? dengan resp balikan dr server==============*/

public class MerchantListAdapter extends BaseAdapter{

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext;
    private List<StoreListMsgResp> mDataSet;

    private int visibleThreshold = 3;
    private boolean loading;
    private OneLoadMoreListener onLoadMoreListener;

    public interface OneLoadMoreListener {
        void onLoadMore();
    }

    public MerchantListAdapter(Context context, List<StoreListMsgResp> mDataSet, GridView gridView) {
        this.mDataSet = mDataSet;
        this.mContext = context;
//        this.status = status;
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold) >= totalItemCount ){
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    loading = true;
                }
            }
        });
    }

    public void swap(List<StoreListMsgResp> datas) {
        mDataSet.clear();
        mDataSet.addAll(datas);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh = null;
        View gridView = convertView;
        vh = new ViewHolder();
        if (getItemViewType(position) == VIEW_ITEM) {
            gridView = LayoutInflater.from(mContext).inflate(R.layout.grid_merchant_list, parent, false);
            final StoreListMsgResp item = mDataSet.get(position);

            vh.iv = (ImageView) gridView.findViewById(R.id.grid_item_image);
            Picasso.with(mContext).load(item.getStoreLogo()).placeholder(R.drawable.image_placeholder)
                    .centerCrop().fit().into(vh.iv);

            PicassoTrustAll.getInstance(mContext.getApplicationContext()).load(item.getStoreLogo()).into(vh.iv);

            vh.iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    VoucherListActivity.startActivity((BaseActivity) mContext, "0", item.getStoreId());
                }
            });
            gridView.setTag(vh);
        } else if (getItemViewType(position) == VIEW_PROG) {
            gridView = LayoutInflater.from(mContext).inflate(R.layout.progressbar_item_horizontal, parent, false);
            vh.progressBar = (ProgressBar) gridView.findViewById(R.id.progressBar);
            vh.progressBar.setIndeterminate(true);
        }

        return gridView;
    }


    public void setOnLoadMoreListener(OneLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class ViewHolder {
        ImageView iv;
        ProgressBar progressBar;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setLoaded(boolean status) {
        loading = status;
    }
}
