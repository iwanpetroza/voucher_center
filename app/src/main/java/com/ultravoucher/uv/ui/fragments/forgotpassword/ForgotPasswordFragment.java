package com.ultravoucher.uv.ui.fragments.forgotpassword;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/30/2017.
 */

public class ForgotPasswordFragment extends BaseDialogFragment implements Validator.ValidationListener{

    private static final String TAG = ForgotPasswordFragment.class.getSimpleName();
    ForgotPasswordPresenter presenter;
    protected String verificationCode;

    @BindView(R.id.d_resetPassword_etEmail)
    @NotEmpty
    EditText etEmail;

    @BindView(R.id.d_resetPassword_pb_loading)
    ProgressBar pbLoading;

    @BindView(R.id.d_resetPasswor_linearLayout)
    LinearLayout llView;

    Validator validator;

    @Override
    protected int getLayout() {
        return R.layout.d_forgot_password;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ForgotPasswordPresenter(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    public static void showDialog(BaseActivity fragment, int requestCode){
        ForgotPasswordFragment dialog = new ForgotPasswordFragment();
        dialog.show(fragment.getSupportFragmentManager(), TAG);
    }

    @OnClick(R.id.d_resetPassword_btnOk)
    public void resetPassword()
    {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        PrefHelper.setString(PrefKey.USERNAME, etEmail.getText()+"");
        presenter.VerificatioCodeInReset();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(getContext(),message,Toast.LENGTH_SHORT);
            }
        }
    }
}
