package com.ultravoucher.uv.ui.activities;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.CheckMemberReq;
import com.ultravoucher.uv.data.api.beans.SharingAmount;
import com.ultravoucher.uv.data.api.beans.SharingBalance;
import com.ultravoucher.uv.data.api.beans.SubmitShareDetails;
import com.ultravoucher.uv.data.api.request.AutoRegistrationReq;
import com.ultravoucher.uv.data.api.request.CheckMemberExistReq;
import com.ultravoucher.uv.data.api.request.InquiryShareReq;
import com.ultravoucher.uv.data.api.request.ShareAmountReq;
import com.ultravoucher.uv.data.api.request.SubmitShareReq;
import com.ultravoucher.uv.data.api.response.AutoRegistrationResponse;
import com.ultravoucher.uv.data.api.response.MemberExistResponse;
import com.ultravoucher.uv.data.api.response.SharingAmountResponse;
import com.ultravoucher.uv.data.api.response.SharingBalanceResponse;
import com.ultravoucher.uv.data.api.response.SubmitShareResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 12/8/2017.
 */

public class ShareVoucherDigitalPresenter {

    ShareVoucherDigitalActivity mActivity;
    List<SharingAmount> datas = new ArrayList<>();
    List<SharingBalance> datasInq = new ArrayList<>();
    int variant;

    String mail, phoneNum;


    public ShareVoucherDigitalPresenter(ShareVoucherDigitalActivity mActivity) {
        this.mActivity = mActivity;
    }

    void CheckMember(final CheckMemberExistReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";

        VoucherApi api = mActivity.getVoucherApi();
        Call<MemberExistResponse> memberExistResponseCall = api.getMemberExist(contentType, req);
        memberExistResponseCall.enqueue(new Callback<MemberExistResponse>() {
            @Override
            public void onResponse(Call<MemberExistResponse> call, Response<MemberExistResponse> response) {
                if (response.code() == 200) {
                    MemberExistResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getRegisteredMember().isEmpty()) {
                            PrefHelper.setString(PrefKey.MEMBERIDTO, resp.getRegisteredMember().get(0).getMemberId());
                            doSubmitSharingVoucher(constructSubmitShare());
                        } else if (!resp.getUnregisteredMember().isEmpty()) {
                            AutoRegister(constructAutoRegis());
                        } else {
                            mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast("Koneksi Tidak Stabil");
                }
                mActivity.dismissProgressDialog();
            }


            @Override
            public void onFailure(Call<MemberExistResponse> call, Throwable t) {
                mActivity.dismissProgressDialog();
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });
    }

    CheckMemberExistReq constructMemberExist() {
        String username = mActivity.etShareEmail.getText().toString();
        String phone = mActivity.etSharePhone.getText().toString().trim();

        String a, b;

        if (phone.contains("-") || phone.contains("+62")) {
            a = phone.replaceAll("\\-", "");
            b = a.replaceAll(" ", "");
            if (b.contains("+62")) {
                phoneNum = b.replaceAll("\\+62", "0");
            }
        } else {
            phoneNum = phone;
        }

        CheckMemberExistReq request = new CheckMemberExistReq();
        List<CheckMemberReq> orderPays = new ArrayList<>();

        CheckMemberReq order = new CheckMemberReq();
        order.setMobileNumber(phoneNum);
        order.setUsername(username);
        orderPays.add(order);

        request.setListMember(orderPays);

        return request;
    }

    void AutoRegister(AutoRegistrationReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";

        VoucherApi api = mActivity.getVoucherApi();
        Call<AutoRegistrationResponse> autoRegistrationResponseCall = api.doAutoRegis(contentType, req);
        autoRegistrationResponseCall.enqueue(new Callback<AutoRegistrationResponse>() {
            @Override
            public void onResponse(Call<AutoRegistrationResponse> call, Response<AutoRegistrationResponse> response) {
                if (response.code() == 200) {
                    AutoRegistrationResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("MEM000")) {
                        PrefHelper.setString(PrefKey.MEMBERIDTO, resp.getMemberId());
                        doSubmitSharingVoucher(constructSubmitShare());
                    } else {
                        mActivity.showProgressDialog(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast(mActivity.CONNECTION_ERROR);
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<AutoRegistrationResponse> call, Throwable t) {
                mActivity.dismissProgressDialog();
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });
    }

    AutoRegistrationReq constructAutoRegis() {
        String username = mActivity.etShareEmail.getText().toString();
        String phone = mActivity.etSharePhone.getText().toString();
        String name = mActivity.etShareName.getText().toString();

        AutoRegistrationReq request = new AutoRegistrationReq();
        request.setUsername(username);
        request.setMobileNumber(phone);
        request.setFirstName(name);
        request.setCreator(PrefHelper.getString(PrefKey.MEMBERID));
        request.setLastName("-");
        return request;
    }


    void getShareAmount(ShareAmountReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mActivity.getVoucherApi();
        Call<SharingAmountResponse> sharingAmountResponseCall = api.getShareAmount(contentType, authToken, dui, req);
        sharingAmountResponseCall.enqueue(new Callback<SharingAmountResponse>() {
            @Override
            public void onResponse(Call<SharingAmountResponse> call, Response<SharingAmountResponse> response) {
                if (response.code() == 200) {
                    SharingAmountResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getSharingBalance().isEmpty()) {
                            datas = resp.getSharingBalance();
                            variant = resp.getSharingBalance().size();

                            if (variant > 1) {
                                List<String> listVariantName = new ArrayList<String>();
                                List<String> listQuantity = new ArrayList<String>();

                                SharingAmount item;
                                for (int i = 0; i < variant; i++) {
                                    item = datas.get(i);
                                    listVariantName.add(String.valueOf(item.getVoucherValue()));
                                    listQuantity.add(String.valueOf(item.getTotalQuantity()));
                                }

                                mActivity.setVariantName(listVariantName);
                                mActivity.setVariantQty(listQuantity);
                            } else {
                                mActivity.etAmountShare.setText("" + resp.getSharingBalance().get(0).getVoucherValue());
                                mActivity.etAmountShare.setEnabled(false);
                                mActivity.amount = String.valueOf(resp.getSharingBalance().get(0).getVoucherValue());
                                mActivity.tvQtySvr.setText("" + resp.getSharingBalance().get(0).getTotalQuantity());
                                mActivity.jmlhv = String.valueOf(resp.getSharingBalance().get(0).getTotalQuantity());
                                mActivity.etShareQty.setEnabled(true);
                            }

                        } else {
                            mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mActivity.showToast("Sesi Anda telah berakhir, Silahkan Login ulang");
                        mActivity.finish();
                        mActivity.doNeedRelogin();
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast("Koneksi gagal, silahkan Coba beberapa saat lagi");
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<SharingAmountResponse> call, Throwable t) {
                mActivity.showToast("Koneksi gagal, silahkan Coba beberapa saat lagi");
            }
        });
    }

    /*void getShareAmount(SharingBalanceReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mActivity.getVoucherApi();
        Call<SharingBalanceResponse> sharingBalanceResponseCall = api.getSharingBalance(contentType, authToken, dui, req);
        sharingBalanceResponseCall.enqueue(new Callback<SharingBalanceResponse>() {
            @Override
            public void onResponse(Call<SharingBalanceResponse> call, Response<SharingBalanceResponse> response) {
                if (response.code() == 200) {
                    SharingBalanceResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getSharingBalance().isEmpty()) {
                            datas = resp.getSharingBalance();
                            variant = resp.getSharingBalance().size();

                            if (variant > 1) {
                                List<String> listVariantName = new ArrayList<>();
                                List<String> listProductId = new ArrayList<>();

                                SharingBalance item;
                                for (int i=0; i<variant; i++) {
                                    item = datas.get(i);
                                    listVariantName.add(String.valueOf(item.getVoucherValue()));
//                                    listProductId.add(item.getDetails().get(i));
                                }
                                mActivity.setVariantName(listVariantName);

                            } else {
                                mActivity.etAmountShare.setText("" + resp.getSharingBalance().get(0).getVoucherValue());
                                mActivity.etAmountShare.setClickable(false);
                            }


                        } else {
                            mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast(mActivity.CONNECTION_ERROR);
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<SharingBalanceResponse> call, Throwable t) {
                mActivity.showToast(mActivity.CONNECTION_ERROR);
                mActivity.dismissProgressDialog();
            }
        });
    }*/

    ShareAmountReq constructSharingBalance() {
        ShareAmountReq req = new ShareAmountReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setMerchantId(mActivity.merchantId);
        req.setNRecords(100);
        req.setPage(0);
        req.setSVoucherClass("D");
        return req;
    }


    void doSubmitSharingVoucher(SubmitShareReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mActivity.getVoucherApi();
        Call<SubmitShareResponse> submitShareResponseCall = api.doShare(contentType, authToken, dui, req);
        submitShareResponseCall.enqueue(new Callback<SubmitShareResponse>() {
            @Override
            public void onResponse(Call<SubmitShareResponse> call, Response<SubmitShareResponse> response) {
                if (response.code() == 200) {
                    SubmitShareResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("SDS001")) {
                        mActivity.showToast("Selamat Voucher Digital anda berhasil dibagikan");
                        mActivity.finish();
                        GeneralActivity.startActivity(mActivity, "share");
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast(mActivity.CONNECTION_ERROR);
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<SubmitShareResponse> call, Throwable t) {
                mActivity.showToast(mActivity.CONNECTION_ERROR);
                mActivity.dismissProgressDialog();
            }
        });
    }

    SubmitShareReq constructSubmitShare() {

        SubmitShareReq req = new SubmitShareReq();
        req.setMemberIdFrom(PrefHelper.getString(PrefKey.MEMBERID));
        req.setMemberIdTo(PrefHelper.getString(PrefKey.MEMBERIDTO));
        req.setMerchantId(mActivity.merchantId);
        List<SubmitShareDetails> submitShareDetailses = new ArrayList<>();


        for (int i = 0; i < datasInq.get(0).getDetails().size(); i++) {
            SubmitShareDetails request = new SubmitShareDetails();
            request.setVoucherCode(datasInq.get(0).getDetails().get(i).getVoucherCode());
            request.setVoucherId(datasInq.get(0).getDetails().get(i).getVoucherId());
            submitShareDetailses.add(request);
        }
        req.setDetails(submitShareDetailses);
        return req;
    }


    void doShareInq(InquiryShareReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);

        VoucherApi api = mActivity.getVoucherApi();
        Call<SharingBalanceResponse> sharingBalanceResponseCall = api.getInquiryShare(contentType, authToken, dui, req);
        sharingBalanceResponseCall.enqueue(new Callback<SharingBalanceResponse>() {
            @Override
            public void onResponse(Call<SharingBalanceResponse> call, Response<SharingBalanceResponse> response) {
                if (response.code() == 200) {
                    SharingBalanceResponse resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getSharingBalance().isEmpty()) {
                            datasInq = resp.getSharingBalance();
                            CheckMember(constructMemberExist());
                        } else {
                            mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                        }
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast("Koneksi gagal, silahkan Coba beberapa saat lagi");
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<SharingBalanceResponse> call, Throwable t) {
                mActivity.showToast("Koneksi gagal, silahkan Coba beberapa saat lagi");
                mActivity.dismissProgressDialog();
            }
        });
    }

    InquiryShareReq constructShareInq() {

        String qty = mActivity.etShareQty.getText().toString();


        InquiryShareReq req = new InquiryShareReq();
        req.setMerchantId(mActivity.merchantId);
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setNRecords(100);
        req.setPage(0);
        req.setQuantity(Integer.parseInt(qty));
        req.setSVoucherClass("D");
        req.setVoucherValue(Integer.parseInt(mActivity.amount));

        return req;
    }

}
