package com.ultravoucher.uv.ui.activities;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.biller.BillerMenuFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryWaletFragment;
import com.ultravoucher.uv.ui.fragments.purchase.PayByVoucherFragment;
import com.ultravoucher.uv.ui.fragments.setting.ChangePasswordFragment;
import com.ultravoucher.uv.ui.fragments.setting.ChangeProfileFragment;
import com.ultravoucher.uv.ui.fragments.setting.CreditFragment;
import com.ultravoucher.uv.ui.fragments.setting.HowToFragment;
import com.ultravoucher.uv.ui.fragments.share.ListShareVoucherDigitalFragment;

import butterknife.BindView;

public class GeneralActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    private static final String STATUS = "status";

    String status;

    public static void startActivity(BaseActivity sourceActivity, String status){
        Intent i = new Intent(sourceActivity, GeneralActivity.class);
        /*TAMBAHAN*/
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        /*END OF TAMBAHAN*/
        i.putExtra(STATUS, status);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getIntent().getStringExtra(STATUS);
        setupToolbar();
        if (status.equalsIgnoreCase("profile")) {
            ChangeProfileFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("addaddress")) {
            toolbarTitle.setText("Tambah Alamat");
        } else if (status.equalsIgnoreCase("listaddress")) {
            toolbarTitle.setText("Pilih Alamat");
        } else if (status.equalsIgnoreCase("changepass")) {
            ChangePasswordFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("share")) {
            ListShareVoucherDigitalFragment.showFragment(this, status);
        } else if (status.contains("pay")) {
//            ListShareVoucherDigitalFragment.showFragment(this, status);
            PayByVoucherFragment.showFragment(this);
        } else if (status.contains("fisik")) {
//            ListShareVoucherDigitalFragment.showFragment(this, status);
            PayByVoucherFragment.showFragment(this);
        }
        else if (status.equalsIgnoreCase("credit")) {
            CreditFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("howto")) {
            HowToFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("historywalet")) {
            HistoryWaletFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("historydigital")) {
            HistoryWaletFragment.showFragment(this);
        } else if (status.equalsIgnoreCase("biller")) {
            BillerMenuFragment.showFragment(this);
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (status.equalsIgnoreCase("profile")) {
            toolbarTitle.setText("Profil");
        } else if (status.equalsIgnoreCase("addaddress")) {
            toolbarTitle.setText("Tambah Alamat");
        } else if (status.equalsIgnoreCase("listaddress")) {
            toolbarTitle.setText("Pilih Alamat");
        } else if (status.equalsIgnoreCase("changepass")) {
            toolbarTitle.setText("Ganti Password");
        } else if (status.equalsIgnoreCase("share")) {
            toolbarTitle.setText("Pilih Voucher Digital");
        } else if (status.equalsIgnoreCase("pay")) {
            toolbarTitle.setText("Pilih Voucher Digital");
        } else if (status.equalsIgnoreCase("fisik")) {
            toolbarTitle.setText("Pilih Voucher Digital");
        } else if (status.equalsIgnoreCase("credit")) {
            toolbarTitle.setText("Credit");
        } else if (status.equalsIgnoreCase("howto")) {
            toolbarTitle.setText("Tata Cara Pickup");
        } else if (status.equalsIgnoreCase("historywalet")) {
            toolbarTitle.setText("History TOPUP");
        } else if (status.equalsIgnoreCase("biller")) {
            toolbarTitle.setText("Bayar");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
