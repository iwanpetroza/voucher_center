package com.ultravoucher.uv.ui.fragments.purchase;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.ListPaymentMethodMsgResp;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GeneralActivity;
import com.ultravoucher.uv.ui.adapters.ListPaymentMethodAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by firwandi.ramli on 12/27/2017.
 */

@RuntimePermissions
public class PaymentMethodFragment extends BaseFragment {

    private static final String TAG = PaymentMethodFragment.class.getSimpleName();

    private static final String STATUS = "status";
    private static final String VCLASS = "vclass";
    private static final String TYPE = "type";

    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    List<Order> orderDatas = new ArrayList<Order>();
    public void setOrderDatas(List<Order> orderDatas) {
        this.orderDatas.clear();
        this.orderDatas.addAll(orderDatas);
    }
    protected String orderNumber = "";
    String status, vClass, type;


    protected List<ListPaymentMethodMsgResp> mList = new ArrayList<>();
    PaymentMethodPresenter mPresenter;
    ListPaymentMethodAdapter mAdapter;

    public static void showFragment(BaseActivity sourceFragment, String status, String vClass, String type) {
        PaymentMethodFragment fragment = new PaymentMethodFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(STATUS, status);
        fragmentExtras.putString(VCLASS, vClass);
        fragmentExtras.putString(TYPE, type);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_paymethod;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = getArguments().getString(STATUS);
        vClass = getArguments().getString(VCLASS);
        type = getArguments().getString(TYPE);
        mPresenter = new PaymentMethodPresenter(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (type.equalsIgnoreCase("purchase")) {
            mPresenter.getOrder();
        }
        initRVPayMethod();
        loadPayMethodList();
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @NeedsPermission({android.Manifest.permission.READ_SMS})
    protected void initRVPayMethod() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new ListPaymentMethodAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                ListPaymentMethodMsgResp listPaymentMethodMsgResp = (ListPaymentMethodMsgResp) childView.getTag();
                if (listPaymentMethodMsgResp.getPaymentCode() == 5 && type.equalsIgnoreCase("purchase")) {
                    confirmDialog();
                } else if (listPaymentMethodMsgResp.getPaymentCode() == 6 && type.equalsIgnoreCase("purchase")) {
                    mPresenter.doPayOrderCC();
                } else if (listPaymentMethodMsgResp.getPaymentCode() == 2 && type.equalsIgnoreCase("purchase")) {

                    PrefHelper.setString(PrefKey.PAYCODE, "2");
                    GeneralActivity.startActivity((BaseActivity) getActivity(), "fisik");

                } else if (listPaymentMethodMsgResp.getPaymentCode() == 1 && type.equalsIgnoreCase("purchase")) {

                    PrefHelper.setString(PrefKey.PAYCODE, "1");
                    GeneralActivity.startActivity((BaseActivity) getActivity(), "fisik");

                } else if (listPaymentMethodMsgResp.getPaymentCode() == 4 && type.equalsIgnoreCase("purchase")) {

                    InsertPinDialogFragment.showFragment((BaseActivity) getActivity(), status, "purchase");

                } else if (listPaymentMethodMsgResp.getPaymentCode() == 4 && type.equalsIgnoreCase("biller")){
                    InsertPinDialogFragment.showFragment((BaseActivity) getActivity(), status, "biller");
                    PrefHelper.setInt(PrefKey.PAYCODE, 4);
                } else if (listPaymentMethodMsgResp.getPaymentCode() == 5 && type.equalsIgnoreCase("biller")){
                    PrefHelper.setInt(PrefKey.PAYCODE, 5);
                    confirmDialog(); //biller
                }

            }

        }));
    }

    private void loadPayMethodList() {

        mPresenter.presentGetListPayMethod(mPresenter.presentGetListReq());

    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(getActivity()), false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
        txtView.setText("Apakah Anda yakin ingin membayar dengan menggunakan VA?");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);

        final AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                if (type.equalsIgnoreCase("biller")){
                    mPresenter.getTransactionNumberBiller();
                } else if (type.equalsIgnoreCase("purchase")) {
                    mPresenter.doPayOrderVA();
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }
}
