package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.purchase.DetailProductFragment;
import com.ultravoucher.uv.ui.fragments.purchase.PaymentMethodFragment;

import butterknife.BindView;

public class DetailProductActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    String status, id, hal;
    private static final String STATUS = "status";
    private static final String ID = "id";
    private static final String HAL = "hal";

    public static void startActivity(BaseActivity sourceActivity, String flag, String status, String id){
        Intent i = new Intent(sourceActivity, DetailProductActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(STATUS, status);
        i.putExtra(ID, id);
        i.putExtra(HAL, flag);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_general;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        status = getIntent().getStringExtra(STATUS);
        id = getIntent().getStringExtra(ID);
        hal = getIntent().getStringExtra(HAL);
        setupToolbar();

        if (hal.equals("D")) {
            PaymentMethodFragment.showFragment(this, "", "", "purchase");
        } else {
            DetailProductFragment.showFragment(this, status, id);
        }
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Detail Produk");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                PrefHelper.setString(PrefKey.TOTAL, "");
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
