package com.ultravoucher.uv.ui.activities;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.ListDeliveryAddressMsgResp;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import butterknife.BindView;


/**
 * Created by asep.surahman on 21/02/2018.
 */

public class UpdateAddressActivity extends BaseActivity {

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.et_address)
    @NotEmpty
    EditText etAddress;

    @BindView(R.id.et_name)
    @NotEmpty
    EditText etName;

    @BindView(R.id.et_detail_address)
    @NotEmpty
    EditText etDetailAddress;

    @BindView(R.id.et_province)
    @NotEmpty
    AutoCompleteTextView etProvince;

    @BindView(R.id.et_city)
    @NotEmpty
    AutoCompleteTextView etCity;

    @BindView(R.id.et_district)
    @NotEmpty
    AutoCompleteTextView etDistrict;

    @BindView(R.id.et_region)
    @NotEmpty
    AutoCompleteTextView etRegion;

    @BindView(R.id.et_postal)
    @NotEmpty
    EditText etPostal;

    @BindView(R.id.et_phone_number)
    @NotEmpty
    EditText etPhoneNumber;

    ListDeliveryAddressMsgResp listDeliveryAddressMsgResp;


    static Bundle bundle;

    @Override
    protected int getLayout() {
        return R.layout.f_update_address;
    }


    public static void startActivity(BaseActivity sourceActivity, ListDeliveryAddressMsgResp listDeliveryAddressMsgResp){
        Intent i = new Intent(sourceActivity, UpdateAddressActivity.class);
        bundle = new Bundle();
        bundle.putSerializable("listAddress", listDeliveryAddressMsgResp);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtras(bundle);
        sourceActivity.startActivity(i);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setupToolbar();
        listDeliveryAddressMsgResp = (ListDeliveryAddressMsgResp) bundle.getSerializable("listAddress");
        setValueAddress();
    }


    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("Update Address");
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setValueAddress(){
        etName.setText(listDeliveryAddressMsgResp.getRecipientName());
        etAddress.setText(listDeliveryAddressMsgResp.getAddressAlias());
        etDetailAddress.setText(listDeliveryAddressMsgResp.getLine());
        etProvince.setText(listDeliveryAddressMsgResp.getProvince());
        etCity.setText(listDeliveryAddressMsgResp.getCity());
        etDistrict.setText(listDeliveryAddressMsgResp.getDistrict());
        etRegion.setText(listDeliveryAddressMsgResp.getVillage());
        etPostal.setText(listDeliveryAddressMsgResp.getPostalCode());
        etPhoneNumber.setText(listDeliveryAddressMsgResp.getMobileNumber());

    }

}
