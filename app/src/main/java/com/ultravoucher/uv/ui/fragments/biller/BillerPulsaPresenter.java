package com.ultravoucher.uv.ui.fragments.biller;

import com.google.gson.Gson;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.InisiateBillerResp;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tunggul.jati on 6/6/2018.
 */

public class BillerPulsaPresenter {

    BillerPulsaFragment mFragment;
    private String contractNo = "";

    public BillerPulsaPresenter(BillerPulsaFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getInisiateData(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId("");
        request.setPin("");
        request.setContractNo("");
        request.setBillerId("");
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InisiateBillerResp> respCall = mFragment.getVoucherApi().inisiateBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InisiateBillerResp>() {
            @Override
            public void onResponse(Call<InisiateBillerResp> call, Response<InisiateBillerResp> response) {
                if(response.code()==200)
                {
                    InisiateBillerResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL003"))
                    {
                        /**
                         * AMOUNT
                         */
                        mFragment.price_Axis_5 = resp.getAmount().getIdPulsaPreAxis5();
                        mFragment.price_Axis_10 = resp.getAmount().getIdPulsaPreAxis10();
                        mFragment.price_Axis_15 = resp.getAmount().getIdPulsaPreAxis15();
                        mFragment.price_Axis_25 = resp.getAmount().getIdPulsaPreAxis25();
                        mFragment.price_Axis_50 = resp.getAmount().getIdPulsaPreAxis50();
                        mFragment.price_Axis_100 = resp.getAmount().getIdPulsaPreAxis100();

                        mFragment.price_Indosat_5 = resp.getAmount().getIdPulsaPreIndosat5();
                        mFragment.price_Indosat_10 = resp.getAmount().getIdPulsaPreIndosat10();
                        mFragment.price_Indosat_25 = resp.getAmount().getIdPulsaPreIndosat25();
                        mFragment.price_Indosat_50 = resp.getAmount().getIdPulsaPreIndosat50();
                        mFragment.price_Indosat_100 = resp.getAmount().getIdPulsaPreIndosat100();

                        mFragment.price_XL_5 = resp.getAmount().getIdPulsaPreXL5();
                        mFragment.price_XL_10 = resp.getAmount().getIdPulsaPreXL10();
                        mFragment.price_XL_25 = resp.getAmount().getIdPulsaPreXL25();
                        mFragment.price_XL_50 = resp.getAmount().getIdPulsaPreXL50();
                        mFragment.price_XL_100 = resp.getAmount().getIdPulsaPreXL100();

                        mFragment.price_Telkomsel_5 = resp.getAmount().getIdPulsaPreTelkomsel5();
                        mFragment.price_Telkomsel_10 = resp.getAmount().getIdPulsaPreTelkomsel10();
                        mFragment.price_Telkomsel_25 = resp.getAmount().getIdPulsaPreTelkomsel25();
                        mFragment.price_Telkomsel_50 = resp.getAmount().getIdPulsaPreTelkomsel50();
                        mFragment.price_Telkomsel_100 = resp.getAmount().getIdPulsaPreTelkomsel100();

                        mFragment.price_Smartfren_5 = resp.getAmount().getIdPulsaPreSmartfren5();
                        mFragment.price_Smartfren_10 = resp.getAmount().getIdPulsaPreSmartfren10();
                        mFragment.price_Smartfren_20 = resp.getAmount().getIdPulsaPreSmartfren20();
                        mFragment.price_Smartfren_25 = resp.getAmount().getIdPulsaPreSmartfren25();
                        mFragment.price_Smartfren_50 = resp.getAmount().getIdPulsaPreSmartfren50();
                        mFragment.price_Smartfren_100 = resp.getAmount().getIdPulsaPreSmartfren100();


                        mFragment.price_three_5 = resp.getAmount().getIdPulsaPre_Three_5();
                        mFragment.price_three_10 = resp.getAmount().getIdPulsaPre_Three_10();
                        mFragment.price_three_20 = resp.getAmount().getIdPulsaPre_Three_20();
                        mFragment.price_three_50 = resp.getAmount().getIdPulsaPre_Three_50();
                        mFragment.price_three_100 = resp.getAmount().getIdPulsaPre_Three_100();
                        /**
                         * FEE
                         */
                        mFragment.fee_Axis_5 = resp.getFee().getIdPulsaPreAxis5();
                        mFragment.fee_Axis_10 = resp.getFee().getIdPulsaPreAxis10();
                        mFragment.fee_Axis_15 = resp.getFee().getIdPulsaPreAxis15();
                        mFragment.fee_Axis_25 = resp.getFee().getIdPulsaPreAxis25();
                        mFragment.fee_Axis_50 = resp.getFee().getIdPulsaPreAxis50();
                        mFragment.fee_Axis_100 = resp.getFee().getIdPulsaPreAxis100();

                        mFragment.fee_Indosat_5 = resp.getFee().getIdPulsaPreIndosat5();
                        mFragment.fee_Indosat_10 = resp.getFee().getIdPulsaPreIndosat10();
                        mFragment.fee_Indosat_25 = resp.getFee().getIdPulsaPreIndosat25();
                        mFragment.fee_Indosat_50 = resp.getFee().getIdPulsaPreIndosat50();
                        mFragment.fee_Indosat_100 = resp.getFee().getIdPulsaPreIndosat100();

                        mFragment.fee_XL_5 = resp.getFee().getIdPulsaPreXL5();
                        mFragment.fee_XL_10 = resp.getFee().getIdPulsaPreXL10();
                        mFragment.fee_XL_25 = resp.getFee().getIdPulsaPreXL25();
                        mFragment.fee_XL_50 = resp.getFee().getIdPulsaPreXL50();
                        mFragment.fee_XL_100 = resp.getFee().getIdPulsaPreXL100();

                        mFragment.fee_Telkomsel_5 = resp.getFee().getIdPulsaPreTelkomsel5();
                        mFragment.fee_Telkomsel_10 = resp.getFee().getIdPulsaPreTelkomsel10();
                        mFragment.fee_Telkomsel_25 = resp.getFee().getIdPulsaPreTelkomsel25();
                        mFragment.fee_Telkomsel_50 = resp.getFee().getIdPulsaPreTelkomsel50();
                        mFragment.fee_Telkomsel_100 = resp.getFee().getIdPulsaPreTelkomsel100();

                        mFragment.fee_Smartfren_5 = resp.getFee().getIdPulsaPreSmartfren5();
                        mFragment.fee_Smartfren_10 = resp.getFee().getIdPulsaPreSmartfren10();
                        mFragment.fee_Smartfren_20 = resp.getFee().getIdPulsaPreSmartfren20();
                        mFragment.fee_Smartfren_25 = resp.getFee().getIdPulsaPreSmartfren25();
                        mFragment.fee_Smartfren_50 = resp.getFee().getIdPulsaPreSmartfren50();
                        mFragment.fee_Smartfren_100 = resp.getFee().getIdPulsaPreSmartfren100();

                        mFragment.fee_three_5 = resp.getFee().getIdPulsaPre_Three_5();
                        mFragment.fee_three_10 = resp.getFee().getIdPulsaPre_Three_10();
                        mFragment.fee_three_20 = resp.getFee().getIdPulsaPre_Three_20();
                        mFragment.fee_three_50 = resp.getFee().getIdPulsaPre_Three_50();
                        mFragment.fee_three_100 = resp.getFee().getIdPulsaPre_Three_100();

                        /**
                         * ACTIVE
                         */
                        mFragment.active_Axis_5 = resp.getActive().getIdPulsaPreAxis5();
                        mFragment.active_Axis_10 = resp.getActive().getIdPulsaPreAxis10();
                        mFragment.active_Axis_15 = resp.getActive().getIdPulsaPreAxis15();
                        mFragment.active_Axis_25 = resp.getActive().getIdPulsaPreAxis25();
                        mFragment.active_Axis_50 = resp.getActive().getIdPulsaPreAxis50();
                        mFragment.active_Axis_100 = resp.getActive().getIdPulsaPreAxis100();

                        mFragment.active_Indosat_5 = resp.getActive().getIdPulsaPreIndosat5();
                        mFragment.active_Indosat_10 = resp.getActive().getIdPulsaPreIndosat10();
                        mFragment.active_Indosat_25 = resp.getActive().getIdPulsaPreIndosat25();
                        mFragment.active_Indosat_50 = resp.getActive().getIdPulsaPreIndosat50();
                        mFragment.active_Indosat_100 = resp.getActive().getIdPulsaPreIndosat100();

                        mFragment.active_XL_5 = resp.getActive().getIdPulsaPreXL5();
                        mFragment.active_XL_10 = resp.getActive().getIdPulsaPreXL10();
                        mFragment.active_XL_25 = resp.getActive().getIdPulsaPreXL25();
                        mFragment.active_XL_50 = resp.getActive().getIdPulsaPreXL50();
                        mFragment.active_XL_100 = resp.getActive().getIdPulsaPreXL100();

                        mFragment.active_Telkomsel_5 = resp.getActive().getIdPulsaPreTelkomsel5();
                        mFragment.active_Telkomsel_10 = resp.getActive().getIdPulsaPreTelkomsel10();
                        mFragment.active_Telkomsel_25 = resp.getActive().getIdPulsaPreTelkomsel25();
                        mFragment.active_Telkomsel_50 = resp.getActive().getIdPulsaPreTelkomsel50();
                        mFragment.active_Telkomsel_100 = resp.getActive().getIdPulsaPreTelkomsel100();

                        mFragment.active_Smartfren_5 = resp.getActive().getIdPulsaPreSmartfren5();
                        mFragment.active_Smartfren_10 = resp.getActive().getIdPulsaPreSmartfren10();
                        mFragment.active_Smartfren_20 = resp.getActive().getIdPulsaPreSmartfren20();
                        mFragment.active_Smartfren_25 = resp.getActive().getIdPulsaPreSmartfren25();
                        mFragment.active_Smartfren_50 = resp.getActive().getIdPulsaPreSmartfren50();
                        mFragment.active_Smartfren_100 = resp.getActive().getIdPulsaPreSmartfren100();

                        mFragment.active_three_5 = resp.getActive().getIdPulsaPre_Three_5();
                        mFragment.active_three_10 = resp.getActive().getIdPulsaPre_Three_10();
                        mFragment.active_three_20 = resp.getActive().getIdPulsaPre_Three_20();
                        mFragment.active_three_50 = resp.getActive().getIdPulsaPre_Three_50();
                        mFragment.active_three_100 = resp.getActive().getIdPulsaPre_Three_100();

                        /**
                         * NOMINAL / PRODUCT VALUE
                         */
                        mFragment.pv_Axis_5 = resp.getProductValue().getIdPulsaPreAxis5();
                        mFragment.pv_Axis_10 = resp.getProductValue().getIdPulsaPreAxis10();
                        mFragment.pv_Axis_15 = resp.getProductValue().getIdPulsaPreAxis15();
                        mFragment.pv_Axis_25 = resp.getProductValue().getIdPulsaPreAxis25();
                        mFragment.pv_Axis_50 = resp.getProductValue().getIdPulsaPreAxis50();
                        mFragment.pv_Axis_100 = resp.getProductValue().getIdPulsaPreAxis100();

                        mFragment.pv_Indosat_5 = resp.getProductValue().getIdPulsaPreIndosat5();
                        mFragment.pv_Indosat_10 = resp.getProductValue().getIdPulsaPreIndosat10();
                        mFragment.pv_Indosat_25 = resp.getProductValue().getIdPulsaPreIndosat25();
                        mFragment.pv_Indosat_50 = resp.getProductValue().getIdPulsaPreIndosat50();
                        mFragment.pv_Indosat_100 = resp.getProductValue().getIdPulsaPreIndosat100();

                        mFragment.pv_XL_5 = resp.getProductValue().getIdPulsaPreXL5();
                        mFragment.pv_XL_10 = resp.getProductValue().getIdPulsaPreXL10();
                        mFragment.pv_XL_25 = resp.getProductValue().getIdPulsaPreXL25();
                        mFragment.pv_XL_50 = resp.getProductValue().getIdPulsaPreXL50();
                        mFragment.pv_XL_100 = resp.getProductValue().getIdPulsaPreXL100();

                        mFragment.pv_Telkomsel_5 = resp.getProductValue().getIdPulsaPreTelkomsel5();
                        mFragment.pv_Telkomsel_10 = resp.getProductValue().getIdPulsaPreTelkomsel10();
                        mFragment.pv_Telkomsel_25 = resp.getProductValue().getIdPulsaPreTelkomsel25();
                        mFragment.pv_Telkomsel_50 = resp.getProductValue().getIdPulsaPreTelkomsel50();
                        mFragment.pv_Telkomsel_100 = resp.getProductValue().getIdPulsaPreTelkomsel100();

                        mFragment.pv_Smartfren_5 = resp.getProductValue().getIdPulsaPreSmartfren5();
                        mFragment.pv_Smartfren_10 = resp.getProductValue().getIdPulsaPreSmartfren10();
                        mFragment.pv_Smartfren_20 = resp.getProductValue().getIdPulsaPreSmartfren20();
                        mFragment.pv_Smartfren_25 = resp.getProductValue().getIdPulsaPreSmartfren25();
                        mFragment.pv_Smartfren_50 = resp.getProductValue().getIdPulsaPreSmartfren50();
                        mFragment.pv_Smartfren_100 = resp.getProductValue().getIdPulsaPreSmartfren100();

                        mFragment.pv_three_5 = resp.getProductValue().getIdPulsaPreThree5();
                        mFragment.pv_three_10 = resp.getProductValue().getIdPulsaPreThree10();
                        mFragment.pv_three_20 = resp.getProductValue().getIdPulsaPreThree20();
                        mFragment.pv_three_50 = resp.getProductValue().getIdPulsaPreThree50();
                        mFragment.pv_three_100 = resp.getProductValue().getIdPulsaPreThree100();

                        mFragment.inisiateHit();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
//                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InisiateBillerResp> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }

    void getCheckBiller(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin("");
        /*test*/
        if (mFragment.praPascaBayar.equalsIgnoreCase("prabayar")){
            contractNo = mFragment.etIdPel.getText().toString();
        } else if (mFragment.praPascaBayar.equalsIgnoreCase("pascabayar")){
            contractNo = mFragment.etIdPelPasca.getText().toString();
        }
        request.setContractNo(contractNo);
        request.setBillerId(mFragment.billerId);
        /*test*/
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayBillerResponse> respCall = mFragment.getVoucherApi().getCheckBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayBillerResponse>() {
            @Override
            public void onResponse(Call<InqPayBillerResponse> call, Response<InqPayBillerResponse> response) {
                if(response.code()==200)
                {
                    InqPayBillerResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL001"))
                    {
                        /*test*/
//                        PrefHelper.clearPreference(PrefKey.BILLER_ID);
//                        mFragment.orderNumber = resp.getTransactionId();
//                        PrefHelper.setString(PrefKey.TRX_ID, resp.getTransactionId());
                        mFragment.price = resp.getPrice();
                        mFragment.fee = resp.getFee();
                        mFragment.totalAmount = resp.getTotalAmount();
                        mFragment.afterHit();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
//                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayBillerResponse> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }
}
