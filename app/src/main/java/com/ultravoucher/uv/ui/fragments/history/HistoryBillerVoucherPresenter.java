package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.BillerVoucherHistoryMsgResp;
import com.ultravoucher.uv.data.api.request.HistoryProgressReq;
import com.ultravoucher.uv.data.api.response.BillerVoucherHistoryResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerVoucherPresenter {

    private HistoryBillerVoucherFragment mFragment;

    public HistoryBillerVoucherPresenter(HistoryBillerVoucherFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetDigitalHistory(HistoryProgressReq req) {
        mFragment.rvList.setVisibility(View.GONE);

        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<BillerVoucherHistoryResponse> historyProgressResponseCall = api.getHistoryBV(contentType, authToken, deviceUId, req);
        historyProgressResponseCall.enqueue(new Callback<BillerVoucherHistoryResponse>() {
            @Override
            public void onResponse(Call<BillerVoucherHistoryResponse> call, Response<BillerVoucherHistoryResponse> response) {
                if (response.code() == 200) {
                    BillerVoucherHistoryResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("OD006")) {
                        if (!resp.getOrderHistory().isEmpty()) {
                            initList(resp.getOrderHistory());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<BillerVoucherHistoryResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }


    protected void presentGetDigitalHistoryLoadMore(HistoryProgressReq req) {

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<BillerVoucherHistoryResponse> historyProgressResponseCall = api.getHistoryBV(contentType, authToken, deviceUId, req);
        historyProgressResponseCall.enqueue(new Callback<BillerVoucherHistoryResponse>() {
            @Override
            public void onResponse(Call<BillerVoucherHistoryResponse> call, Response<BillerVoucherHistoryResponse> response) {
                if (response.code() == 200) {
                    BillerVoucherHistoryResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        if (!resp.getOrderHistory().isEmpty()) {
                            addToList(resp.getOrderHistory());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.mList.removeAll(Collections.singleton(null));
                    mFragment.mAdapter.notifyDataSetChanged();
                    mFragment.mAdapter.setLoaded(true);
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BillerVoucherHistoryResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
//                    String message = mFragment.getString(R.string.connection_error);
//                    mFragment.showToast(message);

                }
            }
        });
    }


    protected void initList(List<BillerVoucherHistoryMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<BillerVoucherHistoryMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (BillerVoucherHistoryMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected HistoryProgressReq presentGetHistoryReq() {
        HistoryProgressReq req = new HistoryProgressReq();
        req.setPage(mFragment.page);
        req.setNRecords(50);
        req.setsVoucherClass("D");
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        return req;
    }
}
