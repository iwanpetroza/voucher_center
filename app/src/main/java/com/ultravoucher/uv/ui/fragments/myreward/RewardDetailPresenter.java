package com.ultravoucher.uv.ui.fragments.myreward;

import android.app.Dialog;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.ReedemRewardReq;
import com.ultravoucher.uv.data.api.request.RewardDetailReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.RedeemRewardResp;
import com.ultravoucher.uv.data.api.response.RewardDetailResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class RewardDetailPresenter {

    private RewardDetailFragment mContext;

    public RewardDetailPresenter(RewardDetailFragment mContext) {
        this.mContext = mContext;
    }

    void presentRedeemRewardResp(ReedemRewardReq req) {

        mContext.showProgressDialog(mContext.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        VoucherApi api = mContext.getVoucherApi();
        Call<RedeemRewardResp> redeemRewardRespCall = api.getRedeemReward(authToken, deviceUniqueId, req);
        redeemRewardRespCall.enqueue(new Callback<RedeemRewardResp>() {
            @Override
            public void onResponse(Call<RedeemRewardResp> call, Response<RedeemRewardResp> response) {
                RedeemRewardResp resp = response.body();

                if (response.code() == 200 && response.body() != null) {
                    if (resp.getAbstractResponse().getResponseStatus().equals("RWRED000")) {
                        // custom dialog
                        final Dialog dialog = new Dialog(mContext, R.style.Voucher_CustomDialog);
                        dialog.setContentView(R.layout.d_redeem_reward);

                        // set the custom dialog components - text, image and button
                        String message = "Selamat\n" +
                                "\n" +
                                "Reward Anda berhasil di Redeem\n" +
                                "\n" +
                                "Silahkan Cek Tab Rewardku\n" +
                                "\n" +
                                "Terima Kasih\n";
                        TextView text = (TextView) dialog.findViewById(R.id.redeemreward_tv_message);
                        text.setText(message);
                        Button closeButton = (Button) dialog.findViewById(R.id.redeemreward_btn_close);
                        // if button is clicked, close the custom dialog
                        closeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                mContext.finish();
                            }
                        });

                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.show();
                            }
                        });
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mContext.showToast("Sesi Anda telah berakhir, Silahkan Login ulang");
                        mContext.finish();
                        mContext.doNeedRelogin();
                    }

                    else {
                        String message = "Maaf, Poin Anda tidak mencukupi";
                        mContext.showToast(message);
                    }
                } else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if (mContext != null) {
                                mContext.doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (mContext != null) {
                        String message = mContext.getString(R.string.connection_error);
                        mContext.showToast(message);
                    }
                }
                mContext.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<RedeemRewardResp> call, Throwable t) {
                String message = mContext.getString(R.string.connection_error);
                mContext.dismissProgressDialog();
                mContext.showToast(message);
            }
        });
    }

    void getRewardDetail(RewardDetailReq request)
    {
        mContext.showProgressDialog("Please Waiting");
        Call<RewardDetailResp> respCall = mContext.getVoucherApi().getRewardDetailById(request);
        respCall.enqueue(new Callback<RewardDetailResp>() {
            @Override
            public void onResponse(Call<RewardDetailResp> call, Response<RewardDetailResp> response) {
                mContext.dismissProgressDialog();
                if(response.code()==200)
                {
                    RewardDetailResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("AUTH000"))
                    {

                        Picasso.with(mContext).load(resp.getRewardResponse().get(0).getProduct().getProductImage()).placeholder(R.drawable.image_placeholder)
                                .centerCrop().fit().into(mContext.detailLogo);

                        mContext.tvSaleprice.setText("" + resp.getRewardResponse().get(0).getRequiredPts() + " Points");
                        mContext.tvName.setText(resp.getRewardResponse().get(0).getRewardName());

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            mContext.tvDescription.setText(Html.fromHtml(resp.getRewardResponse().get(0).getRewardDescription().replace("\t", ""), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            mContext.tvDescription.setText(Html.fromHtml(resp.getRewardResponse().get(0).getRewardDescription().replace("\t", "")));
                        }

//                        mContext.tvSyaratDesc.setText(resp.getRewardResponse().get(0).getRewardTermAndCondition());
                    }
                    else
                    {
                        mContext.tvDescription.setText(Html.fromHtml("<center><h3>Detail Is Not Found</h3></center>"));
                    }
                }
                else
                {
                    mContext.tvDescription.setText(Html.fromHtml("<center><h3>Server Is Down</h3></center>"));
                }

            }

            @Override
            public void onFailure(Call<RewardDetailResp> call, Throwable t) {
                mContext.dismissProgressDialog();
                mContext.tvDescription.setText(Html.fromHtml("<center><h3>Please Check Your Connection</h3></center>"));
            }
        });

    }

    RewardDetailReq constructReq()
    {
        RewardDetailReq data = new RewardDetailReq();
        data.setRewardId(mContext.mRewardData.getRewardId());
        return data;
    }

    ReedemRewardReq presentRedeemRewardReq() {
        ReedemRewardReq request = new ReedemRewardReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setRewardId(mContext.mRewardData.getRewardId());
        request.setQuantity(1);
        request.setMerchantId(mContext.mRewardData.getProduct().getMerchantId());
        request.setActor("Member");
        request.setPosTransactionId("");
        request.setStoreId("");
        return request;
    }
}
