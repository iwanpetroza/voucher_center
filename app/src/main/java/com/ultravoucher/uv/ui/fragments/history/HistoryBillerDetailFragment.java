package com.ultravoucher.uv.ui.fragments.history;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.HistoryBillerDetailMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.BillerDetailHistoryAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = HistoryBillerDetailFragment.class.getSimpleName();

    private static final String TRXID = "trxid";

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;


    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;
    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    String trxId;
    private Handler mHandler;
    protected List<HistoryBillerDetailMsgResp> mList = new ArrayList<>();
    BillerDetailHistoryAdapter mAdapter;
    HistoryBillerDetailPresenter mPresenter;

    public static void showFragment(BaseActivity sourceFragment, String trxId) {
        HistoryBillerDetailFragment fragment = new HistoryBillerDetailFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(TRXID, trxId);
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        trxId = getArguments().getString(TRXID);
        mPresenter = new HistoryBillerDetailPresenter(this);

    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);
        initRVMyHistoryDeals();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                rvList.setVisibility(View.GONE);
                mPresenter.getHistoryBillerDetail(mPresenter.getHistoryBillerDetailReq());
            }
        });

    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new BillerDetailHistoryAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);

            }

        }));

        /*mHandler = new Handler();
        mAdapter.setOnLoadMoreListener(new BillerDetailHistoryAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreHistoryDetail();
                    }
                }, 2000);
            }
        });*/
    }



    @Override
    public void onResume() {
        super.onResume();
        page = 0;
    }

    @Override
    public void onRefresh() {
        page = 0;
        rvList.setVisibility(View.GONE);
    }

    private void loadMoreHistoryDetail() {
        mPresenter.getHistoryBillerDetailLoadMore(mPresenter.getHistoryBillerDetailReq());
    }
}