package com.ultravoucher.uv.ui.fragments.history;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.HistoryWalet;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.HistoryWaletAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 2/9/2018.
 */

public class HistoryWaletFragment extends BaseFragment {

    private static final String TAG = HistoryWaletFragment.class.getSimpleName();

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;
    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    private Handler mHandler;
    protected List<HistoryWalet> mList = new ArrayList<>();
    HistoryWaletAdapter mAdapter;
    HistoryWaletPresenter mPresenter;
    String pass = "";

    public static void showFragment(BaseActivity sourceFragment) {
        HistoryWaletFragment fragment = new HistoryWaletFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        mPresenter = new HistoryWaletPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);

        HistoryWaletPinDialog dialog = new HistoryWaletPinDialog();
        dialog.setCancelable(false);
        dialog.setHistoryListener(new HistoryWaletPinDialog.HistoryWaletPinListener() {
            @Override
            public void onBtnClick(String pass) {
                HistoryWaletFragment.this.pass = pass;
                page = 0;
                initRVMyHistoryDeals();
                loadHistoryWalet();
            }
        });
        dialog.show(getFragmentManager(), "history_dialog");
    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new HistoryWaletAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);

            }
        }));

        /*mHandler = new Handler();

        mAdapter.setOnLoadMoreListener(new HistoryWaletAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mList.removeAll(Collections.singleton(null));
                mList.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        load();
                    }
                }, 2000);

            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        /*page = 0;
        initRVMyHistoryDeals();
        loadHistoryWalet();*/
    }

    private void loadHistoryWalet() {
        mPresenter.presentGetHistoryWalet(mPresenter.presentGetHistoryWalet());
    }
}