package com.ultravoucher.uv.ui.fragments.setting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 11/1/2017.
 */

public class ChangePasswordFragment extends BaseFragment implements Validator.ValidationListener{

    private static final String TAG = ChangePasswordFragment.class.getSimpleName();

    @BindView(R.id.etOldPass)
    @NotEmpty
    EditText etOldPass;

    @BindView(R.id.etNewPass)
    EditText etNewPass;

    @BindView(R.id.etReNewPass)
    EditText etReNewPass;

    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    ChangePasswordPresenter mPresenter;
    Validator mValidator;

    public static void showFragment(BaseActivity sourceFragment) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_change_password;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPresenter = new ChangePasswordPresenter(this);
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @OnClick(R.id.changePass_btn_simpan)
    public void onViewClicked() {
        String pass = etNewPass.getText().toString();
        String confirm = etReNewPass.getText().toString();

        if (pass.trim().length() > 5) {
            if (pass.equals(confirm)) {
                mValidator.validate();
            } else {
                showToast("Konfirmasi password Anda tidak sesuai");
            }
        } else {
            showToast("Password minimum 6 karakter");
        }
    }

    @Override
    public void onValidationSucceeded() {
        mPresenter.doChangePass(mPresenter.constructChangePass());

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }
}
