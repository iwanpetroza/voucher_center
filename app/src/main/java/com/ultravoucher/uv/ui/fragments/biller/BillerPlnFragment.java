package com.ultravoucher.uv.ui.fragments.biller;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.PayMethodActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.forgotpassword.ForgotPasswordFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.PopupAdvDialogFragment;
import com.ultravoucher.uv.utils.AmountFormatter2;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tunggul.jati on 5/18/2018.
 */

public class BillerPlnFragment extends BaseFragment implements Validator.ValidationListener {

    private static final String TAG = BillerPlnFragment.class.getSimpleName();

    @BindView(R.id.btn_list_fav)
    Button btn_list_fav;

    @BindView(R.id.cbFav)
    CheckBox cbFav;

    @BindView(R.id.rl_all)
    RelativeLayout rl_all;

    @BindView(R.id.tvPilihan)
    TextView tvPilihan;

    @BindView(R.id.tvNominal)
    TextView tvNominal;

    @BindView(R.id.tvHarga)
    TextView tvHarga;

    @BindView(R.id.tvFee)
    TextView tvFee;

    @BindView(R.id.mycart_tv_checkout)
    TextView mycart_tv_checkout;

    @BindView(R.id.mycart_btn_checkout)
    LinearLayout mycart_btn_checkout;

    @BindView(R.id.llNominal)
    LinearLayout llNominal;

    @BindView(R.id.etIdPel)
    @Length(min = 10, max = 15,message = "Harap isi data valid")
    EditText etIdPel;

    @BindView(R.id.llCekTagihan)
    LinearLayout llCekTagihan;

    @BindView(R.id.llTagihan)
    LinearLayout llTagihan;

    @BindView(R.id.llAtasNama)
    LinearLayout llAtasNama;

    @BindView(R.id.tvTagihan)
    TextView tvTagihan;

    @BindView(R.id.tvAtasNama)
    TextView tvAtasNama;

    private Validator mValidator;
    BillerPlnPresenter mPresenter;

    /*LIST BILLER ID*/
    private String idPlnPost = "3";
    private String idPlnPre_20 = "4";
    private String idPlnPre_50 = "5";
    private String idPlnPre_100 = "6";
    private String idPlnPre_200 = "7";
    private String idPlnPre_500 = "8";
    private String idPlnPre_1000 = "9";

    /*LIST NOMINAL*/
    private int duaPuluhRb = 20000;
    private int limaPuluhRb = 50000;
    private int seratusRb = 100000;
    private int duaRatusRb = 200000;
    private int limaRatusRb = 500000;
    private int satuJt = 1000000;

    /*LIST PRICE*/
    public int price20 = 0;
    public int price50 = 0;
    public int price100 = 0;
    public int price200 = 0;
    public int price500 = 0;
    public int price1jt = 0;

    /*LIST FEE*/
    public int fee20 = 0;
    public int fee50 = 0;
    public int fee100 = 0;
    public int fee200 = 0;
    public int fee500 = 0;
    public int fee1jt = 0;

    /*LIST ACTIVE*/
    public int active20;
    public int active50;
    public int active100;
    public int active200;
    public int active500;
    public int active1jt;

    /*CHOOSED BY SPINNER*/
    /*private int choosedPrice = price20; //inisiate default spinner data
    private int choosedFee = fee20; //inisiate default spinner data
    private int choosedNominal = duaPuluhRb; //inisiate default spinner data
    public String billerId = idPlnPre_20; //inisiate default spinner data*/
    private int choosedPrice = 0; //inisiate default spinner data
    private int choosedFee = 0; //inisiate default spinner data
    private int choosedNominal = 0; //inisiate default spinner data
    public String billerId = ""; //inisiate default spinner data

    private String praPascaBayar = "prabayar";
    public String price = "";
    public String fee = "";
    public String totalAmount = "";
    public String atasNama = "";
    private boolean isChecked = false;
    private boolean isChoosed = false;
    private boolean isFav = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mValidator = new Validator(this);
        mPresenter = new BillerPlnPresenter(this);
        mValidator.setValidationListener(this);
        PrefHelper.setBoolean(PrefKey.IS_FAV,false);
    }

    public static void showFragment(BaseActivity sourceFragment) {
        BillerPlnFragment fragment = new BillerPlnFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
//        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cbFav.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    isFav = true;
                } else {
                    isFav = false;
                }
                Log.d(TAG, "onCheckedChanged: "+isFav);
            }
        });
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
        rl_all.setVisibility(View.GONE);
        mPresenter.getInisiateData();
        etIdPel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
                    tvHarga.setText("Rp " + AmountFormatter2.format(0));
                    tvFee.setText("Rp " + AmountFormatter2.format(0));
                    mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                    isChecked = false;
                    mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                    llTagihan.setVisibility(View.GONE);
                    llAtasNama.setVisibility(View.GONE);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void inisiateHit(){
        llCekTagihan.setVisibility(View.GONE);
        llAtasNama.setVisibility(View.GONE);
        llTagihan.setVisibility(View.GONE);
        tvNominal.setText("Rp " + AmountFormatter2.format(choosedNominal));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPrice));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFee));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedNominal+choosedFee));
        rl_all.setVisibility(View.VISIBLE);
    }

    public void afterHit(){
        tvAtasNama.setText(atasNama);
        tvTagihan.setText("Rp " + AmountFormatter2.format(Double.parseDouble(price)));
        tvHarga.setText("Rp " + AmountFormatter2.format(Double.parseDouble(price)));
        tvFee.setText("Rp " + AmountFormatter2.format(Double.parseDouble(fee)));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(Double.parseDouble(totalAmount)));
        llTagihan.setVisibility(View.VISIBLE);
        llAtasNama.setVisibility(View.VISIBLE);
        isChecked = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
    }

    @Override
    protected int getLayout() {
        return R.layout.f_biller_pln;
    }

    @OnClick(R.id.btn_list_fav)
    public void getFav() {
        PrefHelper.setInt(PrefKey.CATEGORY, 1); //PLN=1 PULSA=2
        BillerFavoriteDialogFragment.showFragment(this);
    }

    public void afterFav(){
        etIdPel.setText(PrefHelper.getString(PrefKey.CONTRACT_NO_FAV));
    }

    @OnClick(R.id.btnTagihan)
    public void doCheck() {
        mValidator.validate();

//        mPresenter.getCheckBiller();
    }

    @OnClick(R.id.mycart_btn_checkout)
    public void doBayar() {
        if (praPascaBayar.equalsIgnoreCase("pascabayar")){
            if (isChecked) {
                PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
                PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
                PrefHelper.setString(PrefKey.BILLER_ID, billerId);
                PrefHelper.setBoolean(PrefKey.IS_FAV, isFav);
                isChecked = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                Log.d(TAG, "doBayar: "+isFav);
            }
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")) {
            if (isChoosed) {
                mValidator.validate();
                isChoosed = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
            }
        }

//        mValidator.validate();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (praPascaBayar.equalsIgnoreCase("pascabayar")) {
            llTagihan.setVisibility(View.GONE);
            llAtasNama.setVisibility(View.GONE);
        }
        choosedNominal = 0;
        choosedPrice = 0;
        choosedFee = 0;
        tvNominal.setText("Rp " + AmountFormatter2.format(choosedNominal));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPrice));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFee));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedNominal + choosedFee));
        isChecked = false;
        isChoosed = false;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
        cbFav.setChecked(false);
        isFav = false;
    }

    @Override
    public void onValidationSucceeded() {
        if (praPascaBayar.equalsIgnoreCase("pascabayar")){
            mPresenter.getCheckBiller();
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")){
            PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
            PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
            PrefHelper.setString(PrefKey.BILLER_ID, billerId);
            PrefHelper.setBoolean(PrefKey.IS_FAV, isFav);
            Log.d(TAG, "onValidationSucceeded: "+isFav);
        }

        /*if (praPascaBayar.equalsIgnoreCase("pascabayar")){
            if (isChecked) {
                PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
                PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
                PrefHelper.setString(PrefKey.BILLER_ID, billerId);
                isChecked = false;
            }
        } else if (praPascaBayar.equalsIgnoreCase("prabayar")){
            PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
            PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
            PrefHelper.setString(PrefKey.BILLER_ID, billerId);
        }*/


        /*PayMethodActivity.startActivity((BaseActivity) getActivity(), "", "3", "biller");
        PrefHelper.setString(PrefKey.CONTRACT_NO, etIdPel.getText().toString());
        PrefHelper.setString(PrefKey.BILLER_ID, billerId);*/
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.llPilihan)
    public void showDialogPilihan() {
        dialogPilihan();
    }

    @OnClick(R.id.llNominal)
    public void showDialogNominal() {
        dialogNominal();
    }

    private void dialogPilihan(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_pilihan);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btnToken = (LinearLayout) dialog.findViewById(R.id.llToken);
        LinearLayout btnTagihan = (LinearLayout) dialog.findViewById(R.id.llTagihan);
        btnToken.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                tvPilihan.setText(R.string.token);
                choosedNominal = 0;
                choosedPrice = 0;
                choosedFee = 0;
                etIdPel.setText("");
                tvNominal.setText("Rp " + AmountFormatter2.format(choosedNominal));
                tvHarga.setText("Rp " + AmountFormatter2.format(choosedPrice));
                tvFee.setText("Rp " + AmountFormatter2.format(choosedFee));
                mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedNominal+choosedFee));
                llCekTagihan.setVisibility(View.GONE);
                llAtasNama.setVisibility(View.GONE);
                llTagihan.setVisibility(View.GONE);
                llNominal.setVisibility(View.VISIBLE);
                billerId = "";
                isChoosed = false;
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                praPascaBayar = "prabayar";
                dialog.dismiss();
            }
        });
        btnTagihan.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                tvPilihan.setText(R.string.tagihan);
                llCekTagihan.setVisibility(View.VISIBLE);
                llAtasNama.setVisibility(View.GONE);
                llTagihan.setVisibility(View.GONE);
                llNominal.setVisibility(View.GONE);
                choosedNominal = 0;
                choosedPrice = 0;
                choosedFee = 0;
                etIdPel.setText("");
                tvNominal.setText("Rp " + AmountFormatter2.format(0));
                tvHarga.setText("Rp " + AmountFormatter2.format(0));
                tvFee.setText("Rp " + AmountFormatter2.format(0));
                mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(0));
                billerId = idPlnPost;
                praPascaBayar = "pascabayar";
                //test
                mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor2));
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dialogNominal(){
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.d_biller_nominal);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        LinearLayout btn1 = (LinearLayout) dialog.findViewById(R.id.ll1);
        LinearLayout btn2 = (LinearLayout) dialog.findViewById(R.id.ll2);
        LinearLayout btn3 = (LinearLayout) dialog.findViewById(R.id.ll3);
        LinearLayout btn4 = (LinearLayout) dialog.findViewById(R.id.ll4);
        LinearLayout btn5 = (LinearLayout) dialog.findViewById(R.id.ll5);
        LinearLayout btn6 = (LinearLayout) dialog.findViewById(R.id.ll6);
        TextView tv1 = (TextView) dialog.findViewById(R.id.nom1);
        TextView tv2 = (TextView) dialog.findViewById(R.id.nom2);
        TextView tv3 = (TextView) dialog.findViewById(R.id.nom3);
        TextView tv4 = (TextView) dialog.findViewById(R.id.nom4);
        TextView tv5 = (TextView) dialog.findViewById(R.id.nom5);
        TextView tv6 = (TextView) dialog.findViewById(R.id.nom6);
        tv1.setText(R.string.rp20);
        tv2.setText(R.string.rp50);
        tv3.setText(R.string.rp100);
        tv4.setText(R.string.rp200);
        tv5.setText(R.string.rp500);
        tv6.setText(R.string.rp1000);
        if (active20 == -1){
            btn1.setVisibility(View.VISIBLE);
        } else {
            btn1.setVisibility(View.GONE);
        }
        if (active50 == -1){
            btn2.setVisibility(View.VISIBLE);
        } else {
            btn2.setVisibility(View.GONE);
        }
        if (active100 == -1){
            btn3.setVisibility(View.VISIBLE);
        } else {
            btn3.setVisibility(View.GONE);
        }
        if (active200 == -1){
            btn4.setVisibility(View.VISIBLE);
        } else {
            btn4.setVisibility(View.GONE);
        }
        if (active500 == -1){
            btn5.setVisibility(View.VISIBLE);
        } else {
            btn5.setVisibility(View.GONE);
        }
        if (active1jt == -1){
            btn6.setVisibility(View.VISIBLE);
        } else {
            btn6.setVisibility(View.GONE);
        }
        btn1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(duaPuluhRb, price20, fee20, idPlnPre_20);
                dialog.dismiss();
            }
        });
        btn2.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(limaPuluhRb, price50, fee50, idPlnPre_50);
                dialog.dismiss();
            }
        });
        btn3.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(seratusRb, price100, fee100, idPlnPre_100);
                dialog.dismiss();
            }
        });
        btn4.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(duaRatusRb, price200, fee200, idPlnPre_200);
                dialog.dismiss();
            }
        });
        btn5.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(limaRatusRb, price500, fee500, idPlnPre_500);
                dialog.dismiss();
            }
        });
        btn6.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                dataSpinner(satuJt, price1jt, fee1jt, idPlnPre_1000);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void dataSpinner(int nominal, int price, int fee, String biller){
        choosedNominal = nominal;
        choosedPrice = price;
        choosedFee = fee;
        tvNominal.setText("Rp " + AmountFormatter2.format(choosedNominal));
        tvHarga.setText("Rp " + AmountFormatter2.format(choosedPrice));
        tvFee.setText("Rp " + AmountFormatter2.format(choosedFee));
        mycart_tv_checkout.setText("Bayar Rp " + AmountFormatter2.format(choosedNominal+choosedFee));
        billerId = biller;
        isChoosed = true;
        mycart_btn_checkout.setBackgroundColor(getResources().getColor(R.color.newbluecolor));
    }
}
