package com.ultravoucher.uv.ui.fragments.biller;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.data.api.beans.ContractFavorite;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.adapters.AdvBannerMainPageAdapter2;
import com.ultravoucher.uv.ui.adapters.BillerFavoriteAdapter;
import com.ultravoucher.uv.ui.adapters.BillerHistoryAdapter;
import com.ultravoucher.uv.ui.fragments.BaseDialogFragment;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.PopUpAdvDialogPresenter;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tunggul.jati on 7/11/2018.
 */

public class BillerFavoriteDialogFragment extends BaseDialogFragment {

    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;

    protected int page = 0;
    private Handler mHandler;
    protected List<ContractFavorite> mList = new ArrayList<>();
    BillerFavoriteAdapter mAdapter;
    BillerFavoriteDialogPresenter mPresenter;

    private BillerPlnFragment sourceFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = 0;
        mPresenter = new BillerFavoriteDialogPresenter(this);

    }

    @Override
    protected int getLayout() {
        return R.layout.d_favorite_biller;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.getFavoriteList(mPresenter.constructBillerGetFavorite());
//        rl.setVisibility(View.GONE);
        initRVMyHistoryDeals();
    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new BillerFavoriteAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                ContractFavorite contract = (ContractFavorite) childView.getTag();
                PrefHelper.setString(PrefKey.CONTRACT_NO_FAV, contract.getContractNo());
                sourceFragment.afterFav();
                dismiss();
            }

        }));
    }

    public static void showFragment(BillerPlnFragment sourceFragment) {
        BillerFavoriteDialogFragment fragment = new BillerFavoriteDialogFragment();
        fragment.sourceFragment = sourceFragment;
        fragment.show(sourceFragment.getFragmentManager(), "");
//        fragment.show(sourceFragment.getSupportFragmentManager(), "");
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
    }
}
