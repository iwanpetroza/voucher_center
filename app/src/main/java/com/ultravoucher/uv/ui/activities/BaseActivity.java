package com.ultravoucher.uv.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.VoucherApplication;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.prefs.PrefHelper;

import butterknife.BindString;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by firwandi.ramli on 9/25/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
    @BindString(R.string.loading)
    public String LOADING;

    @BindString(R.string.connection_error)
    public String CONNECTION_ERROR;

    private ProgressDialog mProgressDialog;
    private boolean mIsRunning = false;

    protected abstract int getLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Fabric Init
        Fabric.with(this, new Crashlytics());
        mIsRunning = true;
        setContentView(getLayout());
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this, R.style.GenericProgressDialogStyle);
    }

    @Override
    protected void onDestroy() {
        mProgressDialog.dismiss();
        mIsRunning = false;
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showProgressDialog(String message) {
        if (mIsRunning) {
            mProgressDialog.setMessage(message);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    public void dismissProgressDialog() {
        mProgressDialog.hide();
    }

    public void showToast(String msg) {
        if (mIsRunning) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void doNeedRelogin() {
        PrefHelper.clearAllPreferences();
        LoginActivity.startActivity(this, true);
    }

    public VoucherApi getVoucherApi() {
        return VoucherApplication.getInstance().getVoucherApi();
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
