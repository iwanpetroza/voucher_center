package com.ultravoucher.uv.ui.fragments.purchase;

import com.google.gson.Gson;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.request.InqPayOrderReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.api.response.InqPayOrderResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.fragments.biller.BillerInsertOTPDialogFragment;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 1/31/2018.
 */

public class InsertPinDialogPresenter {

    InsertPinDialogFragment mFragment;
    String orderNum = "";

    public InsertPinDialogPresenter(InsertPinDialogFragment mFragment) {
        this.mFragment = mFragment;
    }

    void getOrder()
    {
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        mFragment.setOrderDatas(resp.getOrders());
                        orderNum = resp.getOrders().get(0).getOrderNumber();
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
//                        mFragment.tvTrxAmount.setVisibility(View.VISIBLE);
//                        mFragment.tvTrxAmount.setText(AmountFormatter.format(resp.getTotalTrxAmount()));

                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.showToast("Sesi Anda telah berakhir, Silahkan Login ulang");
                        mFragment.finish();
                        mFragment.doNeedRelogin();
                    }

                    else
                    {
//                        mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        /*if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }*/
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
//                    mFragment.pbLoadTrxMount.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {
//                mFragment.pbLoadTrxMount.setVisibility(View.GONE);
            }
        });
    }

    void getTransactionNumber()
    {
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayOrderReq request = new InqPayOrderReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(""+mFragment.etPassword.getText().toString());
        request.setOrderNumber(orderNum);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayOrderResponse> respCall = mFragment.getVoucherApi().getInqPayOrder(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayOrderResponse>() {
            @Override
            public void onResponse(Call<InqPayOrderResponse> call, Response<InqPayOrderResponse> response) {
                if(response.code()==200)
                {
                    InqPayOrderResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD005"))
                    {
                        mFragment.orderNumber = resp.getTransactionId();
                        InsertOTPDialogFragment viewDialog = new InsertOTPDialogFragment(mFragment.orderDatas, mFragment, mFragment.etPassword.getText().toString(), resp.getTransactionId(), mFragment.status);
                    }
                    else
                    {
                       mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayOrderResponse> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }

    void getTransactionNumberBiller(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(""+mFragment.etPassword.getText().toString());
        request.setOrigin("ANDROID");
        PrefHelper.setString(PrefKey.PIN, ""+mFragment.etPassword.getText().toString());
        /*test*/
        request.setContractNo(PrefHelper.getString(PrefKey.CONTRACT_NO));
        request.setBillerId(PrefHelper.getString(PrefKey.BILLER_ID));
        request.setPaymentMethod(PrefHelper.getInt(PrefKey.PAYCODE));
        request.setFavorite(PrefHelper.getBoolean(PrefKey.IS_FAV));
        /*test*/
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayBillerResponse> respCall = mFragment.getVoucherApi().getInqPayBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayBillerResponse>() {
            @Override
            public void onResponse(Call<InqPayBillerResponse> call, Response<InqPayBillerResponse> response) {
                if(response.code()==200)
                {
                    InqPayBillerResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL001"))
                    {
                        /*test*/
//                        PrefHelper.clearPreference(PrefKey.BILLER_ID);
                        mFragment.orderNumber = resp.getTransactionId();
                        PrefHelper.setString(PrefKey.TRX_ID, resp.getTransactionId());
                        BillerInsertOTPDialogFragment viewDialog = new BillerInsertOTPDialogFragment(mFragment, mFragment.etPassword.getText().toString(), resp.getTransactionId(), mFragment.status, resp.getTotalAmount());
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
//                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayBillerResponse> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }
}
