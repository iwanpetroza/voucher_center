package com.ultravoucher.uv.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.models.GCMMessageModel;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.adapters.HomeViewPagerAdapter;
import com.ultravoucher.uv.ui.fragments.mainpage.HistoryFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.HomePageFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.MyRewardFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.MyVoucherFragment;
import com.ultravoucher.uv.ui.fragments.mainpage.PurchaseVoucherFragment;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class HomePageActivity extends BaseActivity {

    private static final String LOGIN = "login";
    private static final String IS_NOTIFICATION = "IsNotification";
    private static final String GCM_DATA = "GcmData";
    private static final String TAG = HomePageActivity.class.getSimpleName();

    @BindView(R.id.homeTablayout)
    TabLayout mTabLayout;

    @BindView(R.id.homeVP)
    ViewPager mViewPager;

    @BindView(R.id.tvCartAmount)
    TextView tvCount;

    private MenuItem mSetting;
    HomePagePresenter mPresenter;
    String logIn, mId;
    boolean isNotification;
    private GCMMessageModel mGcmModel;

    public static void startActivity(BaseActivity sourceActivity, String login) {
        Intent i = new Intent(sourceActivity, HomePageActivity.class);
        i.putExtra(LOGIN, login);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    public static Intent startActivityFromNotif(Context sourceActivity, GCMMessageModel model)
    {
        Intent i = new Intent(sourceActivity, HomePageActivity.class);
        i.putExtra(IS_NOTIFICATION,true);
        i.putExtra(GCM_DATA, model);
        return i;
    }

    private int[] tabIcons = {
            R.drawable.ic_home_96px,
            R.drawable.ic_beli_96px,
            R.drawable.ic_voucherku_96px,
            R.drawable.ic_reward_putih_96px,
            R.drawable.ic_history_96px,

    };

    @Override
    protected int getLayout() {
        return R.layout.a_home_page;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        logIn = getIntent().getStringExtra(LOGIN);
        isNotification=getIntent().getExtras().getBoolean(IS_NOTIFICATION);
        mPresenter = new HomePagePresenter(this);
//        setupToolbar();

        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

        /*if (logIn.equals("3")) {
            mTabLayout.getTabAt(2).select();
        } else {
            mTabLayout.getTabAt(1).select();
        }*/

        if(isNotification)
        {
            mGcmModel = (GCMMessageModel) getIntent().getSerializableExtra(GCM_DATA);
            mId = mGcmModel.getmId();
            mTabLayout.getTabAt(2).select();
        } else if (logIn.equals("3")) {
            mTabLayout.getTabAt(2).select();
        } else {
            mTabLayout.getTabAt(1).select();
        }


        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 0 || pos == 2 || pos == 3 || pos == 4) {
                    if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                        HomePageActivity.this.finish();
                        LoginActivity.startActivity(HomePageActivity.this, false);
                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getCheckVersion();
        HomePageActivityPermissionsDispatcher.getSMSPermitWithPermissionCheck(this);
    }

    private void setupTabIcons() {

        View view1 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        view1.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_home_300px);
        mTabLayout.getTabAt(0).setCustomView(view1);

        View view2 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        view2.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_beli_300px);
        mTabLayout.getTabAt(1).setCustomView(view2);

        View view3 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        view3.findViewById(R.id.icon).setBackgroundResource(R.drawable.txt_voucherku);
        mTabLayout.getTabAt(2).setCustomView(view3);

        View view4 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        view4.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_reward_300px);
        mTabLayout.getTabAt(3).setCustomView(view4);

        View view5 = getLayoutInflater().inflate(R.layout.custom_tab, null);
        view5.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_histori_300px);
        mTabLayout.getTabAt(4).setCustomView(view5);
    }

    private void setupViewPager(ViewPager viewPager) {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new HomePageFragment(), "Home");
        adapter.addFragment(new PurchaseVoucherFragment(), "Beli");
        adapter.addFragment(new MyVoucherFragment(), "Voucherku");
        adapter.addFragment(new MyRewardFragment(), "Reward");
        adapter.addFragment(new HistoryFragment(), "History");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    this.finish();
                    LoginActivity.startActivity(this, false);
                } else {
                    SettingActivity.startActivity(this);
                }
                return true;

            /*case R.id.action_cart:
//                CheckOutActivity.startActivity(this);
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuinflat = getMenuInflater();
        menuinflat.inflate(R.menu.menu_general_list, menu);
        mSetting = menu.findItem(R.id.action_search);

        final Menu m = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick({R.id.imgBtnCart, R.id.imgBtnSetting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBtnCart:
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    LoginActivity.startActivity(this, false);
                } else {
                    CheckOutActivity.startActivity(this);
                }
                break;
            case R.id.imgBtnSetting:
                if (PrefHelper.getString(PrefKey.AUTH_TOKEN).equals("") || PrefHelper.getString(PrefKey.AUTH_TOKEN) == null) {
                    LoginActivity.startActivity(this, false);
                } else {
                    SettingActivity.startActivity(this);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
            super.onBackPressed();
            return;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomePageActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    //GET SMS PERMISSION
    @NeedsPermission({Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS})
    void getSMSPermit() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
    }
}
