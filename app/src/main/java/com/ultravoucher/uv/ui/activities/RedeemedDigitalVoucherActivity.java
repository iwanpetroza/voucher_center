package com.ultravoucher.uv.ui.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.squareup.picasso.Picasso;
import com.ultravoucher.uv.R;

import butterknife.BindView;
import butterknife.OnClick;

public class RedeemedDigitalVoucherActivity extends BaseActivity {

    private static final String LOGO = "logo";
    private static final String VOUCHERCODE = "vouchercode";
    private static final String NAME = "name";
    private static final String EXP = "exp";

    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.appBar)
    AppBarLayout appBar;

    @BindView(R.id.ivQR)
    ImageView ivQR;

    @BindView(R.id.tvCode)
    TextView tvCode;

    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    @BindView(R.id.tvExp)
    TextView tvExp;

    @BindView(R.id.tvName)
    TextView tvName;

    @BindView(R.id.a_register)
    RelativeLayout aRegister;

    String logo, name, vouchercode, exp = "";
    Bitmap bitmap;
    public final static int QRcodeWidth = 500 ;

    public static void startActivity(BaseActivity sourceActivity, String logo, String vCode, String expired, String name){
        Intent i = new Intent(sourceActivity, RedeemedDigitalVoucherActivity.class);
        i.putExtra(LOGO, logo);
        i.putExtra(VOUCHERCODE, vCode);
        i.putExtra(NAME, name);
        i.putExtra(EXP, expired);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivity(i);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_redeemed_digital_voucher;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        logo = getIntent().getStringExtra(LOGO);
        vouchercode = getIntent().getStringExtra(VOUCHERCODE);
        name = getIntent().getStringExtra(NAME);
        exp = getIntent().getStringExtra(EXP);
        try {
            bitmap = TextToImageEncode(vouchercode);

            ivQR.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        Picasso.with(this).load(logo).fit().centerCrop().into(ivLogo);
        tvCode.setText("Voucher Code : " + vouchercode);
        tvExp.setText("Tanggal Expired : " + exp);
        tvName.setText(name);
    }

    Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        ContextCompat.getColor(this, R.color.black): ContextCompat.getColor(this, R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbarTitle.setText("QR Code Voucher");
    }

    @OnClick(R.id.btnDone)
    public void onViewClicked() {
        finish();
        HomePageActivity.startActivity(this, "3");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        HomePageActivity.startActivity(this, "3");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
