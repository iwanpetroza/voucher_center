package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.HistoryBillerDetailMsgResp;
import com.ultravoucher.uv.data.api.request.HistoryBillerDetailReq;
import com.ultravoucher.uv.data.api.response.HistoryBillerDetailResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerDetailPresenter {

    private HistoryBillerDetailFragment mFragment;

    public HistoryBillerDetailPresenter(HistoryBillerDetailFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void getHistoryBillerDetail(HistoryBillerDetailReq req) {

        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryBillerDetailResponse> historyBillerDetailResponseCall = api.getHistoryBDetail(contentType, authToken, deviceUId, req);
        historyBillerDetailResponseCall.enqueue(new Callback<HistoryBillerDetailResponse>() {
            @Override
            public void onResponse(Call<HistoryBillerDetailResponse> call, Response<HistoryBillerDetailResponse> response) {
                if (response.code() == 200) {
                    HistoryBillerDetailResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("BL001")) {
                        if (!resp.getBillerHistory().isEmpty()) {
                            initList(resp.getBillerHistory());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<HistoryBillerDetailResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }

    protected void getHistoryBillerDetailLoadMore(HistoryBillerDetailReq req) {

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryBillerDetailResponse> historyBillerDetailResponseCall = api.getHistoryBDetail(contentType, authToken, deviceUId, req);
        historyBillerDetailResponseCall.enqueue(new Callback<HistoryBillerDetailResponse>() {
            @Override
            public void onResponse(Call<HistoryBillerDetailResponse> call, Response<HistoryBillerDetailResponse> response) {
                if (response.code() == 200) {
                    if (response.body() != null && response.body().getAbstractResponse().getResponseStatus().equals("BL001")) {
                        if (!response.body().getBillerHistory().isEmpty()) {
                            HistoryBillerDetailResponse resp = response.body();

                            addToList(resp.getBillerHistory());
                        } else {
                            mFragment.mList.removeAll(Collections.singleton(null));
                            mFragment.mAdapter.notifyDataSetChanged();
                            mFragment.mAdapter.setLoaded(true);
                        }
                    } else {
                        mFragment.mList.removeAll(Collections.singleton(null));
                        mFragment.mAdapter.notifyDataSetChanged();
                        mFragment.mAdapter.setLoaded(true);
                    }
                } else {
                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                }
            }

            @Override
            public void onFailure(Call<HistoryBillerDetailResponse> call, Throwable t) {
                if(mFragment!= null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.showToast(message);
                }
            }
        });

    }

    protected void initList(List<HistoryBillerDetailMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected void addToList(List<HistoryBillerDetailMsgResp> list) {
        mFragment.mList.removeAll(Collections.singleton(null));
        mFragment.mAdapter.notifyDataSetChanged();
        for (HistoryBillerDetailMsgResp imr : list) {
            mFragment.mList.add(imr);
            mFragment.mAdapter.notifyDataSetChanged();
        }
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected HistoryBillerDetailReq getHistoryBillerDetailReq() {
        HistoryBillerDetailReq req = new HistoryBillerDetailReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setTransactionId(mFragment.trxId);
        return req;
    }

}
