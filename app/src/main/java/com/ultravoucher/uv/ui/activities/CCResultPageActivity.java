package com.ultravoucher.uv.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.ultravoucher.uv.R;

import butterknife.BindView;

public class CCResultPageActivity extends BaseActivity {

    private static final String POST_ID = "postId";

    @BindView(R.id.toolbar)
    Toolbar toolBar;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.my_webview)
    WebView wv;

    private VideoView videoView;
    private MediaController mController;
    private Uri uriYouTube;
    String webUrl;
    boolean isTrue = false;

    @Override
    protected int getLayout() {
        return R.layout.a_ccresultpage;
    }

    /*protected String link = "";
    private int position = 0;
    VideoPresenter mPresenter;
    RTSPUrlTask truitonTask;*/

    String link;

    public static void startActivity(BaseActivity sourceActivity, String url){
        Intent i = new Intent(sourceActivity, CCResultPageActivity.class);
        i.putExtra(POST_ID, url);
        sourceActivity.startActivity(i);
    }

   /* @Override
    public void onResume() {
        super.onResume();
        mPresenter.doPostById(mPresenter.constructPostByIdReq());
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        link = getIntent().getStringExtra(POST_ID);

        setupToolbar();

        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

 /*       wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });*/

        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message,           JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });
        wv.loadUrl(link);

     /*   if (!webUrl.equals("") && webUrl.contains("true")) {
            webUrl = wv.getUrl();
        } else {

        }*/


        wv.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webUrl = url;

                if (webUrl.contains("true")) {
                    String result[] = webUrl.split("=");
                    String on[] = result[1].split("&");
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Pembayaran Anda Berhasil \n Nomor Pesanan Anda = " + on[0]);
                    startActivity(intent);
                    finish();
                } else if (webUrl.contains("false")) {
                    String result[] = webUrl.split("=");
                    String on[] = result[1].split("&");
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Pembayaran Anda Gagal \n Silahkan Coba Lagi");
                    startActivity(intent);
                    finish();
                }

//                } else if (webUrl.contains("false")) {
//                    isTrue = false;
//                }
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });




//        wv.loadUrl(link);

        /*mPresenter = new VideoPresenter(this);

        videoView = (VideoView) findViewById(R.id.videoView1);
        mController = new MediaController(VideoActivity.this);
        videoView.setMediaController(mController);
        videoView.requestFocus();

        if (savedInstanceState != null) {
            int loc = savedInstanceState.getInt("Loc");
            Log.i("Loaction: ", loc + "");
            uriYouTube = Uri.parse(savedInstanceState.getString("url"));
            videoView.setVideoURI(uriYouTube);
            videoView.seekTo(loc);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Log.v("onPrepared", "ok");
                    mp.start();
                }
            });
        } else {
            truitonTask = new RTSPUrlTask();
            truitonTask.execute(link);
        }*/


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (isTrue) {
        this.finish();
        HomePageActivity.startActivity(this, "");
//        } else {
//            this.finish();
//        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        toolbar_title.setText("PAYMENT");
        toolbar_title.setAllCaps(true);
    }
}
