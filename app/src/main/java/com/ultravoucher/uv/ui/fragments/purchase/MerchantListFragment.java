package com.ultravoucher.uv.ui.fragments.purchase;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.StoreListMsgResp;
import com.ultravoucher.uv.ui.adapters.MerchantListAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/11/2017.
 */

public class MerchantListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "MerchantListFragment";
    private static final String KEYWORD = "keyword";

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.ll_grid)
    LinearLayout llGrid;

    @BindString(R.string.loading)
    String LOADING;

    @BindView(R.id.progress)
    View progressBar;
    @BindView(R.id.progress_message)
    TextView progressMsg;
    @BindView(R.id.no_data_state)
    View noDataState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    /*@BindView(R.id.network_problem_state)
    View networkProblemState;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;*/

    MerchantListAdapter mAdapter;
    MerchantListPresenter mPresenter;
    protected int page = 0;
    private Handler handler;
    String key = "";
    String keyword = "";
    protected List<StoreListMsgResp> mData = new ArrayList<>();

    public static void showFragment(BaseFragment sourceFragment, String keyword) {
        MerchantListFragment fragment = new MerchantListFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(KEYWORD, keyword);
        FragmentTransaction transaction = sourceFragment.getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        transaction.commit();
    }


    @Override
    protected int getLayout() {
        return R.layout.f_merchant_list;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = 0;
        if (key.equals("") || key == null) {
            key = "";
        } else {
            key = getArguments().getString(KEYWORD);
        }

        mPresenter = new MerchantListPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String a = etSearch.getText().toString();

                if (a.equals("")) {
                    keyword = "";
                    gridView.setVisibility(View.GONE);
                    mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
                }
            }
        });


        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                gridView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
        setupGridView();
        mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
    }

    private void setupGridView() {
        mAdapter = new MerchantListAdapter(getContext(), mData, gridView);
        gridView.setAdapter(mAdapter);

        handler = new Handler();
        mAdapter.setOnLoadMoreListener(new MerchantListAdapter.OneLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mData.removeAll(Collections.singleton(null));
                mData.add(null);
                mAdapter.notifyDataSetChanged();
                Log.d(TAG, "onLoadMore: masuk");
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreMerchantList();
                    }
                }, 4000);
            }
        });

    }

    private void loadMoreMerchantList() {
        mPresenter.PresentGetAllMerchantLoadMore(mPresenter.presentGetListMerchantReq());
    }

    @OnClick(R.id.btnSearch)
    void clicked() {
        if (etSearch.getText().toString().equals("")) {
            showToast("Masukkan kata kunci");
        } else {
            page = 0;
            keyword = etSearch.getText().toString();
            gridView.setVisibility(View.GONE);
            mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
        }
    }

    @Override
    public void onRefresh() {
        page = 0;
        gridView.setVisibility(View.GONE);
        mPresenter.getListMerchant(mPresenter.presentGetListMerchantReq());
    }
}
