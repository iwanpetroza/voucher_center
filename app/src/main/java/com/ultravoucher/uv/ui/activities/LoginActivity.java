package com.ultravoucher.uv.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.fragments.forgotpassword.ForgotPasswordFragment;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements Validator.ValidationListener {

    @BindView(R.id.login_et_mail)
    @NotEmpty
    EditText loginEtMail;

    @BindView(R.id.login_mail)
    TextInputLayout loginMail;

    @BindView(R.id.login_et_password)
    @NotEmpty
    @Password(min = 6)
    EditText loginEtPassword;

    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;

    @BindView(R.id.tv)
    TextView tv;

    @BindView(R.id.login_relativeLayout2)
    RelativeLayout loginRelativeLayout2;

    @BindString(R.string.loading)
    String LOADING;

    @BindView(R.id.img)
    ImageView img;

    public static final int RESET_DIALOG_REQ_CODE = 0;
    public static final int RESET_DIALOG_REQ_CODE_BATAL = 1;
    public static final int RESET_DIALOG_REQ_CODE_OK = 2;

    private boolean mStatus = false;
    private Validator mValidator;

    LoginPresenter signPresenter;

    public static void startActivity(BaseActivity sourceActivity, boolean isRelogin){
        if(isRelogin){
            Toast.makeText(sourceActivity,sourceActivity.getString(R.string.relogin),Toast.LENGTH_SHORT).show();
        }
        Intent i = new Intent(sourceActivity, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(i);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Picasso.with(this).load(R.drawable.logo_uv_transparan).centerCrop().fit().into(img);

        mStatus = false;
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
        signPresenter = new LoginPresenter(this);

        final Context context = this;
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    if (loginEtMail.isFocused()) {
                        Rect emailRect = new Rect();
                        loginEtMail.getGlobalVisibleRect(emailRect);
                        if (!emailRect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            loginEtMail.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }

                    if (loginEtPassword.isFocused()) {
                        Rect passRec = new Rect();
                        loginEtPassword.getGlobalVisibleRect(passRec);
                        if (!passRec.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY())) {
                            loginEtPassword.clearFocus();
                            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
    }

    @OnClick({R.id.login_btn_login, R.id.login_btn_forgotPassword, R.id.login_btn_daftar, R.id.login_btn_lewati})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn_login:
                mValidator.validate();
                break;
            case R.id.login_btn_forgotPassword:
                ForgotPasswordFragment.showDialog(this, RESET_DIALOG_REQ_CODE);
                break;
            case R.id.login_btn_daftar:
                RegisterActivity.startActivity(this);
                break;
            case R.id.login_btn_lewati:
                HomePageActivity.startActivity(this, "");
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        signPresenter.login(signPresenter.constructLoginReq());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESET_DIALOG_REQ_CODE) {
            switch (resultCode) {
                case RESET_DIALOG_REQ_CODE_OK:
                    VerificationActivity.startActivity(this, data.getStringExtra(ForgotPasswordPresenter.VERIFICATION_CODE));
                    break;
            }
        }
    }*/
}
