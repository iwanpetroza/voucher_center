package com.ultravoucher.uv.ui.activities;

import android.provider.Settings;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.RegistrationReq;
import com.ultravoucher.uv.data.api.response.RegistrationResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/17/2017.
 */

public class RegisterPresenter {

    RegisterActivity mActivity;

    public RegisterPresenter(RegisterActivity mActivity) {
        this.mActivity =mActivity;
    }

    public void signUp(final RegistrationReq req) {

        mActivity.showProgressDialog(mActivity.LOADING);

        String contentType = "application/json";

        VoucherApi api = mActivity.getVoucherApi();
        Call<RegistrationResp> respCall = api.doRegistration(contentType, req);
        respCall.enqueue(new Callback<RegistrationResp>(){
            @Override
            public void onResponse(Call<RegistrationResp> call, Response<RegistrationResp> response) {
                if (response.code() == 200) {
                    RegistrationResp resp = response.body();
                    if (resp.getAbstractResponse().getResponseStatus().equals("MEM003")) {
                        PrefHelper.setString(PrefKey.VERIFICATION_CODE, resp.getVerificationCode());
                        PrefHelper.setBoolean(PrefKey.IS_REGISTER, true);

                        VerificationActivity.startActivity((BaseActivity) mActivity, "", "");
                    } else {
                        mActivity.showToast(resp.getAbstractResponse().getResponseMessage());
                    }
                } else {
                    mActivity.showToast("Unstable Connection");
                }
                mActivity.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<RegistrationResp> call, Throwable t) {
                mActivity.dismissProgressDialog();
                mActivity.showToast(mActivity.CONNECTION_ERROR);
            }
        });
    }

    RegistrationReq constructRegistrationReq() {
        final String idDevice = Settings.Secure.getString(mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
        String email = mActivity.registrationEtEmail.getText().toString() + "";
        String password = mActivity.registrationEtPassword.getText().toString() + "";
        String confirm = mActivity.registrationEtConfirmPassword.getText().toString() + "";
        String phone = mActivity.registrationEtPhone.getText().toString();

        String modifiedPhone;

        if (phone.contains("+62")) {
            modifiedPhone = phone.replaceAll("\\+62", "0");
        } else {
            modifiedPhone = phone;
        }

        RegistrationReq req = new RegistrationReq();
        req.setDeviceUniqueId(idDevice);
        req.setUsername(email);
        req.setPhoneNumber(modifiedPhone);
        req.setPassword(password);
        req.setFirstName("-");
        req.setLastName("-");
        req.setIsCorporateMember(-1);

        PrefHelper.setString(PrefKey.USERNAME, email);
        return req;
    }
}
