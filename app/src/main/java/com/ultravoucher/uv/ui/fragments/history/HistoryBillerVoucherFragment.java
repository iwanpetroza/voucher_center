package com.ultravoucher.uv.ui.fragments.history;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.BillerVoucherHistoryMsgResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.GeneralHistoryActivity;
import com.ultravoucher.uv.ui.adapters.BillerVoucherHistoryAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.utils.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Firwandi S Ramli on 30/07/18.
 */

public class HistoryBillerVoucherFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = HistoryBillerVoucherFragment.class.getSimpleName();

    //new
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;


    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.GeneralRVList)
    RecyclerView rvList;
    @BindView(R.id.progress_message)
    TextView progressMessage;
    @BindView(R.id.progress)
    LinearLayout progress;
    @BindView(R.id.network_problem_image)
    ImageView networkProblemImage;
    @BindView(R.id.network_problem_message)
    TextView networkProblemMessage;
    @BindView(R.id.network_problem_state)
    LinearLayout networkProblemState;
    @BindView(R.id.no_data_state_message)
    TextView noDataMessage;
    @BindView(R.id.no_data_state)
    LinearLayout noDataState;
    @BindView(R.id.ll)
    LinearLayout ll;

    protected int page = 0;
    private Handler mHandler;
    protected List<BillerVoucherHistoryMsgResp> mList = new ArrayList<>();
    BillerVoucherHistoryAdapter mAdapter;
    HistoryBillerVoucherPresenter mPresenter;

    public static void showFragment(BaseActivity sourceFragment) {
        HistoryBillerVoucherFragment fragment = new HistoryBillerVoucherFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        page = 0;
        mPresenter = new HistoryBillerVoucherPresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_general_listview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ll.setVisibility(View.GONE);
        initRVMyHistoryDeals();

        //new
        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);

                page = 0;
                rvList.setVisibility(View.GONE);
                mPresenter.presentGetDigitalHistory(mPresenter.presentGetHistoryReq());
            }
        });

    }

    protected void initRVMyHistoryDeals() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setLayoutManager(linearLayoutManager);
        mAdapter = new BillerVoucherHistoryAdapter(this, mList, rvList);
        rvList.setItemAnimator(new DefaultItemAnimator());
        rvList.setAdapter(mAdapter);
        rvList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                BillerVoucherHistoryMsgResp item = (BillerVoucherHistoryMsgResp) childView.getTag();

                if (item.getType() == 2) {
                    GeneralHistoryActivity.startActivity((BaseActivity) getActivity(), "historybiller", item.getTransactionId());
                } else if (item.getType() == 1) {
                    GeneralHistoryActivity.startActivity((BaseActivity) getActivity(), "historydigital", item.getTransactionId());
                }


            }

        }));
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 0;
    }

    private void loadHistoryDigitalList() {
        mPresenter.presentGetDigitalHistory(mPresenter.presentGetHistoryReq());
    }

    private void loadMoreHistoryDigitalList() {
        mPresenter.presentGetDigitalHistoryLoadMore(mPresenter.presentGetHistoryReq());
    }

    @Override
    public void onRefresh() {
        page = 0;
        rvList.setVisibility(View.GONE);
        mPresenter.presentGetDigitalHistory(mPresenter.presentGetHistoryReq());
    }

}
