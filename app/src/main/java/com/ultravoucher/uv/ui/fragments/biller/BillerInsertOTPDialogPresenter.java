package com.ultravoucher.uv.ui.fragments.biller;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.PayByWaletBillerReq;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.api.response.PayBillerResp;
import com.ultravoucher.uv.data.api.response.PayOrderResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.fragments.purchase.VAConfirmFragment;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tunggul.jati on 5/31/2018.
 */

public class BillerInsertOTPDialogPresenter {

    BillerInsertOTPDialogFragment mFragment;

    public BillerInsertOTPDialogPresenter(BillerInsertOTPDialogFragment mFragment) {
        this.mFragment = mFragment;
    }

    void doPayOrderWalet() {

        mFragment.showProgressDialog(mFragment.LOADING);
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";
        PayByWaletBillerReq request = new PayByWaletBillerReq();
//        request.setTrxId(mFragment.transactionId);
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(mFragment.pass);
        request.setOtp(mFragment.etOTP.getText().toString());
        request.setContractNo(PrefHelper.getString(PrefKey.CONTRACT_NO));
        request.setTrxId(PrefHelper.getString(PrefKey.TRX_ID));

        Call<PayBillerResp> respCall = mFragment.getVoucherApi().payByWaletBiller(contentType, authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<PayBillerResp>() {
            @Override
            public void onResponse(Call<PayBillerResp> call, Response<PayBillerResp> response) {
                if(response.code()==200)
                {
                    PayBillerResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL002"))
                    {
                        PrefHelper.clearPreference(PrefKey.CONTRACT_NO);
                        PrefHelper.clearPreference(PrefKey.TRX_ID);
                        mFragment.getDialog().dismiss();
                        mFragment.getActivity().finish();
                        PrefHelper.setString(PrefKey.TOKEN_SUBMIT, resp.getToken());
                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), "", resp.getTransactionId(), mFragment.totalTrxAmount, "biller");
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
// VAConfirmationPageActivity.startActivity((BaseActivity) mFragment.getActivity(), resp.getOrders().get(0).getVaNumber(), mFragment.orderNumber);
//                        SmsRadar.stopSmsRadarService(mFragment.getContext());
//                        mFragment.dismiss();
//                        OutputDialogFragment viewDialog = new OutputDialogFragment((BaseFragment) mFragment.mFragment, 2, resp.getTotalTrxAmount());

                    } else if (resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        mFragment.getDialog().dismiss();
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());

                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
//                        SmsRadar.stopSmsRadarService(mFragment.getContext());
//                        mFragment.dismiss();
//                        OutputDialogFragment viewDialog = new OutputDialogFragment(mFragment, 1, 0);

                    }
                }
                else if (response.code() == 400) {

                    mFragment.showToast("Sedang dalam gangguan"/*response.body().getAbstractResponse().getResponseMessage()*/);

                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<PayBillerResp> call, Throwable t) {
                mFragment.showToast("Failed to Connect");
                mFragment.dismissProgressDialog();
            }
        });
    }

    void getTransactionNumberBiller(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(PrefHelper.getString(PrefKey.PIN));
        request.setOrigin("ANDROID");
        /*test*/
        request.setContractNo(PrefHelper.getString(PrefKey.CONTRACT_NO));
        request.setBillerId(PrefHelper.getString(PrefKey.BILLER_ID));
        request.setPaymentMethod(PrefHelper.getInt(PrefKey.PAYCODE));
        request.setFavorite(PrefHelper.getBoolean(PrefKey.IS_FAV));
        /*test*/
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayBillerResponse> respCall = mFragment.getVoucherApi().getInqPayBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayBillerResponse>() {
            @Override
            public void onResponse(Call<InqPayBillerResponse> call, Response<InqPayBillerResponse> response) {
                if(response.code()==200)
                {
                    InqPayBillerResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL001"))
                    {
                        PrefHelper.setString(PrefKey.TRX_ID, resp.getTransactionId());
                        mFragment.reOpenDialog();
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            /*if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }*/
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
//                    mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
                    mFragment.llForm.setVisibility(View.GONE);
                    mFragment.llLoad.setVisibility(View.VISIBLE);
                    mFragment.pbLoad.setVisibility(View.GONE);
                    mFragment.tvError.setVisibility(View.VISIBLE);
                    mFragment.tvError.setPadding(0,100,0,100);
                    mFragment.tvError.setText("Server Is Down");
                }

//                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayBillerResponse> call, Throwable t) {
//                mFragment.showToast(mFragment.CONNECTION_ERROR);
//                mFragment.dismissProgressDialog();

                mFragment.llForm.setVisibility(View.GONE);
                mFragment.llLoad.setVisibility(View.VISIBLE);
                mFragment.pbLoad.setVisibility(View.GONE);
                mFragment.tvError.setVisibility(View.VISIBLE);
                mFragment.tvError.setPadding(0,100,0,100);
                mFragment.tvError.setText("Connection is Error");
            }
        });
    }

}
