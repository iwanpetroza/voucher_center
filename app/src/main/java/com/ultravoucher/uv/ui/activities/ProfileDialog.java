package com.ultravoucher.uv.ui.activities;

import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.ultravoucher.uv.R;

/**
 * Created by firwandi.ramli on 12/27/2017.
 */

public class ProfileDialog {

    public void showDialog(BaseActivity activity){
        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.d_image_is_too_big);

        Button backButton = (Button) dialog.findViewById(R.id.back_btn_dialog);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
