package com.ultravoucher.uv.ui.fragments.mainpage;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.BuildConfig;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.AdvertisingMsgResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.BillerMenuActivity;
import com.ultravoucher.uv.ui.activities.BillerTagihanActivity;
import com.ultravoucher.uv.ui.activities.GeneralActivity;
import com.ultravoucher.uv.ui.activities.ListMyVoucherDigitalActivity;
import com.ultravoucher.uv.ui.activities.ProfileDialog;
import com.ultravoucher.uv.ui.activities.QRMemberActivity;
import com.ultravoucher.uv.ui.activities.SyaratDanKetentuanActivity;
import com.ultravoucher.uv.ui.adapters.AdvBannerMainPageAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by firwandi.ramli on 10/2/2017.
 */

@RuntimePermissions
public class HomePageFragment extends BaseFragment {

    private static final String TAG = "HomePageFragment";

    @BindView(R.id.civLogo)
    CircleImageView civLogo;
    @BindView(R.id.mycart_pb_loading)
    ProgressBar mycartPbLoading;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.imageRL)
    RelativeLayout imageRL;
    @BindView(R.id.tvPoin)
    TextView tvPoin;
    @BindView(R.id.tvNama)
    TextView tvNama;

    @BindView(R.id.tvWalet)
    TextView tvWalet;
    @BindView(R.id.tvBalance)
    TextView tvBalance;

    @BindView(R.id.btnLL)
    LinearLayout btnLL;
    @BindView(R.id.btnBottomLL)
    LinearLayout btnBottomLL;
    @BindView(R.id.homeViewpager)
    ViewPager homeViewpager;
    @BindView(R.id.viewPager)
    RelativeLayout images;

    @BindView(R.id.circleMenuPageIndicator)
    CirclePageIndicator circlePageIndicator;

    private Handler advSliderHandler;
    String mCurrentPhotoPath;
    File image;
    File imageFile;
    Uri mMediaUri;
    File mediaFile;

    private final int SLIDER_DELAY_ADVERTISING_IN_SECOND = 5;
    private static int currentPage = 0;
    AdvBannerMainPageAdapter adapter;
    HomePagePresenter mPresenter;
    List<AdvertisingMsgResp> datas = new ArrayList<>();
    public void setData(List<AdvertisingMsgResp> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        adapter.notifyDataSetChanged();
    }

    static final int CAMERA_CAPTURE = 1;
    final int PICK_IMAGE_REQUEST = 2;
    final int PIC_CROP = 3;
    final int PROFILE_PIC = 4;

    private Uri picUri;
    private Uri picUriCropped;
    private File picFile;
    private boolean mIsThePhotoVisible = false;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    boolean running = false;

    private int request;

    public static HomePageFragment showTabFragment() {
        HomePageFragment tabStore = new HomePageFragment();
        return tabStore;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new HomePagePresenter(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_home_page2;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvNama.setText(PrefHelper.getString(PrefKey.FIRSTNAME));
        if (!PrefHelper.getString(PrefKey.USER_PIC).equals("") && PrefHelper.getString(PrefKey.USER_PIC) != null) {
            Picasso.with(getActivity().getApplicationContext()).load(PrefHelper.getString(PrefKey.USER_PIC))
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder).fit().centerCrop()
                    .into(civLogo);
        }

        if (PrefHelper.getString(PrefKey.INITIAL_RUN).equals("login")) {
            DialogFragment popUp = new PopupAdvDialogFragment();
            popUp.show(getChildFragmentManager(), TAG);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initAdv();
    }

    @OnClick({R.id.btnPhoto, R.id.btnWalet, R.id.btnVoucher, R.id.btnQR, R.id.btnTopup, R.id.btnShare, R.id.btnBill})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnPhoto:
                request = PROFILE_PIC;
                selectImage();
                break;
            case R.id.btnWalet:
                GeneralActivity.startActivity((BaseActivity) getActivity(), "historywalet");
                break;
            case R.id.btnVoucher:
                ListMyVoucherDigitalActivity.startActivity((BaseActivity) getActivity(), "3", "3");
                break;
            case R.id.btnQR:
                QRMemberActivity.startActivity((BaseActivity) getActivity());
                break;
            case R.id.btnTopup:

                SyaratDanKetentuanActivity.startActivity((BaseActivity) getActivity(),
                        "https://corporate.ultravoucher.co.id/help/topup.html?va_bca=" + PrefHelper.getString(PrefKey.VA_NUMBER) + "&va_permata=" + PrefHelper.getString(PrefKey.VA_NUMBERPERMATA), "TOPUP");
                break;
            case R.id.btnShare:
                GeneralActivity.startActivity((BaseActivity) getActivity(), "share");
                break;
            case R.id.btnBill:
//                BillerMenuActivity.startActivity((BaseActivity) getActivity(), "biller");
                BillerTagihanActivity.startActivity((BaseActivity) getActivity(), "tagihan");
                break;
        }
    }

    public void initAdv() {
        mPresenter.presentAdvBanner();
        adapter = new AdvBannerMainPageAdapter(datas, (BaseActivity) getActivity());
        homeViewpager.setAdapter(adapter);
        circlePageIndicator.setViewPager(homeViewpager);
        advSliderHandler = new Handler();
        startPageAdvSwitchTimer();

    }

    private void startPageAdvSwitchTimer() {
        advSliderHandler.postDelayed(advSliderRunnable, SLIDER_DELAY_ADVERTISING_IN_SECOND * 1000);
    }

    private void stopPageAdvSwitchTimer() {
        advSliderHandler.removeCallbacks(advSliderRunnable);
    }

    private Runnable advSliderRunnable = new Runnable() {
        @Override
        public void run() {

            if (currentPage == datas.size()) {
                currentPage = 0;
            }
            homeViewpager.setCurrentItem(currentPage++,true);
            startPageAdvSwitchTimer();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        stopPageAdvSwitchTimer();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        HomePageFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder((BaseActivity) getActivity());
        builder.setTitle("Change Profile Picture");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Photo")) {
                    HomePageFragmentPermissionsDispatcher.startCaptureWithPermissionCheck(HomePageFragment.this);
                } else if (items[i].equals("Choose from Gallery")) {
                    HomePageFragmentPermissionsDispatcher.startSelectImageWithPermissionCheck(HomePageFragment.this);
                }
            }
        });
        builder.show();
    }

    public static File createTemporaryFile(Context context, String folder_name, String ext) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: " + requestCode + ", " + resultCode);
            if (resultCode == RESULT_OK) {
                //user is returning from capturing an image using the camera
                if (requestCode == CAMERA_CAPTURE) {
                    if (request == PROFILE_PIC) {
                        //cropImage();
                        HomePageFragmentPermissionsDispatcher.performCropWithPermissionCheck(HomePageFragment.this);
                    }
                } else if (requestCode == PICK_IMAGE_REQUEST) {
                    picUri = data.getData();
                    Log.d("uriGallery", picUri.toString());

                    if (request == PROFILE_PIC) {
                        HomePageFragmentPermissionsDispatcher.performCropWithPermissionCheck(HomePageFragment.this);
                    }
                }

                //user is returning from cropping the image
                else if (requestCode == UCrop.REQUEST_CROP) {

                    if (request == PROFILE_PIC) {
                        //get the returned data
                        picUriCropped = UCrop.getOutput(data);
                        if (picUriCropped != null) {
                            uploadProfilePic(new File(picUriCropped.getPath()));
                            //uploadProfilePic(picUriCropped);
                        } else {
                            // failed
                        }
                    }
                }
            }
//        }
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA})
    void startCapture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo;
        try {
            photo = createTemporaryFile(getContext(), "prof_pic", ".jpg");
            picFile = photo;
            picUri = FileProvider.getUriForFile(getContext(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photo);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            startActivityForResult(intent, CAMERA_CAPTURE);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE})
    void startSelectImage(){
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    private void cropImage() {
        Intent cropIntent = new Intent("com.android.camera.action.CROP");

        cropIntent.setDataAndType(picUri, "image/*");
        cropIntent.putExtra("crop", true);
        cropIntent.putExtra("outputX", 180);
        cropIntent.putExtra("outputY", 180);

        cropIntent.putExtra("aspectX", 3);
        cropIntent.putExtra("aspectY", 3);
        cropIntent.putExtra("scaleUpIfNeeded", true);
        cropIntent.putExtra("return-data", true);
        startActivityForResult(cropIntent, PIC_CROP);

    }

    public UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.setCompressionQuality(100);
        options.setMaxBitmapSize(1024);

        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
        options.setToolbarTitle("Edit Foto");
        options.setToolbarColor(ContextCompat.getColor(getContext(), android.R.color.black));
        options.setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        return uCrop.withOptions(options);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE})
    public void performCrop() {

        final String filename = Base64.encodeToString(UUID.randomUUID().toString().getBytes(), Base64.NO_WRAP | Base64.URL_SAFE);
        File target = new File(getContext().getCacheDir(), filename);
        UCrop uCrop = UCrop.of(picUri, Uri.fromFile(target));

        uCrop.withAspectRatio(1, 1);
        //uCrop.withMaxResultSize(max_width, max_height);

        advancedConfig(uCrop).start(getContext(), this);
    }

    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE})
    void pictureCrop(Bundle extrasPic){

        String filePath = Environment.getExternalStorageDirectory()
                + "/temporary_holder.jpg";

        Bitmap thePic = BitmapFactory.decodeFile(filePath);
        Log.d(TAG, "pictureCrop: ");
        String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), thePic, "Title", null);
        uploadProfilePic(Uri.parse(path));
    }


    Uri uploadProfilePic(Uri fileUri){
        Log.d(TAG, "uploadProfilePic: " + fileUri.getPath());
//        uploadProfilePic(new File(FileUtils.getPath(getActivity().getApplicationContext(), fileUri)));
        uploadProfilePic(picFile);
        return fileUri;
    }

    void uploadProfilePic(File filedata) {

        long fileSize = filedata.length() / 1024;
        Log.d("File Size", "File size is " + fileSize + " Kb");
        int maxSize = 1024;

        if (fileSize > maxSize) {
            if (running = true) {
                ProfileDialog profileDialog = new ProfileDialog();
                profileDialog.showDialog((BaseActivity) getActivity());
            }
        } else {
            mPresenter.presentUploadProfilePic(filedata);
        }
    }
    //End method for change and crop profile picture/ background picture

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {

        }
    }
}
