package com.ultravoucher.uv.ui.fragments.mainpage;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.adapters.HomeViewPagerAdapter;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryBillerVoucherFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryDigitalFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryFisikFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryProductFragment;
import com.ultravoucher.uv.ui.fragments.history.HistoryRewardFragment;

import butterknife.BindView;

/**
 * Created by firwandi.ramli on 10/26/2017.
 */

public class HistoryFragment extends BaseFragment {
    private static final String TAG = "PurchaseVoucherFragment";

    @BindView(R.id.purchaseTablayout)
    TabLayout mTabLayout;

    @BindView(R.id.purchaseVP)
    ViewPager mViewPager;

    HistoryFragment mContext;

    private int[] tabIcons = {
            R.drawable.ic_statusbayar_v2_180px,
            R.drawable.ic_voucherdigital_v2_180px,
            R.drawable.ic_voucherfisik_v2_180px,
            R.drawable.ic_reward_v2_180px,
    };

    @Override
    protected int getLayout() {
        return R.layout.f_purchase_voucher;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();

    }


    private void setupTabIcons() {
        mTabLayout.getTabAt(0).setIcon(tabIcons[0]);
        mTabLayout.getTabAt(1).setIcon(tabIcons[1]);
        mTabLayout.getTabAt(2).setIcon(tabIcons[2]);
        mTabLayout.getTabAt(3).setIcon(tabIcons[3]);

        View view0 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab3, null);
        view0.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_statusbayar_v2_180px);
        mTabLayout.getTabAt(0).setCustomView(view0);

        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab3, null);
        view1.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_voucherdigital_v2_180px);
        mTabLayout.getTabAt(1).setCustomView(view1);

        View view2 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab3, null);
        view2.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_voucherfisik_v2_180px);
        mTabLayout.getTabAt(2).setCustomView(view2);

        View view3 = getActivity().getLayoutInflater().inflate(R.layout.custom_tab3, null);
        view3.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_reward_v2_180px);
        mTabLayout.getTabAt(3).setCustomView(view3);
    }

    private void setupViewPager(ViewPager viewPager) {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new HistoryProductFragment(), "Belum Terbayar");
        adapter.addFragment(new HistoryBillerVoucherFragment(), "Voucher Digital");
        adapter.addFragment(new HistoryFisikFragment(), "Voucher Digital");
        adapter.addFragment(new HistoryRewardFragment(), "Reward");
//        adapter.addFragment(new HomePageFragment(), "Merchant");
        viewPager.setAdapter(adapter);
    }
}
