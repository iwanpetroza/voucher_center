package com.ultravoucher.uv.ui.fragments.purchase;

import android.view.View;

import com.google.gson.Gson;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.ListPaymentMethodMsgResp;
import com.ultravoucher.uv.data.api.beans.OrderPay;
import com.ultravoucher.uv.data.api.request.InqPayBillerReq;
import com.ultravoucher.uv.data.api.request.ListPaymentMethodReq;
import com.ultravoucher.uv.data.api.request.MyCartReq;
import com.ultravoucher.uv.data.api.request.MyCartResponse;
import com.ultravoucher.uv.data.api.request.PayOrderCCReq;
import com.ultravoucher.uv.data.api.request.PayOrderReq;
import com.ultravoucher.uv.data.api.response.GenerateCCResp;
import com.ultravoucher.uv.data.api.response.GenericResponse;
import com.ultravoucher.uv.data.api.response.InqPayBillerResponse;
import com.ultravoucher.uv.data.api.response.ListPaymentMethodResponse;
import com.ultravoucher.uv.data.api.response.PayOrderResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.CCActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 12/27/2017.
 */

public class PaymentMethodPresenter {

//    int vClass;
    private PaymentMethodFragment mFragment;

    public PaymentMethodPresenter(PaymentMethodFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetListPayMethod(ListPaymentMethodReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListPaymentMethodResponse> listPaymentMethodResponseCall = api.getListPayMethod(contentType, req);
        listPaymentMethodResponseCall.enqueue(new Callback<ListPaymentMethodResponse>() {
            @Override
            public void onResponse(Call<ListPaymentMethodResponse> call, Response<ListPaymentMethodResponse> response) {
                if (response.code() == 200) {
                    ListPaymentMethodResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("MEM002")) {
                        if (!resp.getAvailablePaymentMethod().isEmpty()) {
                            initList(resp.getAvailablePaymentMethod());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListPaymentMethodResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void presentGetListPayMethodBiller(ListPaymentMethodReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";

        VoucherApi api = mFragment.getVoucherApi();
        Call<ListPaymentMethodResponse> listPaymentMethodResponseCall = api.getListPayMethodBiller(contentType, req);
        listPaymentMethodResponseCall.enqueue(new Callback<ListPaymentMethodResponse>() {
            @Override
            public void onResponse(Call<ListPaymentMethodResponse> call, Response<ListPaymentMethodResponse> response) {
                if (response.code() == 200) {
                    ListPaymentMethodResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("MEM002")) {
                        if (!resp.getAvailablePaymentMethod().isEmpty()) {
                            initList(resp.getAvailablePaymentMethod());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                }
                mFragment.progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListPaymentMethodResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                }
            }
        });
    }

    protected void initList(List<ListPaymentMethodMsgResp> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
    }

    protected ListPaymentMethodReq presentGetListReq() {
        int orderType;
        ListPaymentMethodReq req = new ListPaymentMethodReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setPurchasedVoucherClass(Integer.parseInt(mFragment.vClass));
        if (mFragment.type.equalsIgnoreCase("biller")) {
            orderType = 2;
        } else {
            orderType = 1;
        }
        req.setOrderType(orderType);
        return req;
    }


    void getOrder()
    {
        final MyCartReq request = new MyCartReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setStatus(0);

        String authKey = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniq = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<MyCartResponse> callMyCartResp = mFragment.getVoucherApi().getMyCart(authKey, deviceUniq, request);
        callMyCartResp.enqueue(new Callback<MyCartResponse>() {
            @Override
            public void onResponse(Call<MyCartResponse> call, Response<MyCartResponse> response) {
                if(response.code()==200)
                {
                    MyCartResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD004"))
                    {
                        mFragment.setOrderDatas(resp.getOrders());
                        mFragment.orderNumber = resp.getOrders().get(0).getOrderNumber();
                    }
                    else
                    {

                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {

                }
            }

            @Override
            public void onFailure(Call<MyCartResponse> call, Throwable t) {

            }
        });
    }


    void doPayOrderVA()
    {

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";
        PayOrderReq request = new PayOrderReq();
        List<OrderPay> orderPays = new ArrayList<>();
        for(int i=0; i<mFragment.orderDatas.size(); i++)
        {
            OrderPay order = new OrderPay();
            order.setOrderId(mFragment.orderDatas.get(i).getOrderId());
            order.setDiscountValue(mFragment.orderDatas.get(i).getDiscountValue());
            order.setShippingAmount(mFragment.orderDatas.get(i).getShippingAmount());
            order.setTaxAmount(mFragment.orderDatas.get(i).getTaxAmount());
            order.setTotalAmount(mFragment.orderDatas.get(i).getTotalAmount());
            orderPays.add(order);
        }

        if (mFragment.status.equals("D")) {
            request.setOrderNumber(PrefHelper.getString(PrefKey.ORDERNUMBER));
        } else {
            request.setOrderNumber(mFragment.orderNumber);
        }

        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setOrders(orderPays);

        Call<PayOrderResp> respCall = mFragment.getVoucherApi().doPayOrder(contentType, authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<PayOrderResp>() {
            @Override
            public void onResponse(Call<PayOrderResp> call, Response<PayOrderResp> response) {
                if(response.code()==200)
                {
                    PayOrderResp resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("OD002"))
                    {

                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), resp.getOrders().get(0).getVaNumber(), mFragment.orderNumber , String.valueOf(resp.getTotalTrxAmount()), "va");

                    }
                    else
                    {

                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());

                    }
                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            mFragment.doNeedRelogin();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mFragment.showToast("Error Data");
                }
            }

            @Override
            public void onFailure(Call<PayOrderResp> call, Throwable t) {
                mFragment.showToast("Failed to Connect");
            }
        });
    }


    void doPayOrderCC() {

        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);
        String contentType = "application/json";

        PayOrderCCReq request = new PayOrderCCReq();
        List<OrderPay> orderPays = new ArrayList<>();
        for (int i = 0; i < mFragment.orderDatas.size(); i++) {
            OrderPay order = new OrderPay();
            order.setOrderId(mFragment.orderDatas.get(i).getOrderId());
            order.setDiscountValue(mFragment.orderDatas.get(i).getDiscountValue());
            order.setShippingAmount(mFragment.orderDatas.get(i).getShippingAmount());
            order.setTaxAmount(mFragment.orderDatas.get(i).getTaxAmount());
            order.setTotalAmount(mFragment.orderDatas.get(i).getTotalAmount());
            orderPays.add(order);
        }
        if (mFragment.status.equals("D")) {
            request.setOrderNumber(PrefHelper.getString(PrefKey.ORDERNUMBER));
        } else {
            request.setOrderNumber(mFragment.orderNumber);
        }
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setOrders(orderPays);
        request.setReturnUrlChannel(-1);

        Call<GenerateCCResp> generateCCRespCall = mFragment.getVoucherApi().doPayOrderCC(contentType, authToken, deviceUniqueId, request);
        generateCCRespCall.enqueue(new Callback<GenerateCCResp>() {
            @Override
            public void onResponse(Call<GenerateCCResp> call, Response<GenerateCCResp> response) {
                if (response.code() == 200) {
                    GenerateCCResp resp = response.body();

                    if (resp.getAbstractResponse().getResponseStatus().equals("INQ000")) {
                        CCActivity.startActivity((BaseActivity) mFragment.getActivity(), resp.getUrl());
                        mFragment.getActivity().finish();
                    } else {
                        OutputDialogFragment viewDialog = new OutputDialogFragment((BaseFragment) mFragment, 4, 0);
                    }
                } else if (response.code() == 400){
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            if(mFragment!= null && mFragment.isAdded()) {
                                ((BaseActivity) mFragment.getActivity()).doNeedRelogin();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
//                mFragment.showToast(response.body().getAbstractResponse().getResponseMessage());
            }

            @Override
            public void onFailure(Call<GenerateCCResp> call, Throwable t) {
                mFragment.showToast("Failed to Connect");
            }
        });
    }

    void getTransactionNumberBiller(){
        mFragment.showProgressDialog(mFragment.LOADING);
        InqPayBillerReq request = new InqPayBillerReq();
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setPin(PrefHelper.getString(PrefKey.PIN));
        request.setOrigin("ANDROID");
        /*test*/
        request.setContractNo(PrefHelper.getString(PrefKey.CONTRACT_NO));
        request.setBillerId(PrefHelper.getString(PrefKey.BILLER_ID));
        request.setPaymentMethod(PrefHelper.getInt(PrefKey.PAYCODE));
        request.setFavorite(PrefHelper.getBoolean(PrefKey.IS_FAV));
        /*test*/
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUniqueId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        Call<InqPayBillerResponse> respCall = mFragment.getVoucherApi().getInqPayBiller(authToken, deviceUniqueId, request);
        respCall.enqueue(new Callback<InqPayBillerResponse>() {
            @Override
            public void onResponse(Call<InqPayBillerResponse> call, Response<InqPayBillerResponse> response) {
                if(response.code()==200)
                {
                    InqPayBillerResponse resp = response.body();
                    if(resp.getAbstractResponse().getResponseStatus().equals("BL001"))
                    {
                        VAConfirmFragment.showFragment((BaseActivity) mFragment.getActivity(), resp.getVaNumber(), resp.getTransactionId(), ""/*String.valueOf(resp.getTotalTrxAmount())*/, "va");
                        PrefHelper.setString(PrefKey.TRX_ID, resp.getTransactionId());
                    }
                    else
                    {
                        mFragment.showToast(resp.getAbstractResponse().getResponseMessage());
                    }

                }
                else if (response.code() == 400) {
                    try {
                        String errorRawResponse = response.errorBody().string();
                        GenericResponse errorResp = new Gson().fromJson(errorRawResponse, GenericResponse.class);
                        if (errorResp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            mFragment.showToast("Silahkan Login Ulang");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mFragment.showToast("Sedang dalam gangguan");
                }
                else
                {
                    mFragment.showToast("Sedang dalam gangguan");
                }

                mFragment.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<InqPayBillerResponse> call, Throwable t) {
                mFragment.showToast(mFragment.CONNECTION_ERROR);
                mFragment.dismissProgressDialog();
            }
        });
    }

}
