package com.ultravoucher.uv.ui.fragments.myreward;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.VoucherCollectDetailResponse;
import com.ultravoucher.uv.data.api.request.VoucherCollectReq;
import com.ultravoucher.uv.data.api.response.VoucherCollectResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class ListMyRewardPresenter {

    private ListMyRewardFragment mFragment;

    public ListMyRewardPresenter(ListMyRewardFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void getListMyReward(VoucherCollectReq req) {

        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.showProgressDialog(mFragment.getResources().getString(R.string.loading));
//        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String dui = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<VoucherCollectResponse> voucherCollectResponseCall = api.getVoucherCollect(contentType, authToken, dui, req);
        voucherCollectResponseCall.enqueue(new Callback<VoucherCollectResponse>() {
            @Override
            public void onResponse(Call<VoucherCollectResponse> call, Response<VoucherCollectResponse> response) {

                if (response.code() == 200) {
                    VoucherCollectResponse resp = response.body();
                    if (resp != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH000")) {
                        if (!resp.getVoucherCollectDetailResponses().isEmpty()) {
                            if (mFragment.keyword.equals("")) {
                                mFragment.noDataState.setVisibility(View.GONE);
                                initList(resp.getVoucherCollectDetailResponses());
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            } else {
                                mFragment.noDataState.setVisibility(View.GONE);
                                swapped(resp.getVoucherCollectDetailResponses());
                                mFragment.progress.setVisibility(View.GONE);
                                mFragment.rvList.setVisibility(View.VISIBLE);
                            }


                        } else {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
//                            mFragment.getActivity().finish();
//                            mFragment.doNeedRelogin();
                    } else {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }

                    mFragment.swipe_container.setRefreshing(false);

                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.dismissProgressDialog();
//                mFragment.noDataState.setVisibility(View.GONE);
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<VoucherCollectResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
//                    mFragment.noDataState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }

            }
        });
    }

    protected void swapped(List<VoucherCollectDetailResponse> data) {
        mFragment.mAdapter.swap(data);
    }

    protected void initList(List<VoucherCollectDetailResponse> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
    }

    protected VoucherCollectReq construct() {
        VoucherCollectReq request = new VoucherCollectReq();
        request.setKeyword(mFragment.keyword);
        request.setnRecords(50);
        request.setPage(mFragment.page);
        request.setOrder("");
        request.setUsername(PrefHelper.getString(PrefKey.USERNAME));
        return request;
    }

}
