package com.ultravoucher.uv.ui.fragments.history;

import android.view.View;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.beans.Order;
import com.ultravoucher.uv.data.api.request.HistoryProductReq;
import com.ultravoucher.uv.data.api.response.HistoryProductResponse;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by firwandi.ramli on 3/13/2018.
 */

public class HistoryProductPresenter {

    private HistoryProductFragment mFragment;

    public HistoryProductPresenter(HistoryProductFragment mFragment) {
        this.mFragment = mFragment;
    }

    protected void presentGetHistoryProduct(HistoryProductReq req) {
        mFragment.rvList.setVisibility(View.GONE);
        mFragment.noDataState.setVisibility(View.GONE);
        mFragment.networkProblemState.setVisibility(View.GONE);
        mFragment.progress.setVisibility(View.VISIBLE);
        mFragment.progressMessage.setText(mFragment.getResources().getString(R.string.loading));

        String contentType = "application/json";
        String authToken = PrefHelper.getString(PrefKey.AUTH_TOKEN);
        String deviceUId = PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID);

        VoucherApi api = mFragment.getVoucherApi();
        Call<HistoryProductResponse> historyProductResponseCall = api.listHistoryProduct(contentType, authToken, deviceUId, req);
        historyProductResponseCall.enqueue(new Callback<HistoryProductResponse>() {
            @Override
            public void onResponse(Call<HistoryProductResponse> call, Response<HistoryProductResponse> response) {
                if (response.code() == 200) {
                    HistoryProductResponse resp = response.body();

                    if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("OD004")) {
                        if (!resp.getOrders().isEmpty()) {
                            initList(resp.getOrders());
                            mFragment.rvList.setVisibility(View.VISIBLE);
                        } else {
                            if (mFragment != null && mFragment.isAdded()) {
                                String message = mFragment.getString(R.string.no_data);
                                mFragment.noDataState.setVisibility(View.VISIBLE);
                                mFragment.noDataMessage.setText(message);
                            }
                        }
                    } else if (response.body() != null && resp.getAbstractResponse().getResponseStatus().equals("AUTH001")) {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.need_relogin);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    } else {
                        if (mFragment != null && mFragment.isAdded()) {
                            String message = mFragment.getString(R.string.no_data);
                            mFragment.noDataState.setVisibility(View.VISIBLE);
                            mFragment.noDataMessage.setText(message);
                        }
                    }
                    mFragment.swipe_container.setRefreshing(false);
                } else {
                    if (mFragment != null && mFragment.isAdded()) {
                        String message = mFragment.getString(R.string.no_data);
                        mFragment.noDataState.setVisibility(View.VISIBLE);
                        mFragment.noDataMessage.setText(message);
                    }
                    mFragment.swipe_container.setRefreshing(false);
                }
                mFragment.progress.setVisibility(View.GONE);
                mFragment.swipe_container.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<HistoryProductResponse> call, Throwable t) {
                if (mFragment != null && mFragment.isAdded()) {
                    String message = mFragment.getString(R.string.connection_error);
                    mFragment.networkProblemState.setVisibility(View.VISIBLE);
                    mFragment.networkProblemMessage.setText(message);
                    mFragment.progress.setVisibility(View.GONE);
                    mFragment.swipe_container.setRefreshing(false);
                }
            }
        });
    }

    protected void initList(List<Order> list) {
        mFragment.mList.clear();
        mFragment.mList.addAll(list);
        mFragment.mAdapter.notifyDataSetChanged();
        mFragment.mAdapter.setLoaded(false);
        mFragment.page++;
    }

    protected HistoryProductReq constructGetListHistoryProduct() {
        HistoryProductReq req = new HistoryProductReq();
        req.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        req.setnRecords(50);
        req.setOrderStatus(1);
        req.setPage(0);
        return req;
    }

}
