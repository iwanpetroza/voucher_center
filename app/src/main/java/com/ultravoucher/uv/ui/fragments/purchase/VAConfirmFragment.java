package com.ultravoucher.uv.ui.fragments.purchase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.HomePageActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class VAConfirmFragment extends BaseActivity {



    private static final String VA = "va";
    private static final String ORDERNUMBER = "ordernumber";
    private static final String TRX = "trx";
    private static final String STATUS = "status";

        /*@BindView(R.id.topup_desc_permataBank)
        LinearLayout ll_desc_permatabank;

        @BindView(R.id.topup_desc_bankLain)
        LinearLayout ll_desc_banklain;

        @BindView(R.id.topup_ic_arrow_bankpermata)
        ImageView iv_bankpermata;

        @BindView(R.id.topup_ic_arrow_banklain)
        ImageView iv_banklain;

        @BindString(R.string.order_number)
        String order;*/

    /*@BindString(R.string.top_up_account)
    String tvVA;
*/
    @BindView(R.id.tvDesc)
    TextView tv;

    @BindView(R.id.tvDesc2)
    TextView tv2;

    @BindView(R.id.tvDesc3)
    TextView tv3;

    @BindView(R.id.tvDesc4)
    TextView tv4;



    /*@BindView(R.id.tvDesc2)
    TextView tv2;*/

    @BindView(R.id.topup_tv_accountnumber)
    TextView tvVirtualAccount;

    @BindView(R.id.topup_tv_trx)
    TextView tvTrx;

    @BindView(R.id.topup_tv_poin)
    TextView tvPoint;

    @BindView(R.id.topup_tv_poin2)
    TextView tvPoint2;

    protected String vaNumber, orderNum, status;
    protected String trx;

    protected VAConfirmPresenter mPresenter;

    private static final String TAG = VAConfirmFragment.class.getSimpleName();

    @Override
    protected int getLayout() {
        return R.layout.f_va_confirm;
    }

    public static void showFragment(BaseActivity sourceActivity, String va, String orderNumber, String trxAmount, String status) {

        Intent i = new Intent(sourceActivity, VAConfirmFragment.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(VA, va);
        i.putExtra(STATUS, status);
        i.putExtra(ORDERNUMBER, orderNumber);
        i.putExtra(TRX, trxAmount);
        sourceActivity.startActivity(i);

        /*VAConfirmFragment fragment = new VAConfirmFragment();
        Bundle fragmentExtras = new Bundle();
        fragmentExtras.putString(VA, va);
        fragmentExtras.putString(ORDERNUMBER, orderNumber);
        fragmentExtras.putString(STATUS, status);
        fragmentExtras.putInt(TRX, trxAmount);
        FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, TAG);
        fragment.setArguments(fragmentExtras);
        fragmentTransaction.commit();*/
    }

        /*@OnClick(R.id.topup_cv_permatabank)
        void showDescPermataBank(){
            if(ll_desc_permatabank.getVisibility()== View.GONE)
            {
                ll_desc_permatabank.setVisibility(View.VISIBLE);
                iv_bankpermata.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else
            {
                ll_desc_permatabank.setVisibility(View.GONE);
                iv_bankpermata.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            }
        }

        @OnClick(R.id.topup_cv_bankLain)
        void showDescBankLain(){
            if(ll_desc_banklain.getVisibility()== View.GONE)
            {
                ll_desc_banklain.setVisibility(View.VISIBLE);
                iv_banklain.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
            }
            else
            {
                ll_desc_banklain.setVisibility(View.GONE);
                iv_banklain.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
            }
        }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.history_payment_item:
            {
                PaymentHistoryActivity.startActivity((BaseActivity) getActivity());
//                HistoryPaymentFragment.showFragment((BaseActivity) getActivity());
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.menu_history_payment, menu);
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vaNumber = getIntent().getStringExtra(VA);
        orderNum = getIntent().getStringExtra(ORDERNUMBER);
        status = getIntent().getStringExtra(STATUS);
        trx = getIntent().getStringExtra(TRX);

        mPresenter = new VAConfirmPresenter(this);

        if (!status.equals("va") && !status.equals("vc")) {
            mPresenter.getPoint(mPresenter.constructRequest());
        }


        if (status.equals("va")) {
            tv.setText("Pesanan Anda telah berhasil dilakukan. \n " );
            tv2.setText("Nomor pesanan Anda adalah ");
            tv3.setText("" + orderNum + ". \n");
            tv4.setText("Silahkan melakukan pembayaran dengan VA Number");
            tvVirtualAccount.setText(vaNumber + " \n");
            tvTrx.setText("Cek email Anda untuk mengetahui tata cara pembayaran melalui Virtual Account.");

            SpannableString ss = new SpannableString(getResources().getString(R.string.info));
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View widget) {

                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };

            ss.setSpan(clickableSpan, 43, 50, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvPoint.setText(ss);
            tvPoint.setMovementMethod(LinkMovementMethod.getInstance());
            tvPoint.setHighlightColor(getResources().getColor(R.color.t0_white));

        } else if (status.equals("walet")) {
            tv.setText("Pesanan Anda telah berhasil dilakukan. \n " );
            tv2.setText("Nomor pesanan Anda adalah ");
            tv3.setText("" + orderNum + ". \n");
            tv4.setText("Kode Referensi Anda ");
            tvVirtualAccount.setText(vaNumber + " \n");
            tvTrx.setText("");
        } else if (status.equals("vc")) {
            tv.setText("Pesanan Anda telah berhasil dilakukan. \n " );
            tv2.setText("Nomor pesanan Anda adalah ");
            tv3.setText("" + orderNum + ". \n");
            tv4.setText("");
            tvVirtualAccount.setText("");
            tvTrx.setText("");
        } else if (status.equals("biller")) {
            tv.setText("Pembayaran Anda telah berhasil dilakukan. \n ");
            tv2.setText("Nomor pembayaran Anda adalah ");
            tv3.setText("" + orderNum + " \n");

            String token = PrefHelper.getString(PrefKey.TOKEN_SUBMIT);
            if (token.equalsIgnoreCase("") || token == null) {
                tv4.setText("");
                tvVirtualAccount.setText("");
            } else {
                tv4.setText("Kode Token");
                tvVirtualAccount.setText(PrefHelper.getString(PrefKey.TOKEN_SUBMIT));
            }
            tvTrx.setText("");
            token = "";
            PrefHelper.clearPreference(PrefKey.TOKEN_SUBMIT);
        }
    }

    /*@Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




//        tvVirtualAccount.setText(PrefHelper.getString(PrefKey.VIRTUAL_ACCOUNT));
//        tv.setText("VA : " + vaNumber +" ------ "+ orderNum);
        *//*tvVirtualAccount.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });*//*
    }*/

    @OnClick(R.id.btnDone)
    void done() {
//        this.getActivity().onBackPressed();
        this.finish();
        HomePageActivity.startActivity(this, "");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        HomePageActivity.startActivity(this, "");

    }
}