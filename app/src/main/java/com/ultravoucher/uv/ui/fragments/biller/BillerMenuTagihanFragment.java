package com.ultravoucher.uv.ui.fragments.biller;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.BillerPlnActivity;
import com.ultravoucher.uv.ui.activities.BillerPulsaActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;

import butterknife.OnClick;

/**
 * Created by tunggul.jati on 5/18/2018.
 */

public class BillerMenuTagihanFragment extends BaseFragment {

    private static final String TAG = BillerMenuTagihanFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceFragment) {
        BillerMenuTagihanFragment fragment = new BillerMenuTagihanFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
//        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_biller_tagihan;
    }

    @OnClick(R.id.cvPln)
    public void goPln() {
//        BillerPlnFragment.showFragment((BaseActivity) getActivity());
        BillerPlnActivity.startActivity((BaseActivity) getActivity(), "pln");
    }

    @OnClick(R.id.cvPulsa)
    public void goPulsa() {
//        BillerPulsaFragment.showFragment((BaseActivity) getActivity());
        BillerPulsaActivity.startActivity((BaseActivity) getActivity(), "pulsa");
    }
}
