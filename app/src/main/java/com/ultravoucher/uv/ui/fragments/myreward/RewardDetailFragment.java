package com.ultravoucher.uv.ui.fragments.myreward;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.api.beans.RewardResp;
import com.ultravoucher.uv.ui.activities.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by firwandi.ramli on 10/27/2017.
 */

public class RewardDetailFragment extends BaseActivity {

    private static final String REWARD_DATA = "RewardData";

    @BindView(R.id.toolbar_title)
    TextView mToolbarTvTitle;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.appBar)
    AppBarLayout mAppBar;
    @BindView(R.id.imgLoading)
    ProgressBar imgLoading;
    @BindView(R.id.detailLogo)
    ImageView detailLogo;
    @BindView(R.id.rlImage)
    RelativeLayout rlImage;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvSaleprice)
    TextView tvSaleprice;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.tvSyaratDesc)
    TextView tvSyaratDesc;
    @BindView(R.id.tvPengambilanDesc)
    TextView tvPengambilanDesc;

    RewardResp mRewardData;
    RewardDetailPresenter mPresenter;

    public static void startActivity(BaseActivity sourceActivity, RewardResp rewardData) {
        Intent i = new Intent(sourceActivity, RewardDetailFragment.class);
        i.putExtra(REWARD_DATA, rewardData);
        sourceActivity.startActivity(i);
    }


    @Override
    protected int getLayout() {
        return R.layout.f_reward_detail;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new RewardDetailPresenter(this);
        mRewardData = (RewardResp) getIntent().getSerializableExtra(REWARD_DATA);

        setupToolbar();
    }

    @OnClick(R.id.btnTake)
    public void onViewClicked() {

        confirmDialog();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(null);
        mToolbarTvTitle.setText("Reward Detail");
        mToolbarTvTitle.setAllCaps(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mPresenter.getRewardDetail(mPresenter.constructReq());
    }

    public void confirmDialog() {
        LayoutInflater li = LayoutInflater.from(RewardDetailFragment.this);
        View promptsView = li.inflate(R.layout.layout_prompt, new LinearLayout(RewardDetailFragment.this), false);

        final AlertDialog.Builder builder = new AlertDialog.Builder(RewardDetailFragment.this);

        builder.setView(promptsView);

        final TextView txtView = (TextView) promptsView.findViewById(R.id.txtLabel);
        txtView.setText("Apakah Anda yakin ingin mengambil reward ini?");

        final Button btnConfirm = (Button) promptsView.findViewById(R.id.btnConfirm);
        final Button btnCancel = (Button) promptsView.findViewById(R.id.btnCancel);

        final AlertDialog dialog = builder.create();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                mPresenter.presentRedeemRewardResp(mPresenter.presentRedeemRewardReq());
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                onBackPressed();
                return true;

            /*case R.id.action_cart:
                CheckOutActivity.startActivity(this);
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }
}
