package com.ultravoucher.uv.ui.fragments.biller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;
import com.ultravoucher.uv.ui.activities.BillerMenuActivity;
import com.ultravoucher.uv.ui.activities.BillerTagihanActivity;
import com.ultravoucher.uv.ui.fragments.BaseFragment;
import com.ultravoucher.uv.ui.fragments.setting.ChangePasswordPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by tunggul.jati on 5/18/2018.
 */

public class BillerMenuFragment extends BaseFragment {

    private static final String TAG = BillerMenuFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceFragment) {
        BillerMenuFragment fragment = new BillerMenuFragment();
        Bundle fragmentExtras = new Bundle();
        FragmentTransaction transaction = sourceFragment.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG);
//        transaction.addToBackStack(null);
        fragment.setArguments(fragmentExtras);
        transaction.commit();
    }

    @Override
    protected int getLayout() {
        return R.layout.f_biller_menu;
    }

    @OnClick(R.id.cvTagihan)
    public void goTagihan() {
//        BillerMenuTagihanFragment.showFragment((BaseActivity) getActivity());
        BillerTagihanActivity.startActivity((BaseActivity) getActivity(), "tagihan");
    }

    @OnClick(R.id.cvMerchant)
    public void goMerchant() {
    }
}
