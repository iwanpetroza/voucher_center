package com.ultravoucher.uv.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.RemoteMessage;
import com.ultravoucher.uv.R;
import com.ultravoucher.uv.data.models.GCMMessageModel;
import com.ultravoucher.uv.ui.activities.HomePageActivity;

import java.util.Map;

/**
 * Created by Firwandi S Ramli on 03/09/18.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = FirebaseMessagingService.class.getSimpleName();

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String TYPE = "type";
    private static final String ID = "id";
    private Intent intent;

    private static final String TYPE_POST = "1";
    private NotificationManager notificationManager;


    /**
     * Override method to get message from FCM Server
     *
     * @param remoteMessage
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            try {
                //Map jsonResp = new Gson().fromJson(data.getString("message"), Map.class);
                GCMMessageModel model = new GCMMessageModel();
                model.setmId(data.get(ID));
                model.setmMessage(data.get(MESSAGE));
                model.setmTitle(data.get(TITLE));
                model.setmType(data.get(TYPE));
                sendNotificationDetail(model);

            } catch (Exception e) {
                sendNotification(data.get(MESSAGE));
            }

        }

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage.getNotification().getBody());
        }

    }

    private void sendNotification(String message) {
        final int NOTIFY_ID = 1; // ID of notification
        NotificationCompat.Builder builder;


        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(this, HomePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(ID);
            if (mChannel == null) {
                mChannel = new NotificationChannel(ID, TITLE, importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(mChannel);
            }
            Uri uriSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder = new NotificationCompat.Builder(this, ID);
            int requestCode = 0;
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(getResources().getString(R.string.app_name))  // required
                    .setSmallIcon(R.drawable.logoultravoucherhijau) // required
                    .setContentText(message)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(message)
                    .setSound(uriSound)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            Intent intent = new Intent(this, HomePageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            int requestCode = 0;
            Uri uriSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder = new NotificationCompat.Builder(this, "");
            PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(getResources().getString(R.string.app_name))                           // required
                    .setSmallIcon(R.drawable.logoultravoucherhijau) // required
                    .setContentText(message)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(message)
                    .setSound(uriSound)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }
        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID, notification);
    }

    private PendingIntent setupPendingIntent(GCMMessageModel GcmModel, String messageType) {
        intent = null;
        if (messageType.equalsIgnoreCase(TYPE_POST)) {
            intent = HomePageActivity.startActivityFromNotif(this, GcmModel);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        return pendingIntent;
    }

    private void sendNotificationDetail(GCMMessageModel message) {

        final int NOTIFY_ID = 1; // ID of notification
        NotificationCompat.Builder builder;


        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notificationManager.getNotificationChannel(ID);
            if (mChannel == null) {
                mChannel = new NotificationChannel(ID, TITLE, importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, ID);
            PendingIntent pendingIntent = setupPendingIntent(message, message.getmType());
            Uri uriSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setContentTitle(message.getmTitle())  // required
                    .setSmallIcon(R.drawable.logoultravoucherhijau) // required
                    .setContentText(message.getmMessage())  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(uriSound)
                    .setContentIntent(pendingIntent)
                    .setTicker(message.getmMessage())
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            builder = new NotificationCompat.Builder(this, "");
            PendingIntent pendingIntent = setupPendingIntent(message, message.getmType());
            Uri uriSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            builder.setContentTitle(message.getmTitle())                           // required
                    .setSmallIcon(R.drawable.logoultravoucherhijau) // required
                    .setContentText(message.getmMessage())  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(uriSound)
                    .setContentIntent(pendingIntent)
                    .setTicker(message.getmMessage())
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }
        Notification notification = builder.build();
        notificationManager.notify(NOTIFY_ID, notification);

    }
}
