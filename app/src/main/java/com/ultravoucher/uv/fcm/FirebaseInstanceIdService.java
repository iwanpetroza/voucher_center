package com.ultravoucher.uv.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ultravoucher.uv.VoucherApplication;
import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.request.SettingGCMReq;
import com.ultravoucher.uv.data.api.request.UpdateGCMReq;
import com.ultravoucher.uv.data.api.response.SettingGCMResp;
import com.ultravoucher.uv.data.prefs.PrefHelper;
import com.ultravoucher.uv.data.prefs.PrefKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Firwandi S Ramli on 03/09/18.
 */

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService {
    private static final String TAG = FirebaseInstanceIdService.class.getSimpleName();

    /**
     * Override method for refresh token FCM
     */
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String updatedToken = FirebaseInstanceId.getInstance().getToken();
        PrefHelper.setString(PrefKey.FCM_ID, updatedToken);
        Log.d(TAG, "onTokenRefresh: got FCM ID= " + updatedToken);

        if (PrefHelper.getBoolean(PrefKey.IS_LOGIN)) {
            sendRegistrationToServer(updatedToken);
        }

    }

    private void sendRegistrationToServer(String updatedToken) {
        SettingGCMReq request = new SettingGCMReq();
        request.setDeviceUniqueId(PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID));
        request.setMemberId(PrefHelper.getString(PrefKey.MEMBERID));
        request.setRegKey(updatedToken);

        VoucherApi api = getBaseApi();
        Call<SettingGCMResp> settingGcmResp = api.setGcmId(PrefHelper.getString(PrefKey.AUTH_TOKEN), PrefHelper.getString(PrefKey.DEVICE_UNIQUE_ID), request);
        settingGcmResp.enqueue(new Callback<SettingGCMResp>() {
            @Override
            public void onResponse(Call<SettingGCMResp> call, Response<SettingGCMResp> response) {
                if (response.code() == 200) {
                    SettingGCMResp resp = response.body();
                    if (resp.getResponseStatus().equals("003")) {
                        Log.d(TAG, "FCM UPDATE onResponseSuccess");
                    }
                }else{
                    Log.d(TAG, "FCM UPDATE onResponseError");
                }
            }

            @Override
            public void onFailure(Call<SettingGCMResp> call, Throwable t) {
                Log.d(TAG, "FCM UPDATE onResponseFailure");
            }
        });
    }

    private VoucherApi getBaseApi() {
        return VoucherApplication.getInstance().getVoucherApi();
    }


    /**
     * Construct request update gcm
     *
     * @param deviceId
     * @param gcmId
     * @return
     */
    private UpdateGCMReq contructRequest(String deviceId, String gcmId) {
        UpdateGCMReq req = new UpdateGCMReq();
        req.setGcmId(gcmId);
        req.setDeviceId(deviceId);
        return req;
    }


}