package com.ultravoucher.uv;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.Log;

import com.ultravoucher.uv.data.api.VoucherApi;
import com.ultravoucher.uv.data.api.VoucherService;
import com.ultravoucher.uv.utils.Const;
import com.ultravoucher.uv.utils.MainThreadBus;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by firwandi.ramli on 9/25/2017.
 */

public class VoucherApplication extends MultiDexApplication{

    private static VoucherApplication currentApplication;
    private SharedPreferences sharedPreferences;
    private SharedPreferences ncSharedPreferences;

    public SharedPreferences getNcSharedPreferences() {
        return ncSharedPreferences;
    }

    private VoucherService voucherService;

    public VoucherApplication() {
        currentApplication = this;
        voucherService = new VoucherService();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Const.bus = new MainThreadBus();
        setupSharedPreferences();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Ubuntu-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                // new activity created; force its orientation to portrait
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });


        //printHashKey();
    }

    public static VoucherApplication getInstance(){
        return currentApplication;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public VoucherApi getVoucherApi(){
        return voucherService.getApi();
    }

    private void setupSharedPreferences() {
        this.sharedPreferences = getSharedPreferences(VoucherApplication.class.getSimpleName(),
                Context.MODE_PRIVATE);
        this.ncSharedPreferences = getSharedPreferences(VoucherApplication.class.getSimpleName()+"_NC",
                Context.MODE_PRIVATE);
    }

    public void printHashKey(){
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.indivara.ceploc",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
