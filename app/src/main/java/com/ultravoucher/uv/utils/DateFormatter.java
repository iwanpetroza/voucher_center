package com.ultravoucher.uv.utils;

import android.net.ParseException;

/**
 * Created by firwandi.ramli on 2/8/2018.
 */

public class DateFormatter {

    public static String format(String tgl) {

        String tanggal = "";
        if(tgl != null){
            if(!tgl.equals("-")){
                String value[] = tgl.split(" ");
                if(value[0] != null){
                    String valueDate[] = value[0].split("\\-");
                    try{
                        if (valueDate[1].equals("01")) {
                            tanggal = valueDate[2] + " Jan " + valueDate[0];
                        } else if (valueDate[1].equals("02")) {
                            tanggal = valueDate[2] + " Feb " + valueDate[0];
                        } else if (valueDate[1].equals("03")) {
                            tanggal = valueDate[2] + " Mar " + valueDate[0];
                        } else if (valueDate[1].equals("04")) {
                            tanggal = valueDate[2] + " Apr " + valueDate[0];
                        } else if (valueDate[1].equals("05")) {
                            tanggal = valueDate[2] + " Mei " + valueDate[0];
                        } else if (valueDate[1].equals("06")) {
                            tanggal = valueDate[2] + " Jun " + valueDate[0];
                        } else if (valueDate[1].equals("07")) {
                            tanggal = valueDate[2] + " Jul " + valueDate[0];
                        } else if (valueDate[1].equals("08")) {
                            tanggal = valueDate[2] + " Agu " + valueDate[0];
                        } else if (valueDate[1].equals("09")) {
                            tanggal = valueDate[2] + " Sep " + valueDate[0];
                        } else if (valueDate[1].equals("10")) {
                            tanggal = valueDate[2] + " Okt " + valueDate[0];
                        } else if (valueDate[1].equals("11")) {
                            tanggal = valueDate[2] + " Nov " + valueDate[0];
                        } else if (valueDate[1].equals("12")) {
                            tanggal = valueDate[2] + " Des " + valueDate[0];
                        }
                        return tanggal;

                    } catch (ParseException e) {
                        return tgl;
                    }
                }

            }
        }

        return tgl;
    }
}
