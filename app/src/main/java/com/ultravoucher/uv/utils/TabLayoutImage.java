package com.ultravoucher.uv.utils;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.ultravoucher.uv.R;
import com.ultravoucher.uv.ui.activities.BaseActivity;

/**
 * Created by firwandi.ramli on 9/28/2017.
 */

public class TabLayoutImage {
    public static void applyFontedTab(BaseActivity activity, ViewPager viewPager, TabLayout tabLayout) {
        for (int i = 0; i < viewPager.getAdapter().getCount(); i++) {
            TextView tv = (TextView) activity.getLayoutInflater().inflate(R.layout.item_tab_purchase, null);
            if (i == viewPager.getCurrentItem()) tv.setSelected(true);
            tv.setText(viewPager.getAdapter().getPageTitle(i));
            tabLayout.getTabAt(i).setCustomView(tv);
        }
    }
}
