package com.ultravoucher.uv.utils;

import android.net.ParseException;

/**
 * Created by firwandi.ramli on 12/28/2017.
 */

public class AmountFormatter2 {

    public static String format(double amount){
        String amountOnString = String.format("%1$,.0f", amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static String format(String amount){
        try {
            double amountOnDouble = Double.parseDouble(amount);
            String amountOnString = String.format("%1$,.0f", amountOnDouble);
            amountOnString = amountOnString.replaceAll(",",".");
            return amountOnString;
        } catch (ParseException e) {
            return amount;
        }
    }
}
